puts "BSG-info: Running script [info script]\n"

# Get the manycore min x and y values. Can be useful for automating the block
# shaping script to some degree with respect to the manycore size.
set mc_min_x [lindex [regexp -all -inline -- {[0-9]+} [get_object_name [index_collection [sort_collection -dict [get_cells -hier *__tile] name]   0]]] 1]
set mc_min_y [lindex [regexp -all -inline -- {[0-9]+} [get_object_name [index_collection [sort_collection -dict [get_cells -hier *__tile] name]   0]]] 0]
set mc_max_x [lindex [regexp -all -inline -- {[0-9]+} [get_object_name [index_collection [sort_collection -dict [get_cells -hier *__tile] name] end]]] 1]
set mc_max_y [lindex [regexp -all -inline -- {[0-9]+} [get_object_name [index_collection [sort_collection -dict [get_cells -hier *__tile] name] end]]] 0]

# Victim cache cell instances
set vcache_cells [get_cells -hier *__vc]

# Manycore tile cell instances (order from TL to BR)
set mc_tile_cells [list]
for {set i $mc_min_y} {$i <= $mc_max_y} {incr i} {
  for {set j $mc_min_x} {$j <= $mc_max_x} {incr j} {
    append_to_collection mc_tile_cells [get_cells -hier y_${i}__x_${j}__tile]
  }
}

# Calc the width and height of a tile and snap it to cell sites
#lassign [bsg_calc_block_size [index_collection $mc_tile_cells 0] 0.60 aspect 1.75] tile_width tile_height
#set tile_width  [round_to_nearest $tile_width  [unit_width]]
#set tile_height [round_to_nearest $tile_height [unit_height]]

# Calc the width and height of a vcache and snap it to cell sites
#lassign [bsg_calc_block_size [index_collection $vcache_cells 0] 0.72 width $tile_width] cache_width cache_height
#set cache_width  [round_to_nearest $cache_width  [unit_width]]
#set cache_height [round_to_nearest $cache_height [unit_height]]

#set tile_width   240.912
#set tile_height  137.76

set tile_width   241.920
set tile_height  138.240

set cache_width  ${tile_width}
set cache_height 149.760

# Shape the manycore tile blocks
bsg_create_block_array_grid $mc_tile_cells        \
  -grid mib_placement_grid                        \
  -grow_down                                      \
  -relative_to core                               \
  -x 100                                          \
  -y 2250 \
  -rows [expr $mc_min_y + $mc_max_y + 1]          \
  -cols [expr $mc_min_x + $mc_max_x + 1]          \
  -min_channel 5                                  \
  -width $tile_width                              \
  -height $tile_height

# Shape the vcache blocks
bsg_create_block_array_grid $vcache_cells \
  -grid mib_placement_grid                \
  -relative_to [get_cells -hier y_${mc_max_y}__x_${mc_min_x}__tile] \
  -x 0                                    \
  -y [expr -(10+$cache_height)]           \
  -rows 1                                 \
  -cols [expr $mc_min_x + $mc_max_x + 1]  \
  -min_channel 5                          \
  -width $cache_width                     \
  -height $cache_height

puts "BSG-info: Completed script [info script]\n"

