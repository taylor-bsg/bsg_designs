source $::env(BSG_DESIGNS_TARGET_DIR)/tcl/hard/$::env(BSG_DESIGNS_HARD_TARGET)/constraints_include.tcl
source $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_timing_constraint_manycore_generated.tcl


##
##
##
proc bsg_pll_create_clock { pll_path \
                            clk_name \
                            port_name \
                            ref_clk_period \
                            pll_clk_multiplier \
                            clk_uncertainty_percent } {

  puts "INFO \[BSG\]: begin creating pll clock"

  set pll_clk_period      [expr $ref_clk_period / $pll_clk_multiplier]
  set ref_clk_uncertainty [expr $ref_clk_period * ($clk_uncertainty_percent / 100)]
  set pll_clk_uncertainty [expr $pll_clk_period * ($clk_uncertainty_percent / 100)]

  ## Create the reference clock that the PLL runs from
  create_clock -name ${clk_name}_pad \
               -period ${ref_clk_period} \
               [get_ports ${port_name}]
  set_clock_uncertainty ${ref_clk_uncertainty} [get_clocks ${clk_name}_pad]

  ## Create a generated clock as the output fo the PLL
  create_generated_clock -name ${clk_name} \
                         -multiply_by ${pll_clk_multiplier} \
                         -source [get_ports ${port_name}] \
                         -master_clock [get_clocks ${clk_name}_pad] \
                         [get_pins ${pll_path}/out_clk]
  set_clock_uncertainty ${pll_clk_uncertainty} [get_generated_clocks ${clk_name}]

  puts "INFO \[BSG\]: finish creating pll clock"

}


## CORE CLOCK CREATION
#
set core_clk_name                 "core_clk"
set core_clk_port                 "p_PLL_SECTION_1_REF_i"
set core_clk_pll                  "core_clk_pll"
set core_clk_uncertainty_percent  5

bsg_pll_create_clock $core_clk_pll \
                     $core_clk_name \
                     $core_clk_port \
                     ${CORE_CLOCK_PERIOD} \
                     ${CORE_CLK_PLL_MULT} \
                     $core_clk_uncertainty_percent

## MASTER IO CLOCK CREATION
#
set master_io_clk_name                 "master_io_clk"
set master_io_clk_port                 "p_PLL_SECTION_2_REF_i"
set master_io_clk_pll                  "io_master_clk_pll"
set master_io_clk_uncertainty_percent  5

bsg_pll_create_clock $master_io_clk_pll \
                     $master_io_clk_name \
                     $master_io_clk_port \
                     ${MASTER_IO_CLOCK_PERIOD} \
                     ${MASTER_IO_CLK_PLL_MULT} \
                     $master_io_clk_uncertainty_percent

## MANYCORE CLOCK CREATION
#
set manycore_clk_name                 "manycore_clk"
set manycore_clk_port                 "p_PLL_SECTION_3_REF_i"
set manycore_clk_pll                  "manycore_clk_pll"
set manycore_clk_uncertainty_percent  5

bsg_pll_create_clock $manycore_clk_pll \
                     $manycore_clk_name \
                     $manycore_clk_port \
                     ${MANYCORE_CLOCK_PERIOD} \
                     ${MANYCORE_CLK_PLL_MULT} \
                     $manycore_clk_uncertainty_percent

## BSG CHIP TIMING CONSTRAINTS
#
bsg_chip_timing_constraint \
  -package                            ucsd_bsg_332 \
  -reset_port                         [get_ports p_reset_i] \
  -core_clk_name                      $core_clk_name \
  -core_clk                           [get_generated_clocks $core_clk_name] \
  -core_clk_port                      [get_ports $core_clk_port] \
  -core_clk_period                    [expr ${CORE_CLOCK_PERIOD}/${CORE_CLK_PLL_MULT}] \
  -master_io_clk_name                 $master_io_clk_name \
  -master_io_clk                      [get_generated_clocks $master_io_clk_name] \
  -master_io_clk_port                 [get_ports $master_io_clk_port] \
  -master_io_clk_period               [expr ${MASTER_IO_CLOCK_PERIOD}/${MASTER_IO_CLK_PLL_MULT}] \
  -manycore_clk_name                  $manycore_clk_name \
  -manycore_clk                       [get_generated_clocks $manycore_clk_name] \
  -manycore_clk_port                  [get_ports $manycore_clk_port] \
  -manycore_clk_period                [expr ${MANYCORE_CLOCK_PERIOD}/${MANYCORE_CLK_PLL_MULT}] \
  -src_sync_in_clk_period             10000 \
  -src_sync_out_clk_period            10000 \
  -input_cell_rise_fall_difference    $INPUT_CELL_RISE_FALL_DIFF \
  -output_cell_rise_fall_difference_A $OUTPUT_CELL_RISE_FALL_DIFF_A \
  -output_cell_rise_fall_difference_B $OUTPUT_CELL_RISE_FALL_DIFF_B \
  -output_cell_rise_fall_difference_C $OUTPUT_CELL_RISE_FALL_DIFF_C \
  -output_cell_rise_fall_difference_D $OUTPUT_CELL_RISE_FALL_DIFF_D
