#=========================================================================
# Makefile for Integrating Memory Compiler with Stratus
#=========================================================================
#
# This Makefile flow invokes the correct memory compiler for a list of
# memory specs stored in $(SPECS_DIR) to generate an ASCII database as well
# as Verilog model. Then, the flow executes generate_bdm.py to generate a
# corresponding .bdm file to integrate the memory with Stratus HLS
#
# Date   : February 13, 2017
# Author : Khalid Al-Hawaj

default: verilog

#-------------------------------------------------------------------------
# Constants
#-------------------------------------------------------------------------

SPECS_DIR?=srams
OUTPUT_DIR?=.

# Single-Ported SRAM Compiler
ifneq ($(shell which sram_sp_hde_svt_mvt 2>/dev/null),)
MEM_COMPILER_SP=sram_sp_hde_svt_mvt
else
MEM_COMPILER_SP=/afs/eecs.umich.edu/kits/ARM/TSMC_16FFC/sram_sp_hde/bin/sram_sp_hde_svt_mvt
endif

# Duel-Ported SRAM Compiler
ifneq ($(shell which sram_dp_uhde_svt_mvt 2>/dev/null),)
else
MEM_COMPILER_DP=sram_dp_uhde_svt_mvt
endif

# Two-Ported SRAM Compiler
ifneq ($(shell which sram_2p_uhde_svt_mvt 2>/dev/null),)
MEM_COMPILER_2P=sram_2p_uhde_svt_mvt
else
MEM_COMPILER_2P=/afs/eecs.umich.edu/kits/ARM/TSMC_16FFC/sram_2p_uhde/bin/sram_2p_uhde_svt_mvt
endif

ifeq (error, $(shell [ -d $(SPECS_DIR) ] || echo error))
  $(error Folder $(SPECS_DIR) does not exists)
endif

#-------------------------------------------------------------------------
# Needed memories target
#-------------------------------------------------------------------------
# This automatically constructs a list of targets based on files in spec
# directory.
#

MEMORIES=$(basename $(basename $(notdir $(wildcard $(SPECS_DIR)/*.spec))))
MEMORIES_DAT=$(addprefix $(OUTPUT_DIR)/, $(addsuffix .dat, $(MEMORIES)))
MEMORIES_V=$(addprefix $(OUTPUT_DIR)/, $(addsuffix .v, $(MEMORIES)))
MEMORIES_BDM=$(addprefix $(OUTPUT_DIR)/, $(addsuffix .bdm, $(MEMORIES)))

#-------------------------------------------------------------------------
# Create output directory if is doesn't exist
#-------------------------------------------------------------------------

$(OUTPUT_DIR):
	mkdir -p $(OUTPUT_DIR)

#-------------------------------------------------------------------------
# Create SRAMs ASCII database
#-------------------------------------------------------------------------

# Single Ported
$(OUTPUT_DIR)/%.dat: $(SPECS_DIR)/%.sp.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_SP) ascii -spec $<
	mv $(basename $(notdir $@))*.dat $@

# Two Ports
$(OUTPUT_DIR)/%.dat: $(SPECS_DIR)/%.2p.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_2P) ascii -spec $<
	mv $(basename $(notdir $@))*.dat $@

# Double Ported
$(OUTPUT_DIR)/%.dat: $(SPECS_DIR)/%.dp.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_DP) ascii -spec $<
	mv $(basename $(notdir $@))*.dat $@

#-------------------------------------------------------------------------
# Create SRAMs Verilog model
#-------------------------------------------------------------------------

# Single Ported
$(OUTPUT_DIR)/%.v: $(SPECS_DIR)/%.sp.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_SP) verilog -spec $<
	-mv $(basename $(notdir $@))*.v $@.tmp
	echo '`ifndef SYNTHESIS' > $@
	cat $@.tmp >> $@
	echo '`endif' >> $@
	rm $@.tmp

# Two Ports
$(OUTPUT_DIR)/%.v: $(SPECS_DIR)/%.2p.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_2P) verilog -spec $<
	-mv $(basename $(notdir $@))*.v $@.tmp
	echo '`ifndef SYNTHESIS' > $@
	cat $@.tmp >> $@
	echo '`endif' >> $@
	rm $@.tmp

# Double Ported
$(OUTPUT_DIR)/%.v: $(SPECS_DIR)/%.dp.spec $(OUTPUT_DIR)
	rm -f $@
	$(MEM_COMPILER_DP) verilog -spec $<
	-mv $(basename $(notdir $@))*.v $@.tmp
	echo '`ifndef SYNTHESIS' > $@
	cat $@.tmp >> $@
	echo '`endif' >> $@
	rm $@.tmp

#-------------------------------------------------------------------------
# Create Stratus SRAM description file (.bdm)
#-------------------------------------------------------------------------
# Convert *.dat to *.bdm and update Stratus memory library

$(OUTPUT_DIR)/%.bdm: %.dat $(OUTPUT_DIR)
	python generate_bdm.py --ascii-dat $< --verbose
	-mv $(basename $(notdir $@))*.bdm $@
	bdw_memgen $@

#-------------------------------------------------------------------------
# Default make target
#-------------------------------------------------------------------------

.PHONY: all verilog

all: $(MEMORIES_V) $(MEMORIES_DAT) $(MEMORIES_BDM)
verilog: $(MEMORIES_V)

#-------------------------------------------------------------------------
# Clean up
#-------------------------------------------------------------------------

junk += $(OUTPUT_DIR)/*.dat $(OUTPUT_DIR)/*.bdm $(OUTPUT_DIR)/*.log $(OUTPUT_DIR)/*.v *.log

clean:
	rm -rf $(junk) *~ \#*
