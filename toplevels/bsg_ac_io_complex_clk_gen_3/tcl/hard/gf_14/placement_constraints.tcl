puts "BSG-info: Running script [info script]\n"


set prev_uplink_bound [create_bound -name "prev_uplink" -type soft -boundary {{237.0180 2014.3200} {307.1580 2763.1200}}]
add_to_bound ${prev_uplink_bound} [get_cells -hier -filter "full_name=~io_complex/prev/uplink/*"]

set prev_downlink_bound [create_bound -name "prev_downlink" -type soft -boundary {{307.1580 2697.3600} {1040.0580 2763.1200}}]
add_to_bound ${prev_downlink_bound} [get_cells -hier -filter "full_name=~io_complex/prev/downlink/*"]

set next_downlink_bound [create_bound -name "next_downlink" -type soft -boundary {{2692.8420 2014.3200} {2762.9820 2763.1200}}]
add_to_bound ${next_downlink_bound} [get_cells -hier -filter "full_name=~io_complex/next/downlink/*"]

set next_uplink_bound [create_bound -name "next_uplink" -type soft -boundary {{1959.9420 2697.3600} {2692.8420 2763.1200}}]
add_to_bound ${next_uplink_bound} [get_cells -hier -filter "full_name=~io_complex/next/uplink/*"]

set loopback_bound [create_bound -name "loopback" -type hard -boundary {{1000 2000} {2000 2400}}]
add_to_bound ${loopback_bound} [get_cells -hier -filter "full_name=~loopback/*"]
