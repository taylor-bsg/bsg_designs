`include "bsg_rocket_pkg.vh"

module bsg_rocket_accelerator_array
  import bsg_rocket_pkg::*;
# (parameter nodes_p = 0)
  (input                       clk_i
  ,input         [nodes_p-1:0] reset_i
  ,input         [nodes_p-1:0] en_i
  // rocc cmd in
  ,input         [nodes_p-1:0] rocc_cmd_v_i
  ,input      bsg_rocc_cmd_pkt rocc_cmd_data_i [nodes_p-1:0]
  ,output        [nodes_p-1:0] rocc_cmd_ready_o
  // rocc resp out
  ,output        [nodes_p-1:0] rocc_resp_v_o
  ,output    bsg_rocc_resp_pkt rocc_resp_data_o [nodes_p-1:0]
  ,input         [nodes_p-1:0] rocc_resp_ready_i
  // rocc mem req out
  ,output        [nodes_p-1:0] rocc_mem_req_v_o
  ,output bsg_rocc_mem_req_pkt rocc_mem_req_data_o [nodes_p-1:0]
  ,input         [nodes_p-1:0] rocc_mem_req_ready_i
  // rocc mem resp in
  ,input         [nodes_p-1:0] rocc_mem_resp_v_i
  ,input bsg_rocc_mem_resp_pkt rocc_mem_resp_data_i [nodes_p-1:0]
  // rocc ctrl out
  ,output  bsg_rocc_ctrl_in_pkt rocc_ctrl_o [nodes_p-1:0]
  // rocc ctrl in
  ,input  bsg_rocc_ctrl_out_pkt rocc_ctrl_i [nodes_p-1:0]);

  // bnn - accelerator[0]
  bsg_rocket_accelerator_bnn bnn
      (.clk_i(clk_i)
       // ctrl
       ,.reset_i(reset_i[0])
       ,.en_i(en_i[0])
       // rocc cmd in
       ,.rocc_cmd_v_i(rocc_cmd_v_i[0])
       ,.rocc_cmd_data_i(rocc_cmd_data_i[0])
       ,.rocc_cmd_ready_o(rocc_cmd_ready_o[0])
       // rocc resp out
       ,.rocc_resp_v_o(rocc_resp_v_o[0])
       ,.rocc_resp_data_o(rocc_resp_data_o[0])
       ,.rocc_resp_ready_i(rocc_resp_ready_i[0])
       // rocc mem req out
       ,.rocc_mem_req_v_o(rocc_mem_req_v_o[0])
       ,.rocc_mem_req_data_o(rocc_mem_req_data_o[0])
       ,.rocc_mem_req_ready_i(rocc_mem_req_ready_i[0])
       // rocc mem resp in
       ,.rocc_mem_resp_v_i(rocc_mem_resp_v_i[0])
       ,.rocc_mem_resp_data_i(rocc_mem_resp_data_i[0])
       // rocc ctrl out
       ,.rocc_ctrl_o(rocc_ctrl_o[0])
       // rocc ctrl in
       ,.rocc_ctrl_i(rocc_ctrl_i[0]));

  // Add more accelerators to the array here
  //
  // E.g., memcopy - accelerator[1]

endmodule
