module ManycoreQueue
(
  input wire  [ 0: 0] clk             ,
  input wire  [ 0: 0] reset           ,

  output reg  [63: 0] manycore_req_msg,
  input  wire [ 0: 0] manycore_req_rdy,
  output reg  [ 0: 0] manycore_req_val
);

/**
 *
 * +-------------+
 * |             |
 * |             |
 * |  BLACK BOX  |
 * |             |
 * |             |
 * +-------------+
 *
 **/

`ifndef SYNTHESIS
  reg [1023: 0] buff;
  reg [  63: 0] start_idx;

  `ifndef TEST_VECTOR_SIZE
  `define TEST_VECTOR_SIZE 131072
  `endif

  reg [63: 0] test_array [0:`TEST_VECTOR_SIZE - 1];

  initial
  begin
    if ($value$plusargs("TEST_VECTOR_FILENAME=%s", buff))
    begin
      $readmemh(buff, test_array);
    end
    if (!$value$plusargs("TEST_VECTOR_OFFSET=%d", start_idx))
    begin
      start_idx = 0;
    end
  end

  reg [63: 0]      ptr;
  reg [63: 0] comb_ptr;
  reg [63: 0] next_ptr;

  reg [63: 0] c_manycore_req_msg;
  reg [ 0: 0] c_manycore_req_val;

  always@(*)
  begin

    c_manycore_req_msg = 64'h0;
    c_manycore_req_val =  1'h0;

    comb_ptr           = ptr;

    if (comb_ptr < `TEST_VECTOR_SIZE)
    begin
      c_manycore_req_val = 1'b1;
      c_manycore_req_msg = test_array[comb_ptr];
    end

    if (reset)
    begin
      // Reset mode
      c_manycore_req_val = 1'b0;
      comb_ptr           = start_idx;
    end
    else
    begin
      // Normal mode
      if (comb_ptr < `TEST_VECTOR_SIZE)
      begin
        if (manycore_req_rdy)
        begin
          comb_ptr = comb_ptr + 1;
        end
      end
    end

    // Combinational to Sequential
    next_ptr = comb_ptr;

    // Ports
    manycore_req_msg = c_manycore_req_msg;
    manycore_req_val = c_manycore_req_val;

  end

  always@(posedge clk)
  begin

    ptr <= next_ptr;

  end

`endif
endmodule
