`include "bsg_rocc.v"

module Accel0
   import bsg_chip_pkg::*;
(input clk, input reset,
    output io_cmd_ready,
    input  io_cmd_valid,
    input [6:0] io_cmd_bits_inst_funct,
    input [4:0] io_cmd_bits_inst_rs2,
    input [4:0] io_cmd_bits_inst_rs1,
    input  io_cmd_bits_inst_xd,
    input  io_cmd_bits_inst_xs1,
    input  io_cmd_bits_inst_xs2,
    input [4:0] io_cmd_bits_inst_rd,
    input [6:0] io_cmd_bits_inst_opcode,
    input [63:0] io_cmd_bits_rs1,
    input [63:0] io_cmd_bits_rs2,
    input  io_resp_ready,
    output io_resp_valid,
    output[4:0] io_resp_bits_rd,
    output[63:0] io_resp_bits_data,
    input  io_mem_req_ready,
    output io_mem_req_valid,
    output[39:0] io_mem_req_bits_addr,
    output[9:0] io_mem_req_bits_tag,
    output[4:0] io_mem_req_bits_cmd,
    output[2:0] io_mem_req_bits_typ,
    //output io_mem_req_bits_kill
    output io_mem_req_bits_phys,
    output[63:0] io_mem_req_bits_data,
    input  io_mem_resp_valid,
    input [39:0] io_mem_resp_bits_addr,
    input [9:0] io_mem_resp_bits_tag,
    input [4:0] io_mem_resp_bits_cmd,
    input [2:0] io_mem_resp_bits_typ,
    input [63:0] io_mem_resp_bits_data,
    input  io_mem_resp_bits_nack,
    input  io_mem_resp_bits_replay,
    input  io_mem_resp_bits_has_data,
    input [63:0] io_mem_resp_bits_data_word_bypass,
    input [63:0] io_mem_resp_bits_store_data,
    //input  io_mem_replay_next_valid
    //input [9:0] io_mem_replay_next_bits
    //input  io_mem_xcpt_ma_ld
    //input  io_mem_xcpt_ma_st
    //input  io_mem_xcpt_pf_ld
    //input  io_mem_xcpt_pf_st
    output io_mem_invalidate_lr,
    //input  io_mem_ordered
    output io_busy,
    input  io_s,
    output io_interrupt,
    input  io_autl_acquire_ready,
    output io_autl_acquire_valid,
    //output[25:0] io_autl_acquire_bits_addr_block
    //output[2:0] io_autl_acquire_bits_client_xact_id
    //output[1:0] io_autl_acquire_bits_addr_beat
    //output io_autl_acquire_bits_is_builtin_type
    //output[2:0] io_autl_acquire_bits_a_type
    //output[16:0] io_autl_acquire_bits_union
    //output[127:0] io_autl_acquire_bits_data
    output io_autl_grant_ready,
    input  io_autl_grant_valid,
    input [1:0] io_autl_grant_bits_addr_beat,
    input [2:0] io_autl_grant_bits_client_xact_id,
    input [3:0] io_autl_grant_bits_manager_xact_id,
    input  io_autl_grant_bits_is_builtin_type,
    input [3:0] io_autl_grant_bits_g_type,
    input [127:0] io_autl_grant_bits_data,
    //input  io_fpu_req_ready
    //output io_fpu_req_valid
    //output[4:0] io_fpu_req_bits_cmd
    //output io_fpu_req_bits_ldst
    //output io_fpu_req_bits_wen
    //output io_fpu_req_bits_ren1
    //output io_fpu_req_bits_ren2
    //output io_fpu_req_bits_ren3
    //output io_fpu_req_bits_swap12
    //output io_fpu_req_bits_swap23
    //output io_fpu_req_bits_single
    //output io_fpu_req_bits_fromint
    //output io_fpu_req_bits_toint
    //output io_fpu_req_bits_fastpipe
    //output io_fpu_req_bits_fma
    //output io_fpu_req_bits_div
    //output io_fpu_req_bits_sqrt
    //output io_fpu_req_bits_round
    //output io_fpu_req_bits_wflags
    //output[2:0] io_fpu_req_bits_rm
    //output[1:0] io_fpu_req_bits_typ
    //output[64:0] io_fpu_req_bits_in1
    //output[64:0] io_fpu_req_bits_in2
    //output[64:0] io_fpu_req_bits_in3
    //output io_fpu_resp_ready
    //input  io_fpu_resp_valid
    //input [64:0] io_fpu_resp_bits_data
    //input [4:0] io_fpu_resp_bits_exc
    input  io_exception,
    //input [11:0] io_csr_waddr
    //input [63:0] io_csr_wdata
    //input  io_csr_wen
    input  io_host_id
);


   // shared with client; factor
   localparam bank_size_lp       = bsg_chip_pkg::bank_size_gp;
   localparam bank_num_lp        = bsg_chip_pkg::bank_num_gp;
   localparam imem_size_lp       = bsg_chip_pkg::imem_size_gp;

   localparam addr_width_lp      = bsg_chip_pkg::addr_width_gp;
   localparam data_width_lp      = bsg_chip_pkg::data_width_gp;
   localparam hetero_type_vec_lp = bsg_chip_pkg::hetero_type_vec_gp;

   localparam num_tiles_x_lp     = bsg_chip_pkg::num_tiles_x_gp;
   localparam num_tiles_y_lp     = bsg_chip_pkg::num_tiles_y_gp;

   localparam debug_lp           = 0;
   
   localparam rocc_num_lp        = bsg_chip_pkg::rocc_num_p;
   localparam rocc_dist_vec_lp   = bsg_chip_pkg::rocc_dist_vec_p;

    rocc_core_cmd_s     core_cmd_s;
    rocc_core_resp_s    core_resp_s;
    rocc_mem_req_s      mem_req_s;
    rocc_mem_resp_s     mem_resp_s;

    assign  core_cmd_s.instr.funct7     =io_cmd_bits_inst_funct;
    assign  core_cmd_s.instr.rs2        =io_cmd_bits_inst_rs2;
    assign  core_cmd_s.instr.rs1        =io_cmd_bits_inst_rs1;
    assign  core_cmd_s.instr.xd         =io_cmd_bits_inst_xd ;
    assign  core_cmd_s.instr.xs1        =io_cmd_bits_inst_xs1 ;
    assign  core_cmd_s.instr.xs2        =io_cmd_bits_inst_xs2 ;
    assign  core_cmd_s.instr.rd         =io_cmd_bits_inst_rd  ;
    assign  core_cmd_s.instr.op         =io_cmd_bits_inst_opcode ; 
    assign  core_cmd_s.rs1_val          =io_cmd_bits_rs1 ; 
    assign  core_cmd_s.rs2_val          =io_cmd_bits_rs2 ; 
    assign  core_cmd_s.core_host_id     =io_host_id ;

    assign  io_resp_bits_rd             =core_resp_s.rd;
    assign  io_resp_bits_data           =core_resp_s.rd_data;

    assign  io_mem_req_bits_addr        =mem_req_s.req_addr;
    assign  io_mem_req_bits_tag         =mem_req_s.req_tag;
    assign  io_mem_req_bits_cmd         =mem_req_s.req_cmd;
    assign  io_mem_req_bits_typ         =mem_req_s.req_typ;
    assign  io_mem_req_bits_phys        =mem_req_s.req_phys;
    assign  io_mem_req_bits_data        =mem_req_s.req_data;

    assign  mem_resp_s.resp_addr             =io_mem_resp_bits_addr;
    assign  mem_resp_s.resp_tag              =io_mem_resp_bits_tag;
    assign  mem_resp_s.resp_cmd              =eRoCC_mem_cmd'(io_mem_resp_bits_cmd);
    assign  mem_resp_s.resp_typ              =eRoCC_mem_typ'(io_mem_resp_bits_typ);
    assign  mem_resp_s.resp_data             =io_mem_resp_bits_data;
    assign  mem_resp_s.resp_nack             =io_mem_resp_bits_nack;
    assign  mem_resp_s.resp_replay           =io_mem_resp_bits_replay;
    assign  mem_resp_s.resp_has_data         =io_mem_resp_bits_has_data;
    assign  mem_resp_s.resp_data_word_bypass =io_mem_resp_bits_data_word_bypass;
    assign  mem_resp_s.resp_store_data       =io_mem_resp_bits_store_data;


    bsg_manycore_rocc_wrapper #(
    .rocc_num_p             ( rocc_num_lp     )
   ,.rocc_dist_vec_p        ( rocc_dist_vec_lp)

   ,.bank_size_p            ( bank_size_lp     )
   ,.num_banks_p            ( bank_num_lp      )
   ,.imem_size_p            ( imem_size_lp     )

   ,.num_tiles_x_p          ( num_tiles_x_lp   )
   ,.num_tiles_y_p          ( num_tiles_y_lp   )

   ,.hetero_type_vec_p      ( hetero_type_vec_lp) 
    
   ,.debug_p                ( debug_lp          )   
   ,.extra_io_rows_p        ( 1                 )

   ,.addr_width_p           ( addr_width_lp     )
   ,.data_width_p           ( data_width_lp     )
  )manycore_rocc_inst
  ( .clk_i    ( clk     )
   ,.reset_i  ( reset   )

   //core control signals
   ,.core_status_i      (  io_s                )
   ,.core_exception_i   (  io_exception        )
   ,.acc_interrupt_o    (  io_interrupt        )
   ,.acc_busy_o         (  io_busy             ) 
   //command signals
   ,.core_cmd_valid_i   (  io_cmd_valid        )
   ,.core_cmd_s_i       (  core_cmd_s          )
   ,.core_cmd_ready_o   (  io_cmd_ready        )

   ,.core_resp_valid_o  (  io_resp_valid       )
   ,.core_resp_s_o      (  core_resp_s         ) 
   ,.core_resp_ready_i  (  io_resp_ready       )
   //mem signals
   ,.mem_req_valid_o    (  io_mem_req_valid    )
   ,.mem_req_s_o        (  mem_req_s           )
   ,.mem_req_ready_i    (  io_mem_req_ready    )

   ,.mem_resp_valid_i   (  io_mem_resp_valid   )
   ,.mem_resp_s_i       (  mem_resp_s          )
  );

  //assign unsigned signals in  manycore as done in the example
  assign    io_mem_invalidate_lr        = 1'b0;
  // input io_autl_acquire_ready
  assign    io_autl_acquire_valid       = 1'b0;
  assign    io_autl_grant_ready         = 1'b0;
  // input io_autl_grant_valid      
  // input  io_autl_grant_valid,
  // input [1:0] io_autl_grant_bits_addr_beat,
  // input [2:0] io_autl_grant_bits_client_xact_id,
  // input [3:0] io_autl_grant_bits_manager_xact_id,
  // input  io_autl_grant_bits_is_builtin_type,
  // input [3:0] io_autl_grant_bits_g_type,
  // input [127:0] io_autl_grant_bits_data,
endmodule
