/**
 *  manycore.v
 */

`include "bsg_manycore_packet.vh"

module manycore
  import bsg_noc_pkg::*; // {P=0, W, E, N, S}
  #(parameter addr_width_p="inv"
    , parameter data_width_p="inv"
    , parameter num_tiles_x_p="inv"
    , parameter num_tiles_y_p="inv"
    , parameter x_cord_width_p="inv"
    , parameter y_cord_width_p="inv"
    , parameter load_id_width_p="inv"

    , parameter dmem_size_p="inv"
    , parameter icache_entries_p="inv"
    , parameter icache_tag_width_p="inv"
    , parameter dram_ch_addr_width_p="inv"
    , parameter epa_addr_width_p="inv"
    , parameter hetero_type_vec_p="inv"

    , parameter link_sif_width_lp=
      `bsg_manycore_link_sif_width(addr_width_p,data_width_p,x_cord_width_p,y_cord_width_p,load_id_width_p)
  )
  (
    input clk_i
    ,input reset_i

    ,input [num_tiles_x_p-1:0][link_sif_width_lp-1:0] s_link_sif_i 
    ,output logic [num_tiles_x_p-1:0][link_sif_width_lp-1:0] s_link_sif_o

    ,input [num_tiles_x_p-1:0][link_sif_width_lp-1:0] io_link_sif_i
    ,output logic [num_tiles_x_p-1:0][link_sif_width_lp-1:0] io_link_sif_o
  );

  // manycore
  //
  `declare_bsg_manycore_link_sif_s(addr_width_p,data_width_p,x_cord_width_p,y_cord_width_p,load_id_width_p);

  bsg_manycore_link_sif_s [E:W][num_tiles_y_p:0] hor_link_sif_li;
  bsg_manycore_link_sif_s [E:W][num_tiles_y_p:0] hor_link_sif_lo;

  bsg_manycore_link_sif_s [S:N][num_tiles_x_p-1:0] ver_link_sif_li;
  bsg_manycore_link_sif_s [S:N][num_tiles_x_p-1:0] ver_link_sif_lo;

  bsg_manycore_link_sif_s [num_tiles_x_p-1:0] io_link_sif_li;
  bsg_manycore_link_sif_s [num_tiles_x_p-1:0] io_link_sif_lo;

  bsg_manycore #(
    .data_width_p(data_width_p)
    ,.addr_width_p(addr_width_p)
    ,.dmem_size_p(dmem_size_p)
    ,.icache_entries_p(icache_entries_p)
    ,.icache_tag_width_p(icache_tag_width_p)
    ,.load_id_width_p(load_id_width_p)
    ,.num_tiles_x_p(num_tiles_x_p)
    ,.num_tiles_y_p(num_tiles_y_p)
    ,.epa_addr_width_p (epa_addr_width_p)
    ,.dram_ch_addr_width_p(dram_ch_addr_width_p)
    ,.hetero_type_vec_p(hetero_type_vec_p)
    ,.stub_w_p({num_tiles_y_p{1'b0}})
    ,.stub_e_p({num_tiles_y_p{1'b0}})
    ,.stub_n_p({num_tiles_x_p{1'b0}})
    ,.stub_s_p({num_tiles_x_p{1'b0}})
    ,.repeater_output_p(0)
    ,.debug_p(0)

  ) bm (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.hor_link_sif_i(hor_link_sif_li)
    ,.hor_link_sif_o(hor_link_sif_lo)

    ,.ver_link_sif_i(ver_link_sif_li)
    ,.ver_link_sif_o(ver_link_sif_lo)

    ,.io_link_sif_i(io_link_sif_li)
    ,.io_link_sif_o(io_link_sif_lo)
  );

  for (genvar i = 0; i < num_tiles_x_p; i++) begin
    assign io_link_sif_li[i] = io_link_sif_i[i];
    assign io_link_sif_o[i] = io_link_sif_lo[i];
    assign ver_link_sif_li[S][i] = s_link_sif_i[i];
    assign s_link_sif_o[i] = ver_link_sif_lo[S][i];
  end


  // tie-off
  //
  for (genvar i = 0; i < num_tiles_y_p+1; i++) begin: tieoff_hor
    bsg_manycore_link_sif_tieoff #(
      .addr_width_p(addr_width_p)
      ,.data_width_p(data_width_p)
      ,.x_cord_width_p(x_cord_width_p)
      ,.y_cord_width_p(y_cord_width_p)
      ,.load_id_width_p(load_id_width_p)
    ) tieoff_w (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.link_sif_i(hor_link_sif_lo[W][i])
      ,.link_sif_o(hor_link_sif_li[W][i])
    );

    bsg_manycore_link_sif_tieoff #(
      .addr_width_p(addr_width_p)
      ,.data_width_p(data_width_p)
      ,.x_cord_width_p(x_cord_width_p)
      ,.y_cord_width_p(y_cord_width_p)
      ,.load_id_width_p (load_id_width_p)
    ) tieoff_e (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.link_sif_i(hor_link_sif_lo[E][i])
      ,.link_sif_o(hor_link_sif_li[E][i])
    );
  end

  for (genvar j = 0; j < num_tiles_x_p; j++) begin: tieoff_north
    bsg_manycore_link_sif_tieoff #(
      .addr_width_p(addr_width_p)
      ,.data_width_p(data_width_p)
      ,.x_cord_width_p(x_cord_width_p)
      ,.y_cord_width_p(y_cord_width_p)
      ,.load_id_width_p (load_id_width_p)
    ) tieoff_n (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.link_sif_i(ver_link_sif_lo[N][j])
      ,.link_sif_o(ver_link_sif_li[N][j])
    );
  end



endmodule
