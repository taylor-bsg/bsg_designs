/**
 *  fsb_mesh_node.v
 */

`include "bsg_manycore_packet.vh"

module fsb_mesh_node
  import bsg_chip_pkg::*;
  import bsg_dram_ctrl_pkg::*;
  #(parameter ring_width_p="inv"
    , parameter id_width_p="inv"
    , parameter master_p="inv"
    , parameter master_id_p="inv"
    , parameter client_id_p="inv"
  )
  (
    input clk_i
    , input reset_i

    // FSB side
    , input en_i

    , input v_i
    , output logic ready_o
    , input [ring_width_p-1:0] data_i

    , input yumi_i
    , output logic v_o
    , output logic [ring_width_p-1:0] data_o

    // DRAM ctrl side
    , output logic app_en_o
    , input app_rdy_i
    , output eAppCmd app_cmd_o
    , output logic [dram_ctrl_awidth_gp-1:0] app_addr_o

    , output logic app_wdf_wren_o
    , input app_wdf_rdy_i
    , output logic [dram_ctrl_dwidth_gp-1:0] app_wdf_data_o
    , output logic [(dram_ctrl_dwidth_gp>>3)-1:0] app_wdf_mask_o
    , output logic app_wdf_end_o

    , input app_rd_data_valid_i
    , input [dram_ctrl_dwidth_gp-1:0] app_rd_data_i
    , input app_rd_data_end_i
  );

  wire unused = en_i;

  // manycore
  //
  `declare_bsg_manycore_link_sif_s(addr_width_gp,data_width_gp,x_cord_width_gp,y_cord_width_gp,load_id_width_gp);
  bsg_manycore_link_sif_s [num_tiles_x_gp-1:0] s_link_sif_li;
  bsg_manycore_link_sif_s [num_tiles_x_gp-1:0] s_link_sif_lo;
  bsg_manycore_link_sif_s [num_tiles_x_gp-1:0] io_link_sif_li;
  bsg_manycore_link_sif_s [num_tiles_x_gp-1:0] io_link_sif_lo;

  manycore #(
    .addr_width_p(addr_width_gp)
    ,.data_width_p(data_width_gp)
    ,.num_tiles_x_p(num_tiles_x_gp)
    ,.num_tiles_y_p(num_tiles_y_gp)
    ,.x_cord_width_p(x_cord_width_gp)
    ,.y_cord_width_p(y_cord_width_gp)
    ,.load_id_width_p(load_id_width_gp)

    ,.dmem_size_p(dmem_size_gp)
    ,.icache_entries_p(icache_entries_gp)
    ,.icache_tag_width_p(icache_tag_width_gp)
    ,.dram_ch_addr_width_p(dram_ch_addr_width_gp)
    ,.epa_addr_width_p(epa_addr_width_gp)
    ,.hetero_type_vec_p(hetero_type_vec_gp)
  ) manycore_inst (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.s_link_sif_i(s_link_sif_li)
    ,.s_link_sif_o(s_link_sif_lo)

    ,.io_link_sif_i(io_link_sif_li)
    ,.io_link_sif_o(io_link_sif_lo)
  );

  
  // manycore_links_to_fsb
  //
  logic [ring_width_p-1:0] data_i_inv, data_i_rep, data_o_inv, data_o_prerep; // buffers

  bsg_inv #(
    .width_p(ring_width_p)
    ,.harden_p(1)
    ,.vertical_p(0)
  ) data_i_hi (
    .i(data_i)
    ,.o(data_i_inv)
  );

  bsg_inv #(
    .width_p(ring_width_p)
    ,.harden_p(1)
    ,.vertical_p(0)
  ) data_i_mid (
    .i(data_i_inv)
    ,.o(data_i_rep)
  );

  bsg_inv #(
    .width_p(ring_width_p)
    ,.harden_p(1)
    ,.vertical_p(0)
  ) data_o_mid (
    .o(data_o)
    ,.i(data_o_inv)
  );

  bsg_inv #(
    .width_p(ring_width_p)
    ,.harden_p(1)
    ,.vertical_p(0)
  ) data_o_lo (
    .o(data_o_inv)
    ,.i(data_o_prerep)
  );

  bsg_manycore_links_to_fsb #(
    .ring_width_p(ring_width_p)
    ,.id_width_p(id_width_p)
    ,.dest_id_p(master_id_p)
    ,.num_links_p(1)
    ,.addr_width_p(addr_width_gp)
    ,.data_width_p(data_width_gp)
    ,.x_cord_width_p(x_cord_width_gp)
    ,.y_cord_width_p(y_cord_width_gp)
    ,.load_id_width_p(load_id_width_gp)
    ,.remote_credits_p(fsb_remote_credits_gp)
    ,.use_pseudo_large_fifo_p(1)
  ) l2f (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.links_sif_i(io_link_sif_lo[fsb_x_cord_gp])
    ,.links_sif_o(io_link_sif_li[fsb_x_cord_gp])

    ,.v_i(v_i)
    ,.data_i(data_i_rep)
    ,.ready_o(ready_o)

    ,.v_o(v_o)
    ,.data_o(data_o_prerep)
    ,.yumi_i(yumi_i)
  );

  // vcache_row
  //
  logic [num_cache_gp-1:0][x_cord_width_gp-1:0] cache_x;
  logic [num_cache_gp-1:0][y_cord_width_gp-1:0] cache_y; 

  for (genvar i = 0; i < num_cache_gp; i++) begin
    assign cache_x[i] = (x_cord_width_gp)'(i);
    assign cache_y[i] = (y_cord_width_gp)'(num_tiles_y_gp);
  end

  vcache_row #(
    .num_cache_p(num_cache_gp)
    ,.sets_p(cache_sets_gp)
    ,.ways_p(cache_ways_gp)
    ,.block_size_in_words_p(cache_block_size_in_words_gp)

    ,.addr_width_p(addr_width_gp)
    ,.data_width_p(data_width_gp)
    ,.x_cord_width_p(x_cord_width_gp)
    ,.y_cord_width_p(y_cord_width_gp)
    ,.load_id_width_p(load_id_width_gp)

    ,.dram_ctrl_data_width_p(dram_ctrl_dwidth_gp)
    ,.dram_ctrl_addr_width_p(dram_ctrl_awidth_gp)
  ) vcache_row_inst (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.s_link_sif_i(s_link_sif_lo[num_cache_gp-1:0])
    ,.s_link_sif_o(s_link_sif_li[num_cache_gp-1:0])

    ,.cache_x_i(cache_x)
    ,.cache_y_i(cache_y)

    ,.app_en_o(app_en_o)
    ,.app_rdy_i(app_rdy_i)
    ,.app_cmd_o(app_cmd_o)
    ,.app_addr_o(app_addr_o)

    ,.app_wdf_wren_o(app_wdf_wren_o)
    ,.app_wdf_rdy_i(app_wdf_rdy_i)
    ,.app_wdf_data_o(app_wdf_data_o)
    ,.app_wdf_mask_o(app_wdf_mask_o)
    ,.app_wdf_end_o(app_wdf_end_o)

    ,.app_rd_data_valid_i(app_rd_data_valid_i)
    ,.app_rd_data_i(app_rd_data_i)
    ,.app_rd_data_end_i(app_rd_data_end_i)
  );

  // tieoff for unused links
  //
  for (genvar j = num_cache_gp; j < num_tiles_x_gp; j++) begin: tieoff_s
    bsg_manycore_link_sif_tieoff #(
      .addr_width_p(addr_width_gp)
      ,.data_width_p(data_width_gp)
      ,.x_cord_width_p(x_cord_width_gp)
      ,.y_cord_width_p(y_cord_width_gp)
      ,.load_id_width_p(load_id_width_gp)
    ) tieoff_s (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.link_sif_i(s_link_sif_lo[j])
      ,.link_sif_o(s_link_sif_li[j])
    );
  end

  for (genvar j = 0; j < fsb_x_cord_gp; j++) begin: tieoff_io
    bsg_manycore_link_sif_tieoff #(
      .addr_width_p(addr_width_gp)
      ,.data_width_p(data_width_gp)
      ,.x_cord_width_p(x_cord_width_gp)
      ,.y_cord_width_p(y_cord_width_gp)
      ,.load_id_width_p(load_id_width_gp)
    ) tieoff_io (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      ,.link_sif_i(io_link_sif_lo[j])
      ,.link_sif_o(io_link_sif_li[j])
    );
  end

endmodule
