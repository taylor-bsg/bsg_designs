`ifndef BSG_CHIP_PKG_V
`define BSG_CHIP_PKG_V

package bsg_chip_pkg;

  // manycore setting
  parameter addr_width_gp         = 26;   // in words
  parameter data_width_gp         = 32;
  parameter num_tiles_x_gp        = 6;
  parameter num_tiles_y_gp        = 6;
  parameter x_cord_width_gp       = `BSG_SAFE_CLOG2(num_tiles_x_gp);
  parameter y_cord_width_gp       = `BSG_SAFE_CLOG2(num_tiles_y_gp+2);
  parameter load_id_width_gp      = 11;

  parameter dmem_size_gp          = 1024; // in words
  parameter icache_entries_gp     = 1024; // in words
  parameter icache_tag_width_gp   = 12; 
  parameter dram_ch_addr_width_gp = 25;
  parameter epa_addr_width_gp     = 16;
  parameter hetero_type_vec_gp    = 0;

  // comm-link setting
  parameter num_channels_gp = 2;

  // FSB setting
  parameter fsb_remote_credits_gp = 128;
  parameter fsb_mesh_node_idx = 0;
  parameter fsb_test_node_idx = 1;
  parameter fsb_x_cord_gp = num_tiles_x_gp - 1;

  // vcache setting
  parameter num_cache_gp                  = 4;
  parameter cache_ways_gp                 = 2;
  parameter cache_sets_gp                 = 2**8;
  parameter cache_block_size_in_words_gp  = 8;



  // for bsg_test_node_master
  parameter tile_id_ptr_gp  = -1;
  parameter max_cycles_gp   = 500000;


  //dram controller
 // parameter dram_ctrl_x_cord_gp = 0;
  parameter dram_ctrl_dwidth_gp = 128;
  parameter dram_ctrl_awidth_gp = 29;   // 2x2 Gbit DRAM

  parameter dram_dfi_width_gp   = 64;
  parameter dram_ba_width_gp    = 2;
  parameter dram_addr_width_gp  = 14;

  //FSB configuration

  parameter dram_ch_num_gp =2;

endpackage

`endif
