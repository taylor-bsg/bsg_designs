/**
 *  bsg_clk_gen_wrapper.v
 */

`include "bsg_tag.vh"

module bsg_clk_gen_wrapper
  import bsg_tag_pkg::bsg_tag_s;
  #(parameter num_clk_endpoint_p="inv" // 3
    , parameter num_dram_clk_endpoint_p="inv" // 1 
    , parameter lines_per_endpoint_p="inv" // 4

    , localparam tags_per_endpoint_lp = 3
    , localparam tag_els_lp
      = (((num_clk_endpoint_p+num_dram_clk_endpoint_p)*tags_per_endpoint_lp)+num_clk_endpoint_p)

    , localparam ds_width_lp = 8
    , localparam num_adgs_lp = 1
  )
  (
    // async reset
    input async_reset_i

    // bsg_tag signals
    , input tag_clk_i
    , input tag_data_i
    , input tag_en_i

    // external clock input
    , input [num_clk_endpoint_p-1:0] ext_clk_i
    , output logic [num_clk_endpoint_p-1:0] clk_o

    // dram clk
    , output logic [num_dram_clk_endpoint_p-1:0] dram_clk_2x_o
    , output logic [num_dram_clk_endpoint_p-1:0] dram_clk_1x_o

    // delay clk
    , input [num_dram_clk_endpoint_p-1:0][lines_per_endpoint_p-1:0] dly_clk_i
    , output logic [num_dram_clk_endpoint_p-1:0][lines_per_endpoint_p-1:0] dly_clk_o
  );


  // for bsg_clk_gen v2, we need 3 bsg_tag endpoints
  // we also need one tag to select clk for each endpoint.

  // declaring payload struct
  //
  `declare_bsg_clk_gen_osc_tag_payload_s(num_adgs_lp)
  `declare_bsg_clk_gen_ds_tag_payload_s(ds_width_lp)

  // max payload length
  //
  localparam tag_max_payload_length_lp =
    `BSG_MAX($bits(bsg_clk_gen_osc_tag_payload_s),$bits(bsg_clk_gen_ds_tag_payload_s));
  localparam lg_tag_max_payload_length_lp = $clog2(tag_max_payload_length_lp+1);

  // bsg_tag_client id mapping ///////////////
  //
  //  0 : core_clk osc                (5 bits)
  //  1 : iom_clk osc                 (5 bits)
  //  2 : fsb_clk osc                 (5 bits)
  //  3 : core_clk osc trigger        (1 bit )
  //  4 : iom_clk osc trigger         (1 bit )
  //  5 : fsb_clk osc trigger         (1 bit )
  //  6 : core_clk downsample         (8 bits)
  //  7 : iom_clk downsample          (8 bits)
  //  8 : fsb_clk downsample          (8 bits)
  //  9 : core_clk output select      (2 bits)
  // 10 : iom_clk output select       (2 bits)
  // 11 : fsb_clk output select       (2 bits)
  //
  // 12 : dram_clk osc                (5 bits)
  // 13 : dram_clk osc trigger        (1 bit )
  // 14 : dram_clk downsample         (8 bits)
  //
  ////////////////////////////////////////////

  bsg_tag_s [num_clk_endpoint_p-1:0] osc_tags;
  bsg_tag_s [num_clk_endpoint_p-1:0] osc_trigger_tags;
  bsg_tag_s [num_clk_endpoint_p-1:0] ds_tags;
  bsg_tag_s [num_clk_endpoint_p-1:0] select_tags;

  bsg_tag_s [num_dram_clk_endpoint_p-1:0] dram_osc_tags;
  bsg_tag_s [num_dram_clk_endpoint_p-1:0] dram_osc_trigger_tags;
  bsg_tag_s [num_dram_clk_endpoint_p-1:0] dram_ds_tags;

  bsg_tag_s [tag_els_lp-1:0] tags;

  assign {
    dram_ds_tags,
    dram_osc_trigger_tags,
    dram_osc_tags,
    select_tags,
    ds_tags,
    osc_trigger_tags,
    osc_tags
  } = tags;

  bsg_tag_master #(
    .els_p(tag_els_lp)
    ,.lg_width_p(lg_tag_max_payload_length_lp)
  ) btm (
    .clk_i(tag_clk_i)
    ,.data_i(tag_data_i)
    ,.en_i(tag_en_i)
    ,.clients_r_o(tags)
  );

  // core clock generator
  //
  logic [num_clk_endpoint_p-1:0][1:0] clk_select;

  for (genvar i = 0; i < num_clk_endpoint_p; i++) begin: clk_gen

    bsg_tag_client_unsync #(
      .width_p(2)
    ) btc_unsync_obs_mux (
      .bsg_tag_i(select_tags[i])
      ,.data_async_r_o(clk_select[i])
    );

    bsg_clk_gen #(
      .downsample_width_p(ds_width_lp)
      ,.num_adgs_p(num_adgs_lp)
      ,.version_p(2)
    ) clk_gen_inst (

      .async_osc_reset_i(async_reset_i)

      ,.bsg_osc_tag_i         (osc_tags[i])
      ,.bsg_osc_trigger_tag_i (osc_trigger_tags[i])
      ,.bsg_ds_tag_i          (ds_tags[i])

      ,.ext_clk_i             (ext_clk_i[i])
      ,.select_i              (clk_select[i])
      ,.clk_o                 (clk_o[i])
    );
  end
  
  // dram clock generator
  //
  for (genvar i = 0; i < num_dram_clk_endpoint_p; i++) begin: dram_clk_gen
    bsg_dram_clk_gen #(
      .num_lines_p(lines_per_endpoint_p)
      ,.downsample_width_p(ds_width_lp)
      ,.num_adgs_p(num_adgs_lp)
      ,.version_p(2)
    ) dram_clk_gen_inst (
      .async_osc_reset_i(async_reset_i)

      ,.bsg_osc_tag_i         (dram_osc_tags[i])
      ,.bsg_osc_trigger_tag_i (dram_osc_trigger_tags[i])
      ,.bsg_ds_tag_i          (dram_ds_tags[i])

      ,.osc_clk_o             (dram_clk_2x_o[i])
      ,.div_clk_o             (dram_clk_1x_o[i])

      ,.dly_clk_i             (dly_clk_i[i])
      ,.dly_clk_o             (dly_clk_o[i])
    );
  end

endmodule
