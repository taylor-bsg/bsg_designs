/**
 *  bsg_test_node_client.v
 */

module bsg_test_node_client
  #(parameter ring_width_p="inv")
  (
    input clk_i
    , input reset_i
    , input en_i

    , input  v_i
    , input [ring_width_p-1:0] data_i
    , output logic ready_o

    , output logic v_o
    , output logic [ring_width_p-1:0] data_o
    , input yumi_i
  );

  logic in_fifo_v_lo;
  logic in_fifo_yumi_li;
  logic [ring_width_p-1:0] in_fifo_data_lo;

  bsg_two_fifo #(
    .width_p(ring_width_p)
  ) fifo_in (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.v_i(v_i)
    ,.ready_o(ready_o)
    ,.data_i(data_i)

    ,.v_o(in_fifo_v_lo)
    ,.yumi_i(in_fifo_yumi_li)
    ,.data_o(in_fifo_data_lo)
  );

  logic out_fifo_v_li;
  logic out_fifo_ready_lo;
  logic [ring_width_p-1:0] out_fifo_data_li;
  
  bsg_two_fifo #(
    .width_p(ring_width_p)
  ) fifo_out (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.v_i(out_fifo_v_li)
    ,.ready_o(out_fifo_ready_lo)
    ,.data_i(out_fifo_data_li)

    ,.v_o(v_o)
    ,.yumi_i(yumi_i)
    ,.data_o(data_o)
  );

  loopback #(
    .width_p(ring_width_p)
  ) loopback_inst (
    .clk_i(clk_i)
    ,.reset_i(reset_i)
   
    ,.v_i(in_fifo_v_lo)
    ,.yumi_o(in_fifo_yumi_li)
    ,.data_i(in_fifo_data_lo) 

    ,.v_o(out_fifo_v_li)
    ,.ready_i(out_fifo_ready_lo)
    ,.data_o(out_fifo_data_li)
  );


  // synopsys translate_off
  if (debug_p) begin

    always_ff @ (negedge clk_i) begin
      if (v_i & ready_o) begin
        $display("## bsg_test_node_client received %x", data_i);
      end
    end

    always_ff @ (negedge clk_i) begin
      if (v_o & yumi_i) begin
        $display("## bsg_test_node_client sent %x", data_o);
      end
    end

  end
  // synopsys translate_on

endmodule
