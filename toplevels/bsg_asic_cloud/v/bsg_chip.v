/**
 *  bsg_chip.v
 */

`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

module bsg_chip
  import bsg_chip_pkg::*;
  `include "bsg_pinout.v"
  `include "bsg_iopads.v"

  // global parameters
  //
  localparam num_channels_lp = num_channels_gp;

  // bsg_clk_gen_wrapper
  //
  wire tag_clk_li;
  wire tag_data_li;
  wire tag_en_li;

  assign tag_clk_li  = bsg_tag_clk_i_int;
  assign tag_data_li = bsg_tag_data_i_int;
  assign tag_en_li   = bsg_tag_en_i_int;

  // [0] : core_clk
  // [1] : iom_clk
  // [2] : fsb_clk
  wire [2:0] ext_clk_li; 

  assign ext_clk_li[0] = clk_A_i_int;
  assign ext_clk_li[1] = clk_B_i_int;
  assign ext_clk_li[2] = clk_C_i_int;

  wire clk_async_reset;
  wire obs_clk_lo;
  wire [2:0] clk_lo;
  wire dfi_clk_2x_lo;
  wire dfi_clk_1x_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_mux;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_dly;

  assign clk_async_reset = clk_async_reset_i_int;
  assign clk_o_int = obs_clk_lo;

  bsg_clk_gen_wrapper #(
    .num_clk_endpoint_p(3)
    ,.num_dram_clk_endpoint_p(1)
    ,.lines_per_endpoint_p(4)
  ) clk_gen_wrapper_inst (

    .async_reset_i(clk_async_reset)

    ,.tag_clk_i(tag_clk_li)
    ,.tag_data_i(tag_data_li)
    ,.tag_en_i(tag_en_li)

    ,.ext_clk_i(ext_clk_li)   // external clk
    ,.clk_o(clk_lo)           // final clk

    ,.dram_clk_2x_o(dfi_clk_2x_lo)
    ,.dram_clk_1x_o(dfi_clk_1x_lo)

    ,.dly_clk_i(ddr_dqs_p_mux)
    ,.dly_clk_o(ddr_dqs_p_dly)
  );

  wire core_clk_lo;
  wire iom_clk_lo;
  wire fsb_clk_lo;

  assign core_clk_lo = clk_lo[0];
  assign iom_clk_lo  = clk_lo[1];
  assign fsb_clk_lo  = clk_lo[2];
 
  // observe clock from outside
  // 0: core_clk
  // 1: iom_clk
  // 2: fsb_clk
  // 3: dfi_clk_1x
  // 4: dfi_clk_2x
  // 5: unused
  // 6: unused
  // 7: unused
  wire [2:0] obs_clk_sel_li;
  
  assign obs_clk_sel_li[0] = sel_0_i_int;
  assign obs_clk_sel_li[1] = sel_1_i_int;
  assign obs_clk_sel_li[2] = sel_2_i_int;

  wire [7:0] clk_list;
  assign clk_list = {
    3'b0,
    dfi_clk_2x_lo,
    dfi_clk_1x_lo,
    clk_lo
  };

  bsg_mux #(
    .width_p(1)
    ,.els_p(8)
    ,.balanced_p(1)
    ,.harden_p(1)
  ) obs_clk_mux (
    .data_i(clk_list)
    ,.sel_i(obs_clk_sel_li)
    ,.data_o(clk_o_int)
  );


  // comm_link channel
  //
  wire [num_channels_lp-1:0] io_clk_tline_li;
  wire [num_channels_lp-1:0] io_valid_tline_li;
  wire [num_channels_lp-1:0] io_token_clk_tline_lo;
  wire [8:0] io_data_tline_li [num_channels_lp-1:0];

  wire [num_channels_lp-1:0] im_clk_tline_lo;
  wire [num_channels_lp-1:0] im_valid_tline_lo;
  wire [num_channels_lp-1:0] im_token_clk_tline_li;
  wire [8:0] im_data_tline_lo [num_channels_lp-1:0];

  bsg_chip_swizzle_adapter swizzle_adapter (
    .guts_ci_clk_o(io_clk_tline_li[0])
    ,.guts_ci_v_o(io_valid_tline_li[0])
    ,.guts_ci_data_o(io_data_tline_li[0])
    ,.guts_ci_tkn_i(io_token_clk_tline_lo[0])
    
    ,.guts_ci2_clk_o(io_clk_tline_li[1])
    ,.guts_ci2_v_o(io_valid_tline_li[1])
    ,.guts_ci2_data_o(io_data_tline_li[1])
    ,.guts_ci2_tkn_i(io_token_clk_tline_lo[1])

    ,.guts_co_clk_i(im_clk_tline_lo[0])
    ,.guts_co_v_i(im_valid_tline_lo[0])
    ,.guts_co_data_i(im_data_tline_lo[0])
    ,.guts_co_tkn_o(im_token_clk_tline_li[0])

    ,.guts_co2_clk_i(im_clk_tline_lo[1])
    ,.guts_co2_v_i(im_valid_tline_lo[1])
    ,.guts_co2_data_i(im_data_tline_lo[1])
    ,.guts_co2_tkn_o(im_token_clk_tline_li[1])

    ,.port_ci_clk_i(ci_clk_i_int)
    ,.port_ci_v_i(ci_v_i_int)
    ,.port_ci_data_i({
      ci_8_i_int, ci_7_i_int, ci_6_i_int, ci_5_i_int,
      ci_4_i_int, ci_3_i_int, ci_2_i_int, ci_1_i_int,
      ci_0_i_int
    })
    ,.port_ci_tkn_o(ci_tkn_o_int)

    ,.port_co_clk_i(co_clk_i_int)
    ,.port_co_v_i(co_v_i_int)
    ,.port_co_data_i({
      co_8_i_int, co_7_i_int, co_6_i_int, co_5_i_int,
      co_4_i_int, co_3_i_int, co_2_i_int, co_1_i_int,
      co_0_i_int
    })
    ,.port_co_tkn_o(co_tkn_o_int)

    ,.port_ci2_clk_o(ci2_clk_o_int)
    ,.port_ci2_v_o(ci2_v_o_int)
    ,.port_ci2_data_o({
      ci2_8_o_int, ci2_7_o_int, ci2_6_o_int, ci2_5_o_int,
      ci2_4_o_int, ci2_3_o_int, ci2_2_o_int, ci2_1_o_int,
      ci2_0_o_int
    })
    ,.port_ci2_tkn_i(ci2_tkn_i_int)

    ,.port_co2_clk_o(co2_clk_o_int)
    ,.port_co2_v_o(co2_v_o_int)
    ,.port_co2_data_o({
      co2_8_o_int, co2_7_o_int, co2_6_o_int, co2_5_o_int,
      co2_4_o_int, co2_3_o_int, co2_2_o_int, co2_1_o_int,
      co2_0_o_int
    })
    ,.port_co2_tkn_i(co2_tkn_i_int)
  );  


  /*
  assign io_clk_tline_li[0] = ci_clk_i_int;
  assign io_valid_tline_li[0] = ci_v_i_int;
  assign ci_tkn_o_int = io_token_clk_tline_lo[0];
  assign io_data_tline_li[0] = {
    ci_7_i_int, ci_6_i_int, ci_5_i_int, ci_4_i_int,
    ci_3_i_int, ci_2_i_int, ci_1_i_int, ci_0_i_int
  };

  assign io_clk_tline_li[1] = ci2_clk_i_int;
  assign io_valid_tline_li[1] = ci2_v_i_int;
  assign ci2_tkn_o_int = io_token_clk_tline_lo[1];
  assign io_data_tline_li[1] = {
    ci2_7_i_int, ci2_6_i_int, ci2_5_i_int, ci2_4_i_int,
    ci2_3_i_int, ci2_2_i_int, ci2_1_i_int, ci2_0_i_int
  };

  assign co_clk_o_int = im_clk_tline_lo[0];
  assign co_v_o_int = im_valid_tline_lo[0];
  assign im_token_clk_tline_li[0] = co_tkn_i_int;
  assign {
    co_7_o_int, co_6_o_int, co_5_o_int, co_4_o_int,
    co_3_o_int, co_2_o_int, co_1_o_int, co_0_o_int
  } = im_data_tline_lo[0];

  assign co2_clk_o_int = im_clk_tline_lo[1];
  assign co2_v_o_int = im_valid_tline_lo[1];
  assign im_token_clk_tline_li[1] = co2_tkn_i_int;
  assign { 
    co2_7_o_int, co2_6_o_int, co2_5_o_int, co2_4_o_int,
    co2_3_o_int, co2_2_o_int, co2_1_o_int, co2_0_o_int
  } = im_data_tline_lo[1];
*/

  // DDR pins
  //
  wire ddr_ck_p_lo;
  wire ddr_ck_n_lo;
  wire ddr_cke_lo;
  wire ddr_cs_n_lo;
  wire ddr_ras_n_lo;
  wire ddr_cas_n_lo;
  wire ddr_we_n_lo;
  wire ddr_reset_n_lo;
  wire ddr_odt_lo;
  wire [2:0] ddr_ba_lo;
  wire [15:0] ddr_addr_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dm_lo;  // [3:0]

  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_oen_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_pre_mux;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_ien_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_li;

  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_oen_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_ien_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_li;

  wire [(dram_dfi_width_gp>>1)-1:0] ddr_dq_li;
  wire [(dram_dfi_width_gp>>1)-1:0] ddr_dq_lo;
  wire [(dram_dfi_width_gp>>1)-1:0] ddr_dq_oen_lo;

  assign ddr_ck_p_o_int = ddr_ck_p_lo;
  assign ddr_ck_n_o_int = ddr_ck_n_lo;
  assign ddr_cke_o_int = ddr_cke_lo;
  assign ddr_cs_n_o_int = ddr_cs_n_lo;
  assign ddr_ras_n_o_int = ddr_ras_n_lo;
  assign ddr_cas_n_o_int = ddr_cas_n_lo;
  assign ddr_we_n_o_int = ddr_we_n_lo;
  assign ddr_reset_n_o_int = ddr_reset_n_lo;
  assign ddr_odt_o_int = ddr_odt_lo;

  assign ddr_ba_0_o_int = ddr_ba_lo[0];
  assign ddr_ba_1_o_int = ddr_ba_lo[1];
  assign ddr_ba_2_o_int = ddr_ba_lo[2];

  assign ddr_addr_0_o_int  = ddr_addr_lo[0];
  assign ddr_addr_1_o_int  = ddr_addr_lo[1];
  assign ddr_addr_2_o_int  = ddr_addr_lo[2];
  assign ddr_addr_3_o_int  = ddr_addr_lo[3];
  assign ddr_addr_4_o_int  = ddr_addr_lo[4];
  assign ddr_addr_5_o_int  = ddr_addr_lo[5];
  assign ddr_addr_6_o_int  = ddr_addr_lo[6];
  assign ddr_addr_7_o_int  = ddr_addr_lo[7];
  assign ddr_addr_8_o_int  = ddr_addr_lo[8];
  assign ddr_addr_9_o_int  = ddr_addr_lo[9];
  assign ddr_addr_10_o_int = ddr_addr_lo[10];
  assign ddr_addr_11_o_int = ddr_addr_lo[11];
  assign ddr_addr_12_o_int = ddr_addr_lo[12];
  assign ddr_addr_13_o_int = ddr_addr_lo[13];
  assign ddr_addr_14_o_int = ddr_addr_lo[14];
  assign ddr_addr_15_o_int = ddr_addr_lo[15];

  assign ddr_dm_0_o_int    = ddr_dm_lo[0];
  assign ddr_dm_1_o_int    = ddr_dm_lo[1];
  assign ddr_dm_2_o_int    = ddr_dm_lo[2];
  assign ddr_dm_3_o_int    = ddr_dm_lo[3];

  assign ddr_dqs_p_0_o_int = ddr_dqs_p_lo[0];   assign ddr_dqs_p_0_oen_int = ddr_dqs_p_oen_lo[0];
  assign ddr_dqs_p_1_o_int = ddr_dqs_p_lo[1];   assign ddr_dqs_p_1_oen_int = ddr_dqs_p_oen_lo[1];
  assign ddr_dqs_p_2_o_int = ddr_dqs_p_lo[2];   assign ddr_dqs_p_2_oen_int = ddr_dqs_p_oen_lo[2];
  assign ddr_dqs_p_3_o_int = ddr_dqs_p_lo[3];   assign ddr_dqs_p_3_oen_int = ddr_dqs_p_oen_lo[3];
  assign ddr_dqs_n_0_o_int = ddr_dqs_n_lo[0];   assign ddr_dqs_n_0_oen_int = ddr_dqs_n_oen_lo[0];
  assign ddr_dqs_n_1_o_int = ddr_dqs_n_lo[1];   assign ddr_dqs_n_1_oen_int = ddr_dqs_n_oen_lo[1];
  assign ddr_dqs_n_2_o_int = ddr_dqs_n_lo[2];   assign ddr_dqs_n_2_oen_int = ddr_dqs_n_oen_lo[2];
  assign ddr_dqs_n_3_o_int = ddr_dqs_n_lo[3];   assign ddr_dqs_n_3_oen_int = ddr_dqs_n_oen_lo[3];

  assign ddr_dq_li[0]  = ddr_dq_0_i_int;  assign ddr_dq_0_o_int  = ddr_dq_lo[0];  assign ddr_dq_0_oen_int  = ddr_dq_oen_lo[0];
  assign ddr_dq_li[1]  = ddr_dq_1_i_int;  assign ddr_dq_1_o_int  = ddr_dq_lo[1];  assign ddr_dq_1_oen_int  = ddr_dq_oen_lo[1];
  assign ddr_dq_li[2]  = ddr_dq_2_i_int;  assign ddr_dq_2_o_int  = ddr_dq_lo[2];  assign ddr_dq_2_oen_int  = ddr_dq_oen_lo[2];
  assign ddr_dq_li[3]  = ddr_dq_3_i_int;  assign ddr_dq_3_o_int  = ddr_dq_lo[3];  assign ddr_dq_3_oen_int  = ddr_dq_oen_lo[3];
  assign ddr_dq_li[4]  = ddr_dq_4_i_int;  assign ddr_dq_4_o_int  = ddr_dq_lo[4];  assign ddr_dq_4_oen_int  = ddr_dq_oen_lo[4];
  assign ddr_dq_li[5]  = ddr_dq_5_i_int;  assign ddr_dq_5_o_int  = ddr_dq_lo[5];  assign ddr_dq_5_oen_int  = ddr_dq_oen_lo[5];
  assign ddr_dq_li[6]  = ddr_dq_6_i_int;  assign ddr_dq_6_o_int  = ddr_dq_lo[6];  assign ddr_dq_6_oen_int  = ddr_dq_oen_lo[6];
  assign ddr_dq_li[7]  = ddr_dq_7_i_int;  assign ddr_dq_7_o_int  = ddr_dq_lo[7];  assign ddr_dq_7_oen_int  = ddr_dq_oen_lo[7];
  assign ddr_dq_li[8]  = ddr_dq_8_i_int;  assign ddr_dq_8_o_int  = ddr_dq_lo[8];  assign ddr_dq_8_oen_int  = ddr_dq_oen_lo[8];
  assign ddr_dq_li[9]  = ddr_dq_9_i_int;  assign ddr_dq_9_o_int  = ddr_dq_lo[9];  assign ddr_dq_9_oen_int  = ddr_dq_oen_lo[9];
  assign ddr_dq_li[10] = ddr_dq_10_i_int; assign ddr_dq_10_o_int = ddr_dq_lo[10]; assign ddr_dq_10_oen_int = ddr_dq_oen_lo[10];
  assign ddr_dq_li[11] = ddr_dq_11_i_int; assign ddr_dq_11_o_int = ddr_dq_lo[11]; assign ddr_dq_11_oen_int = ddr_dq_oen_lo[11];
  assign ddr_dq_li[12] = ddr_dq_12_i_int; assign ddr_dq_12_o_int = ddr_dq_lo[12]; assign ddr_dq_12_oen_int = ddr_dq_oen_lo[12];
  assign ddr_dq_li[13] = ddr_dq_13_i_int; assign ddr_dq_13_o_int = ddr_dq_lo[13]; assign ddr_dq_13_oen_int = ddr_dq_oen_lo[13];
  assign ddr_dq_li[14] = ddr_dq_14_i_int; assign ddr_dq_14_o_int = ddr_dq_lo[14]; assign ddr_dq_14_oen_int = ddr_dq_oen_lo[14];
  assign ddr_dq_li[15] = ddr_dq_15_i_int; assign ddr_dq_15_o_int = ddr_dq_lo[15]; assign ddr_dq_15_oen_int = ddr_dq_oen_lo[15];
  assign ddr_dq_li[16] = ddr_dq_16_i_int; assign ddr_dq_16_o_int = ddr_dq_lo[16]; assign ddr_dq_16_oen_int = ddr_dq_oen_lo[16];
  assign ddr_dq_li[17] = ddr_dq_17_i_int; assign ddr_dq_17_o_int = ddr_dq_lo[17]; assign ddr_dq_17_oen_int = ddr_dq_oen_lo[17];
  assign ddr_dq_li[18] = ddr_dq_18_i_int; assign ddr_dq_18_o_int = ddr_dq_lo[18]; assign ddr_dq_18_oen_int = ddr_dq_oen_lo[18];
  assign ddr_dq_li[19] = ddr_dq_19_i_int; assign ddr_dq_19_o_int = ddr_dq_lo[19]; assign ddr_dq_19_oen_int = ddr_dq_oen_lo[19];
  assign ddr_dq_li[20] = ddr_dq_20_i_int; assign ddr_dq_20_o_int = ddr_dq_lo[20]; assign ddr_dq_20_oen_int = ddr_dq_oen_lo[20];
  assign ddr_dq_li[21] = ddr_dq_21_i_int; assign ddr_dq_21_o_int = ddr_dq_lo[21]; assign ddr_dq_21_oen_int = ddr_dq_oen_lo[21];
  assign ddr_dq_li[22] = ddr_dq_22_i_int; assign ddr_dq_22_o_int = ddr_dq_lo[22]; assign ddr_dq_22_oen_int = ddr_dq_oen_lo[22];
  assign ddr_dq_li[23] = ddr_dq_23_i_int; assign ddr_dq_23_o_int = ddr_dq_lo[23]; assign ddr_dq_23_oen_int = ddr_dq_oen_lo[23];
  assign ddr_dq_li[24] = ddr_dq_24_i_int; assign ddr_dq_24_o_int = ddr_dq_lo[24]; assign ddr_dq_24_oen_int = ddr_dq_oen_lo[24];
  assign ddr_dq_li[25] = ddr_dq_25_i_int; assign ddr_dq_25_o_int = ddr_dq_lo[25]; assign ddr_dq_25_oen_int = ddr_dq_oen_lo[25];
  assign ddr_dq_li[26] = ddr_dq_26_i_int; assign ddr_dq_26_o_int = ddr_dq_lo[26]; assign ddr_dq_26_oen_int = ddr_dq_oen_lo[26];
  assign ddr_dq_li[27] = ddr_dq_27_i_int; assign ddr_dq_27_o_int = ddr_dq_lo[27]; assign ddr_dq_27_oen_int = ddr_dq_oen_lo[27];
  assign ddr_dq_li[28] = ddr_dq_28_i_int; assign ddr_dq_28_o_int = ddr_dq_lo[28]; assign ddr_dq_28_oen_int = ddr_dq_oen_lo[28];
  assign ddr_dq_li[29] = ddr_dq_29_i_int; assign ddr_dq_29_o_int = ddr_dq_lo[29]; assign ddr_dq_29_oen_int = ddr_dq_oen_lo[29];
  assign ddr_dq_li[30] = ddr_dq_30_i_int; assign ddr_dq_30_o_int = ddr_dq_lo[30]; assign ddr_dq_30_oen_int = ddr_dq_oen_lo[30];
  assign ddr_dq_li[31] = ddr_dq_31_i_int; assign ddr_dq_31_o_int = ddr_dq_lo[31]; assign ddr_dq_31_oen_int = ddr_dq_oen_lo[31];

  assign ddr_dqs_p_pre_mux[0] = ddr_dqs_p_0_i_int;
  assign ddr_dqs_p_pre_mux[1] = ddr_dqs_p_1_i_int;
  assign ddr_dqs_p_pre_mux[2] = ddr_dqs_p_2_i_int;
  assign ddr_dqs_p_pre_mux[3] = ddr_dqs_p_3_i_int;

  for (genvar k = 0; k < (dram_dfi_width_gp>>4); k++) begin
    bsg_mux #(
      .width_p(1)
      ,.els_p(2)
      ,.balanced_p(1)
      ,.harden_p(1)
    ) dfi_clk_mux (
      .data_i({1'b0, ddr_dqs_p_pre_mux[k]})
      ,.sel_i(ddr_dqs_p_ien_lo[k])
      ,.data_o (ddr_dqs_p_mux[k])
    );
  end

  assign ddr_dqs_p_li = ddr_dqs_p_dly;

  // bsg_chip_guts
  //
  bsg_chip_guts #(
    .num_channels_p(num_channels_lp)
  ) guts (
    // clk and reset
    .core_clk_i(core_clk_lo)
    ,.io_master_clk_i(iom_clk_lo)
    ,.fsb_clk_i(fsb_clk_lo)
    ,.async_reset_i(core_async_reset_i_int)
    ,.dmc_rst_i(clk_async_reset_i_int)

    // comm link interface
    ,.io_clk_tline_i(io_clk_tline_li)
    ,.io_valid_tline_i(io_valid_tline_li)
    ,.io_data_tline_i({io_data_tline_li[1][7:0], io_data_tline_li[0][7:0]})
    ,.io_token_clk_tline_o(io_token_clk_tline_lo)

    ,.im_clk_tline_o(im_clk_tline_lo)
    ,.im_valid_tline_o(im_valid_tline_lo)
    ,.im_data_tline_o({im_data_tline_lo[1][7:0], im_data_tline_lo[0][7:0]})
    ,.im_token_clk_tline_i(im_token_clk_tline_li)
    
    ,.im_slave_reset_tline_r_o()

    // DDR command and address interface
    ,.ddr_ck_p_o(ddr_ck_p_lo)
    ,.ddr_ck_n_o(ddr_ck_n_lo)
    ,.ddr_cke_o(ddr_cke_lo)
    ,.ddr_ba_o(ddr_ba_lo)
    ,.ddr_addr_o(ddr_addr_lo)
    ,.ddr_cs_n_o(ddr_cs_n_lo)
    ,.ddr_ras_n_o(ddr_ras_n_lo)
    ,.ddr_cas_n_o(ddr_cas_n_lo)
    ,.ddr_we_n_o(ddr_we_n_lo)
    ,.ddr_reset_n_o(ddr_reset_n_lo)
    ,.ddr_odt_o(ddr_odt_lo)

    // DDR data interface
    ,.ddr_dm_oen_o()
    ,.ddr_dm_o(ddr_dm_lo)

    ,.ddr_dqs_p_oen_o(ddr_dqs_p_oen_lo)
    ,.ddr_dqs_p_ien_o(ddr_dqs_p_ien_lo)
    ,.ddr_dqs_p_o(ddr_dqs_p_lo)
    ,.ddr_dqs_p_i(ddr_dqs_p_li)

    ,.ddr_dqs_n_oen_o(ddr_dqs_n_oen_lo)
    ,.ddr_dqs_n_ien_o(ddr_dqs_n_ien_lo)
    ,.ddr_dqs_n_o(ddr_dqs_n_lo)
    ,.ddr_dqs_n_i(ddr_dqs_n_li)

    ,.ddr_dq_oen_o(ddr_dq_oen_lo)
    ,.ddr_dq_o(ddr_dq_lo)
    ,.ddr_dq_i(ddr_dq_li)

    // dram_clk
    ,.dfi_clk_2x_i(dfi_clk_2x_lo)
    ,.dfi_clk_i(dfi_clk_1x_lo)
  );

`include "bsg_pinout_end.v"
