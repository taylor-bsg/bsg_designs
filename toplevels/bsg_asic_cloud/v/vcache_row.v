/**
 *  vcache_row.v
 */

`include "bsg_manycore_packet.vh"
`include "bsg_cache_dma_pkt.vh"

module vcache_row
  import bsg_dram_ctrl_pkg::*;
  #(parameter num_cache_p="inv"
    , parameter sets_p="inv"
    , parameter ways_p="inv"
    , parameter block_size_in_words_p="inv"

    , parameter addr_width_p="inv"
    , parameter data_width_p="inv"
    , parameter x_cord_width_p="inv"
    , parameter y_cord_width_p="inv"
    , parameter load_id_width_p="inv"

    , parameter dram_ctrl_data_width_p="inv"
    , parameter dram_ctrl_addr_width_p="inv"

    , parameter byte_offset_width_lp=`BSG_SAFE_CLOG2(data_width_p>>3)
    
    , parameter link_sif_width_lp=
      `bsg_manycore_link_sif_width(addr_width_p,data_width_p,x_cord_width_p,y_cord_width_p,load_id_width_p)
  )
  (
    input clk_i
    , input reset_i

    // manycore side
    , input [num_cache_p-1:0][link_sif_width_lp-1:0] s_link_sif_i
    , output logic [num_cache_p-1:0][link_sif_width_lp-1:0] s_link_sif_o

    , input [num_cache_p-1:0][x_cord_width_p-1:0] cache_x_i 
    , input [num_cache_p-1:0][y_cord_width_p-1:0] cache_y_i

    // dram_ctrl side
    , output logic app_en_o
    , input app_rdy_i
    , output eAppCmd app_cmd_o
    , output logic [dram_ctrl_addr_width_p-1:0] app_addr_o

    , output logic app_wdf_wren_o
    , input app_wdf_rdy_i
    , output logic [dram_ctrl_data_width_p-1:0] app_wdf_data_o
    , output logic [(dram_ctrl_data_width_p>>3)-1:0] app_wdf_mask_o
    , output logic app_wdf_end_o

    , input app_rd_data_valid_i
    , input [dram_ctrl_data_width_p-1:0] app_rd_data_i
    , input app_rd_data_end_i
  );

  `declare_bsg_cache_dma_pkt_s(addr_width_p+byte_offset_width_lp-1);

  bsg_cache_dma_pkt_s [num_cache_p-1:0] dma_pkt;
  logic [num_cache_p-1:0] dma_pkt_valid;
  logic [num_cache_p-1:0] dma_pkt_yumi;

  logic [num_cache_p-1:0][data_width_p-1:0] dma_data_li;
  logic [num_cache_p-1:0] dma_data_v_li;
  logic [num_cache_p-1:0] dma_data_ready_lo;

  logic [num_cache_p-1:0][data_width_p-1:0] dma_data_lo;
  logic [num_cache_p-1:0] dma_data_v_lo;
  logic [num_cache_p-1:0] dma_data_yumi_li;

  for (genvar i = 0; i < num_cache_p; i++) begin: vcache
    vcache #(
      .addr_width_p(addr_width_p)
      ,.data_width_p(data_width_p)
      ,.x_cord_width_p(x_cord_width_p)
      ,.y_cord_width_p(y_cord_width_p)
      ,.load_id_width_p(load_id_width_p)

      ,.sets_p(sets_p)
      ,.ways_p(ways_p)
      ,.block_size_in_words_p(block_size_in_words_p)
    ) vcache_inst (
      .clk_i(clk_i)
      ,.reset_i(reset_i)

      ,.cache_x_i(cache_x_i[i])
      ,.cache_y_i(cache_y_i[i])

      ,.link_sif_i(s_link_sif_i[i])
      ,.link_sif_o(s_link_sif_o[i])
      
      ,.dma_pkt_o(dma_pkt[i])
      ,.dma_pkt_v_o(dma_pkt_valid[i])
      ,.dma_pkt_yumi_i(dma_pkt_yumi[i])

      ,.dma_data_i(dma_data_li[i])
      ,.dma_data_v_i(dma_data_v_li[i])
      ,.dma_data_ready_o(dma_data_ready_lo[i])

      ,.dma_data_o(dma_data_lo[i])
      ,.dma_data_v_o(dma_data_v_lo[i])
      ,.dma_data_yumi_i(dma_data_yumi_li[i])
    );
  end

  bsg_cache_to_dram_ctrl #(
    .num_cache_p(num_cache_p)
    ,.addr_width_p(addr_width_p+byte_offset_width_lp-1)
    ,.data_width_p(data_width_p)
    ,.block_size_in_words_p(block_size_in_words_p)
  
    ,.dram_ctrl_burst_len_p(1)
    ,.dram_ctrl_data_width_p(dram_ctrl_data_width_p)
  ) cache_to_dram_ctrl (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.dma_pkt_i(dma_pkt)
    ,.dma_pkt_v_i(dma_pkt_valid)
    ,.dma_pkt_yumi_o(dma_pkt_yumi)

    ,.dma_data_o(dma_data_li)
    ,.dma_data_v_o(dma_data_v_li)
    ,.dma_data_ready_i(dma_data_ready_lo)

    ,.dma_data_i(dma_data_lo)
    ,.dma_data_v_i(dma_data_v_lo)
    ,.dma_data_yumi_o(dma_data_yumi_li)

    ,.app_en_o(app_en_o)
    ,.app_rdy_i(app_rdy_i)
    ,.app_cmd_o(app_cmd_o)
    ,.app_addr_o(app_addr_o)

    ,.app_wdf_wren_o(app_wdf_wren_o)
    ,.app_wdf_rdy_i(app_wdf_rdy_i)
    ,.app_wdf_data_o(app_wdf_data_o)
    ,.app_wdf_mask_o(app_wdf_mask_o)
    ,.app_wdf_end_o(app_wdf_end_o)

    ,.app_rd_data_valid_i(app_rd_data_valid_i)
    ,.app_rd_data_i(app_rd_data_i)
    ,.app_rd_data_end_i(app_rd_data_end_i)
  );
  

endmodule;
