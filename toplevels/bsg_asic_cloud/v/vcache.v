/**
 *  vcache.v
 *
 *  this module wraps bsg_manycore_link_to_cache and bsg_cache together.
 */

`include "bsg_cache_pkt.vh"
`include "bsg_cache_dma_pkt.vh"

module vcache
  #(parameter addr_width_p="inv"
    ,parameter data_width_p="inv"
    ,parameter x_cord_width_p="inv"
    ,parameter y_cord_width_p="inv"
    ,parameter load_id_width_p="inv"
    
    ,parameter sets_p="inv"
    ,parameter ways_p="inv"
    ,parameter block_size_in_words_p="inv"

    ,parameter byte_offset_width_lp=`BSG_SAFE_CLOG2(data_width_p>>3)
    ,parameter cache_addr_width_lp=(addr_width_p+byte_offset_width_lp-1)

    ,parameter link_sif_width_lp=
      `bsg_manycore_link_sif_width(addr_width_p,data_width_p,x_cord_width_p,y_cord_width_p,load_id_width_p)
    ,parameter bsg_cache_dma_pkt_width_lp=
      `bsg_cache_dma_pkt_width(cache_addr_width_lp)
  )
  (
    input clk_i
    ,input reset_i

    // manycore side
    ,input [x_cord_width_p-1:0] cache_x_i
    ,input [y_cord_width_p-1:0] cache_y_i

    ,input [link_sif_width_lp-1:0] link_sif_i
    ,output logic [link_sif_width_lp-1:0] link_sif_o

    // cache dma side
    ,output logic [bsg_cache_dma_pkt_width_lp-1:0] dma_pkt_o
    ,output logic dma_pkt_v_o
    ,input dma_pkt_yumi_i

    ,input [data_width_p-1:0] dma_data_i
    ,input dma_data_v_i
    ,output logic dma_data_ready_o

    ,output logic [data_width_p-1:0] dma_data_o
    ,output logic dma_data_v_o
    ,input dma_data_yumi_i
  );

  `declare_bsg_cache_pkt_s(cache_addr_width_lp, data_width_p);
  bsg_cache_pkt_s cache_pkt;
  logic link_to_cache_valid;
  logic link_to_cache_ready;

  logic [data_width_p-1:0] cache_to_link_data;
  logic cache_to_link_valid;
  logic cache_to_link_yumi;

  bsg_manycore_link_to_cache #(
    .link_addr_width_p(addr_width_p)
    ,.data_width_p(data_width_p)
    ,.x_cord_width_p(x_cord_width_p)
    ,.y_cord_width_p(y_cord_width_p)
    ,.load_id_width_p(load_id_width_p)
    ,.sets_p(sets_p)
    ,.ways_p(ways_p)
    ,.block_size_in_words_p(block_size_in_words_p)
  ) link_to_cache (
    .clk_i(clk_i)
    ,.reset_i(reset_i)
     
    ,.my_x_i(cache_x_i)
    ,.my_y_i(cache_y_i)
     
    ,.link_sif_i(link_sif_i)
    ,.link_sif_o(link_sif_o)

    ,.cache_pkt_o(cache_pkt)
    ,.v_o(link_to_cache_valid)
    ,.ready_i(link_to_cache_ready)

    ,.data_i(cache_to_link_data)
    ,.v_i(cache_to_link_valid)
    ,.yumi_o(cache_to_link_yumi)
  );

  bsg_cache #(
    .data_width_p(data_width_p)
    ,.addr_width_p(cache_addr_width_lp)
    ,.block_size_in_words_p(block_size_in_words_p)
    ,.sets_p(sets_p)
  ) cache (
    .clk_i(clk_i)
    ,.reset_i(reset_i)
    
    ,.cache_pkt_i(cache_pkt)
    ,.v_i(link_to_cache_valid)
    ,.ready_o(link_to_cache_ready)
    
    ,.data_o(cache_to_link_data)
    ,.v_o(cache_to_link_valid)
    ,.yumi_i(cache_to_link_yumi)

    ,.v_we_o()

    ,.dma_pkt_o(dma_pkt_o)
    ,.dma_pkt_v_o(dma_pkt_v_o)
    ,.dma_pkt_yumi_i(dma_pkt_yumi_i)

    ,.dma_data_i(dma_data_i)
    ,.dma_data_v_i(dma_data_v_i)
    ,.dma_data_ready_o(dma_data_ready_o)

    ,.dma_data_o(dma_data_o)
    ,.dma_data_v_o(dma_data_v_o)
    ,.dma_data_yumi_i(dma_data_yumi_i)
  );


endmodule
