/**
 *  black_parrot.v
 */

module black_parrot
  import bp_common_pkg::*;
  import bp_cce_pkg::*;
  import bp_be_pkg::*;
  import bp_be_rv64_pkg::*;
  #(parameter core_els_p=1
    , parameter num_lce_p=2
    , parameter num_cce_p=1
    , parameter lce_assoc_p=8
    , parameter lce_sets_p=64
    , parameter block_size_in_bits_p=512
    , parameter paddr_width_p=22
    , parameter vaddr_width_p=56
    , parameter asid_width_p=10
    , parameter branch_metadata_fwd_width_p=36
    , parameter btb_indx_width_p=9
    , parameter bht_indx_width_p=5
    , parameter ras_addr_width_p=22
    , parameter cce_num_inst_ram_els_p=256

    , localparam cce_id_width_lp=`BSG_SAFE_CLOG2(num_cce_p)
    , localparam lce_id_width_lp=`BSG_SAFE_CLOG2(num_lce_p)

    , localparam cce_inst_ram_addr_width_lp=`BSG_SAFE_CLOG2(cce_num_inst_ram_els_p)

    , localparam block_size_in_bytes_lp=(block_size_in_bits_p>>3)

    , localparam mem_cce_resp_width_lp=
      `bp_mem_cce_resp_width(paddr_width_p,num_lce_p,lce_assoc_p)
    , localparam mem_cce_data_resp_width_lp=
      `bp_mem_cce_data_resp_width(paddr_width_p,block_size_in_bits_p,num_lce_p,lce_assoc_p)
    , localparam cce_mem_cmd_width_lp=
      `bp_cce_mem_cmd_width(paddr_width_p,num_lce_p,lce_assoc_p)
    , localparam cce_mem_data_cmd_width_lp=
      `bp_cce_mem_data_cmd_width(paddr_width_p,block_size_in_bits_p,num_lce_p,lce_assoc_p)

    , localparam pipe_stage_reg_width_lp=`bp_be_pipe_stage_reg_width(branch_metadata_fwd_width_p)
    , localparam calc_result_width_lp=`bp_be_calc_result_width(branch_metadata_fwd_width_p)
    , localparam exception_width_lp=`bp_be_exception_width
  )
  (
    input clk_i
    , input reset_i

    , output [num_cce_p-1:0][cce_mem_cmd_width_lp-1:0] mem_cmd_o
    , output [num_cce_p-1:0] mem_cmd_v_o
    , input [num_cce_p-1:0] mem_cmd_yumi_i

    , output [num_cce_p-1:0][cce_mem_data_cmd_width_lp-1:0] mem_data_cmd_o
    , output [num_cce_p-1:0] mem_data_cmd_v_o
    , input [num_cce_p-1:0] mem_data_cmd_yumi_i

    , input [num_cce_p-1:0][mem_cce_resp_width_lp-1:0] mem_resp_i
    , input [num_cce_p-1:0] mem_resp_v_i
    , output [num_cce_p-1:0] mem_resp_ready_o

    , input [num_cce_p-1:0][mem_cce_data_resp_width_lp-1:0] mem_data_resp_i
    , input [num_cce_p-1:0] mem_data_resp_v_i
    , output [num_cce_p-1:0] mem_data_resp_ready_o
  );

  logic [num_cce_p-1:0][cce_inst_ram_addr_width_lp-1:0] cce_inst_addr;
  logic [num_cce_p-1:0][`bp_cce_inst_width-1:0] cce_inst_data;

  logic [pipe_stage_reg_width_lp-1:0] cmt_trace_stage_reg;
  logic [calc_result_width_lp-1:0] cmt_trace_result;
  logic [exception_width_lp-1:0] cmt_trace_exc;

  bp_multi_top #(
    .core_els_p(core_els_p)
    ,.vaddr_width_p(vaddr_width_p)
    ,.paddr_width_p(paddr_width_p)
    ,.asid_width_p(asid_width_p)
    ,.branch_metadata_fwd_width_p(branch_metadata_fwd_width_p)
    ,.btb_indx_width_p(btb_indx_width_p)
    ,.bht_indx_width_p(bht_indx_width_p)
    ,.ras_addr_width_p(ras_addr_width_p)
    
    ,.num_cce_p(num_cce_p)
    ,.num_lce_p(num_lce_p)
    ,.lce_assoc_p(lce_assoc_p)
    ,.lce_sets_p(lce_sets_p)
    ,.cce_block_size_in_bytes_p(block_size_in_bytes_lp)
    ,.cce_num_inst_ram_els_p(cce_num_inst_ram_els_p)
  ) multi_top (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.cce_inst_boot_rom_addr_o(cce_inst_addr)
    ,.cce_inst_boot_rom_data_i(cce_inst_data)
    
    ,.mem_cmd_o(mem_cmd_o)
    ,.mem_cmd_v_o(mem_cmd_v_o)
    ,.mem_cmd_yumi_i(mem_cmd_yumi_i)

    ,.mem_data_cmd_o(mem_data_cmd_o)
    ,.mem_data_cmd_v_o(mem_data_cmd_v_o)
    ,.mem_data_cmd_yumi_i(mem_data_cmd_yumi_i) 

    ,.mem_resp_i(mem_resp_i)
    ,.mem_resp_v_i(mem_resp_v_i)
    ,.mem_resp_ready_o(mem_resp_ready_o)

    ,.mem_data_resp_i(mem_data_resp_i)
    ,.mem_data_resp_v_i(mem_data_resp_v_i)
    ,.mem_data_resp_ready_o(mem_data_resp_ready_o)

    ,.cmt_trace_stage_reg_o(cmt_trace_stage_reg)
    ,.cmt_trace_result_o(cmt_trace_result)
    ,.cmt_trace_exc_o(cmt_trace_exc)
  );

  // synopsys translate_off
  bp_be_nonsynth_tracer #(
    .vaddr_width_p(vaddr_width_p)
    ,.paddr_width_p(paddr_width_p)
    ,.asid_width_p(asid_width_p)
    ,.branch_metadata_fwd_width_p(branch_metadata_fwd_width_p)
    ,.core_els_p(core_els_p)
    ,.num_lce_p(num_lce_p)
  ) tracer (
    .clk_i(clk_i)
    ,.reset_i(reset_i)
  
    ,.proc_cfg_i('0)

    ,.cmt_trace_stage_reg_i(cmt_trace_stage_reg)
    ,.cmt_trace_result_i(cmt_trace_result)
    ,.cmt_trace_exc_i(cmt_trace_exc)
  );
  // synopsys translate_on

  for (genvar i = 0; i < num_cce_p; i++) begin: cce_inst_rom
    bp_cce_inst_rom #(
      .width_p(`bp_cce_inst_width)
      ,.addr_width_p(cce_inst_ram_addr_width_lp)
    ) cce_inst_rom (
      .addr_i(cce_inst_addr[i])
      ,.data_o(cce_inst_data[i])
    );
  end


endmodule
