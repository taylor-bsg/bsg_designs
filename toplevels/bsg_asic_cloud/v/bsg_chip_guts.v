/**
 *  bsg_chip_guts.v
 */

module bsg_chip_guts
  import bsg_chip_pkg::*;
  import bsg_dram_ctrl_pkg::*;
  #(parameter num_channels_p="inv"
    , parameter channel_width_p=8
    , parameter nodes_p=2
    , parameter [nodes_p-1:0] enabled_at_start_vec_p={2'b11}
    , parameter master_p=0
    , parameter master_to_client_speedup_p=100
    , parameter master_bypass_test_p=5'b00000
    , localparam ring_bytes_lp = 10
    , localparam ring_width_lp = ring_bytes_lp*channel_width_p
  )
  (
    // clk and reset
    input core_clk_i
    , input io_master_clk_i
    , input fsb_clk_i 
    , input dmc_rst_i
    , input async_reset_i

    // comm_link input
    , input [num_channels_p-1:0] io_clk_tline_i
    , input [num_channels_p-1:0] io_valid_tline_i
    , input [channel_width_p-1:0] io_data_tline_i [num_channels_p-1:0]
    , output logic [num_channels_p-1:0] io_token_clk_tline_o

    // comm_link output
    , output logic [num_channels_p-1:0] im_clk_tline_o
    , output logic [num_channels_p-1:0] im_valid_tline_o
    , output logic [channel_width_p-1:0] im_data_tline_o [num_channels_p-1:0]
    , input [num_channels_p-1:0] im_token_clk_tline_i

    , output logic im_slave_reset_tline_r_o

    // DDR
    , input dfi_clk_i
    , input dfi_clk_2x_i

    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dm_oen_o
    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dm_o

    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_oen_o
    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_ien_o
    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_o
    , input [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_p_i

    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_oen_o
    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_ien_o
    , output logic [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_o
    , input [(dram_dfi_width_gp>>4)-1:0] ddr_dqs_n_i

    , output logic [(dram_dfi_width_gp>>1)-1:0] ddr_dq_oen_o
    , output logic [(dram_dfi_width_gp>>1)-1:0] ddr_dq_o
    , input [(dram_dfi_width_gp>>1)-1:0] ddr_dq_i

    , output logic ddr_ck_p_o
    , output logic ddr_ck_n_o
    , output logic ddr_cke_o
    , output logic [2:0] ddr_ba_o
    , output logic [15:0] ddr_addr_o
    , output logic ddr_cs_n_o
    , output logic ddr_ras_n_o
    , output logic ddr_cas_n_o
    , output logic ddr_we_n_o
    , output logic ddr_reset_n_o
    , output logic ddr_odt_o
  );


  
  // comm_link
  //
  wire core_calib_done_r_lo; // fsb reset

  wire [ring_width_lp-1:0] fsb_cl_data;
  wire fsb_cl_valid;
  wire fsb_cl_ready;

  wire [ring_width_lp-1:0] cl_fsb_data;
  wire cl_fsb_valid;
  wire cl_fsb_yumi;

  bsg_comm_link #(
    .channel_width_p(channel_width_p)
    ,.core_channels_p(ring_bytes_lp)
    ,.link_channels_p(num_channels_p)
    ,.master_p(master_p)
    ,.master_to_slave_speedup_p(master_to_client_speedup_p)
    ,.master_bypass_test_p(master_bypass_test_p)
  ) comm_link (
    .core_clk_i(fsb_clk_i)
    ,.io_master_clk_i(io_master_clk_i)
    ,.async_reset_i(async_reset_i)

    ,.core_calib_done_r_o(core_calib_done_r_lo)

    ,.core_valid_i(fsb_cl_valid)
    ,.core_data_i(fsb_cl_data)
    ,.core_ready_o(fsb_cl_ready)

    ,.core_valid_o(cl_fsb_valid)
    ,.core_data_o(cl_fsb_data)
    ,.core_yumi_i(cl_fsb_yumi)

    ,.io_valid_tline_i(io_valid_tline_i)
    ,.io_data_tline_i(io_data_tline_i)
    ,.io_clk_tline_i(io_clk_tline_i)
    ,.io_token_clk_tline_o(io_token_clk_tline_o)

    ,.im_valid_tline_o(im_valid_tline_o)
    ,.im_data_tline_o(im_data_tline_o)
    ,.im_clk_tline_o(im_clk_tline_o)
    ,.token_clk_tline_i(im_token_clk_tline_i)

    ,.im_slave_reset_tline_r_o(im_slave_reset_tline_r_o)
  );

  // FSB
  //
  wire [nodes_p-1:0] fsb_node_en_r_lo;
  wire [nodes_p-1:0] fsb_node_reset_r_lo;

  wire [nodes_p-1:0] fsb_node_valid;
  wire [nodes_p-1:0] fsb_node_ready;
  wire [ring_width_lp-1:0] fsb_node_data [nodes_p-1:0];

  wire [nodes_p-1:0] node_fsb_valid;
  wire [nodes_p-1:0] node_fsb_yumi;
  wire [ring_width_lp-1:0] node_fsb_data [nodes_p-1:0];

  wire fsb_reset_lo ;

  bsg_sync_sync #(
    .width_p(1)
  ) fsb_rst_sync (
    .oclk_i(fsb_clk_i)
    ,.iclk_data_i(~core_calib_done_r_lo)
    ,.oclk_data_o(fsb_reset_lo)
  );

  bsg_fsb #(
    .width_p(ring_width_lp)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p({nodes_p{1'b0}})
    ,.enabled_at_start_vec_p(enabled_at_start_vec_p)
  ) fsb (
    .clk_i(fsb_clk_i)
    ,.reset_i(fsb_reset_lo)

    ,.node_reset_r_o(fsb_node_reset_r_lo)
    ,.node_en_r_o(fsb_node_en_r_lo)

    ,.node_v_i(node_fsb_valid)
    ,.node_data_i(node_fsb_data)
    ,.node_yumi_o(node_fsb_yumi)

    ,.node_v_o(fsb_node_valid)
    ,.node_data_o(fsb_node_data)
    ,.node_ready_i(fsb_node_ready)

    ,.asm_v_i(cl_fsb_valid)
    ,.asm_data_i(cl_fsb_data)
    ,.asm_yumi_o(cl_fsb_yumi)

    ,.asm_v_o(fsb_cl_valid)
    ,.asm_data_o(fsb_cl_data)
    ,.asm_ready_i(fsb_cl_ready)
  );

  // fsb_mesh_node
  //
  wire mesh_node_reset_r_lo;
  wire mesh_node_en_r_lo;

  wire buffer_mesh_node_valid;
  wire buffer_mesh_node_ready;
  wire [ring_width_lp-1:0] buffer_mesh_node_data;

  wire mesh_node_buffer_valid;
  wire mesh_node_buffer_yumi;
  wire [ring_width_lp-1:0] mesh_node_buffer_data;

  bsg_sync_sync #(
    .width_p(1)
  ) core_rst_sync (
    .oclk_i(core_clk_i)
    ,.iclk_data_i(fsb_node_reset_r_lo[fsb_mesh_node_idx])
    ,.oclk_data_o(mesh_node_reset_r_lo)
  );

  bsg_fsb_node_async_buffer #( 
    .ring_width_p(ring_width_lp)
    ,.fifo_els_p(4)
  ) core_async_fifo (  
    .L_clk_i(core_clk_i)
    ,.L_reset_i(mesh_node_reset_r_lo) 
    ,.L_en_o(mesh_node_en_r_lo)

    ,.L_v_o(buffer_mesh_node_valid)
    ,.L_data_o(buffer_mesh_node_data)
    ,.L_ready_i(buffer_mesh_node_ready)

    ,.L_v_i(mesh_node_buffer_valid)
    ,.L_data_i(mesh_node_buffer_data)
    ,.L_yumi_o(mesh_node_buffer_yumi)

    ,.R_clk_i(fsb_clk_i)
    ,.R_reset_i(fsb_node_reset_r_lo[fsb_mesh_node_idx])
    ,.R_en_i(fsb_node_en_r_lo[fsb_mesh_node_idx])

    ,.R_v_i(fsb_node_valid[fsb_mesh_node_idx])
    ,.R_data_i(fsb_node_data[fsb_mesh_node_idx])
    ,.R_ready_o(fsb_node_ready[fsb_mesh_node_idx])

    ,.R_v_o(node_fsb_valid[fsb_mesh_node_idx])
    ,.R_data_o(node_fsb_data[fsb_mesh_node_idx])
    ,.R_yumi_i(node_fsb_yumi[fsb_mesh_node_idx]) 
  );

  logic app_en;
  logic app_rdy;
  eAppCmd app_cmd;
  logic [dram_ctrl_awidth_gp-1:0] app_addr;

  logic app_wdf_wren;
  logic app_wdf_rdy;
  logic [dram_ctrl_dwidth_gp-1:0] app_wdf_data;
  logic [(dram_ctrl_dwidth_gp>>3)-1:0] app_wdf_mask;
  logic app_wdf_end;

  logic app_rd_data_valid;
  logic [dram_ctrl_dwidth_gp-1:0] app_rd_data;
  logic app_rd_data_end;

  fsb_mesh_node #(
    .ring_width_p(ring_width_lp)
    ,.id_width_p (`BSG_SAFE_CLOG2(nodes_p))
    ,.master_id_p(fsb_mesh_node_idx)
    ,.client_id_p(fsb_mesh_node_idx)
  ) mesh_node (
    .clk_i(core_clk_i)
    ,.reset_i(mesh_node_reset_r_lo)
    ,.en_i(mesh_node_en_r_lo)

    ,.v_i(buffer_mesh_node_valid)
    ,.data_i(buffer_mesh_node_data)
    ,.ready_o(buffer_mesh_node_ready)

    ,.v_o(mesh_node_buffer_valid)
    ,.data_o(mesh_node_buffer_data)
    ,.yumi_i(mesh_node_buffer_yumi)

    ,.app_en_o(app_en)
    ,.app_rdy_i(app_rdy)
    ,.app_cmd_o(app_cmd)
    ,.app_addr_o(app_addr)

    ,.app_wdf_wren_o(app_wdf_wren)
    ,.app_wdf_rdy_i(app_wdf_rdy)
    ,.app_wdf_data_o(app_wdf_data)
    ,.app_wdf_mask_o(app_wdf_mask)
    ,.app_wdf_end_o(app_wdf_end)

    ,.app_rd_data_valid_i(app_rd_data_valid)
    ,.app_rd_data_i(app_rd_data)
    ,.app_rd_data_end_i(app_rd_data_end)
  );

  // DRAM_CTRL
  //
   bsg_dmc #(
    .UI_ADDR_WIDTH(dram_ctrl_awidth_gp)
    ,.UI_DATA_WIDTH(dram_ctrl_dwidth_gp)
    ,.DFI_DATA_WIDTH(dram_dfi_width_gp)
    ,.DQ_GROUP(4)
  ) dram_ctrl (
    .sys_rst(~async_reset_i) // ACTIVE LOW !!!

    ,.app_addr(app_addr>>1) // half address
    ,.app_cmd(app_cmd)
    ,.app_en(app_en)
    ,.app_rdy(app_rdy)

    ,.app_wdf_wren(app_wdf_wren)
    ,.app_wdf_data(app_wdf_data)
    ,.app_wdf_mask(app_wdf_mask)
    ,.app_wdf_end(app_wdf_end)
    ,.app_wdf_rdy(app_wdf_rdy)

    ,.app_rd_data_valid(app_rd_data_valid)
    ,.app_rd_data(app_rd_data)
    ,.app_rd_data_end(app_rd_data_end)

    ,.app_ref_req(1'b0)
    ,.app_ref_ack()
    ,.app_zq_req(1'b0)
    ,.app_zq_ack()
    ,.app_sr_req(1'b0)
    ,.app_sr_active()

    ,.init_calib_complete()

    ,.ddr_ck_p_o(ddr_ck_p_o)
    ,.ddr_ck_n_o(ddr_ck_n_o)
    ,.ddr_cke_o(ddr_cke_o)
    ,.ddr_ba_o(ddr_ba_o)
    ,.ddr_addr_o(ddr_addr_o)
    ,.ddr_cs_n_o(ddr_cs_n_o)
    ,.ddr_ras_n_o(ddr_ras_n_o)
    ,.ddr_cas_n_o(ddr_cas_n_o)
    ,.ddr_we_n_o(ddr_we_n_o)
    ,.ddr_reset_n_o(ddr_reset_n_o)
    ,.ddr_odt_o(ddr_odt_o)

    ,.ddr_dm_oen_o(ddr_dm_oen_o)
    ,.ddr_dm_o(ddr_dm_o)
    ,.ddr_dqs_p_oen_o(ddr_dqs_p_oen_o)
    ,.ddr_dqs_p_ien_o(ddr_dqs_p_ien_o)
    ,.ddr_dqs_p_o(ddr_dqs_p_o)
    ,.ddr_dqs_p_i(ddr_dqs_p_i)
    ,.ddr_dqs_n_oen_o(ddr_dqs_n_oen_o)
    ,.ddr_dqs_n_ien_o(ddr_dqs_n_ien_o)
    ,.ddr_dqs_n_o(ddr_dqs_n_o)
    ,.ddr_dqs_n_i(ddr_dqs_n_i)
    ,.ddr_dq_oen_o(ddr_dq_oen_o)
    ,.ddr_dq_o(ddr_dq_o)
    ,.ddr_dq_i(ddr_dq_i)

    ,.ui_clk(core_clk_i)
    ,.dfi_clk_2x(dfi_clk_2x_i)
    ,.dfi_clk(dfi_clk_i)

    ,.ui_clk_sync_rst()
    ,.device_temp()
  );
    
  // test node
  //
  wire test_node_en_r_lo;
  wire test_node_reset_r_lo;

  wire buffer_test_node_valid;
  wire buffer_test_node_ready;
  wire [ring_width_lp-1:0] buffer_test_node_data;

  wire test_node_buffer_valid;
  wire test_node_buffer_yumi;
  wire [ring_width_lp-1:0] test_node_buffer_data;

  bsg_sync_sync #(
    .width_p(1)
  ) test_node_rst_sync (
    .oclk_i(core_clk_i)   
    ,.iclk_data_i(fsb_node_reset_r_lo[fsb_test_node_idx])
    ,.oclk_data_o(test_node_reset_r_lo)
  );

  bsg_fsb_node_async_buffer #(
    .ring_width_p(ring_width_lp)
    ,.fifo_els_p(4)
  ) test_node_async_fifo (  
    .L_clk_i(core_clk_i)  
    ,.L_reset_i(test_node_reset_r_lo) 
    ,.L_en_o(test_node_en_r_lo)

    ,.L_v_o(buffer_test_node_valid)
    ,.L_data_o(buffer_test_node_data)
    ,.L_ready_i(buffer_test_node_ready)

    ,.L_v_i(test_node_buffer_valid)
    ,.L_data_i(test_node_buffer_data)
    ,.L_yumi_o(test_node_buffer_yumi)

    ,.R_clk_i(fsb_clk_i)
    ,.R_reset_i(fsb_node_reset_r_lo[fsb_test_node_idx])
    ,.R_en_i(fsb_node_en_r_lo[fsb_test_node_idx])

    ,.R_v_i(fsb_node_valid[fsb_test_node_idx])
    ,.R_data_i(fsb_node_data[fsb_test_node_idx])
    ,.R_ready_o(fsb_node_ready[fsb_test_node_idx])

    ,.R_v_o(node_fsb_valid[fsb_test_node_idx])
    ,.R_data_o(node_fsb_data[fsb_test_node_idx])
    ,.R_yumi_i(node_fsb_yumi[fsb_test_node_idx]) 
  );

  bsg_test_node_client #(
    .ring_width_p(ring_width_lp)
  ) test_node (
    .clk_i(core_clk_i)
    ,.reset_i(test_node_reset_r_lo)
    ,.en_i(test_node_en_r_lo)

    ,.v_i(buffer_test_node_valid)
    ,.data_i(buffer_test_node_data)
    ,.ready_o(buffer_test_node_ready)

    ,.v_o(test_node_buffer_valid)
    ,.data_o(test_node_buffer_data)
    ,.yumi_i(test_node_buffer_yumi)
  );

endmodule
