/**
 *  loopback.v 
 */

module loopback
  #(parameter width_p="inv")
  (
	  input clk_i
    , input reset_i 

    , input [width_p-1:0] data_i
    , input v_i
    , output logic yumi_o

    , output logic [width_p-1:0] data_o
    , output logic v_o
    , input ready_i
  );

  wire unused0 = clk_i;
  wire unused1 = reset_i;

  assign data_o = data_i;
  assign v_o = v_i;
  assign yumi_o = v_i & ready_i;

endmodule

