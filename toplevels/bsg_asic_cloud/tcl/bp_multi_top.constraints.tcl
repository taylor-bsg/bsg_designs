create_clock -period 2.0 -name CLK [get_ports clk_i]

set_output_delay 1.0 [all_outputs] -clock [get_clocks CLK] -max
set_output_delay 0.0 [all_outputs] -clock [get_clocks CLK] -min

set_input_delay 1.0 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -max
set_input_delay 0.0 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -min

set_max_transition 0.15 $DESIGN_NAME
set_max_fanout 16 $DESIGN_NAME

set_driving_cell -lib_cell INVD1BWP [remove_from_collection [all_inputs] [get_ports clk_i]]
set_load 0.02 [all_outputs]

foreach_in_collection design [get_designs bsg_rp*] {
  current_design $design
  set_dont_touch [get_cells *]
}
current_design $DESIGN_NAME

set_ungroup [get_designs *]

set_app_var compile_final_drc_fix all

define_name_rule verilog -preserve_struct_port
change_names -rules verilog -hierarchy
