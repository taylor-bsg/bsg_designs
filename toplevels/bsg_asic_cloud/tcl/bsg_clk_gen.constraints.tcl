source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag.constraints.tcl

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen.constraints.tcl

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_cdc.constraints.tcl

set TAG_CLK_PERIOD 6.0
set OSC_PERIOD     1.8

if { $USE_LIB_SET == "bc_wc" } {
  set CLK_PERIOD     2.0
} elseif { $USE_LIB_SET == "typ" } {
  set CLK_PERIOD     1.25
}

#######################################################################################################################
#bsg_tag_clock_create bsg_tag_clk clk_i data_i en_i $JTAG_CLK_PERIOD 3.0
bsg_tag_clock_create osc_tag_clk                                              \
                     [get_ports bsg_osc_tag_i\[clk\]]                         \
                     [get_ports "bsg_osc_tag_i\[op\] bsg_osc_tag_i\[param\]"] \
                     [get_ports bsg_osc_tag_i\[en\]]                          \
                     $TAG_CLK_PERIOD
bsg_tag_clock_create osc_trigger_tag_clk                                                      \
                     [get_ports bsg_osc_trigger_tag_i\[clk\]]                                 \
                     [get_ports "bsg_osc_trigger_tag_i\[op\] bsg_osc_trigger_tag_i\[param\]"] \
                     [get_ports bsg_osc_trigger_tag_i\[en\]]                                  \
                     $TAG_CLK_PERIOD
bsg_tag_clock_create ds_tag_clk                                             \
                     [get_ports bsg_ds_tag_i\[clk\]]                        \
                     [get_ports "bsg_ds_tag_i\[op\] bsg_ds_tag_i\[param\]"] \
                     [get_ports bsg_ds_tag_i\[en\]]                         \
                     $TAG_CLK_PERIOD

bsg_clk_gen_clock_create "" clk $OSC_PERIOD $CLK_PERIOD

report_clocks
bsg_chip_async_constraints
set cdc_clocks [get_clocks clk]
append_to_collection cdc_clocks [get_clocks osc_tag_clk]
append_to_collection cdc_clocks [get_clocks osc_trigger_tag_clk]
append_to_collection cdc_clocks [get_clocks ds_tag_clk]
bsg_chip_cdc_constraints $cdc_clocks

####
foreach_in_collection adt [get_cells -hier adt] {
  set path [get_attribute $adt full_name]
  set_disable_timing [get_cells $path/M1]
  set_disable_timing [get_cells $path/sel_r_reg_0]
}

foreach_in_collection cdt [get_cells -hier cdt] {
  set path [get_attribute $cdt full_name]
  set_disable_timing [get_cells $path/M1]
}

foreach_in_collection fdt [get_cells -hier fdt] {
  set path [get_attribute $fdt full_name]
  set_disable_timing [get_cells $path/M2]
}

####
foreach_in_collection clk [all_clocks] {
  set clock_uncertainty [expr [get_attribute $clk period] * 0.05]
  set_clock_uncertainty $clock_uncertainty $clk
}

####
#set_boundary_optimization [get_designs bsg_rp*] false
#set_app_var compile_preserve_subdesign_interfaces true
#set_app_var compile_enable_constant_propagation_with_no_boundary_opt false

####
foreach_in_collection design [get_designs bsg_rp*] {
  current_design $design
  set_dont_touch [get_cells *]
}
current_design $DESIGN_NAME

####
set_ungroup [get_designs *]

####
define_name_rule verilog -preserve_struct_port
change_names -rules verilog -hierarchy
