source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag.constraints.tcl

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_dram_clk_gen.constraints.tcl

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_dmc.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_comm_link.constraints.tcl

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_cdc.constraints.tcl

#####################################################################################################################
#   TIMING SETTINGS
set TAG_CLK_PERIOD 6.0
set OSC_PERIOD     1.8

if { ${analysis_type} == "bc_wc" } {
  set CORE_CLOCK_PERIOD    2.0
  set MIO_CLOCK_PERIOD     5.0
  set FSB_CLOCK_PERIOD     3.3
  set DFI_2X_CLOCK_PERIOD  2.4
} elseif { ${analysis_type} == "single_typical" } {
  set CORE_CLOCK_PERIOD    1.25
  set MIO_CLOCK_PERIOD     3.0
  set FSB_CLOCK_PERIOD     2.0
  set DFI_2X_CLOCK_PERIOD  2.4
}

#######################################################################################################################
bsg_tag_clock_create tag_clk p_bsg_tag_clk_i p_bsg_tag_data_i p_bsg_tag_en_i $TAG_CLK_PERIOD

bsg_clk_gen_clock_create clk_gen_wrapper_inst/clk_gen[0].clk_gen_inst/ core_clk $OSC_PERIOD $CORE_CLOCK_PERIOD
bsg_clk_gen_clock_create clk_gen_wrapper_inst/clk_gen[1].clk_gen_inst/ iom_clk  $OSC_PERIOD $MIO_CLOCK_PERIOD
bsg_clk_gen_clock_create clk_gen_wrapper_inst/clk_gen[2].clk_gen_inst/ fsb_clk  $OSC_PERIOD $FSB_CLOCK_PERIOD

bsg_dram_clk_gen_clock_create clk_gen_wrapper_inst/dram_clk_gen[0].dram_clk_gen_inst/ dfi_clk_2x $DFI_2X_CLOCK_PERIOD

create_clock -period $OSC_PERIOD -name core_clk_ext [get_ports p_clk_A_i]
create_clock -period $OSC_PERIOD -name iom_clk_ext  [get_ports p_clk_B_i]
create_clock -period $OSC_PERIOD -name fsb_clk_ext  [get_ports p_clk_C_i]

####
set dqs0_port  [get_ports         p_ddr_dqs_p_0_io]
set dqs1_port  [get_ports         p_ddr_dqs_p_1_io]
set dqs2_port  [get_ports         p_ddr_dqs_p_2_io]
set dqs3_port  [get_ports         p_ddr_dqs_p_3_io]
set dm0_port   [get_ports         p_ddr_dm_0_o]
set dm1_port   [get_ports         p_ddr_dm_1_o]
set dm2_port   [get_ports         p_ddr_dm_2_o]
set dm3_port   [get_ports         p_ddr_dm_3_o]
set dq0_ports  [get_ports -regexp {p_ddr_dq_([0-7])_io}]
set dq1_ports  [get_ports -regexp {p_ddr_dq_([8-9]|1[0-5])_io}]
set dq2_ports  [get_ports -regexp {p_ddr_dq_(1[6-9]|2[0-3])_io}]
set dq3_ports  [get_ports -regexp {p_ddr_dq_(2[4-9]|3[0-1])_io}]
set ck_p_port  [get_ports         p_ddr_ck_p_o]
set ck_n_port  [get_ports         p_ddr_ck_n_o]
set ctrl_ports [get_ports -filter "name=~p_ddr_*_o&&name!~p_ddr_ck_*&&name!~p_ddr_dm_*"]

bsg_dmc_timing_constraints \
  core_clk                 \
  dfi_clk_2x_osc_ds        \
  dfi_clk_2x_osc           \
  $ck_p_port               \
  $ck_n_port               \
  $dqs0_port               \
  $dqs1_port               \
  $dqs2_port               \
  $dqs3_port               \
  $dm0_port                \
  $dm1_port                \
  $dm2_port                \
  $dm3_port                \
  $dq0_ports               \
  $dq1_ports               \
  $dq2_ports               \
  $dq3_ports               \
  $ctrl_ports

####
set ch0_in_clk_port  [get_ports p_ci_clk_i]
set ch0_in_dv_port   [remove_from_collection [get_ports p_ci_*_i] $ch0_in_clk_port]
set ch0_in_tkn_port  [get_ports p_ci_tkn_o]
set ch0_out_clk_port [get_ports p_co_clk_o]
set ch0_out_dv_port  [remove_from_collection [get_ports p_co_*_o] $ch0_out_clk_port]
set ch0_out_tkn_port [get_ports p_co_tkn_i]

bsg_comm_link_timing_constraints \
  iom_clk                        \
  a                              \
  $ch0_in_clk_port               \
  $ch0_in_dv_port                \
  $ch0_in_tkn_port               \
  $ch0_out_clk_port              \
  $ch0_out_dv_port               \
  $ch0_out_tkn_port              \
  0.2                            \
  0.2

set ch1_in_clk_port  [get_ports p_ci2_clk_i]
set ch1_in_dv_port   [remove_from_collection [get_ports p_ci2_*_i] $ch1_in_clk_port]
set ch1_in_tkn_port  [get_ports p_ci2_tkn_o]
set ch1_out_clk_port [get_ports p_co2_clk_o]
set ch1_out_dv_port  [remove_from_collection [get_ports p_co2_*_o] $ch1_out_clk_port]
set ch1_out_tkn_port [get_ports p_co2_tkn_i]

bsg_comm_link_timing_constraints \
  iom_clk                        \
  b                              \
  $ch1_in_clk_port               \
  $ch1_in_dv_port                \
  $ch1_in_tkn_port               \
  $ch1_out_clk_port              \
  $ch1_out_dv_port               \
  $ch1_out_tkn_port              \
  0.2                            \
  0.2

##########################################################################################

####
set_app_var timing_enable_multiple_clocks_per_reg true

report_clocks
bsg_chip_async_constraints
set cdc_clocks                  [get_clocks tag_clk]
append_to_collection cdc_clocks [get_clocks core_clk]
append_to_collection cdc_clocks [get_clocks iom_clk]
append_to_collection cdc_clocks [get_clocks fsb_clk]
append_to_collection cdc_clocks [get_clocks sdi_a_clk]
append_to_collection cdc_clocks [get_clocks sdi_b_clk]
append_to_collection cdc_clocks [get_clocks sdo_a_tkn_clk]
append_to_collection cdc_clocks [get_clocks sdo_b_tkn_clk]
append_to_collection cdc_clocks [get_clocks dfi_clk_2x_osc]
append_to_collection cdc_clocks [get_clocks dfi_clk_2x_osc_ds]
bsg_chip_cdc_constraints $cdc_clocks

####
foreach_in_collection adt [get_cells -hier adt] {
  set path [get_attribute $adt full_name]
  set_disable_timing [get_cells $path/M1]
  set_disable_timing [get_cells $path/sel_r_reg_0]
}

foreach_in_collection cdt [get_cells -hier cdt] {
  set path [get_attribute $cdt full_name]
  set_disable_timing [get_cells $path/M1]
}

foreach_in_collection fdt [get_cells -hier fdt] {
  set path [get_attribute $fdt full_name]
  set_disable_timing [get_cells $path/M2]
}

####
foreach_in_collection clk [all_clocks] {
  set clock_uncertainty [expr [get_attribute $clk period] * 0.05]
  set_clock_uncertainty $clock_uncertainty $clk
}

####
set_driving_cell -lib_cell PDT12DGZ -pin PAD [get_ports -filter "direction==in"]
set_driving_cell -lib_cell PDD12DGZ -pin PAD [get_ports -filter "direction==inout"]
set_load 10.0 [get_ports -filter "direction==out"]
set_load 10.0 [get_ports -filter "direction==inout"]
