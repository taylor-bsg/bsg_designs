#------------------------------------------------------------
# Do NOT arbitrarily change the order of files. Some module
# and macro definitions may be needed by the subsequent files
#------------------------------------------------------------

set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_manycore_dir $::env(BSG_MANYCORE_DIR)
set bsg_designs_dir $::env(BSG_DESIGNS_DIR)
set bsg_designs_target_dir $::env(BSG_DESIGNS_TARGET_DIR)
set bsg_drlp_dir $::env(BSG_DRLP_DIR)
set bsg_packaging_dir $::env(BSG_PACKAGING_DIR)
set bp_dir $::env(BP_DIR)

set BSG_MANYCORE_SOURCE_LIST [join "
 $bsg_ip_cores_dir/bsg_misc/bsg_transpose.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff_gatestack.v
 $bsg_ip_cores_dir/bsg_misc/bsg_mux2_gatestack.v
 $bsg_ip_cores_dir/bsg_misc/bsg_muxi2_gatestack.v
 $bsg_ip_cores_dir/bsg_misc/bsg_crossbar_o_by_i.v
 $bsg_ip_cores_dir/bsg_misc/bsg_cycle_counter.v
 $bsg_ip_cores_dir/bsg_misc/bsg_round_robin_arb.v
 $bsg_ip_cores_dir/bsg_misc/bsg_arb_fixed.v
 $bsg_ip_cores_dir/bsg_misc/bsg_priority_encode.v
 $bsg_ip_cores_dir/bsg_misc/bsg_priority_encode_one_hot_out.v
 $bsg_ip_cores_dir/bsg_misc/bsg_mux_one_hot.v
 $bsg_ip_cores_dir/bsg_misc/bsg_mux.v
 $bsg_ip_cores_dir/bsg_misc/bsg_encode_one_hot.v
 $bsg_ip_cores_dir/bsg_misc/bsg_scan.v
 $bsg_ip_cores_dir/bsg_misc/bsg_counter_up_down.v
 $bsg_ip_cores_dir/bsg_misc/bsg_circular_ptr.v
 $bsg_ip_cores_dir/bsg_misc/bsg_counter_up_down_variable.v
 $bsg_ip_cores_dir/bsg_misc/bsg_adder_cin.v
 $bsg_ip_cores_dir/bsg_misc/bsg_nor2.v
 $bsg_ip_cores_dir/bsg_misc/bsg_buf_ctrl.v
 $bsg_ip_cores_dir/bsg_misc/bsg_buf.v
 $bsg_ip_cores_dir/bsg_misc/bsg_inv.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff_en.v
 $bsg_ip_cores_dir/bsg_misc/bsg_xnor.v
 $bsg_ip_cores_dir/bsg_misc/bsg_imul_iterative.v
 $bsg_ip_cores_dir/bsg_misc/bsg_idiv_iterative_controller.v
 $bsg_ip_cores_dir/bsg_misc/bsg_idiv_iterative.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff_chain.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_round_robin_n_to_1.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_1_to_n_tagged_fifo.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_1_to_n_tagged.v
 $bsg_ip_cores_dir/bsg_misc/bsg_counter_clear_up.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel_in.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel_out.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_2r1w.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_2r1w_synth.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_2r1w_sync.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_2r1w_sync_synth.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w_sync.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w_synth.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w_sync_synth.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_banked_crossbar.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_tracker.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_mask_write_byte.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_mask_write_byte_synth.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync.v
 $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_synth.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_small.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1rw_large.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_pseudo_large.v
 $bsg_ip_cores_dir/bsg_dataflow/bsg_two_fifo.v
 $bsg_ip_cores_dir/bsg_noc/bsg_noc_pkg.v
 $bsg_ip_cores_dir/bsg_noc/bsg_noc_links.vh
 $bsg_ip_cores_dir/bsg_noc/bsg_mesh_router.v
 $bsg_ip_cores_dir/bsg_noc/bsg_mesh_stitch.v
 $bsg_ip_cores_dir/bsg_noc/bsg_mesh_router_buffered.v
 $bsg_ip_cores_dir/bsg_misc/bsg_decode_with_v.v
 $bsg_ip_cores_dir/bsg_misc/bsg_decode.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff_reset.v
 $bsg_ip_cores_dir/bsg_misc/bsg_clkbuf.v
 $bsg_ip_cores_dir/bsg_misc/bsg_dff_reset_en.v
 $bsg_manycore_dir/v/vanilla_bean/alu.v
 $bsg_manycore_dir/v/vanilla_bean/cl_decode.v
 $bsg_manycore_dir/v/vanilla_bean/cl_state_machine.v
 $bsg_manycore_dir/v/vanilla_bean/scoreboard.v
 $bsg_manycore_dir/v/vanilla_bean/load_packer.v
 $bsg_manycore_dir/v/vanilla_bean/hobbit.v
 $bsg_manycore_dir/v/vanilla_bean/rf_2r1w_sync_wrapper.v
 $bsg_manycore_dir/v/vanilla_bean/bsg_manycore_proc_vanilla.v
 $bsg_manycore_dir/v/vanilla_bean/imul_idiv_iterative.v
 $bsg_manycore_dir/v/bsg_manycore_hetero_socket.v
 $bsg_manycore_dir/v/bsg_manycore_tile.v
 $bsg_manycore_dir/v/bsg_manycore_mesh.v
 $bsg_manycore_dir/v/bsg_manycore_mesh_node.v
 $bsg_manycore_dir/v/bsg_manycore_links_to_fsb.v
 $bsg_manycore_dir/v/bsg_manycore.v
 $bsg_manycore_dir/v/bsg_manycore_pkt_encode.v
 $bsg_manycore_dir/v/bsg_manycore_pkt_decode.v
 $bsg_manycore_dir/v/bsg_manycore_endpoint.v
 $bsg_manycore_dir/v/bsg_manycore_endpoint_standard.v
 $bsg_manycore_dir/v/bsg_1hold.v
 $bsg_manycore_dir/v/bsg_manycore_link_sif_tieoff.v
 $bsg_manycore_dir/v/bsg_manycore_accel_default.v
"]

set SVERILOG_SOURCE_FILES [join "
  $bsg_ip_cores_dir/bsg_misc/bsg_defines.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_pkg.v
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_pkg.v
  $bsg_designs_target_dir/v/bsg_chip_pkg.v
  $bsg_manycore_dir/v/lpddr1/bsg_dram_ctrl_pkg.v
  $bp_dir/bp_common/src/include/bp_common_pkg.vh
  $bp_dir/bp_common/src/include/bp_common_me_if.vh
  $bp_dir/bp_fe/src/include/bp_fe_icache_pkg.vh
  $bp_dir/bp_fe/src/include/bp_fe_itlb_pkg.vh
  $bp_dir/bp_fe/src/include/bp_fe_pkg.vh
  $bp_dir/bp_be/src/include/bp_be_rv64_pkg.vh
  $bp_dir/bp_be/src/include/bp_be_pkg.vh
  $bp_dir/bp_be/src/include/bp_be_dcache/bp_be_dcache_pkg.vh
  $bp_dir/bp_me/src/include/v/bp_cce_pkg.v
  $bp_dir/bp_me/src/include/v/bp_me_network_pkg.v
  $bsg_packaging_dir/uw_bga/pinouts/bsg_asic_cloud/common/verilog/bsg_chip_swizzle_adapter.v
  $BSG_MANYCORE_SOURCE_LIST
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_clk_gen.v
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_dram_clk_gen.v
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_clk_gen_osc.v
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_dly_line.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_client.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_client_unsync.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_master.v
  $bsg_ip_cores_dir/bsg_misc/bsg_counter_clock_downsample.v
  $bsg_ip_cores_dir/bsg_misc/bsg_strobe.v
  $bsg_ip_cores_dir/bsg_misc/bsg_nand.v
  $bsg_ip_cores_dir/bsg_misc/bsg_and.v
  $bsg_ip_cores_dir/bsg_misc/bsg_nor3.v
  $bsg_ip_cores_dir/bsg_misc/bsg_reduce.v
  $bsg_ip_cores_dir/bsg_misc/bsg_tiehi.v
  $bsg_ip_cores_dir/bsg_misc/bsg_tielo.v
  $bsg_ip_cores_dir/bsg_misc/bsg_thermometer_count.v
  $bsg_ip_cores_dir/bsg_misc/bsg_popcount.v
  $bsg_ip_cores_dir/bsg_misc/bsg_gray_to_binary.v
  $bsg_ip_cores_dir/bsg_misc/bsg_binary_plus_one_to_gray.v
  $bsg_ip_cores_dir/bsg_misc/bsg_rotate_right.v
  $bsg_ip_cores_dir/bsg_misc/bsg_mux_segmented.v
  $bsg_ip_cores_dir/bsg_misc/bsg_mux_butterfly.v
  $bsg_ip_cores_dir/bsg_misc/bsg_swap.v
  $bsg_ip_cores_dir/bsg_misc/bsg_lru_pseudo_tree_decode.v
  $bsg_ip_cores_dir/bsg_misc/bsg_lru_pseudo_tree_encode.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_credit_counter.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_fifo.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_ptr_gray.v
  $bsg_ip_cores_dir/bsg_async/bsg_launch_sync_sync.v
  $bsg_ip_cores_dir/bsg_async/bsg_sync_sync.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_mask_write_bit.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_mask_write_bit_synth.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_assembler_in.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_assembler_out.v
  $bsg_ip_cores_dir/bsg_noc/bsg_wormhole_router.v
  $bsg_ip_cores_dir/bsg_noc/bsg_wormhole_router_adapter_in.v
  $bsg_ip_cores_dir/bsg_noc/bsg_wormhole_router_adapter_out.v
  $bsg_designs_dir/modules/bsg_guts_new_pipelined/bsg_comm_link_fuser.v
  $bsg_designs_dir/modules/bsg_guts_new_pipelined/bsg_comm_link_kernel.v
  $bsg_designs_dir/modules/bsg_guts_new_pipelined/bsg_comm_link.v
  $bsg_designs_target_dir/v/bsg_chip_guts.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_channel_control_slave.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_input.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_output.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_flatten_2D_array.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_make_2D_array.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_round_robin_fifo_to_fifo.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_sbox.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_scatter_gather.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_narrowed.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_narrow.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_large.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_parallel_in_serial_out.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_serial_in_parallel_out.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_round_robin_2_to_2.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_shift_reg.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_murn_gateway.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_in.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_out.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_node_async_buffer.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb.v
  $bsg_designs_target_dir/v/bsg_clk_gen_wrapper.v
  $bsg_designs_target_dir/v/bsg_chip.v
  $bsg_designs_target_dir/v/fsb_mesh_node.v
  $bsg_designs_target_dir/v/bsg_test_node_client.v
  $bsg_designs_target_dir/v/loopback.v
  $bsg_designs_target_dir/v/manycore.v
  $bsg_designs_target_dir/v/vcache.v
  $bsg_designs_target_dir/v/vcache_row.v
  $bsg_ip_cores_dir/bsg_dram_ctrl/dmc_fifo.v
  $bsg_ip_cores_dir/bsg_dram_ctrl/bsg_dmc_controller.v
  $bsg_ip_cores_dir/bsg_dram_ctrl/bsg_dmc_phy.v
  $bsg_ip_cores_dir/bsg_dram_ctrl/bsg_dmc.v 
  $bsg_manycore_dir/v/bsg_manycore_link_sif_async_buffer.v
  $bsg_ip_cores_dir/bsg_test/bsg_nonsynth_clock_gen.v  
  $bsg_ip_cores_dir/bsg_cache/bsg_cache.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_miss.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_dma.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_sbuf.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_sbuf_queue.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_to_dram_ctrl.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_to_dram_ctrl_rx.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_cache_to_dram_ctrl_tx.v 
  $bsg_ip_cores_dir/bsg_cache/bsg_manycore_link_to_cache.v 
  $bp_dir/bp_fe/src/v/bp_fe_top.v
  $bp_dir/bp_fe/src/v/bp_fe_pc_gen.v
  $bp_dir/bp_fe/src/v/bp_fe_branch_predictor.v
  $bp_dir/bp_fe/src/v/bp_fe_bht.v
  $bp_dir/bp_fe/src/v/bp_fe_btb.v
  $bp_dir/bp_fe/src/v/bp_fe_icache.v
  $bp_dir/bp_fe/src/v/bp_fe_itlb.v
  $bp_dir/bp_fe/src/v/bp_fe_instr_scan.v
  $bp_dir/bp_fe/src/v/bp_fe_lce.v
  $bp_dir/bp_fe/src/v/bp_fe_lce_req.v
  $bp_dir/bp_fe/src/v/bp_fe_lce_cmd.v
  $bp_dir/bp_fe/src/v/bp_fe_lce_data_cmd.v
  $bp_dir/bp_be/src/v/bp_be_top.v
  $bp_dir/bp_be/src/v/bp_be_checker/bp_be_checker_top.v
  $bp_dir/bp_be/src/v/bp_be_checker/bp_be_detector.v
  $bp_dir/bp_be/src/v/bp_be_checker/bp_be_director.v
  $bp_dir/bp_be/src/v/bp_be_checker/bp_be_scheduler.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_calculator_top.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_regfile.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_instr_decoder.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_bypass.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_pipe_int.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_int_alu.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_pipe_mul.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_pipe_mem.v
  $bp_dir/bp_be/src/v/bp_be_calculator/bp_be_pipe_fp.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_mmu_top.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_lce.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_lce_cmd.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_lce_data_cmd.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_lce_req.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_wbuf.v
  $bp_dir/bp_be/src/v/bp_be_mmu/bp_be_dcache/bp_be_dcache_wbuf_queue.v
  $bp_dir/bp_be/test/common/bp_be_nonsynth_tracer.v
  $bp_dir/bp_me/src/v/bp_me_top.v
  $bp_dir/bp_me/src/v/cce/bp_cce_top.v
  $bp_dir/bp_me/src/v/cce/bp_cce.v
  $bp_dir/bp_me/src/v/cce/bp_cce_pc.v
  $bp_dir/bp_me/src/v/cce/bp_cce_inst_decode.v
  $bp_dir/bp_me/src/v/cce/bp_cce_alu.v
  $bp_dir/bp_me/src/v/cce/bp_cce_dir.v
  $bp_dir/bp_me/src/v/cce/bp_cce_gad.v
  $bp_dir/bp_me/src/v/cce/bp_cce_reg.v
  $bp_dir/bp_me/src/v/network/bp_me_network.v
  $bp_dir/bp_me/src/v/network/bp_me_network_channel_mesh.v
  $bp_dir/bp_me/src/v/network/bp_me_network_channel_data_cmd.v
  $bp_dir/bp_me/src/v/network/bp_me_network_channel_data_resp.v
  $bp_dir/bp_me/src/v/network/bp_me_network_pkt_encode_data_cmd.v
  $bp_dir/bp_me/src/v/network/bp_me_network_pkt_encode_data_resp.v
  $bp_dir/bp_me/src/v/roms/demo-v2/bp_cce_inst_rom_demo-v2_lce2_wg64_assoc8.v
  $bp_dir/bp_be/src/v/common/bsg_fifo_1r1w_rolly.v
  $bp_dir/bp_top/src/v/bp_multi_top.v
  $bp_dir/bp_top/src/v/bp_core.v
"]
