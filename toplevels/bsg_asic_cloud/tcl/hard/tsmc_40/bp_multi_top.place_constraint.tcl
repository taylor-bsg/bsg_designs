puts "Flow-Info: Running script [info script]\n"

set tile_width  [get_attribute [get_core_area] tile_width]
set tile_height [get_attribute [get_core_area] tile_height]

set_attribute [all_macro_cells] is_fixed true
if {[get_fp_cells -filter "is_hard_macro"] != ""} {
  set_attribute [get_fp_cells -filter "is_hard_macro"] is_fixed false
}

set macro_keepout [expr $tile_width * 2]
set macro_keepout $tile_height
set_keepout_margin -all_macros -outer [list $macro_keepout $macro_keepout $macro_keepout $macro_keepout]

set macro_list ""
for {set row 0} {$row<2} {incr row} {
  set macro_row ""
  for {set column 0} {$column<4} {incr column} {
    set idx [expr $row*4+$column]
    append_to_collection macro_row [get_fp_cells -filter "is_hard_macro&&full_name=~*dcache*data_mem_$idx*"]
  }
  lappend macro_list $macro_row
}
set_fp_macro_array -name dcache_data_mem_array -elements $macro_list -use_keepout_margin
set_fp_macro_options [get_fp_cells -filter "is_hard_macro&&name=~*dcache*data_mem*"] -legal_orientations {E FW}
set_fp_relative_location -name dcache_data_array_rl -target_cell dcache_data_mem_array -target_corner br -anchor_corner br
set dcache_array [get_fp_cells -filter "is_hard_macro&&name=~*regfile*"]
append_to_collection dcache_array [get_fp_cells -filter "is_hard_macro&&name=~*dcache*tag_mem*"]
append_to_collection dcache_array [get_fp_cells -filter "is_hard_macro&&name=~*dcache*stat_mem*"]
set_fp_macro_options $dcache_array -legal_orientations {W FE}
set_fp_macro_array -name dcache_array -elements $dcache_array -align_edge b -use_keepout_margin
set_fp_relative_location -name dcache_array_rl -target_cell dcache_array -target_corner br -x_offset [expr -320 * $tile_height] -anchor_corner br

set macro_list ""
for {set row 0} {$row<2} {incr row} { 
  set macro_row ""
  for {set column 0} {$column<4} {incr column} { 
    set idx [expr $row*4+$column]
    append_to_collection macro_row [get_fp_cells -filter "is_hard_macro&&name=~*icache*data_mem_banks_$idx*"]
  }
  lappend macro_list $macro_row
}
set_fp_macro_options [get_fp_cells -filter "is_hard_macro&&name=~*icache*data_mem_banks*"] -legal_orientations {W FE}
set_fp_macro_array -name icache_data_mem_array -elements $macro_list -use_keepout_margin
set_fp_relative_location -name icache_data_array_rl -target_cell icache_data_mem_array

set icache_array [get_fp_cells -filter "is_hard_macro&&name=~*icache*metadata_mem*"]
append_to_collection icache_array [get_fp_cells -filter "is_hard_macro&&name=~*icache*tag_mem*"]
append_to_collection icache_array [get_fp_cells -filter "is_hard_macro&&name=~*btb_mem*"]
set_fp_macro_options $icache_array -legal_orientations {E FW}
set_fp_macro_array -name icache_array -elements $icache_array -align_edge b -use_keepout_margin
set_fp_relative_location -name icache_array_rl -target_cell icache_array -x_offset [expr 320 * $tile_height]

set cce_array [get_fp_cells -filter "is_hard_macro&&name=~*cce_inst_ram*"]
append_to_collection cce_array [get_fp_cells -filter "is_hard_macro&&name=~*directory*"]
set_fp_macro_options $cce_array -legal_orientations E
set_fp_macro_array -name cce_array -elements $cce_array -align_edge t -use_keepout_margin
set_fp_relative_location -name cce_array_rl -target_cell cce_array -target_corner tl -anchor_corner tl -x_offset [expr 400 * $tile_height]

set core_bbox [get_attribute [get_core_area] bbox]
set core_llx  [lindex [lindex $core_bbox 0] 0]
set core_lly  [lindex [lindex $core_bbox 0] 1]
set core_urx  [lindex [lindex $core_bbox 1] 0]
set core_ury  [lindex [lindex $core_bbox 1] 1]

#create_bounds -name fe -cycle_color -coordinate [list [list $core_llx $core_lly] [list [expr ($core_llx + $core_urx) / 2.0] [expr ($core_lly + $core_ury) / 2.0]] [list $core_llx [expr ($core_lly + $core_ury) / 2.0]] [list [expr $core_llx + 350 * $tile_height] [expr ($core_lly + $core_ury) / 2.0 + 200 * $tile_height]]] -type hard [get_cells -hier *fe]
#create_bounds -name be -cycle_color -coordinate [list [list [expr ($core_llx + $core_urx) / 2.0] $core_lly] [list $core_urx [expr ($core_lly + $core_ury) / 2.0]] [list [expr $core_urx - 350 * $tile_height] [expr ($core_lly + $core_ury) / 2.0]] [list $core_urx [expr ($core_lly + $core_ury) / 2.0 + 200 * $tile_height]]] -type hard [get_cells -hier *be]
#create_bounds -name cce -cycle_color -coordinate [list [list [expr $core_llx + 380 * $tile_height] [expr $core_ury - 200 * $tile_height]] [list [expr $core_urx - 380 * $tile_height] $core_ury]] -type hard [get_cells -hier *bp_cce]

if {[file exists ${DESIGN_REF_DATA_PATH}/results/dc/${DESIGN_NAME}.rp_groups.tcl]} {
  if {[sizeof_collection [get_rp_groups]] == 0} {
    puts "## BSG: processing rp groups file"
    source ${DESIGN_REF_DATA_PATH}/results/dc/${DESIGN_NAME}.rp_groups.tcl
    set_dont_touch [get_cells -of_objects [all_rp_groups]]
  }
} else {
  puts "## BSG: no rp groups file find!\n"
}

puts "Flow-Info: Completed script [info script]\n"
