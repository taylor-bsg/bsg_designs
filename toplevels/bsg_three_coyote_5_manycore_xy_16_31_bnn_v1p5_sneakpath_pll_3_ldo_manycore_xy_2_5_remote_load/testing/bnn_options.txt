"bnn_random_conv_8.riscv.hex"                ""                      ""
"bnn_random_conv_16.riscv.hex"               ""                      ""
"bnn_random_conv_32.riscv.hex"               ""                      ""

"bnn_random_pool_8.riscv.hex"                ""                      ""
"bnn_random_pool_16.riscv.hex"               ""                      ""
"bnn_random_pool_32.riscv.hex"               ""                      ""

"bnn_random_dense.riscv.hex"                 ""                      ""

"bnn_random_last.riscv.hex"                  ""                      ""

"bnn_layer_1.riscv.hex"                      ""                      ""
"bnn_layer_2.riscv.hex"                      ""                      ""
"bnn_layer_3.riscv.hex"                      ""                      ""
"bnn_layer_4.riscv.hex"                      ""                      ""
"bnn_layer_5.riscv.hex"                      ""                      ""
"bnn_layer_6.riscv.hex"                      ""                      ""
"bnn_layer_7.riscv.hex"                      ""                      ""
"bnn_layer_8.riscv.hex"                      ""                      ""
"bnn_layer_9.riscv.hex"                      ""                      ""

"bnn.riscv.hex"                              ""                      ""

"bnn_random_conv_8_sneakpath.riscv.hex"      "sp_wts_random.hex"     "10"
"bnn_random_conv_16_sneakpath.riscv.hex"     "sp_wts_random.hex"     "10"
"bnn_random_conv_32_sneakpath.riscv.hex"     "sp_wts_random.hex"     "10"

"bnn_random_pool_8_sneakpath.riscv.hex"      "sp_wts_random.hex"     "20"
"bnn_random_pool_16_sneakpath.riscv.hex"     "sp_wts_random.hex"     "20"
"bnn_random_pool_32_sneakpath.riscv.hex"     "sp_wts_random.hex"     "20"

"bnn_random_dense_sneakpath.riscv.hex"       "sp_wts_random.hex"     "20"

"bnn_random_last_sneakpath.riscv.hex"        "sp_wts_random.hex"     "10"

"bnn_layer_1_sneakpath.riscv.hex"            "sp_wts_layer1.hex"     ""
"bnn_layer_2_sneakpath.riscv.hex"            "sp_wts_layer2.hex"     ""
"bnn_layer_3_sneakpath.riscv.hex"            "sp_wts_layer3.hex"     ""
"bnn_layer_4_sneakpath.riscv.hex"            "sp_wts_layer4.hex"     ""
"bnn_layer_5_sneakpath.riscv.hex"            "sp_wts_layer5.hex"     ""
"bnn_layer_6_sneakpath.riscv.hex"            "sp_wts_layer6.hex"     ""
"bnn_layer_7_sneakpath.riscv.hex"            "sp_wts_layer7.hex"     ""
"bnn_layer_8_sneakpath.riscv.hex"            "sp_wts_layer8.hex"     ""
