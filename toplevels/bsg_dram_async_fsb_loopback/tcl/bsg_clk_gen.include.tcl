set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)

set SVERILOG_INCLUDE_PATHS [join "
  $bsg_ip_cores_dir/bsg_tag
  $bsg_ip_cores_dir/bsg_clk_gen
"]
