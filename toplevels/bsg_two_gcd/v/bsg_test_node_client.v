//==========================================================================
// bsg_test_node_client
//==========================================================================
// This is a test node client that contains a 16-bit GCD unit.
//
// This bsg_design was created by Cornell during the DARPA Craft project
// to better understand bsg_designs and their testing infrastructure.
//
// Date   : January 14, 2017
// Author : Christopher Torng (clt67@cornell.edu)

module  bsg_test_node_client
   import bsg_fsb_pkg::*;
  #(parameter ring_width_p="inv"
    , parameter master_id_p="inv"
    , parameter client_id_p="inv"
    )

  (input clk_i
   , input reset_i

   // control
   , input en_i

   // input channel
   , input  v_i
   , input [ring_width_p-1:0] data_i
   , output ready_o

   // output channel
   , output v_o
   , output [ring_width_p-1:0] data_o
   , input yumi_i   // late

   );

   localparam debug_lp=1;

   // synopsys translate_off
   if (debug_lp)
     begin
        always @(negedge clk_i)
          if (v_i & ready_o)
            $display("## bsg_test_node_client received %x",data_i);

        always @(negedge clk_i)
          if (v_o & yumi_i)
            $display("## bsg_test_node_client sent %x",data_o);
     end
   // synopsys translate_on

   // the default interface gives all design
   // control to the switch: you have to say
   // ahead of time if you can receive data
   // and it won't tell you until the last minute
   // if it took your data.

   // we reverse the situation by having an
   // input and output fifo. these
   // are not required, but make the hw
   // design easier at the cost of some
   // area and latency.

   logic                   in_fifo_v;
   bsg_fsb_pkt_s           in_fifo_data;
   logic                   in_fifo_yumi;

   logic                   out_fifo_ready;
   bsg_fsb_pkt_s           out_fifo_data;
   logic                   out_fifo_v;

   logic [31:0]            gcd_req_msg;
   logic                   gcd_req_val;
   logic                   gcd_req_rdy;

   logic [15:0]            gcd_resp_msg;
   logic                   gcd_resp_val;
   logic                   gcd_resp_rdy;

   //---------------------------------------------------------------------
   // Input FIFO
   //---------------------------------------------------------------------

   bsg_two_fifo #( .width_p(ring_width_p)) fifo_in
     (.clk_i(clk_i)

      ,.reset_i(reset_i)

      ,.ready_o(ready_o)
      ,.v_i    (v_i    )
      ,.data_i (data_i )

      ,.v_o   (in_fifo_v)
      ,.data_o(in_fifo_data)
      ,.yumi_i(in_fifo_yumi)
      );

   // fifo_in control signals
   // en_i is not really necessary but we do it to prevent unused input

   assign in_fifo_yumi = in_fifo_v & en_i & gcd_req_rdy;

   //---------------------------------------------------------------------
   // Pipe buffer
   //---------------------------------------------------------------------
   // The GCD unit does not keep the metadata in the bsg_fsb_pkt_s. Use
   // this pipe buffer to buffer the metadata while the GCD unit works on
   // computation.

   bsg_fsb_pkt_s piped_bsg_fsb_pkt_s;

   always_ff@( posedge clk_i )
     if (reset_i)
       piped_bsg_fsb_pkt_s <= 0;
     else begin
       // Latch in_fifo_data when GCD unit accepts a request
       if (gcd_req_val & gcd_req_rdy)
         piped_bsg_fsb_pkt_s <= in_fifo_data;
     end

   //---------------------------------------------------------------------
   // GCD unit
   //---------------------------------------------------------------------
   // Note: This is a 16-bit GCD unit.
   //
   // The request message takes the lower 32 bits of the in_fifo_data.data
   // and gets the two operands from the upper and lower halves.
   //
   // The response message is 16 bits and sets the least significant bits
   // of out_fifo_data.data.

   bsg_two_gcd gcd (
     .clk_i     (clk_i),
     .reset     (reset_i),

     .req_msg   (gcd_req_msg),
     .req_val   (gcd_req_val),
     .req_rdy   (gcd_req_rdy),

     .resp_msg  (gcd_resp_msg),
     .resp_val  (gcd_resp_val),
     .resp_rdy  (gcd_resp_rdy)
   );

   // GCD request signals

   assign gcd_req_msg  = in_fifo_data.data[31:0];
   assign gcd_req_val  = in_fifo_v;

   // GCD response signals

   assign gcd_resp_rdy = out_fifo_ready;

   // Package GCD response into a bsg_fsb_pkt_s for the out_fifo_data. Use
   // the piped bsg_fsb_pkt_s to get the metadata.

   always_comb
     begin
        // out_fifo_data        = in_fifo_data;

        // Explicitly list the fields here for better understanding

        out_fifo_data.cmd    = piped_bsg_fsb_pkt_s.cmd;
        out_fifo_data.opcode = piped_bsg_fsb_pkt_s.opcode;
        out_fifo_data.data   = gcd_resp_msg;

        // swap source and dest

        out_fifo_data.srcid  = piped_bsg_fsb_pkt_s.destid;
        out_fifo_data.destid = piped_bsg_fsb_pkt_s.srcid;
     end

   //---------------------------------------------------------------------
   // Output FIFO
   //---------------------------------------------------------------------

   bsg_two_fifo #( .width_p(ring_width_p)) fifo_out
     (.clk_i(clk_i)

      ,.reset_i(reset_i)

      ,.ready_o(out_fifo_ready)
      ,.v_i    (out_fifo_v    )
      ,.data_i (out_fifo_data )

      ,.v_o   (v_o   )
      ,.data_o(data_o)
      ,.yumi_i(yumi_i)
      );

   // fifo_out control signals

   assign out_fifo_v = gcd_resp_val;

endmodule
