==========================================================================
`bsg_two_gcd`
==========================================================================
Date   : January 14, 2017
Author : Christopher Torng (clt67@cornell.edu)

This bsg design contains a 16-bit GCD unit.

This design was created by Cornell during the DARPA Craft project to
better understand bsg designs and their testing infrastructure.

## Notes by ctorng (01/14/2017)

I started from `bsg_two_loopback` in the `bsg_designs` repo on branch
`bsg_tsmc180`. I made the following changes to add the GCD unit.

** Getting Started **

I set up my repos like this according to Mike Taylor's checkout
instructions:

    % TOP_DIR=$PWD
    % git clone git@bitbucket.org:taylor-bsg/bsg_designs.git
    % git clone git@bitbucket.org:taylor-bsg/bsg_ip_cores.git
    % git clone git@bitbucket.org:taylor-bsg/board.git
    % git clone git@bitbucket.org:taylor-bsg/bsg_packaging.git

    % git clone git@bitbucket.org:taylor-bsg/chip.git
    % cd $TOP_DIR/chip
    % git checkout bsg_tsmc180
    % cd $TOP_DIR

    % git clone git@bitbucket.org:taylor-bsg/cad.git
    % cd $TOP_DIR/cad
    % git checkout bsg_tsmc180
    % cd $TOP_DIR

Then I ran their `bsg_two_loopback` design with these steps:

- First customize the CAD tool environment in `cad/common/mk/cadenv.mk`.
  This is how the scripts know where the tools are (e.g., `vcs`,
  `dc_shell`, `icc_shell`, etc.). I created Cornell's cadenv is in this
  directory as `cornell_cadenv.mk`. I temporarily overwrite `cadenv.mk` if
  I want to run on Cornell servers.

- Choose the design to simulate. Tests are run from within the chip repo
  in `bsg_one_loopback`, no matter what design you are testing (ignore the
  name, legacy reasons).

    To choose a design, edit `chip/bsg_one_loopback/Makefile.include` and
    make sure `BSG_DESIGNS_TARGET` is pointing at the design under test
    (e.g., `bsg_two_loopback`).

Then test like this:

    % cd chip/bsg_one_loopback/testing/rtl
    % make

This should end with a VCS simulation. There should be no error messages.
I played with the check inside of test node master (in
`$bsg_designs_dir/modules/bsg_guts/loopback/bsg_test_node_master.v`) to
make sure it was actually checking stuff (i.e., I broke the check by
checking for result + 1'b1).

** Creating `bsg_two_gcd` **

I then followed Mike Taylor's instructions on creating my own `bsg_design`:

    How to create your own loopback-style design foo:

    a. Copy bsg_designs/toplevels/bsg_two_loopback, to bsg_designs/toplevels/bsg_two_foo
    b. Copy $bsg_designs_dir/modules/bsg_guts/loopback/bsg_test_node_client.v to bsg_designs/toplevels/bsg_two_foo/v/.
    c. Copy $bsg_designs_dir/modules/bsg_guts/loopback/bsg_test_node_master.v to bsg_designs/toplevels/bsg_two_foo/testing/.
    d. Modify the bsg_test_node_client.v to instantiate your design (DUT). Don't change the interface, module name or file name.
    e. Modify the bsg_test_node_master.v to instantiate your tester code. Don't change the interface, module name or file name.
    f. Update the paths of those two files in tcl/filelist.tcl, and testing/filelist.tcl.
    g. Simulate with VCS. Our VCS setup is in chip/bsg_one_loopback/testing/rtl. You edit chip/bsg_one_loopback/Makefile.include to specify which bsg_design you want to run.

Specifically for `bsg_two_gcd`, I also copied our `GcdUnit.v` into `/v`
before beginning to modify the test node master and client.

** Modifications to `bsg_test_node_client` **

In the original loopback design, `fifo_in` passed directly to `fifo_out`.

I updated the test node client so that it looks like this:

    +---------+        +----------+        +----------+
    |         |   +->  |   GCD    | --+    |          |
    |         |   |    +----------+   |    |          |
    | fifo_in | --+                   +--> | fifo_out |
    |         |   |    +----------+   |    |          |
    |         |   +->  | pipe_buf | --+    |          |
    +---------+        +----------+        +----------+

I instantiated a GCD unit with this interface:

- Uses ready/valid request and response interfaces
- The input request message is 32 bits and expects the two operands to
be in the upper and lower 16 bits
- The output response message is 16 bits

The GCD request message is the lower 32b of the data from `fifo_in`.

The GCD response message sets the lower 16b of the data in `fifo_out`.

Note that the GCD unit does not handle any of the `bsg_fsb_pkt_s`
metadata. So I added a buffer called `pipe_buf` that just latches the
packet and can later be used to get the metadata when assembling the
response message into `fifo_out`.

** Modifications to `bsg_test_node_master` **

The original loopback design uses a `test_bsg_data_gen` to generate a
`bsg_fsb_pkt_s`.

I made a hardwired "test case" that just overwrites the data in
`out_fifo_data.data` with the 32'b GCD test vector. Search for "Set GCD
test vector" to see where I do this.

Then I hardwire the response check for the result I am expecting.  Search
for "Check GCD result" to see where I do this.

So basically I run a whole simulation for a single test vector. Since this
is just an example design to get started, this is fine for now.

** Simulation **

To simulate, I go to `chip/bsg_one_loopback/testing/rtl`. In
`chip/bsg_one_loopback/Makefile.include`, I set the `BSG_DESIGNS_TARGET`
to `bsg_two_gcd`.

Then I run make here:

    % cd chip/bsg_one_loopback/testing/rtl
    % make

I see no errors. To double-check what's going on, I went back to the test
node master and made it check for the wrong result. Then I reran the
simulations and saw a bunch of errors saying what it expects and what it
got. This all looks right, so I think it is passing this smoke test.

