`ifndef BSG_CHIP_PKG_V
`define BSG_CHIP_PKG_V

package bsg_chip_pkg;

  `include "bsg_defines.v"
  `include "bsg_manycore_packet.vh"
  `include "bsg_cache_dma_pkt.vh"

  //////////////////////////////////////////////////
  //
  // BSG CLOCK GENERATOR PARAMETERS
  //

  localparam clk_gen_num_endpoints_gp = 3;
  localparam clk_gen_ds_width_gp      = 6;
  localparam clk_gen_num_adgs_gp      = 1;

  //////////////////////////////////////////////////
  //
  // BSG MANYCORE PARAMETERS
  //

  localparam manycore_num_tiles_x_gp = 4;
  localparam manycore_num_tiles_y_gp = 5;

  localparam manycore_addr_width_gp = 28;
  localparam manycore_data_width_gp = 32;

  localparam manycore_dmem_size_gp        = 1024;
  localparam manycore_icache_entries_gp   = 1024;
  localparam manycore_icache_tag_width_gp = 12;

  localparam manycore_load_id_width_gp       = 12;
  localparam manycore_epa_byte_addr_width_gp = 18;

  localparam manycore_dram_ch_addr_width_gp = 27;
  localparam manycore_dram_ch_start_col_gp  = 0;

  localparam manycore_extra_io_rows_gp = 1;

  localparam manycore_x_cord_width_gp      = `BSG_SAFE_CLOG2(manycore_num_tiles_x_gp);
  localparam manycore_y_cord_width_gp      = `BSG_SAFE_CLOG2(manycore_num_tiles_y_gp + manycore_extra_io_rows_gp);
  localparam manycore_byte_offset_width_gp = `BSG_SAFE_CLOG2(manycore_data_width_gp>>3);


  //////////////////////////////////////////////////
  //
  // VICTIM CACHE PARAMETERS
  //

  localparam vcache_num_cache_gp           = manycore_num_tiles_x_gp;
  localparam vcache_ways_gp                = 2;
  localparam vcache_sets_gp                = 2**9;
  localparam vcache_block_size_in_words_gp = 8;
  localparam vcache_size_gp                = vcache_ways_gp*vcache_sets_gp*vcache_block_size_in_words_gp;

  //////////////////////////////////////////////////
  //
  // BSG CHIP IO COMPLEX PARAMETERS
  //

  localparam link_channel_width_gp = 8;
  localparam link_num_channels_gp = 1;
  localparam link_width_gp = 34;
  localparam link_lg_fifo_depth_gp = 6;
  localparam link_lg_credit_to_token_decimation_gp = 3;
  localparam link_use_extra_data_bit_gp = 1;

  localparam ct_num_in_gp = 3;
  localparam ct_tag_width_gp = `BSG_SAFE_CLOG2(ct_num_in_gp + 1);
  localparam ct_width_gp = link_width_gp - ct_tag_width_gp;

  localparam ct_remote_credits_gp = 64;
  localparam ct_credit_decimation_gp = ct_remote_credits_gp/4;
  localparam ct_lg_credit_decimation_gp = `BSG_SAFE_CLOG2(ct_credit_decimation_gp/2+1);
  localparam ct_use_pseudo_large_fifo_gp = 1;

  localparam network_a_wh_len_width_gp = 2;
  localparam network_a_wh_cord_markers_pos_x_gp = 0;
  localparam network_a_wh_cord_markers_pos_y_gp = 4;
  localparam network_a_wh_cord_width_gp = network_a_wh_cord_markers_pos_y_gp - network_a_wh_cord_markers_pos_x_gp;
  
  localparam network_b_wh_len_width_gp = 4;
  localparam network_b_wh_cord_markers_pos_x_gp = 0;
  localparam network_b_wh_cord_markers_pos_y_gp = 4 + `BSG_SAFE_CLOG2(vcache_num_cache_gp);
  localparam network_b_wh_cord_width_gp = network_b_wh_cord_markers_pos_y_gp - network_b_wh_cord_markers_pos_x_gp;

  //////////////////////////////////////////////////
  //
  // BSG CHIP TAG PARAMETERS AND STRUCTS
  //

  // Total number of clients the master will be driving.
  // Set mininum client_id_width to 6-bits (fixed to 6 most of the time), no need to 
  // update trace file when adding or removing Vcahces.
  localparam tag_num_clients_gp = `BSG_MAX((1<<5)+2, 22+vcache_num_cache_gp);

  localparam tag_max_payload_width_in_io_complex_gp       = (`BSG_MAX(network_b_wh_cord_width_gp, network_a_wh_cord_width_gp) + 1);
  localparam tag_max_payload_width_in_manycore_complex_gp = (`BSG_MAX(network_b_wh_cord_width_gp, network_a_wh_cord_width_gp) + 1);
  localparam tag_max_payload_width_in_clk_gen_pd_gp       = `BSG_MAX(clk_gen_ds_width_gp+1, clk_gen_num_adgs_gp+4);

  // Set mininum payload width to 10-bits (fixed to 10 most of the time), no need to 
  // update trace file when adding or removing Vcahces.
  localparam tag_max_payload_width_gp = `BSG_MAX(tag_max_payload_width_in_io_complex_gp
                                        , `BSG_MAX(tag_max_payload_width_in_manycore_complex_gp
                                        , `BSG_MAX(tag_max_payload_width_in_clk_gen_pd_gp
                                        , 10)));

  // The number of bits required to represent the max payload width
  localparam tag_lg_max_payload_width_gp = `BSG_SAFE_CLOG2(tag_max_payload_width_gp + 1);

endpackage // bsg_chip_pkg

`endif // BSG_CHIP_PKG_V

