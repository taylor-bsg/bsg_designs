// 
// bsg_cache_dma_async_to_wormhole.v
// 
// Paul Gao   06/2019
//  
// 

`include "bsg_cache_dma_pkt.vh"
`include "bsg_noc_links.vh"
`include "bsg_wormhole_router.vh"

// FIXME: move to bsg_cache_dma_pkt.vh
`define declare_bsg_cache_dma_return_pkt_s        \
  typedef struct packed {                         \
    logic write_not_read;                         \
  } bsg_cache_dma_return_pkt_s

`define bsg_cache_dma_return_pkt_width  1

module bsg_cache_dma_async_to_wormhole
  
 #(// cache dma configuration
   parameter cache_addr_width_p                 = "inv"
  ,parameter data_width_p                       = "inv"
  ,parameter block_size_in_words_p              = "inv"

  // wormhole network configuration
  ,parameter flit_width_p                       = "inv"
  ,parameter dims_p                             = 2
  ,parameter int cord_markers_pos_p[dims_p:0]   = '{5, 4, 0}
  ,parameter len_width_p                        = "inv"
  
  ,localparam cord_width_lp                     = cord_markers_pos_p[dims_p]
  ,localparam bsg_cache_dma_pkt_width_lp        = `bsg_cache_dma_pkt_width(cache_addr_width_p)
  ,localparam bsg_cache_dma_return_pkt_width_lp = `bsg_cache_dma_return_pkt_width
  ,localparam bsg_ready_and_link_sif_width_lp   = `bsg_ready_and_link_sif_width(flit_width_p)
  )
  
  (// Cache DMA side
   input                                                dma_clk_i
  ,input                                                dma_reset_i
  // Sending address and write_en               
  ,input        [bsg_cache_dma_pkt_width_lp-1:0]        dma_pkt_i
  ,input                                                dma_pkt_v_i
  ,output logic                                         dma_pkt_yumi_o
  // Sending cache block                        
  ,input        [data_width_p-1:0]                      dma_data_i
  ,input                                                dma_data_v_i
  ,output logic                                         dma_data_yumi_o
  // Receiving write_en
  ,output logic [bsg_cache_dma_return_pkt_width_lp-1:0] dma_return_pkt_o
  ,output logic                                         dma_return_pkt_v_o
  ,input                                                dma_return_pkt_ready_i
  // Receiving cache block                      
  ,output logic [data_width_p-1:0]                      dma_data_o
  ,output logic                                         dma_data_v_o
  ,input                                                dma_data_ready_i
  // Wormhole coordinate IDs
  ,input        [cord_width_lp-1:0]                     dma_my_cord_i
  ,input        [cord_width_lp-1:0]                     dma_dest_cord_i

  // Wormhole side
  ,input                                          wh_clk_i
  ,input                                          wh_reset_i
  // bsg noc links
  ,input  [bsg_ready_and_link_sif_width_lp-1:0]   wh_link_i
  ,output [bsg_ready_and_link_sif_width_lp-1:0]   wh_link_o
  );
  
  localparam lg_fifo_depth_lp = 3;
  genvar i;
  
  /********************* Packet definition *********************/
  
  // Define cache DMA packet
  `declare_bsg_cache_dma_pkt_s(cache_addr_width_p);
  `declare_bsg_cache_dma_return_pkt_s;
  
  // Define wormhole header packet
  `declare_bsg_wormhole_router_header_s(cord_width_lp, len_width_p, bsg_wormhole_hdr_s);
  localparam hdr_width_lp = $bits(bsg_wormhole_hdr_s);
  
  // Wormhole header flit data structure
  typedef struct packed {
    logic [flit_width_p-hdr_width_lp-cord_width_lp-1-1:0] data;
    logic                                                 write_not_read;
    logic [cord_width_lp-1:0]                             src_cord;
    bsg_wormhole_hdr_s                                    hdr;
  } wormhole_hdr_flit_s;
  
  // each cache_dma_pkt / data word takes up 1 wormhole flit
  localparam addr_len_lp  = 1;
  localparam data_len_lp  = block_size_in_words_p;
  
  // Different wormhole packet length
  localparam fill_len_lp  = addr_len_lp;
  localparam evict_len_lp = addr_len_lp + data_len_lp;
  
  // synopsys translate_off
  initial
  begin
    // len width must be wide enough for evict packet
    assert (len_width_p >= `BSG_SAFE_CLOG2(evict_len_lp+1))
    else $error("Wormhole packet len width %d is too narrow for evict packet length %d. Please increase len width.", len_width_p, evict_len_lp+1);
    
    // flit width must be wider than cache_dma_pkt width
    assert (flit_width_p >= bsg_cache_dma_pkt_width_lp)
    else $error("Wormhole flit width %d is too narrow for cache dma packet width %d.", flit_width_p, bsg_cache_dma_pkt_width_lp);
    
    // flit width must be wider than cache data width
    assert (flit_width_p >= data_width_p)
    else $error("Wormhole flit width %d is too narrow for cache data width %d.", flit_width_p, data_width_p);
  end
  // synopsys translate_on

  
  /********************* Async fifo to wormhole link *********************/
  
  `declare_bsg_ready_and_link_sif_s(flit_width_p, bsg_ready_and_link_sif_s);
  bsg_ready_and_link_sif_s wh_link_i_cast, wh_link_o_cast;
  
  assign wh_link_i_cast = wh_link_i;
  assign wh_link_o      = wh_link_o_cast;
  
  logic  wh_async_fifo_full_lo;
  assign wh_link_o_cast.ready_and_rev = ~wh_async_fifo_full_lo;
  
  logic dma_async_fifo_enq_li, dma_async_fifo_full_lo,
        dma_async_fifo_valid_lo, dma_async_fifo_deq_li;
  logic [flit_width_p-1:0] dma_async_fifo_data_li, dma_async_fifo_data_lo;

  bsg_async_fifo
 #(.lg_size_p(lg_fifo_depth_lp)
  ,.width_p  (flit_width_p)
  ) wh_to_dma
  (.w_clk_i  (wh_clk_i)
  ,.w_reset_i(wh_reset_i)
  ,.w_enq_i  (wh_link_i_cast.v & wh_link_o_cast.ready_and_rev)
  ,.w_data_i (wh_link_i_cast.data)
  ,.w_full_o (wh_async_fifo_full_lo)

  ,.r_clk_i  (dma_clk_i)
  ,.r_reset_i(dma_reset_i)
  ,.r_deq_i  (dma_async_fifo_deq_li)
  ,.r_data_o (dma_async_fifo_data_lo)
  ,.r_valid_o(dma_async_fifo_valid_lo)
  );

  bsg_async_fifo
 #(.lg_size_p(lg_fifo_depth_lp)
  ,.width_p  (flit_width_p)
  ) dma_to_wh
  (.w_clk_i  (dma_clk_i)
  ,.w_reset_i(dma_reset_i)
  ,.w_enq_i  (dma_async_fifo_enq_li)
  ,.w_data_i (dma_async_fifo_data_li)
  ,.w_full_o (dma_async_fifo_full_lo)

  ,.r_clk_i  (wh_clk_i)
  ,.r_reset_i(wh_reset_i)
  ,.r_deq_i  (wh_link_o_cast.v & wh_link_i_cast.ready_and_rev)
  ,.r_data_o (wh_link_o_cast.data)
  ,.r_valid_o(wh_link_o_cast.v)
  );
  
  
  /********************* dma packet fifo *********************/
  
  // This two-element fifo is necessary to avoid bubble between address flit
  // and data flit for cache evict operation
  
  logic dma_pkt_fifo_valid_lo, dma_pkt_fifo_yumi_li;
  bsg_cache_dma_pkt_s dma_pkt_fifo_data_lo;
  
  logic dma_pkt_fifo_ready_lo;
  assign dma_pkt_yumi_o = dma_pkt_v_i & dma_pkt_fifo_ready_lo;
  
  bsg_two_fifo
 #(.width_p(bsg_cache_dma_pkt_width_lp))
  dma_pkt_fifo
  (.clk_i  (dma_clk_i  )
  ,.reset_i(dma_reset_i)
  ,.ready_o(dma_pkt_fifo_ready_lo)
  ,.data_i (dma_pkt_i            )
  ,.v_i    (dma_pkt_v_i          )
  ,.v_o    (dma_pkt_fifo_valid_lo)
  ,.data_o (dma_pkt_fifo_data_lo )
  ,.yumi_i (dma_pkt_fifo_yumi_li )
  );
  
  
  /********************* cache DMA <-> Wormhole *********************/
  
  localparam count_width_lp = `BSG_SAFE_CLOG2(data_len_lp);
  
  // send cache DMA packet
  bsg_cache_dma_pkt_s send_dma_pkt;
  assign send_dma_pkt = dma_pkt_fifo_data_lo;
  
  // send wormhole hdr flit
  wormhole_hdr_flit_s send_wormhole_hdr_flit;
  assign send_wormhole_hdr_flit.data           = '0;
  assign send_wormhole_hdr_flit.hdr.cord       = dma_dest_cord_i;
  assign send_wormhole_hdr_flit.src_cord       = dma_my_cord_i;
  assign send_wormhole_hdr_flit.write_not_read = send_dma_pkt.write_not_read;
  assign send_wormhole_hdr_flit.hdr.len        = (send_dma_pkt.write_not_read)?
                                                 len_width_p'(evict_len_lp)
                                               : len_width_p'(fill_len_lp);
  // receive wormhole hdr flit
  wormhole_hdr_flit_s receive_wormhole_hdr_flit;
  assign receive_wormhole_hdr_flit = dma_async_fifo_data_lo;
  assign dma_data_o                = dma_async_fifo_data_lo[data_width_p-1:0];
  
  // receive cache DMA packet
  bsg_cache_dma_return_pkt_s receive_dma_pkt;
  assign receive_dma_pkt.write_not_read = receive_wormhole_hdr_flit.write_not_read;
  assign dma_return_pkt_o               = receive_dma_pkt;

  // data flits counter
  logic clear_li, up_li, last_data_lo;
  logic [count_width_lp-1:0] count_r;

  assign last_data_lo = (count_r == count_width_lp'(data_len_lp-1));
  
  bsg_counter_clear_up
 #(.max_val_p (data_len_lp-1)
  ,.init_val_p(0)
  ) count
  (.clk_i     (dma_clk_i    )
  ,.reset_i   (dma_reset_i  )
  ,.clear_i   (clear_li     )
  ,.up_i      (up_li        )
  ,.count_o   (count_r      )
  );
  
  // State machine
  typedef enum logic [2:0] {
    RESET
   ,READY
   ,SEND_ADDR
   ,SEND_DATA
   ,RECEIVE_DATA
  } dma_state_e;
  
  dma_state_e dma_state_r, dma_state_n;
  
  // synopsys sync_set_reset "dma_reset_i"
  always_ff @(posedge dma_clk_i)
    if (dma_reset_i)
        dma_state_r <= RESET;
    else
        dma_state_r <= dma_state_n;
  
  always_comb
  begin
    // internal control
    dma_state_n            = dma_state_r;
    up_li                  = 1'b0;
    clear_li               = 1'b0;
    // send control
    dma_pkt_fifo_yumi_li    = 1'b0;
    dma_data_yumi_o        = 1'b0;
    dma_async_fifo_enq_li  = 1'b0;
    // send data
    dma_async_fifo_data_li = '0;
    // receive control
    dma_return_pkt_v_o     = 1'b0;
    dma_data_v_o           = 1'b0;
    dma_async_fifo_deq_li  = 1'b0;
    
    case (dma_state_r)
    RESET:
      begin
        dma_state_n = READY;
      end
    READY:
      begin
        // Send wormhole header flit
        dma_async_fifo_data_li = send_wormhole_hdr_flit;
        if (dma_pkt_fifo_valid_lo)
          begin
            if (~dma_async_fifo_full_lo)
              begin
                dma_async_fifo_enq_li = 1'b1;
                dma_state_n           = SEND_ADDR;
              end
          end
        else if (dma_async_fifo_valid_lo)
          begin
            dma_return_pkt_v_o        = 1'b1;
            if (dma_return_pkt_ready_i)
              begin
                dma_async_fifo_deq_li = 1'b1;
                dma_state_n       = (receive_wormhole_hdr_flit.write_not_read)? 
                                        READY : RECEIVE_DATA;
              end
          end
      end
    SEND_ADDR:
      begin
        // Send address flit
        dma_async_fifo_data_li        = flit_width_p'(send_dma_pkt);
        if (dma_pkt_fifo_valid_lo & ~dma_async_fifo_full_lo)
          begin
            dma_pkt_fifo_yumi_li      = 1'b1;
            dma_async_fifo_enq_li     = 1'b1;
            dma_state_n               = (send_dma_pkt.write_not_read)? SEND_DATA : READY;
          end
      end
    SEND_DATA:
      begin
        // Send data flit
        dma_async_fifo_data_li        = flit_width_p'(dma_data_i);
        if (dma_data_v_i & ~dma_async_fifo_full_lo)
          begin
            dma_data_yumi_o           = 1'b1;
            dma_async_fifo_enq_li     = 1'b1;
            up_li                     = (last_data_lo)? 1'b0  : 1'b1;
            clear_li                  = (last_data_lo)? 1'b1  : 1'b0;
            dma_state_n               = (last_data_lo)? READY : SEND_DATA;
          end
      end
    RECEIVE_DATA:
      begin
        if (dma_async_fifo_valid_lo)
          begin
            dma_data_v_o              = 1'b1;
            if (dma_data_ready_i)
              begin
                dma_async_fifo_deq_li = 1'b1;
                up_li                 = (last_data_lo)? 1'b0  : 1'b1;
                clear_li              = (last_data_lo)? 1'b1  : 1'b0;
                dma_state_n           = (last_data_lo)? READY : RECEIVE_DATA;
              end
          end
      end
    default:
      begin
      end
    endcase
  end
  
endmodule
