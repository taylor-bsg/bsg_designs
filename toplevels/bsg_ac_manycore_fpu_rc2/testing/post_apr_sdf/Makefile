.DEFAULT_GOAL=build

# Note: most variables that are file/dir paths are ?= because they can be
# overriden by the chip repo if this makefile is called from the chip
# infrastructure.

TOP_DIR            ?= $(shell git rev-parse --show-toplevel)
ROOT_DIR           ?= $(abspath $(TOP_DIR)/../)
BSG_DESIGNS_TARGET ?= $(notdir $(abspath ../../))

BSG_WORK_DIR := $(abspath ./)
BSG_OUT_DIR  := $(BSG_WORK_DIR)/out
BSG_OUT_SIM  := $(BSG_OUT_DIR)/simv
NBF_FILE_OPT := +nbf_file=../spmd/hello/hello.nbf

include $(ROOT_DIR)/bsg_cadenv/cadenv.mk

# Repository setup
export BSG_DESIGNS_DIR        ?= $(ROOT_DIR)/bsg_designs
export BSG_DESIGNS_TARGET_DIR ?= $(BSG_DESIGNS_DIR)/toplevels/$(BSG_DESIGNS_TARGET)
export BASEJUMP_STL_DIR       ?= $(ROOT_DIR)/basejump_stl
export BSG_PACKAGING_DIR      ?= $(ROOT_DIR)/bsg_packaging
export BSG_MANYCORE_DIR       ?= $(ROOT_DIR)/bsg_manycore
export BOARD_DIR              ?= $(ROOT_DIR)/board

export TESTING_BSG_DESIGNS_DIR        ?= $(BSG_OUT_DIR)/root/bsg_designs
export TESTING_BASEJUMP_STL_DIR       ?= $(BSG_OUT_DIR)/root/basejump_stl
export TESTING_BSG_PACKAGING_DIR      ?= $(BSG_OUT_DIR)/root/bsg_packaging
export TESTING_BSG_MANYCORE_DIR       ?= $(BSG_OUT_DIR)/root/bsg_manycore
export TESTING_BOARD_DIR              ?= $(BSG_OUT_DIR)/root/board
export TESTING_BSG_DESIGNS_TARGET_DIR ?= $(TESTING_BSG_DESIGNS_DIR)/toplevels/$(BSG_DESIGNS_TARGET)

export BSG_PACKAGE           ?=uw_bga
export BSG_PINOUT            ?=bsg_asic_cloud
export BSG_PACKAGING_FOUNDRY ?=gf_14_invecas_1p8v
export BSG_PADMAPPING        ?=default

# Additional setup for RTL-Hard
export BSG_CHIP_DIR             ?= $(ROOT_DIR)/bsg_14
export MEMGEN_RUN_DIR           ?= $(BSG_CHIP_DIR)/pdk_prep/memgen
export BSG_TOPLEVEL_DESIGN_TYPE ?=bsg_chip
export BSG_TARGET_PROCESS       ?=gf_14

include $(BSG_CHIP_DIR)/cad/pdk_setup/pdk_setup_dir.mk

########################################
## VCS OPTIONS
########################################

# Common VCS Options (will be used most of the time by all corners)
VCS_OPTIONS := -full64
VCS_OPTIONS += -notice
#VCS_OPTIONS += -debug_pp
#VCS_OPTIONS += +vcs+vcdpluson
VCS_OPTIONS += -V
VCS_OPTIONS += +v2k
VCS_OPTIONS += -sverilog -assert svaext
VCS_OPTIONS += +noportcoerce
VCS_OPTIONS += +vc
VCS_OPTIONS += +vcs+loopreport
VCS_OPTIONS += -timescale=1ps/1ps
VCS_OPTIONS += -diag timescale 
VCS_OPTIONS += -o $(BSG_OUT_SIM)
VCS_OPTIONS += -Mdir=$(BSG_OUT_DIR)
VCS_OPTIONS += -top bsg_config bsg_config.v
VCS_OPTIONS += +warn=all,noOPD,noTMR,noTFIPC,noIWNF

########################################
## Chip and Testing Filelists and Liblists
########################################

BSG_TOP_SIM_MODULE = bsg_asic_cloud_pcb
BSG_CHIP_INSTANCE_PATH = $(BSG_TOP_SIM_MODULE).IC0.ASIC

VCS_OPTIONS += +define+BSG_TOP_SIM_MODULE=$(BSG_TOP_SIM_MODULE)
VCS_OPTIONS += +define+BSG_CHIP_INSTANCE_PATH=$(BSG_CHIP_INSTANCE_PATH)

export BSG_CHIP_LIBRARY_NAME = bsg_chip
export BSG_CHIP_FILELIST = $(BSG_OUT_DIR)/$(BSG_CHIP_LIBRARY_NAME).filelist
export BSG_CHIP_LIBRARY = $(BSG_OUT_DIR)/$(BSG_CHIP_LIBRARY_NAME).library

VCS_OPTIONS += +define+BSG_CHIP_LIBRARY_NAME=$(BSG_CHIP_LIBRARY_NAME)
VCS_OPTIONS += -f $(BSG_CHIP_FILELIST)
VCS_OPTIONS += -libmap $(BSG_CHIP_LIBRARY)

export BSG_DESIGNS_TESTING_LIBRARY_NAME = bsg_design_testing
export BSG_DESIGNS_TESTING_FILELIST = $(BSG_OUT_DIR)/$(BSG_DESIGNS_TESTING_LIBRARY_NAME).filelist
export BSG_DESIGNS_TESTING_LIBRARY = $(BSG_OUT_DIR)/$(BSG_DESIGNS_TESTING_LIBRARY_NAME).library

VCS_OPTIONS += +define+BSG_DESIGNS_TESTING_LIBRARY_NAME=$(BSG_DESIGNS_TESTING_LIBRARY_NAME)
VCS_OPTIONS += -f $(BSG_DESIGNS_TESTING_FILELIST)
VCS_OPTIONS += -libmap $(BSG_DESIGNS_TESTING_LIBRARY)

$(BSG_CHIP_FILELIST): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_CHIP_LIBRARY): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_DESIGNS_TESTING_FILELIST): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_DESIGNS_TESTING_LIBRARY): $(BSG_OUT_DIR)/root
	/usr/bin/tclsh bsg_config.tcl

########################################
## Trace Replay Roms
########################################

BSG_TRACE_FILES := $(notdir $(wildcard $(BSG_WORK_DIR)/../traces/*.tr))
BSG_TRACE_ROMS  := $(addprefix $(BSG_OUT_DIR)/,${BSG_TRACE_FILES:.tr=_rom.v})

$(BSG_OUT_DIR)/%_rom.v: $(BSG_WORK_DIR)/../traces/%.tr | $(BSG_OUT_DIR)
	$(BASEJUMP_STL_DIR)/bsg_mem/bsg_ascii_to_rom.py $< $*_rom > $@

VCS_OPTIONS += $(addprefix -v ,$(BSG_TRACE_ROMS))

########################################
## DRAM Definitions
########################################

VCS_OPTIONS += +define+den2048Mb+sg5+x16+FULL_MEM

########################################
## SDF Annotation
########################################

sdf_corner = max

#corner = sspg0p72v125c
#rc_corner = SigCmax

ifndef corner
  corner = tt0p80v25c
endif

ifndef rc_corner
  rc_corner = nominal
endif

VCS_OPTIONS += +sdfverbose +neg_tchk +overlap +multisource_int_delays
VCS_OPTIONS += -negdelay
#VCS_OPTIONS += +notimingcheck
VCS_OPTIONS += -sdf $(sdf_corner):$(BSG_CHIP_INSTANCE_PATH):$(BSG_CHIP_DIR)/current_build/ptsi/$(BSG_TOPLEVEL_DESIGN_TYPE)/results/$(corner)/$(rc_corner)/$(BSG_TOPLEVEL_DESIGN_TYPE).sdf

ifdef CORNER_SS
  VCS_OPTIONS += +define+CORNER_SS=1
endif

EXPAND_VERILOG = $(BSG_DESIGNS_DIR)/util/expand_verilog_hierarchy.py
BSG_CDC_PATH_FILE = $(BSG_OUT_DIR)/cdc_instance_list.v
BSG_CHIP_NETLIST_V = $(BSG_OUT_DIR)/$(BSG_TOPLEVEL_DESIGN_TYPE)_chip_finish.v
BSG_CHIP_NETLIST_V_GZ = $(BSG_CHIP_DIR)/current_build/pnr/$(BSG_TOPLEVEL_DESIGN_TYPE)/results/$(BSG_TOPLEVEL_DESIGN_TYPE)_chip_finish.v.gz

VCS_OPTIONS += +optconfigfile+$(BSG_CDC_PATH_FILE)

# grab all of the sync flops
$(BSG_CDC_PATH_FILE): $(BSG_CHIP_NETLIST_V_GZ)
	cp $(BSG_CHIP_NETLIST_V_GZ) $(BSG_CHIP_NETLIST_V).gz
	gzip -d $(BSG_CHIP_NETLIST_V).gz
	$(EXPAND_VERILOG) $(BSG_CHIP_NETLIST_V)| grep "_hard_sync_int" | awk '{ print "instance { $(BSG_CHIP_INSTANCE_PATH)." $$$$2 " } { noTiming };" }' | sort > $@

########################################
## Run Targets
########################################


build: $(BSG_OUT_SIM)
$(BSG_OUT_SIM): $(BSG_CHIP_FILELIST) $(BSG_CHIP_LIBRARY) $(BSG_DESIGNS_TESTING_FILELIST) $(BSG_DESIGNS_TESTING_LIBRARY) $(BSG_TRACE_ROMS) $(BSG_CDC_PATH_FILE)
	$(VCS) $(VCS_OPTIONS) | tee -i $(BSG_OUT_DIR)/build.log
	cp Makefile.machine.include $(BSG_OUT_DIR)


$(BSG_OUT_DIR)/root: | $(BSG_OUT_DIR)
	ln -nsf $(ROOT_DIR) $@

$(BSG_OUT_DIR):
	mkdir -p $@

clean:
	rm -rf $(BSG_OUT_DIR)
	rm -rf DVEfiles
	rm -rf stack.info.*
	rm -f  vc_hdrs.h
	rm -f  vcdplus.vpd
	rm -f  inter.vpd
	rm -f  ucli.key
	rm -f  sdfAnnotateInfo

