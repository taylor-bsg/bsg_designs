`timescale 1ps/1ps

`ifndef IO_MASTER_CLK_PERIOD
  `define IO_MASTER_CLK_PERIOD 1000.0
`endif

`ifndef ROUTER_CLK_PERIOD
  `define ROUTER_CLK_PERIOD 1000.0
`endif

`ifndef TAG_CLK_PERIOD
  `define TAG_CLK_PERIOD 10000.0
`endif

module bsg_asic_cloud_wrapper
  
import bsg_chip_pkg::*;
  
 #(localparam bsg_manycore_link_sif_width_lp = `bsg_manycore_link_sif_width(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp)
  )
  
  (// Manycore clk and reset
   input                    mc_clk_i
  ,output                   mc_reset_o
  ,output                   init_done_o
   
  // Manycore links
  ,input  [bsg_manycore_link_sif_width_lp-1:0] mc_links_sif_i
  ,output [bsg_manycore_link_sif_width_lp-1:0] mc_links_sif_o
  );

  //
  // Comm link wires (Between GW and IC0)
  //

  wire IC0_GW_CL_CLK;
  wire IC0_GW_CL_V;
  wire IC0_GW_CL_TKN;
  wire IC0_GW_CL_D0;
  wire IC0_GW_CL_D1;
  wire IC0_GW_CL_D2;
  wire IC0_GW_CL_D3;
  wire IC0_GW_CL_D4;
  wire IC0_GW_CL_D5;
  wire IC0_GW_CL_D6;
  wire IC0_GW_CL_D7;
  wire IC0_GW_CL_D8;

  wire GW_IC0_CL_CLK;
  wire GW_IC0_CL_V;
  wire GW_IC0_CL_TKN;
  wire GW_IC0_CL_D0;
  wire GW_IC0_CL_D1;
  wire GW_IC0_CL_D2;
  wire GW_IC0_CL_D3;
  wire GW_IC0_CL_D4;
  wire GW_IC0_CL_D5;
  wire GW_IC0_CL_D6;
  wire GW_IC0_CL_D7;
  wire GW_IC0_CL_D8;
  
  wire GW_TAG_CLKO;
  wire GW_TAG_DATAO;
  wire GW_IC0_TAG_EN;
  wire GW_IC1_TAG_EN;
  
  wire GW_CLKA;
  wire GW_CLKB;
  wire GW_CLKC;
  
  logic io_master_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`IO_MASTER_CLK_PERIOD)) io_master_clk_gen (.o(io_master_clk));

  logic router_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`ROUTER_CLK_PERIOD)) router_clk_gen (.o(router_clk));

  logic tag_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`TAG_CLK_PERIOD)) tag_clk_gen (.o(tag_clk));
  
  bsg_gateway_chip_core 
   #(.num_asic_p( 1 )
    ) GW
    (.mc_clk_i       ( mc_clk_i )
    ,.mc_reset_o     ( mc_reset_o )
    ,.init_done_o    ( init_done_o )
    
    ,.io_master_clk_i( io_master_clk )
    ,.router_clk_i   ( router_clk )
    ,.tag_clk_i      ( tag_clk )
  
    ,.mc_links_sif_i ( mc_links_sif_i )
    ,.mc_links_sif_o ( mc_links_sif_o )
    
    ,.ci_clk_i       ( IC0_GW_CL_CLK )
    ,.ci_v_i         ( IC0_GW_CL_V )
    ,.ci_data_i      ( {IC0_GW_CL_D8, IC0_GW_CL_D7, IC0_GW_CL_D6, IC0_GW_CL_D5, IC0_GW_CL_D4, IC0_GW_CL_D3, IC0_GW_CL_D2, IC0_GW_CL_D1, IC0_GW_CL_D0} )
    ,.ci_tkn_o       ( IC0_GW_CL_TKN )
    
    ,.co_clk_i       (  )
    ,.co_v_i         (  )
    ,.co_data_i      (  )
    ,.co_tkn_o       (  )
                     
    ,.ci2_clk_o      ( GW_IC0_CL_CLK )
    ,.ci2_v_o        ( GW_IC0_CL_D4 )
    ,.ci2_data_o     ( {GW_IC0_CL_D0, GW_IC0_CL_D1, GW_IC0_CL_D2, GW_IC0_CL_V, GW_IC0_CL_D3, GW_IC0_CL_D8, GW_IC0_CL_D7, GW_IC0_CL_D5, GW_IC0_CL_D6} )
    ,.ci2_tkn_i      ( GW_IC0_CL_TKN )
                     
    ,.co2_clk_o      (  )
    ,.co2_v_o        (  )
    ,.co2_data_o     (  )
    ,.co2_tkn_i      (  )
                     
    ,.tag_clk_o      ( GW_TAG_CLKO )
    ,.tag_en_o       ( {GW_IC1_TAG_EN ,GW_IC0_TAG_EN} )
    ,.tag_data_o     ( GW_TAG_DATAO )
                     
    ,.clk_a_o        ( GW_CLKA )
    ,.clk_b_o        ( GW_CLKB )
    ,.clk_c_o        ( GW_CLKC )
    );
  
  bsg_chip IC0
  (.p_ci_clk_i ()
  ,.p_ci_v_i   ()
  ,.p_ci_tkn_o ()
  ,.p_ci_0_i   ()
  ,.p_ci_1_i   ()
  ,.p_ci_2_i   ()
  ,.p_ci_3_i   ()
  ,.p_ci_4_i   ()
  ,.p_ci_5_i   ()
  ,.p_ci_6_i   ()
  ,.p_ci_7_i   ()
  ,.p_ci_8_i   ()

  ,.p_co_clk_i (GW_IC0_CL_CLK)
  ,.p_co_v_i   (GW_IC0_CL_V)
  ,.p_co_tkn_o (GW_IC0_CL_TKN)
  ,.p_co_0_i   (GW_IC0_CL_D0)
  ,.p_co_1_i   (GW_IC0_CL_D1)
  ,.p_co_2_i   (GW_IC0_CL_D2)
  ,.p_co_3_i   (GW_IC0_CL_D3)
  ,.p_co_4_i   (GW_IC0_CL_D4)
  ,.p_co_5_i   (GW_IC0_CL_D5)
  ,.p_co_6_i   (GW_IC0_CL_D6)
  ,.p_co_7_i   (GW_IC0_CL_D7)
  ,.p_co_8_i   (GW_IC0_CL_D8)

  ,.p_ci2_clk_o()
  ,.p_ci2_v_o  ()
  ,.p_ci2_tkn_i()
  ,.p_ci2_0_o  ()
  ,.p_ci2_1_o  ()
  ,.p_ci2_2_o  ()
  ,.p_ci2_3_o  ()
  ,.p_ci2_4_o  ()
  ,.p_ci2_5_o  ()
  ,.p_ci2_6_o  ()
  ,.p_ci2_7_o  ()
  ,.p_ci2_8_o  ()

  ,.p_co2_clk_o(IC0_GW_CL_CLK)
  ,.p_co2_v_o  (IC0_GW_CL_D4)
  ,.p_co2_tkn_i(IC0_GW_CL_TKN)
  ,.p_co2_0_o  (IC0_GW_CL_D8)
  ,.p_co2_1_o  (IC0_GW_CL_D7)
  ,.p_co2_2_o  (IC0_GW_CL_V)
  ,.p_co2_3_o  (IC0_GW_CL_D6)
  ,.p_co2_4_o  (IC0_GW_CL_D5)
  ,.p_co2_5_o  (IC0_GW_CL_D3)
  ,.p_co2_6_o  (IC0_GW_CL_D2)
  ,.p_co2_7_o  (IC0_GW_CL_D1)
  ,.p_co2_8_o  (IC0_GW_CL_D0)


  ,.p_ddr_ck_p_o()
  ,.p_ddr_ck_n_o()

  ,.p_ddr_cke_o()

  ,.p_ddr_cs_n_o ()
  ,.p_ddr_ras_n_o()
  ,.p_ddr_cas_n_o()
  ,.p_ddr_we_n_o ()

  ,.p_ddr_reset_n_o ()
  ,.p_ddr_odt_o     ()

  ,.p_ddr_ba_0_o()
  ,.p_ddr_ba_1_o()
  ,.p_ddr_ba_2_o()

  ,.p_ddr_addr_0_o ()
  ,.p_ddr_addr_1_o ()
  ,.p_ddr_addr_2_o ()
  ,.p_ddr_addr_3_o ()
  ,.p_ddr_addr_4_o ()
  ,.p_ddr_addr_5_o ()
  ,.p_ddr_addr_6_o ()
  ,.p_ddr_addr_7_o ()
  ,.p_ddr_addr_8_o ()
  ,.p_ddr_addr_9_o ()
  ,.p_ddr_addr_10_o()
  ,.p_ddr_addr_11_o()
  ,.p_ddr_addr_12_o()
  ,.p_ddr_addr_13_o()
  ,.p_ddr_addr_14_o()
  ,.p_ddr_addr_15_o()

  ,.p_ddr_dm_0_o()
  ,.p_ddr_dm_1_o()
  ,.p_ddr_dm_2_o()
  ,.p_ddr_dm_3_o()

  ,.p_ddr_dqs_p_0_io()
  ,.p_ddr_dqs_n_0_io()
  ,.p_ddr_dqs_p_1_io()
  ,.p_ddr_dqs_n_1_io()
  ,.p_ddr_dqs_p_2_io()
  ,.p_ddr_dqs_n_2_io()
  ,.p_ddr_dqs_p_3_io()
  ,.p_ddr_dqs_n_3_io()

  ,.p_ddr_dq_0_io ()
  ,.p_ddr_dq_1_io ()
  ,.p_ddr_dq_2_io ()
  ,.p_ddr_dq_3_io ()
  ,.p_ddr_dq_4_io ()
  ,.p_ddr_dq_5_io ()
  ,.p_ddr_dq_6_io ()
  ,.p_ddr_dq_7_io ()
  ,.p_ddr_dq_8_io ()
  ,.p_ddr_dq_9_io ()
  ,.p_ddr_dq_10_io()
  ,.p_ddr_dq_11_io()
  ,.p_ddr_dq_12_io()
  ,.p_ddr_dq_13_io()
  ,.p_ddr_dq_14_io()
  ,.p_ddr_dq_15_io()
  ,.p_ddr_dq_16_io()
  ,.p_ddr_dq_17_io()
  ,.p_ddr_dq_18_io()
  ,.p_ddr_dq_19_io()
  ,.p_ddr_dq_20_io()
  ,.p_ddr_dq_21_io()
  ,.p_ddr_dq_22_io()
  ,.p_ddr_dq_23_io()
  ,.p_ddr_dq_24_io()
  ,.p_ddr_dq_25_io()
  ,.p_ddr_dq_26_io()
  ,.p_ddr_dq_27_io()
  ,.p_ddr_dq_28_io()
  ,.p_ddr_dq_29_io()
  ,.p_ddr_dq_30_io()
  ,.p_ddr_dq_31_io()

  ,.p_bsg_tag_clk_i  (GW_TAG_CLKO)
  ,.p_bsg_tag_en_i   (GW_IC0_TAG_EN)
  ,.p_bsg_tag_data_i (GW_TAG_DATAO)
  ,.p_bsg_tag_clk_o  ()
  ,.p_bsg_tag_data_o ()

  ,.p_clk_A_i(GW_CLKA)
  ,.p_clk_B_i(GW_CLKB)
  ,.p_clk_C_i(GW_CLKC)

  ,.p_clk_o  ()

  ,.p_sel_0_i()
  ,.p_sel_1_i()
  ,.p_sel_2_i()

  ,.p_clk_async_reset_i  ()
  ,.p_core_async_reset_i ()

  ,.p_misc_o ()
  );

endmodule
