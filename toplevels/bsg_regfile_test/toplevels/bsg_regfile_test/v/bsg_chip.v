
//==============================================================================
//
// BSG CHIP
//
// This is the toplevel for the ASIC. This chip uses the UW BGA package found
// inside bsg_packaging/uw_bga. For physical design reasons, the input pins
// have been swizzled (ie. re-arranged) from their original meaning. We use the
// bsg_chip_swizzle_adapter in every ASIC to abstract away detail.
//

`include "bsg_defines.v"

module bsg_chip
(input logic clk_i
 , output logic reset_i

 , input rd_w_v_i
 , input [4:0] rd_addr_i
 , input [63:0] rd_data_i

 , input rs1_v_i
 , input [4:0] rs1_addr_i
 , output [63:0] rs1_data_o

 , input rs2_v_i
 , input [4:0] rs2_addr_i
 , output [63:0] rs2_data_o

 , input [1:0] sel_i
 );

  logic [63:0] hard_rs1_data_lo, hard_rs2_data_lo;
  bsg_mem_2r1w_sync
   #(.width_p(64), .els_p(32), .harden_p(1))
   regfile_hard
    (.clk_i(clk_i)
     ,.reset_i(reset_i)

     ,.w_v_i(rd_w_v_i)
     ,.w_addr_i(rd_addr_i)
     ,.w_data_i(rd_data_i)

     ,.r0_v_i(rs1_v_i)
     ,.r0_addr_i(rs1_addr_i)
     ,.r0_data_o(hard_rs1_data_lo)

     ,.r1_v_i(rs2_v_i)
     ,.r1_addr_i(rs2_addr_i)
     ,.r1_data_o(hard_rs2_data_lo)
     );

  logic [63:0] synth_rs1_data_lo, synth_rs2_data_lo;
  bsg_mem_2r1w_sync
   #(.width_p(64), .els_p(32), .harden_p(0))
   regfile_synth
    (.clk_i(clk_i)
     ,.reset_i(reset_i)

     ,.w_v_i(rd_w_v_i)
     ,.w_addr_i(rd_addr_i)
     ,.w_data_i(rd_data_i)

     ,.r0_v_i(rs1_v_i)
     ,.r0_addr_i(rs1_addr_i)
     ,.r0_data_o(synth_rs1_data_lo)

     ,.r1_v_i(rs2_v_i)
     ,.r1_addr_i(rs2_addr_i)
     ,.r1_data_o(synth_rs2_data_lo)
     );

  logic [63:0] async_rs1_data_lo, async_rs2_data_lo;
  bsg_mem_2r1w
   #(.width_p(64), .els_p(32))
   regfile_async
    (.w_clk_i(clk_i)
     ,.w_reset_i(reset_i)

     ,.w_v_i(rd_w_v_i)
     ,.w_addr_i(rd_addr_i)
     ,.w_data_i(rd_data_i)

     ,.r0_v_i(rs1_v_i)
     ,.r0_addr_i(rs1_addr_i)
     ,.r0_data_o(async_rs1_data_lo)

     ,.r1_v_i(rs2_v_i)
     ,.r1_addr_i(rs2_addr_i)
     ,.r1_data_o(async_rs2_data_lo)
     );

  logic [63:0] latch_rs1_data_lo, latch_rs2_data_lo;
  bsg_mem_2r1w_latch
   #(.width_p(64), .els_p(32))
   regfile_latch
    (.w_clk_i(clk_i)
     ,.w_reset_i(reset_i)

     ,.w_v_i(rd_w_v_i)
     ,.w_addr_i(rd_addr_i)
     ,.w_data_i(rd_data_i)

     ,.r0_v_i(rs1_v_i)
     ,.r0_addr_i(rs1_addr_i)
     ,.r0_data_o(latch_rs1_data_lo)

     ,.r1_v_i(rs2_v_i)
     ,.r1_addr_i(rs2_addr_i)
     ,.r1_data_o(latch_rs2_data_lo)
     );

  bsg_mux
   #(.width_p(64), .els_p(4))
   output_rs1_mux
    (.data_i({latch_rs1_data_lo, async_rs1_data_lo, synth_rs1_data_lo, hard_rs1_data_lo})
     ,.sel_i(sel_i)
     ,.data_o(rs1_data_o)
     );

  bsg_mux
   #(.width_p(64), .els_p(4))
   output_rs2_mux
    (.data_i({latch_rs2_data_lo, async_rs2_data_lo, synth_rs2_data_lo, hard_rs2_data_lo})
     ,.sel_i(sel_i)
     ,.data_o(rs2_data_o)
     );

endmodule

