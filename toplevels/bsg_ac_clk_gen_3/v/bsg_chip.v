`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

//==============================================================================
//
// BSG CHIP
//
// This is the toplevel for the ASIC. This chip uses the UW BGA package found
// inside bsg_packaging/uw_bga. For physical design reasons, the input pins
// have been swizzled (ie. re-arranged) from their original meaning. We use the
// bsg_chip_swizzle_adapter in every ASIC to abstract away detail.
//

module bsg_chip

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

`include "bsg_pinout.v"
`include "bsg_iopads.v"

  // Creating a wrapper so that we don't need the IO cells.
  bsg_chip_wrapper
    wrapper
      (.*); 

endmodule


