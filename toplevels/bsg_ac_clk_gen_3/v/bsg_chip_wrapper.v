module bsg_chip_wrapper

  import bsg_tag_pkg::*;

   import bsg_chip_pkg::*;


   ( input bsg_tag_clk_i_int
     , input bsg_tag_en_i_int
     , input bsg_tag_data_i_int

     , input clk_A_i_int
     , input clk_B_i_int
     , input clk_C_i_int

     , output logic co2_0_o_int
     , output logic co2_1_o_int
     , output logic co2_2_o_int
     );


     //////////////////////////////////////////////////
     //
     // BSG Tag Master Instance
     //

     // All tag lines from the btm
   bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;


   bsg_tag_s [clk_gen_num_endpoints_gp-1:0] osc_tag_lines_lo;

   bsg_tag_s [clk_gen_num_endpoints_gp-1:0] osc_trigger_tag_lines_lo;

   bsg_tag_s [clk_gen_num_endpoints_gp-1:0] ds_tag_lines_lo;

   bsg_tag_s [clk_gen_num_endpoints_gp-1:0] sel_tag_lines_lo;


   wire bsg_tag_s async_reset_tag_lines_lo = tag_lines_lo[0];


   assign osc_tag_lines_lo         = tag_lines_lo[clk_gen_num_endpoints_gp:1];

   assign osc_trigger_tag_lines_lo = tag_lines_lo[clk_gen_num_endpoints_gp*2:clk_gen_num_endpoints_gp+1];

   assign ds_tag_lines_lo          = tag_lines_lo[clk_gen_num_endpoints_gp*3:clk_gen_num_endpoints_gp*2+1];

   assign sel_tag_lines_lo         = tag_lines_lo[clk_gen_num_endpoints_gp*4:clk_gen_num_endpoints_gp*3+1];


     // Rename the tag lines (determines the client ID!)
     //wire bsg_tag_s prev_link_io_tag_lines_lo   = tag_lines_lo[13];
     //wire bsg_tag_s prev_link_core_tag_lines_lo = tag_lines_lo[14];
     //wire bsg_tag_s prev_ct_core_tag_lines_lo   = tag_lines_lo[15];
     //wire bsg_tag_s next_link_io_tag_lines_lo   = tag_lines_lo[16];
     //wire bsg_tag_s next_link_core_tag_lines_lo = tag_lines_lo[17];
     //wire bsg_tag_s next_ct_core_tag_lines_lo   = tag_lines_lo[18];
     //wire bsg_tag_s router_core_tag_lines_lo    = tag_lines_lo[19];

   // BSG tag master instance
   bsg_tag_master #(.els_p( tag_num_clients_gp )
                    ,.lg_width_p( tag_lg_max_payload_width_gp )
                    )
   btm
     (.clk_i      ( bsg_tag_clk_i_int )
      ,.data_i     ( bsg_tag_en_i_int ? bsg_tag_data_i_int : 1'b0 )
      ,.en_i       ( 1'b1 )
      ,.clients_r_o( tag_lines_lo )
      );

/*
   this code is just to test some synthesis results for hashing in gf14
   bsg_ac_clk_gen is a fast test

   wire [31:0] foo,index;

   bsg_tag_client #(.width_p(32)) btc
     (.bsg_tag_i(tag_lines_lo[0] )
      ,.recv_clk_i(clk_A_i_int)
      ,.recv_reset_i(1'b0)
      ,.recv_new_r_o()
      ,.recv_data_r_o(foo)
      );

   bsg_tag_client #(.width_p(32)) btc_index
     (.bsg_tag_i(tag_lines_lo[1] )
      ,.recv_clk_i(clk_A_i_int)
      ,.recv_reset_i(1'b0)
      ,.recv_new_r_o()
      ,.recv_data_r_o(index)
      );

   wire [31:0]        foo_div_9 = foo/9;
   wire [31:0]       foo_mod_9 = foo%9;

   assign co2_2_o_int = foo_div_9[index[4:0]];
   assign co2_1_o_int = foo_mod_9[index[4:0]];
*/
   //////////////////////////////////////////////////
   //
   // BSG Clock Generator Power Domain
   //

   logic router_clk_lo;

   logic io_master_clk_lo;

   logic manycore_clk_lo;


   bsg_clk_gen_power_domain #(.num_clk_endpoint_p( clk_gen_num_endpoints_gp )
                              ,.ds_width_p( clk_gen_ds_width_gp )
                              ,.num_adgs_p( clk_gen_num_adgs_gp )
                              )
   clk_gen_pd
     (.async_reset_tag_lines_i ( async_reset_tag_lines_lo )
      ,.osc_tag_lines_i         ( osc_tag_lines_lo )
      ,.osc_trigger_tag_lines_i ( osc_trigger_tag_lines_lo )
      ,.ds_tag_lines_i          ( ds_tag_lines_lo )
      ,.sel_tag_lines_i         ( sel_tag_lines_lo )

      ,.ext_clk_i({ clk_A_i_int })

      ,.clk_o({ manycore_clk_lo })
      //,.clk_o()
      );

   assign co2_0_o_int = manycore_clk_lo;

endmodule
