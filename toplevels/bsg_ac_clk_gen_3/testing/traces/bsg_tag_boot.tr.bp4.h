# bp4 payload is 9 bits and tag width is 6 bits
#

@define BSG_CONCAT(x,y) x @@ y
@define BSG_CONCAT3(a,b,c) a @@ b @@ c
@define BSG_CONCAT4(a,b,c,d) a @@ b @@ c @@ d
@define BSG_CONCAT5(a,b,c,d,e) a @@ b @@ c @@ d @@ e
@define BSG_CONCAT6(a,b,c,d,e,f) a @@ b @@ c @@ d @@ e @@ f


# ASYNC RESET
@define OSC_ASYNC_RESET_LEN 0001
@define OSC_ASYNC_RESET_LEN_UNARY 000000001

# OSC
@define OSC_RAW_LEN 0101
@define OSC_RAW_LEN_UNARY 000011111

# OSC TRIGGER
@define OSC_TRIGGER_LEN 0001
@define OSC_TRIGGER_LEN_UNARY 000000001

# Downsampler
@define DS_LEN 0111
@define DS_LEN_UNARY 001111111

# Clockselect (Mux)
@define SM_LEN 0010
@define SM_LEN_UNARY 000000011

# RESET TAG CLIENT MACRO
#SEND  en   id=0  r l=1  (ASYNC RESET)
# 0001___01___00001_0_101___0011111

@define TAG_CLIENT_RESET(id6, len, len_unary) BSG_CONCAT6(0001___01___,id6,_0_,len,___,len_unary)

### Select the output clock (0=raw osc, 1=ds osc, 2=ext, 3=off)
#
# Since this is unsynchronized, it will create havoc on your clock
# if you change it live.
#
# SEND  en   id=4 d l=2   {clk_select}
# 0001___01___00100_1_010___0000000

@define SM_RAW_OSC 00
@define SM_DS      01
@define SM_EXT     10
@define SM_OFF     11

@define SELECT_MUX(id,bits2) BSG_CONCAT4(0001___01___,id,_1_0010___0000000,bits2)

# Oscillator asynchronous reset
# SEND  en   id=0  d l=1   {async_reset}
# this should clear the synchronizer in the trigger as well

@define OSC_ASYNC_RESET_HIGH(id) BSG_CONCAT3(0001___01___,id,_1_0001___000000001)
@define OSC_ASYNC_RESET_LOW(id)  BSG_CONCAT3(0001___01___,id,_1_0001___000000000)

### Set osc triggers low
#SEND  en   id=2  r l=1  (OSC TRIGGER LOW)
#0001___01___00010_1_001___0000000

@define OSC_TRIGGER_LOW(id) BSG_CONCAT3(0001___01___,id,_1_0001___000000000)

### Set osc triggers high
###
### When trigger is high, the oscillator will be exposed
### to the raw bits from the BSG_TAG
### You should set the trigger low before you start scanning
### in data.
### Note: if oscillator is in ASIC_RESET_HIGH, the trigger will not work
#SEND  en   id=2  d l=1 (OSC TRIGGER HIGH)

@define OSC_TRIGGER_HIGH(id) BSG_CONCAT3(0001___01___,id,_1_0001___000000001)

### Program the raw oscillators speed
### Do this when trigger is low to avoid disturbing the oscillator
### then raise and lower trigger while oscillator is active
#SEND  en   id=1  d l=5   {adt, cdt, fdt}
#0001___01___00001_1_101___0000000

@define OSC_RAW_VAL(id,bits5) BSG_CONCAT4(0001___01___,id,_1_0101___0000,bits5)

# Downsampler reset
#SEND  en   id=3  d l=7   {ds_val, reset}
#0001___01___00011_1_111___0000001

@define DS_RESET_HIGH(id) BSG_CONCAT3(0001___01___,id,_1_0111___000000001)

### Only takes place when osccilator is valid
### DS=downsampler

@define DS_RESET_LOW_AND_VAL(id,bits6) BSG_CONCAT5(0001___01___,id,_1_0111___00,bits6,0)

@define TRY_ALL_DS_VALUES(id) DS_RESET_LOW_AND_VAL(id,111111)

@define TRY_ALL_DS_VALUES_X(id)                         \
DS_RESET_LOW_AND_VAL(id,000000)                          \n\
DS_RESET_LOW_AND_VAL(id,000001)                          \n\
DS_RESET_LOW_AND_VAL(id,000010)                          \n\
DS_RESET_LOW_AND_VAL(id,000011)                          \n\
DS_RESET_LOW_AND_VAL(id,000100)                          \n\
DS_RESET_LOW_AND_VAL(id,000101)                          \n\
DS_RESET_LOW_AND_VAL(id,000110)                          \n\
DS_RESET_LOW_AND_VAL(id,000111)                          \n\
DS_RESET_LOW_AND_VAL(id,001000)                          \n\
DS_RESET_LOW_AND_VAL(id,001001)                          \n\
DS_RESET_LOW_AND_VAL(id,001010)                          \n\
DS_RESET_LOW_AND_VAL(id,001011)                          \n\
DS_RESET_LOW_AND_VAL(id,001100)                          \n\
DS_RESET_LOW_AND_VAL(id,001101)                          \n\
DS_RESET_LOW_AND_VAL(id,001110)                          \n\
DS_RESET_LOW_AND_VAL(id,001111)                          \n\
DS_RESET_LOW_AND_VAL(id,010000)                          \n\
DS_RESET_LOW_AND_VAL(id,010001)                          \n\
DS_RESET_LOW_AND_VAL(id,010010)                          \n\
DS_RESET_LOW_AND_VAL(id,010011)                          \n\
DS_RESET_LOW_AND_VAL(id,010100)                          \n\
DS_RESET_LOW_AND_VAL(id,010101)                          \n\
DS_RESET_LOW_AND_VAL(id,010110)                          \n\
DS_RESET_LOW_AND_VAL(id,010111)                          \n\
DS_RESET_LOW_AND_VAL(id,011000)                          \n\
DS_RESET_LOW_AND_VAL(id,011001)                          \n\
DS_RESET_LOW_AND_VAL(id,011010)                          \n\
DS_RESET_LOW_AND_VAL(id,011011)                          \n\
DS_RESET_LOW_AND_VAL(id,011100)                          \n\
DS_RESET_LOW_AND_VAL(id,011101)                          \n\
DS_RESET_LOW_AND_VAL(id,011110)                          \n\
DS_RESET_LOW_AND_VAL(id,011111)                          \n\
DS_RESET_LOW_AND_VAL(id,100000)                          \n\
DS_RESET_LOW_AND_VAL(id,100001)                          \n\
DS_RESET_LOW_AND_VAL(id,100010)                          \n\
DS_RESET_LOW_AND_VAL(id,100011)                          \n\
DS_RESET_LOW_AND_VAL(id,100100)                          \n\
DS_RESET_LOW_AND_VAL(id,100101)                          \n\
DS_RESET_LOW_AND_VAL(id,100110)                          \n\
DS_RESET_LOW_AND_VAL(id,100111)                          \n\
DS_RESET_LOW_AND_VAL(id,101000)                          \n\
DS_RESET_LOW_AND_VAL(id,101001)                          \n\
DS_RESET_LOW_AND_VAL(id,101010)                          \n\
DS_RESET_LOW_AND_VAL(id,101011)                          \n\
DS_RESET_LOW_AND_VAL(id,101100)                          \n\
DS_RESET_LOW_AND_VAL(id,101101)                          \n\
DS_RESET_LOW_AND_VAL(id,101110)                          \n\
DS_RESET_LOW_AND_VAL(id,101111)                          \n\
DS_RESET_LOW_AND_VAL(id,110000)                          \n\
DS_RESET_LOW_AND_VAL(id,110001)                          \n\
DS_RESET_LOW_AND_VAL(id,110010)                          \n\
DS_RESET_LOW_AND_VAL(id,110011)                          \n\
DS_RESET_LOW_AND_VAL(id,110100)                          \n\
DS_RESET_LOW_AND_VAL(id,110101)                          \n\
DS_RESET_LOW_AND_VAL(id,110110)                          \n\
DS_RESET_LOW_AND_VAL(id,110111)                          \n\
DS_RESET_LOW_AND_VAL(id,111000)                          \n\
DS_RESET_LOW_AND_VAL(id,111001)                          \n\
DS_RESET_LOW_AND_VAL(id,111010)                          \n\
DS_RESET_LOW_AND_VAL(id,111011)                          \n\
DS_RESET_LOW_AND_VAL(id,111100)                          \n\
DS_RESET_LOW_AND_VAL(id,111101)                          \n\
DS_RESET_LOW_AND_VAL(id,111110)                          \n\
DS_RESET_LOW_AND_VAL(id,111111)
