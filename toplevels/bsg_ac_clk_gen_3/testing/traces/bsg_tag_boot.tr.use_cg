################################################################################
# DESIGN SPECIFIC TAG INFO
#
# Number | Description            | Width
# -------+------------------------+-------
#     0  | Clkgen Async Reset     | 1-bit
#     1  | Clkgen Oscillator      | 5-bits
#     2  | Clkgen Osc Trigger     | 1-bit
#     3  | Clkgen Downsampoler    | 7-bits
#     4  | Clkgen Clock Select    | 2-bits
#
# Number of masters = 2
# Number of clients = 20
# Max data width = 7
#
# Packet lengths
# <4b>___<2b>___<5b>_1b_<3b>___<7b>
# <4b>___<18b>
#
# There are 2 masters (ASIC and GW) but they are replicated systems. The lsb of
# the masterEn is the ASIC and the msb is the GW. The GW will be given WHID 0,0
# and the ASIC will be given WHID 1,0. Therefore, we want to keep the prev link
# of the GW in reset and the next link of the ASIC in reset.
#

################################################################################
# Tag Trace Replay Packet Format
#
# M = number of masters
# N = max(1, clog2(#_of_tag_clients))
# D = max(client_1_width, client_2_width, ..., client_n_width)
# L = clog2(D+1)
#
# |<    4-bits    >|< M-bits >|< N-bits >|<     1-bit    >|< L-bits >|< D-bits >|
# +----------------+----------+----------+----------------+----------+----------+
# | replay command | masterEn |  nodeID  | data_not_reset |  length  |   data   |
# +----------------+----------+----------+----------------+----------+----------+
#
# Replay Commands
#   0 = 0000 = Wait a cycle
#   1 = 0001 = Send data
#   2 = 0010 = Receive data
#   3 = 0011 = Assert done_o ouput signal
#   4 = 0100 = End test (calls $finish)
#   5 = 0101 = Wait for cycle_counter == 0
#   6 = 0110 = Initialize cycle_counter with a 16 bit number

################################################################################
#
# RESET BSG TAG MASTER
#
# First, we must reset the bsg_tag_master. To do this, we send a 1, then we
# send a bunch of 0's! By a bunch, the exact amount is (2^clog2(N+1+L+D))+1

# Send a full 0 packet to all masters
0001___11___00000_0_000___0000000

# Wait ~32 cycles
0110___000000000000100000
0101___000000000000000000

################################################################################
#  ___  ___   ___ _____ ___ _____ ___    _   ___    ___ _    _  _____ ___ _  _
# | _ )/ _ \ / _ \_   _/ __|_   _| _ \  /_\ | _ \  / __| |  | |/ / __| __| \| |
# | _ \ (_) | (_) || | \__ \ | | |   / / _ \|  _/ | (__| |__| ' < (_ | _|| .` |
# |___/\___/ \___/ |_| |___/ |_| |_|_\/_/ \_\_|    \___|____|_|\_\___|___|_|\_|
#
################################################################################

################################################################################
#
# RESET BSG TAG CLIENTS
#
# Next, we should reset each client node. To do this we send a packet
# that has all 1's for data, and has data_not_reset=0. The nodeID should
# be the ID of the client we are reseting, and length should be the
# corrent length of the packet. We should send this packet to each client.

#SEND  en   id=0  r l=1  (ASYNC RESET)
0001___01___00000_0_001___0000001
#SEND  en   id=1  r l=5  (OSC)
0001___01___00001_0_101___0011111
#SEND  en   id=2  r l=1  (OSC TRIGGER)
0001___01___00010_0_001___0000001
#SEND  en   id=3  r l=7  (Downsampler)
0001___01___00011_0_111___1111111
#SEND  en   id=4 r l=2   (Clockselect)
0001___01___00100_0_010___0000011

################################################################################
#
# START CONFIGURATION
#
# The bsg tag network is now live! We can begin our configuration.

### Set osc triggers low

#SEND  en   id=2  d l=1   {trigger}
0001___01___00010_1_001___0000000

### Program the raw oscillators speed

#SEND  en   id=1  d l=5   {adt, cdt, fdt}
0001___01___00001_1_101___0000000

### Trigger oscillators

#SEND  en   id=2  d l=1   {trigger}
0001___01___00010_1_001___0000001
0001___01___00010_1_001___0000000

### Async clk-gen reset to get things moving

#SEND  en   id=0  d l=1   {async_reset}
0001___01___00000_1_001___0000000
0001___01___00000_1_001___0000001
0001___01___00000_1_001___0000000

### Set downsamples and reset

#SEND  en   id=3 d l=7   {ds_val, reset}
0001___01___00011_1_111___0000001
0001___01___00011_1_111___0000000

### Select the output clock (0=raw osc, 1=ds osc, 2=ext, 3=off)

#SEND  en   id=4 d l=2   {clk_select}
0001___01___00100_1_010___0000001

################################################################################
#
# Done!
#
# Configuration is complete and we are out of reset. We should indicate we are
# done to allow the next part of the testbench to come alive.

# Wait ~1k cycles
0110___000000010000000000
0101___000000000000000000

# Assert finish
0100___000000000000000000

