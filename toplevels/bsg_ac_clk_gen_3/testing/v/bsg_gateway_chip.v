`timescale 1ps/1ps

`ifndef MANYCORE_CLK_PERIOD
  `define MANYCORE_CLK_PERIOD 5000.0
`endif

`ifndef IO_MASTER_CLK_PERIOD
  `define IO_MASTER_CLK_PERIOD 5000.0
`endif

`ifndef ROUTER_CLK_PERIOD
  `define ROUTER_CLK_PERIOD 5000.0
`endif

`ifndef TAG_CLK_PERIOD
  `define TAG_CLK_PERIOD 7000.0
`endif

module bsg_gateway_chip

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

`include "bsg_pinout_inverted.v"

  //////////////////////////////////////////////////
  //
  // Waveform Dump
  //

  initial
    begin
      $vcdpluson;
      $vcdplusmemon;
      $vcdplusautoflushon;
    end

  //////////////////////////////////////////////////
  //
  // Nonsynth Clock Generator(s)
  //

  logic manycore_clk;
  assign p_clk_A_o = manycore_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`MANYCORE_CLK_PERIOD)) manycore_clk_gen (.o(manycore_clk));

  logic io_master_clk;
  assign p_clk_B_o = io_master_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`IO_MASTER_CLK_PERIOD)) io_master_clk_gen (.o(io_master_clk));

  logic router_clk;
  assign p_clk_C_o = router_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`ROUTER_CLK_PERIOD)) router_clk_gen (.o(router_clk));

  logic tag_clk;

   // invert the tag clock to avoid races
  assign p_bsg_tag_clk_o = ~tag_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`TAG_CLK_PERIOD)) tag_clk_gen (.o(tag_clk));

  //////////////////////////////////////////////////
  //
  // Nonsynth Reset Generator(s)
  //

  logic tag_reset;
  bsg_nonsynth_reset_gen #(.num_clocks_p(1),.reset_cycles_lo_p(10),.reset_cycles_hi_p(5))
    tag_reset_gen
      (.clk_i(tag_clk)
      ,.async_reset_o(tag_reset)
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Track Replay
  //

  localparam tag_trace_rom_addr_width_lp = 32;
// RC2  localparam tag_trace_rom_data_width_lp = 22;
   // RC3
//  localparam tag_trace_rom_data_width_lp = 27;
  // BP4
   localparam tag_trace_rom_data_width_lp = 26;

  logic [tag_trace_rom_addr_width_lp-1:0] rom_addr_li;
  logic [tag_trace_rom_data_width_lp-1:0] rom_data_lo;

  logic [1:0] tag_trace_en_r_lo;
  logic       tag_trace_done_lo;

  // TAG TRACE ROM
  bsg_tag_boot_rom #(.width_p( tag_trace_rom_data_width_lp )
                    ,.addr_width_p( tag_trace_rom_addr_width_lp )
                    )
    tag_trace_rom
      (.addr_i( rom_addr_li )
      ,.data_o( rom_data_lo )
      );

   initial
     begin
	$display("%m: tag_max_payload_width_gp: %b, tag_num_clients_gp:%b",tag_max_payload_width_gp,tag_num_clients_gp);
     end

  // TAG TRACE REPLAY
  bsg_tag_trace_replay #(.rom_addr_width_p( tag_trace_rom_addr_width_lp )
                        ,.rom_data_width_p( tag_trace_rom_data_width_lp )
                        ,.num_masters_p( 2 )
                        ,.num_clients_p( tag_num_clients_gp )
                        ,.max_payload_width_p( tag_max_payload_width_gp )
                        )
    tag_trace_replay
      (.clk_i   ( tag_clk )
      ,.reset_i ( tag_reset    )
      ,.en_i    ( 1'b1            )

      ,.rom_addr_o( rom_addr_li )
      ,.rom_data_i( rom_data_lo )

      ,.valid_i ( 1'b0 )
      ,.data_i  ( '0 )
      ,.ready_o ()

      ,.valid_o    ()
      ,.en_r_o     ( tag_trace_en_r_lo )
      ,.tag_data_o ( p_bsg_tag_data_o )
      ,.yumi_i     ( 1'b1 )

      ,.done_o  ( tag_trace_done_lo )
      ,.error_o ()
      ) ;

  assign p_bsg_tag_en_o = tag_trace_en_r_lo[0];


   // ,input p_clk_i

   // clock select mux, should be to core clk

   assign p_sel_0_o = 1'b0;
   assign p_sel_1_o = 1'b1;
   
   
//   wire watch_me = bsg_asic_cloud_pcb.IC0.ASIC.clk_gen_pd.clk_gen_0__clk_gen_inst.mux_inst.data_o;   /* clk gen 0 of HBM */
      wire watch_me = bsg_asic_cloud_pcb.IC0.ASIC.clk_gen_pd.clk_gen_0__clk_gen_inst.mux_inst.data_o[0];   /* clk gen 0 of HBM */
//   wire watch_me = bsg_asic_cloud_pcb.IC0.ASIC.clk_gen_pd.clk_gen_2__clk_gen_inst.mux_inst.data_o;   /* clk gen 2 of HBM */
//   wire watch_me = bsg_asic_cloud_pcb.IC0.ASIC.clk_gen_pd.clk_gen_1__clk_gen_inst.mux_inst.data_o;   /* clk gen 1 of HBM */
//   wire watch_me = p_ci_8_i;  /* ac_clk_3 test */

   bsg_nonsynth_clk_watcher #(.tolerance_p(1),.checkin_p(500)) cw (.clk_i(watch_me));

//  //////////////////////////////////////////////////
//  //
//  // BSG Tag Master Instance
//  //
//
//  // All tag lines from the btm
//  bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;
//
//  // Rename the tag lines (copy from bsg_chip.v)
//  wire bsg_tag_s prev_link_io_tag_lines_lo   = tag_lines_lo[13];
//  wire bsg_tag_s prev_link_core_tag_lines_lo = tag_lines_lo[14];
//  wire bsg_tag_s prev_ct_core_tag_lines_lo   = tag_lines_lo[15];
//  wire bsg_tag_s next_link_io_tag_lines_lo   = tag_lines_lo[16];
//  wire bsg_tag_s next_link_core_tag_lines_lo = tag_lines_lo[17];
//  wire bsg_tag_s next_ct_core_tag_lines_lo   = tag_lines_lo[18];
//  wire bsg_tag_s router_core_tag_lines_lo    = tag_lines_lo[19];
//
//  // BSG tag master instance
//  bsg_tag_master #(.els_p( tag_num_clients_gp )
//                  ,.lg_width_p( tag_lg_max_payload_width_gp )
//                  )
//    btm
//      (.clk_i      ( p_bsg_tag_clk_o )
//      ,.data_i     ( tag_trace_en_r_lo[1] ? p_bsg_tag_data_o : 1'b0 )
//      ,.en_i       ( 1'b1 )
//      ,.clients_r_o( tag_lines_lo )
//      );

endmodule

