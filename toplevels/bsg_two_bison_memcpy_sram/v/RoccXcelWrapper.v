//-----------------------------------------------------------------------------
// RoccXcelWrapper
//-----------------------------------------------------------------------------
// Module: <class 'memcpy_sram_xcel.MemcpySramXcelHLS.MemcpySramXcelHLS'>
// NumOfReqs: 4
// mem_ifc_types: <pclib.ifcs.MemMsg.MemMsg4B object at 0x7f49db019d90>
// dump-vcd: False
// verilator-xinit: zeros
`default_nettype none
module RoccXcelWrapper
(
  input  wire [   0:0] clk,
  output reg  [   0:0] io_busy,
  input  wire [   6:0] io_cmd_bits_inst_funct,
  input  wire [   6:0] io_cmd_bits_inst_opcode,
  input  wire [   4:0] io_cmd_bits_inst_rd,
  input  wire [   4:0] io_cmd_bits_inst_rs1,
  input  wire [   4:0] io_cmd_bits_inst_rs2,
  input  wire [   0:0] io_cmd_bits_inst_xd,
  input  wire [   0:0] io_cmd_bits_inst_xs1,
  input  wire [   0:0] io_cmd_bits_inst_xs2,
  input  wire [  63:0] io_cmd_bits_rs1,
  input  wire [  63:0] io_cmd_bits_rs2,
  output wire [   0:0] io_cmd_ready,
  input  wire [   0:0] io_cmd_valid,
  output wire [  39:0] io_mem_req_bits_addr,
  output wire [   4:0] io_mem_req_bits_cmd,
  output wire [  63:0] io_mem_req_bits_data,
  output wire [   0:0] io_mem_req_bits_phys,
  output wire [   9:0] io_mem_req_bits_tag,
  output wire [   2:0] io_mem_req_bits_typ,
  input  wire [   0:0] io_mem_req_ready,
  output wire [   0:0] io_mem_req_valid,
  input  wire [  39:0] io_mem_resp_bits_addr,
  input  wire [   4:0] io_mem_resp_bits_cmd,
  input  wire [  63:0] io_mem_resp_bits_data,
  input  wire [  63:0] io_mem_resp_bits_data_word_bypass,
  input  wire [   0:0] io_mem_resp_bits_has_data,
  input  wire [   0:0] io_mem_resp_bits_nack,
  input  wire [   0:0] io_mem_resp_bits_replay,
  input  wire [  63:0] io_mem_resp_bits_store_data,
  input  wire [   9:0] io_mem_resp_bits_tag,
  input  wire [   2:0] io_mem_resp_bits_typ,
  input  wire [   0:0] io_mem_resp_valid,
  output wire [  63:0] io_resp_bits_data,
  output wire [   4:0] io_resp_bits_rd,
  input  wire [   0:0] io_resp_ready,
  output wire [   0:0] io_resp_valid,
  input  wire [   0:0] reset
);

  // mem_adapter temporaries
  wire   [   0:0] mem_adapter$clk;
  wire   [ 109:0] mem_adapter$xcel_mem_req_msg;
  wire   [   0:0] mem_adapter$xcel_mem_req_val;
  wire   [  39:0] mem_adapter$mem_resp_bits_addr;
  wire   [   4:0] mem_adapter$mem_resp_bits_cmd;
  wire   [  63:0] mem_adapter$mem_resp_bits_data;
  wire   [  63:0] mem_adapter$mem_resp_bits_data_word_bypass;
  wire   [   0:0] mem_adapter$mem_resp_bits_has_data;
  wire   [   0:0] mem_adapter$mem_resp_bits_nack;
  wire   [   0:0] mem_adapter$mem_resp_bits_replay;
  wire   [  63:0] mem_adapter$mem_resp_bits_store_data;
  wire   [   9:0] mem_adapter$mem_resp_bits_tag;
  wire   [   2:0] mem_adapter$mem_resp_bits_typ;
  wire   [   0:0] mem_adapter$mem_resp_valid;
  wire   [   0:0] mem_adapter$mem_req_ready;
  wire   [   0:0] mem_adapter$reset;
  wire   [   0:0] mem_adapter$xcel_mem_resp_rdy;
  wire   [   0:0] mem_adapter$busy;
  wire   [   0:0] mem_adapter$xcel_mem_req_rdy;
  wire   [  39:0] mem_adapter$mem_req_bits_addr;
  wire   [   4:0] mem_adapter$mem_req_bits_cmd;
  wire   [  63:0] mem_adapter$mem_req_bits_data;
  wire   [   0:0] mem_adapter$mem_req_bits_phys;
  wire   [   9:0] mem_adapter$mem_req_bits_tag;
  wire   [   2:0] mem_adapter$mem_req_bits_typ;
  wire   [   0:0] mem_adapter$mem_req_valid;
  wire   [  79:0] mem_adapter$xcel_mem_resp_msg;
  wire   [   0:0] mem_adapter$xcel_mem_resp_val;

  MemToRoccMemAdapter_0x96ad3c9d8499f35 mem_adapter
  (
    .clk                            ( mem_adapter$clk ),
    .xcel_mem_req_msg               ( mem_adapter$xcel_mem_req_msg ),
    .xcel_mem_req_val               ( mem_adapter$xcel_mem_req_val ),
    .mem_resp_bits_addr             ( mem_adapter$mem_resp_bits_addr ),
    .mem_resp_bits_cmd              ( mem_adapter$mem_resp_bits_cmd ),
    .mem_resp_bits_data             ( mem_adapter$mem_resp_bits_data ),
    .mem_resp_bits_data_word_bypass ( mem_adapter$mem_resp_bits_data_word_bypass ),
    .mem_resp_bits_has_data         ( mem_adapter$mem_resp_bits_has_data ),
    .mem_resp_bits_nack             ( mem_adapter$mem_resp_bits_nack ),
    .mem_resp_bits_replay           ( mem_adapter$mem_resp_bits_replay ),
    .mem_resp_bits_store_data       ( mem_adapter$mem_resp_bits_store_data ),
    .mem_resp_bits_tag              ( mem_adapter$mem_resp_bits_tag ),
    .mem_resp_bits_typ              ( mem_adapter$mem_resp_bits_typ ),
    .mem_resp_valid                 ( mem_adapter$mem_resp_valid ),
    .mem_req_ready                  ( mem_adapter$mem_req_ready ),
    .reset                          ( mem_adapter$reset ),
    .xcel_mem_resp_rdy              ( mem_adapter$xcel_mem_resp_rdy ),
    .busy                           ( mem_adapter$busy ),
    .xcel_mem_req_rdy               ( mem_adapter$xcel_mem_req_rdy ),
    .mem_req_bits_addr              ( mem_adapter$mem_req_bits_addr ),
    .mem_req_bits_cmd               ( mem_adapter$mem_req_bits_cmd ),
    .mem_req_bits_data              ( mem_adapter$mem_req_bits_data ),
    .mem_req_bits_phys              ( mem_adapter$mem_req_bits_phys ),
    .mem_req_bits_tag               ( mem_adapter$mem_req_bits_tag ),
    .mem_req_bits_typ               ( mem_adapter$mem_req_bits_typ ),
    .mem_req_valid                  ( mem_adapter$mem_req_valid ),
    .xcel_mem_resp_msg              ( mem_adapter$xcel_mem_resp_msg ),
    .xcel_mem_resp_val              ( mem_adapter$xcel_mem_resp_val )
  );

  // xcel temporaries
  wire   [  79:0] xcel$memresp_msg;
  wire   [   0:0] xcel$memresp_val;
  wire   [ 159:0] xcel$xcelreq_msg;
  wire   [   0:0] xcel$xcelreq_val;
  wire   [   0:0] xcel$clk;
  wire   [   0:0] xcel$reset;
  wire   [   0:0] xcel$memreq_rdy;
  wire   [   0:0] xcel$xcelresp_rdy;
  wire   [   0:0] xcel$memresp_rdy;
  wire   [   0:0] xcel$xcelreq_rdy;
  wire   [ 109:0] xcel$memreq_msg;
  wire   [   0:0] xcel$memreq_val;
  wire   [  68:0] xcel$xcelresp_msg;
  wire   [   0:0] xcel$xcelresp_val;

  MemcpySramXcelHLS_0xa89d07e0dbd52f9 xcel
  (
    .memresp_msg  ( xcel$memresp_msg ),
    .memresp_val  ( xcel$memresp_val ),
    .xcelreq_msg  ( xcel$xcelreq_msg ),
    .xcelreq_val  ( xcel$xcelreq_val ),
    .clk          ( xcel$clk ),
    .reset        ( xcel$reset ),
    .memreq_rdy   ( xcel$memreq_rdy ),
    .xcelresp_rdy ( xcel$xcelresp_rdy ),
    .memresp_rdy  ( xcel$memresp_rdy ),
    .xcelreq_rdy  ( xcel$xcelreq_rdy ),
    .memreq_msg   ( xcel$memreq_msg ),
    .memreq_val   ( xcel$memreq_val ),
    .xcelresp_msg ( xcel$xcelresp_msg ),
    .xcelresp_val ( xcel$xcelresp_val )
  );

  // xcel_adapter temporaries
  wire   [   0:0] xcel_adapter$reset;
  wire   [   0:0] xcel_adapter$xcel_req_rdy;
  wire   [   0:0] xcel_adapter$clk;
  wire   [   6:0] xcel_adapter$core_cmd_bits_inst_funct;
  wire   [   6:0] xcel_adapter$core_cmd_bits_inst_opcode;
  wire   [   4:0] xcel_adapter$core_cmd_bits_inst_rd;
  wire   [   4:0] xcel_adapter$core_cmd_bits_inst_rs1;
  wire   [   4:0] xcel_adapter$core_cmd_bits_inst_rs2;
  wire   [   0:0] xcel_adapter$core_cmd_bits_inst_xd;
  wire   [   0:0] xcel_adapter$core_cmd_bits_inst_xs1;
  wire   [   0:0] xcel_adapter$core_cmd_bits_inst_xs2;
  wire   [  63:0] xcel_adapter$core_cmd_bits_rs1;
  wire   [  63:0] xcel_adapter$core_cmd_bits_rs2;
  wire   [   0:0] xcel_adapter$core_cmd_valid;
  wire   [   0:0] xcel_adapter$core_resp_ready;
  wire   [  68:0] xcel_adapter$xcel_resp_msg;
  wire   [   0:0] xcel_adapter$xcel_resp_val;
  wire   [ 159:0] xcel_adapter$xcel_req_msg;
  wire   [   0:0] xcel_adapter$xcel_req_val;
  wire   [   0:0] xcel_adapter$core_cmd_ready;
  wire   [  63:0] xcel_adapter$core_resp_bits_data;
  wire   [   4:0] xcel_adapter$core_resp_bits_rd;
  wire   [   0:0] xcel_adapter$core_resp_valid;
  wire   [   0:0] xcel_adapter$xcel_resp_rdy;

  RoccXcelToXcelAdapter_0x469384a45dd936fe xcel_adapter
  (
    .reset                     ( xcel_adapter$reset ),
    .xcel_req_rdy              ( xcel_adapter$xcel_req_rdy ),
    .clk                       ( xcel_adapter$clk ),
    .core_cmd_bits_inst_funct  ( xcel_adapter$core_cmd_bits_inst_funct ),
    .core_cmd_bits_inst_opcode ( xcel_adapter$core_cmd_bits_inst_opcode ),
    .core_cmd_bits_inst_rd     ( xcel_adapter$core_cmd_bits_inst_rd ),
    .core_cmd_bits_inst_rs1    ( xcel_adapter$core_cmd_bits_inst_rs1 ),
    .core_cmd_bits_inst_rs2    ( xcel_adapter$core_cmd_bits_inst_rs2 ),
    .core_cmd_bits_inst_xd     ( xcel_adapter$core_cmd_bits_inst_xd ),
    .core_cmd_bits_inst_xs1    ( xcel_adapter$core_cmd_bits_inst_xs1 ),
    .core_cmd_bits_inst_xs2    ( xcel_adapter$core_cmd_bits_inst_xs2 ),
    .core_cmd_bits_rs1         ( xcel_adapter$core_cmd_bits_rs1 ),
    .core_cmd_bits_rs2         ( xcel_adapter$core_cmd_bits_rs2 ),
    .core_cmd_valid            ( xcel_adapter$core_cmd_valid ),
    .core_resp_ready           ( xcel_adapter$core_resp_ready ),
    .xcel_resp_msg             ( xcel_adapter$xcel_resp_msg ),
    .xcel_resp_val             ( xcel_adapter$xcel_resp_val ),
    .xcel_req_msg              ( xcel_adapter$xcel_req_msg ),
    .xcel_req_val              ( xcel_adapter$xcel_req_val ),
    .core_cmd_ready            ( xcel_adapter$core_cmd_ready ),
    .core_resp_bits_data       ( xcel_adapter$core_resp_bits_data ),
    .core_resp_bits_rd         ( xcel_adapter$core_resp_bits_rd ),
    .core_resp_valid           ( xcel_adapter$core_resp_valid ),
    .xcel_resp_rdy             ( xcel_adapter$xcel_resp_rdy )
  );

  // signal connections
  assign io_cmd_ready                               = xcel_adapter$core_cmd_ready;
  assign io_mem_req_bits_addr                       = mem_adapter$mem_req_bits_addr;
  assign io_mem_req_bits_cmd                        = mem_adapter$mem_req_bits_cmd;
  assign io_mem_req_bits_data                       = mem_adapter$mem_req_bits_data;
  assign io_mem_req_bits_phys                       = mem_adapter$mem_req_bits_phys;
  assign io_mem_req_bits_tag                        = mem_adapter$mem_req_bits_tag;
  assign io_mem_req_bits_typ                        = mem_adapter$mem_req_bits_typ;
  assign io_mem_req_valid                           = mem_adapter$mem_req_valid;
  assign io_resp_bits_data                          = xcel_adapter$core_resp_bits_data;
  assign io_resp_bits_rd                            = xcel_adapter$core_resp_bits_rd;
  assign io_resp_valid                              = xcel_adapter$core_resp_valid;
  assign mem_adapter$clk                            = clk;
  assign mem_adapter$mem_req_ready                  = io_mem_req_ready;
  assign mem_adapter$mem_resp_bits_addr             = io_mem_resp_bits_addr;
  assign mem_adapter$mem_resp_bits_cmd              = io_mem_resp_bits_cmd;
  assign mem_adapter$mem_resp_bits_data             = io_mem_resp_bits_data;
  assign mem_adapter$mem_resp_bits_data_word_bypass = io_mem_resp_bits_data_word_bypass;
  assign mem_adapter$mem_resp_bits_has_data         = io_mem_resp_bits_has_data;
  assign mem_adapter$mem_resp_bits_nack             = io_mem_resp_bits_nack;
  assign mem_adapter$mem_resp_bits_replay           = io_mem_resp_bits_replay;
  assign mem_adapter$mem_resp_bits_store_data       = io_mem_resp_bits_store_data;
  assign mem_adapter$mem_resp_bits_tag              = io_mem_resp_bits_tag;
  assign mem_adapter$mem_resp_bits_typ              = io_mem_resp_bits_typ;
  assign mem_adapter$mem_resp_valid                 = io_mem_resp_valid;
  assign mem_adapter$reset                          = reset;
  assign mem_adapter$xcel_mem_req_msg               = xcel$memreq_msg;
  assign mem_adapter$xcel_mem_req_val               = xcel$memreq_val;
  assign mem_adapter$xcel_mem_resp_rdy              = xcel$memresp_rdy;
  assign xcel$clk                                   = clk;
  assign xcel$memreq_rdy                            = mem_adapter$xcel_mem_req_rdy;
  assign xcel$memresp_msg                           = mem_adapter$xcel_mem_resp_msg;
  assign xcel$memresp_val                           = mem_adapter$xcel_mem_resp_val;
  assign xcel$reset                                 = reset;
  assign xcel$xcelreq_msg                           = xcel_adapter$xcel_req_msg;
  assign xcel$xcelreq_val                           = xcel_adapter$xcel_req_val;
  assign xcel$xcelresp_rdy                          = xcel_adapter$xcel_resp_rdy;
  assign xcel_adapter$clk                           = clk;
  assign xcel_adapter$core_cmd_bits_inst_funct      = io_cmd_bits_inst_funct;
  assign xcel_adapter$core_cmd_bits_inst_opcode     = io_cmd_bits_inst_opcode;
  assign xcel_adapter$core_cmd_bits_inst_rd         = io_cmd_bits_inst_rd;
  assign xcel_adapter$core_cmd_bits_inst_rs1        = io_cmd_bits_inst_rs1;
  assign xcel_adapter$core_cmd_bits_inst_rs2        = io_cmd_bits_inst_rs2;
  assign xcel_adapter$core_cmd_bits_inst_xd         = io_cmd_bits_inst_xd;
  assign xcel_adapter$core_cmd_bits_inst_xs1        = io_cmd_bits_inst_xs1;
  assign xcel_adapter$core_cmd_bits_inst_xs2        = io_cmd_bits_inst_xs2;
  assign xcel_adapter$core_cmd_bits_rs1             = io_cmd_bits_rs1;
  assign xcel_adapter$core_cmd_bits_rs2             = io_cmd_bits_rs2;
  assign xcel_adapter$core_cmd_valid                = io_cmd_valid;
  assign xcel_adapter$core_resp_ready               = io_resp_ready;
  assign xcel_adapter$reset                         = reset;
  assign xcel_adapter$xcel_req_rdy                  = xcel$xcelreq_rdy;
  assign xcel_adapter$xcel_resp_msg                 = xcel$xcelresp_msg;
  assign xcel_adapter$xcel_resp_val                 = xcel$xcelresp_val;


  // PYMTL SOURCE:
  //
  // @s.combinational
  // def comb():
  //         s.io_busy.value = s.mem_adapter.busy

  // logic for comb()
  always @ (*) begin
    io_busy = mem_adapter$busy;
  end


endmodule // RoccXcelWrapper
`default_nettype wire

//-----------------------------------------------------------------------------
// MemToRoccMemAdapter_0x96ad3c9d8499f35
//-----------------------------------------------------------------------------
// mem_ifc_req_type: 110
// mem_ifc_resp_type: 80
// number_of_requests: 4
// dump-vcd: False
// verilator-xinit: zeros
`default_nettype none
module MemToRoccMemAdapter_0x96ad3c9d8499f35
(
  output reg  [   0:0] busy,
  input  wire [   0:0] clk,
  output reg  [  39:0] mem_req_bits_addr,
  output reg  [   4:0] mem_req_bits_cmd,
  output reg  [  63:0] mem_req_bits_data,
  output reg  [   0:0] mem_req_bits_phys,
  output reg  [   9:0] mem_req_bits_tag,
  output reg  [   2:0] mem_req_bits_typ,
  input  wire [   0:0] mem_req_ready,
  output reg  [   0:0] mem_req_valid,
  input  wire [  39:0] mem_resp_bits_addr,
  input  wire [   4:0] mem_resp_bits_cmd,
  input  wire [  63:0] mem_resp_bits_data,
  input  wire [  63:0] mem_resp_bits_data_word_bypass,
  input  wire [   0:0] mem_resp_bits_has_data,
  input  wire [   0:0] mem_resp_bits_nack,
  input  wire [   0:0] mem_resp_bits_replay,
  input  wire [  63:0] mem_resp_bits_store_data,
  input  wire [   9:0] mem_resp_bits_tag,
  input  wire [   2:0] mem_resp_bits_typ,
  input  wire [   0:0] mem_resp_valid,
  input  wire [   0:0] reset,
  input  wire [ 109:0] xcel_mem_req_msg,
  output reg  [   0:0] xcel_mem_req_rdy,
  input  wire [   0:0] xcel_mem_req_val,
  output reg  [  79:0] xcel_mem_resp_msg,
  input  wire [   0:0] xcel_mem_resp_rdy,
  output reg  [   0:0] xcel_mem_resp_val
);

  // wire declarations
  wire   [   9:0] tags$000;
  wire   [   9:0] tags$001;
  wire   [   9:0] tags$002;
  wire   [   9:0] tags$003;
  wire   [   9:0] next_tags$000;
  wire   [   9:0] next_tags$001;
  wire   [   9:0] next_tags$002;
  wire   [   9:0] next_tags$003;
  wire   [  63:0] data$000;
  wire   [  63:0] data$001;
  wire   [  63:0] data$002;
  wire   [  63:0] data$003;
  wire   [  63:0] next_data$000;
  wire   [  63:0] next_data$001;
  wire   [  63:0] next_data$002;
  wire   [  63:0] next_data$003;


  // register declarations
  reg    [   1:0] head;
  reg    [   1:0] next_head;
  reg    [   3:0] next_rdylist;
  reg    [   1:0] next_tail;
  reg    [   3:0] next_valid;
  reg    [   1:0] ptr;
  reg    [   3:0] rdylist;
  reg    [   1:0] tail;
  reg    [  63:0] tmp_data;
  reg    [  31:0] tmp_msgaddr;
  reg    [  63:0] tmp_msgdata;
  reg    [   7:0] tmp_msgtag;
  reg    [   9:0] tmp_tag;
  reg    [   3:0] valid;
  reg    [  39:0] wire_mem_req_bits_addr;
  reg    [   4:0] wire_mem_req_bits_cmd;
  reg    [  63:0] wire_mem_req_bits_data;
  reg    [   0:0] wire_mem_req_bits_phys;
  reg    [   9:0] wire_mem_req_bits_tag;
  reg    [   2:0] wire_mem_req_bits_typ;
  reg    [   0:0] wire_mem_req_valid;
  reg    [   0:0] wire_xcel_mem_req_rdy;
  reg    [  79:0] wire_xcel_mem_resp_msg;
  reg    [   0:0] wire_xcel_mem_resp_val;

  // localparam declarations
  localparam LOAD = 0;
  localparam STORE = 1;
  localparam TYPE_WRITE = 1;
  localparam addr_sz = 32;
  localparam data_sz = 64;
  localparam max_len = 3;
  localparam nreqs_nbits = 2;
  localparam num_of_reqs = 4;
  localparam tag_sz = 8;

  // loop variable declarations
  integer i;


  // array declarations
  reg    [  63:0] data[0:3];
  assign data$000 = data[  0];
  assign data$001 = data[  1];
  assign data$002 = data[  2];
  assign data$003 = data[  3];
  reg    [  63:0] next_data[0:3];
  assign next_data$000 = next_data[  0];
  assign next_data$001 = next_data[  1];
  assign next_data$002 = next_data[  2];
  assign next_data$003 = next_data[  3];
  reg    [   9:0] next_tags[0:3];
  assign next_tags$000 = next_tags[  0];
  assign next_tags$001 = next_tags[  1];
  assign next_tags$002 = next_tags[  2];
  assign next_tags$003 = next_tags[  3];
  reg    [   9:0] tags[0:3];
  assign tags$000 = tags[  0];
  assign tags$001 = tags[  1];
  assign tags$002 = tags[  2];
  assign tags$003 = tags[  3];

  // PYMTL SOURCE:
  //
  // @s.tick_rtl
  // def seq():
  //
  //       ## Simple logic here
  //       ## Reg = next
  //
  //       ## Pointers
  //       s.head   .next = s.next_head
  //       s.tail   .next = s.next_tail
  //       ## Lists
  //       s.rdylist.next = s.next_rdylist
  //       s.valid  .next = s.next_valid
  //       ## Memories
  //       for i in xrange(s.num_of_reqs):
  //         s.data[i].next = s.next_data[i]
  //         s.tags[i].next = s.next_tags[i]

  // logic for seq()
  always @ (posedge clk) begin
    head <= next_head;
    tail <= next_tail;
    rdylist <= next_rdylist;
    valid <= next_valid;
    for (i=0; i < num_of_reqs; i=i+1)
    begin
      data[i] <= next_data[i];
      tags[i] <= next_tags[i];
    end
  end

  // PYMTL SOURCE:
  //
  // @s.combinational
  // def comb():
  //
  //       # Avoid latches
  //
  //       s.tmp_msgdata.value = 0
  //       s.tmp_msgaddr.value = 0
  //       s.tmp_msgtag .value = 0
  //
  //       ## We push back responses, then we accept requests.
  //       ## As we know that requests will take at least one cycle to be satisfied,
  //       ## by satisfying responses first, we make sure to maximize throughput
  //
  //       # Crossing fingers! we try this again ... We do double assignment
  //       s.next_head .value   = s.head
  //       s.next_tail .value   = s.tail
  //       s.next_valid.value   = s.valid
  //
  //       s.next_rdylist.value = s.rdylist
  //
  //       # Data memories
  //       for i in xrange( s.num_of_reqs ):
  //         s.next_data[i].value = s.data[i]
  //         s.next_tags[i].value = s.tags[i]
  //
  //       # Eliminate latches
  //       s.ptr.value = 0
  //
  //       # Ports initialization
  //       # hawajkm: Double assignment didn't work for ports.
  //       #s.xcel_mem_req.rdy .value  = 0
  //
  //       #s.xcel_mem_resp.val.value = 0
  //       #s.xcel_mem_resp.msg.value = 0
  //
  //       #s.mem_req.valid    .value = 0
  //       #s.mem_req.bits_addr.value = 0
  //       #s.mem_req.bits_data.value = 0
  //       #s.mem_req.bits_tag .value = 0
  //       #s.mem_req.bits_cmd .value = 0
  //       #s.mem_req.bits_typ .value = 0
  //       #s.mem_req.bits_phys.value = 0
  //       s.wire_xcel_mem_req_rdy .value  = 0
  //
  //       s.wire_xcel_mem_resp_val.value = 0
  //       s.wire_xcel_mem_resp_msg.value = 0
  //
  //       s.wire_mem_req_valid                 .value = 0
  //       ## PyMTL bug
  //       s.wire_mem_req_bits_addr             .value = 0
  //       s.tmp_msgaddr                        .value = s.xcel_mem_req.msg.addr
  //       s.wire_mem_req_bits_addr[0:s.addr_sz].value = s.tmp_msgaddr[0:s.addr_sz]
  //       ## PyMTL bug
  //       s.wire_mem_req_bits_data             .value = 0
  //       s.tmp_msgdata                        .value = s.xcel_mem_req.msg.data
  //       s.wire_mem_req_bits_data[0:s.data_sz].value = s.tmp_msgdata[0:s.data_sz]
  //       s.wire_mem_req_bits_tag              .value = s.next_tail
  //       s.wire_mem_req_bits_cmd              .value = 0
  //       s.wire_mem_req_bits_typ              .value = s.max_len
  //       s.wire_mem_req_bits_phys             .value = 0
  //       # Type conversion
  //       if(s.xcel_mem_req.msg.type_ == mem_ifc_req_type.TYPE_WRITE):
  //         s.wire_mem_req_bits_cmd.value = RoCC.STORE
  //       else:
  //         s.wire_mem_req_bits_cmd.value = RoCC.LOAD
  //       # Length Conversion
  //       if   s.xcel_mem_resp.msg.len <= 0:
  //         s.wire_mem_req_bits_typ.value = s.max_len
  //       elif s.xcel_mem_resp.msg.len <= 1:
  //         s.wire_mem_req_bits_typ.value = 0
  //       elif s.xcel_mem_resp.msg.len <= 2:
  //         s.wire_mem_req_bits_typ.value = 1
  //       elif s.xcel_mem_resp.msg.len <= 4:
  //         s.wire_mem_req_bits_typ.value = 2
  //       elif s.xcel_mem_resp.msg.len <= 8:
  //         s.wire_mem_req_bits_typ.value = 3
  //       else:
  //         s.wire_mem_req_bits_typ.value = s.max_len
  //
  //       ## Responses
  //       if(s.mem_resp.valid):
  //         ## We have a response coming back
  //         ## Its number in the re-order buffer would be in the tag "opaque"
  //         s.ptr.value = s.mem_resp.bits_tag[:s.nreqs_nbits]
  //
  //         # Set ready bits
  //         s.next_rdylist[s.ptr].value = 1
  //
  //         # Record data
  //         s.next_data[s.ptr].value = s.mem_resp.bits_data
  //
  //       ## Send responses
  //       if(s.next_rdylist[s.next_head] and s.next_valid[s.next_head]):
  //         ## If response ready, send it
  //         #s.xcel_mem_resp.val.value = 1
  //         s.wire_xcel_mem_resp_val.value = 1
  //         # PyMTL double slicing bug avoidance
  //         s.tmp_tag .value = s.next_tags[s.next_head]
  //         s.tmp_data.value = s.next_data[s.next_head]
  //         # Set appropriate values; assume response message is zeroed out bydefault
  //         s.tmp_msgtag                                .value = 0
  //         s.tmp_msgtag                   [0:s.tag_sz ].value = s.tmp_tag[0:s.tag_sz]
  //         s.wire_xcel_mem_resp_msg.opaque             .value = s.tmp_msgtag
  //         s.tmp_msgdata                               .value = 0
  //         s.tmp_msgdata                  [0:s.data_sz].value = s.tmp_data[0:s.data_sz]
  //         s.wire_xcel_mem_resp_msg.data               .value = s.tmp_msgdata
  //
  //         if(s.xcel_mem_resp.rdy):
  //           ## Reset the entry
  //           s.next_rdylist[s.next_head].value = 0
  //           s.next_valid  [s.next_head].value = 0
  //
  //           ## Increment head pointer
  //           s.next_head.value = s.next_head + 1
  //           if s.next_head >= s.num_of_reqs:
  //             s.next_head.value = 0
  //
  //       ## Get requests in
  //       if(not s.next_valid[s.next_tail] and s.mem_req.ready):
  //         ## We are ready for am entry
  //         #s.xcel_mem_req.rdy.value = 1
  //         s.wire_xcel_mem_req_rdy.value = 1
  //
  //         ## If it is a valid data, record and increment the tail
  //         if(s.xcel_mem_req.val):
  //           ## Get the data
  //           # PyMTL double slicing bug avoidance
  //           s.tmp_data.value = 0
  //           s.tmp_tag .value = 0
  //           # Set temps
  //           s.tmp_msgdata            .value = s.xcel_mem_req.msg.data
  //           s.tmp_data  [0:s.data_sz].value = s.tmp_msgdata[0:s.data_sz]
  //           s.tmp_msgtag             .value = s.xcel_mem_req.msg.opaque
  //           s.tmp_tag   [0:s.tag_sz ].value = s.tmp_msgtag [0:s.tag_sz ]
  //           # Assign them to the array
  //           s.next_data[s.next_tail].value = s.tmp_data
  //           s.next_tags[s.next_tail].value = s.tmp_tag
  //           # FIXME: We need the len set properly for future designs
  //
  //           ## Set the output ports
  //           #s.mem_req.valid       .value = 1
  //           s.wire_mem_req_valid   .value = 1
  //
  //           ## Set the valid bits
  //           s.next_valid[s.next_tail].value = 1
  //
  //           ## Increment tail
  //           s.next_tail.value = s.next_tail + 1
  //           if s.next_tail >= s.num_of_reqs:
  //             s.next_tail.value = 0
  //
  //       ## Assign output ports
  //       if s.reset:
  //         s.next_head.value = 0
  //         s.next_tail.value = 0
  //
  //         s.next_rdylist.value = 0
  //         s.next_valid  .value = 0
  //
  //         for i in xrange(s.num_of_reqs):
  //           s.next_data[i].value = 0
  //           s.next_tags[i].value = 0
  //         s.xcel_mem_req.rdy .value = 0
  //
  //         s.xcel_mem_resp.val.value = 0
  //         s.xcel_mem_resp.msg.value = 0
  //
  //         s.mem_req.valid    .value = 0
  //         s.mem_req.bits_addr.value = 0
  //         s.mem_req.bits_data.value = 0
  //         s.mem_req.bits_tag .value = 0
  //         s.mem_req.bits_cmd .value = 0
  //         s.mem_req.bits_typ .value = 3
  //         s.mem_req.bits_phys.value = 0
  //
  //         s.busy.value = 0
  //
  //       else:
  //         s.xcel_mem_req.rdy .value = s.wire_xcel_mem_req_rdy
  //
  //         s.xcel_mem_resp.val.value = s.wire_xcel_mem_resp_val
  //         s.xcel_mem_resp.msg.value = s.wire_xcel_mem_resp_msg
  //
  //         s.mem_req.valid    .value = s.wire_mem_req_valid
  //         s.mem_req.bits_addr.value = s.wire_mem_req_bits_addr
  //         s.mem_req.bits_data.value = s.wire_mem_req_bits_data
  //         s.mem_req.bits_tag .value = s.wire_mem_req_bits_tag
  //         s.mem_req.bits_cmd .value = s.wire_mem_req_bits_cmd
  //         s.mem_req.bits_typ .value = s.wire_mem_req_bits_typ
  //         s.mem_req.bits_phys.value = s.wire_mem_req_bits_phys
  //
  //         # Memory adapter is busy if there is a memory request in-flight
  //         s.busy.value = (s.next_head != s.next_tail) or ((s.next_head == s.next_tail) and s.next_valid[s.next_head])

  // logic for comb()
  always @ (*) begin
    tmp_msgdata = 0;
    tmp_msgaddr = 0;
    tmp_msgtag = 0;
    next_head = head;
    next_tail = tail;
    next_valid = valid;
    next_rdylist = rdylist;
    for (i=0; i < num_of_reqs; i=i+1)
    begin
      next_data[i] = data[i];
      next_tags[i] = tags[i];
    end
    ptr = 0;
    wire_xcel_mem_req_rdy = 0;
    wire_xcel_mem_resp_val = 0;
    wire_xcel_mem_resp_msg = 0;
    wire_mem_req_valid = 0;
    wire_mem_req_bits_addr = 0;
    tmp_msgaddr = xcel_mem_req_msg[(99)-1:67];
    wire_mem_req_bits_addr[(addr_sz)-1:0] = tmp_msgaddr[(addr_sz)-1:0];
    wire_mem_req_bits_data = 0;
    tmp_msgdata = xcel_mem_req_msg[(64)-1:0];
    wire_mem_req_bits_data[(data_sz)-1:0] = tmp_msgdata[(data_sz)-1:0];
    wire_mem_req_bits_tag = next_tail;
    wire_mem_req_bits_cmd = 0;
    wire_mem_req_bits_typ = max_len;
    wire_mem_req_bits_phys = 0;
    if ((xcel_mem_req_msg[(110)-1:107] == TYPE_WRITE)) begin
      wire_mem_req_bits_cmd = STORE;
    end
    else begin
      wire_mem_req_bits_cmd = LOAD;
    end
    if ((xcel_mem_resp_msg[(67)-1:64] <= 0)) begin
      wire_mem_req_bits_typ = max_len;
    end
    else begin
      if ((xcel_mem_resp_msg[(67)-1:64] <= 1)) begin
        wire_mem_req_bits_typ = 0;
      end
      else begin
        if ((xcel_mem_resp_msg[(67)-1:64] <= 2)) begin
          wire_mem_req_bits_typ = 1;
        end
        else begin
          if ((xcel_mem_resp_msg[(67)-1:64] <= 4)) begin
            wire_mem_req_bits_typ = 2;
          end
          else begin
            if ((xcel_mem_resp_msg[(67)-1:64] <= 8)) begin
              wire_mem_req_bits_typ = 3;
            end
            else begin
              wire_mem_req_bits_typ = max_len;
            end
          end
        end
      end
    end
    if (mem_resp_valid) begin
      ptr = mem_resp_bits_tag[(nreqs_nbits)-1:0];
      next_rdylist[ptr] = 1;
      next_data[ptr] = mem_resp_bits_data;
    end
    else begin
    end
    if ((next_rdylist[next_head]&&next_valid[next_head])) begin
      wire_xcel_mem_resp_val = 1;
      tmp_tag = next_tags[next_head];
      tmp_data = next_data[next_head];
      tmp_msgtag = 0;
      tmp_msgtag[(tag_sz)-1:0] = tmp_tag[(tag_sz)-1:0];
      wire_xcel_mem_resp_msg[(77)-1:69] = tmp_msgtag;
      tmp_msgdata = 0;
      tmp_msgdata[(data_sz)-1:0] = tmp_data[(data_sz)-1:0];
      wire_xcel_mem_resp_msg[(64)-1:0] = tmp_msgdata;
      if (xcel_mem_resp_rdy) begin
        next_rdylist[next_head] = 0;
        next_valid[next_head] = 0;
        next_head = (next_head+1);
        if ((next_head >= num_of_reqs)) begin
          next_head = 0;
        end
        else begin
        end
      end
      else begin
      end
    end
    else begin
    end
    if ((!next_valid[next_tail]&&mem_req_ready)) begin
      wire_xcel_mem_req_rdy = 1;
      if (xcel_mem_req_val) begin
        tmp_data = 0;
        tmp_tag = 0;
        tmp_msgdata = xcel_mem_req_msg[(64)-1:0];
        tmp_data[(data_sz)-1:0] = tmp_msgdata[(data_sz)-1:0];
        tmp_msgtag = xcel_mem_req_msg[(107)-1:99];
        tmp_tag[(tag_sz)-1:0] = tmp_msgtag[(tag_sz)-1:0];
        next_data[next_tail] = tmp_data;
        next_tags[next_tail] = tmp_tag;
        wire_mem_req_valid = 1;
        next_valid[next_tail] = 1;
        next_tail = (next_tail+1);
        if ((next_tail >= num_of_reqs)) begin
          next_tail = 0;
        end
        else begin
        end
      end
      else begin
      end
    end
    else begin
    end
    if (reset) begin
      next_head = 0;
      next_tail = 0;
      next_rdylist = 0;
      next_valid = 0;
      for (i=0; i < num_of_reqs; i=i+1)
      begin
        next_data[i] = 0;
        next_tags[i] = 0;
      end
      xcel_mem_req_rdy = 0;
      xcel_mem_resp_val = 0;
      xcel_mem_resp_msg = 0;
      mem_req_valid = 0;
      mem_req_bits_addr = 0;
      mem_req_bits_data = 0;
      mem_req_bits_tag = 0;
      mem_req_bits_cmd = 0;
      mem_req_bits_typ = 3;
      mem_req_bits_phys = 0;
      busy = 0;
    end
    else begin
      xcel_mem_req_rdy = wire_xcel_mem_req_rdy;
      xcel_mem_resp_val = wire_xcel_mem_resp_val;
      xcel_mem_resp_msg = wire_xcel_mem_resp_msg;
      mem_req_valid = wire_mem_req_valid;
      mem_req_bits_addr = wire_mem_req_bits_addr;
      mem_req_bits_data = wire_mem_req_bits_data;
      mem_req_bits_tag = wire_mem_req_bits_tag;
      mem_req_bits_cmd = wire_mem_req_bits_cmd;
      mem_req_bits_typ = wire_mem_req_bits_typ;
      mem_req_bits_phys = wire_mem_req_bits_phys;
      busy = ((next_head != next_tail)||((next_head == next_tail)&&next_valid[next_head]));
    end
  end


endmodule // MemToRoccMemAdapter_0x96ad3c9d8499f35
`default_nettype wire

//-----------------------------------------------------------------------------
// MemcpySramXcelHLS_0xa89d07e0dbd52f9
//-----------------------------------------------------------------------------
// dump-vcd: False
// verilator-xinit: zeros
`default_nettype none
module MemcpySramXcelHLS_0xa89d07e0dbd52f9
(
  input  wire [   0:0] clk,
  output wire [ 109:0] memreq_msg,
  input  wire [   0:0] memreq_rdy,
  output wire [   0:0] memreq_val,
  input  wire [  79:0] memresp_msg,
  output reg  [   0:0] memresp_rdy,
  input  wire [   0:0] memresp_val,
  input  wire [   0:0] reset,
  input  wire [ 159:0] xcelreq_msg,
  output reg  [   0:0] xcelreq_rdy,
  input  wire [   0:0] xcelreq_val,
  output wire [  68:0] xcelresp_msg,
  input  wire [   0:0] xcelresp_rdy,
  output wire [   0:0] xcelresp_val
);

  // register declarations
  reg    [   0:0] xcel$memreq_rdy;
  reg    [   0:0] xcel$xcelresp_rdy;

  // xcel temporaries
  wire   [  79:0] xcel$memresp_msg;
  wire   [   0:0] xcel$memresp_val;
  wire   [   0:0] xcel$clk;
  wire   [ 159:0] xcel$xcelreq_msg;
  wire   [   0:0] xcel$xcelreq_val;
  wire   [   0:0] xcel$reset;
  wire   [   0:0] xcel$memresp_rdy;
  wire   [   0:0] xcel$xcelreq_rdy;
  wire   [ 109:0] xcel$memreq_msg;
  wire   [   0:0] xcel$memreq_val;
  wire   [  68:0] xcel$xcelresp_msg;
  wire   [   0:0] xcel$xcelresp_val;

  MemcpySramXcelHLS_v_0x6620a72a117ce74d xcel
  (
    .memresp_msg  ( xcel$memresp_msg ),
    .memresp_val  ( xcel$memresp_val ),
    .clk          ( xcel$clk ),
    .xcelreq_msg  ( xcel$xcelreq_msg ),
    .xcelreq_val  ( xcel$xcelreq_val ),
    .reset        ( xcel$reset ),
    .memreq_rdy   ( xcel$memreq_rdy ),
    .xcelresp_rdy ( xcel$xcelresp_rdy ),
    .memresp_rdy  ( xcel$memresp_rdy ),
    .xcelreq_rdy  ( xcel$xcelreq_rdy ),
    .memreq_msg   ( xcel$memreq_msg ),
    .memreq_val   ( xcel$memreq_val ),
    .xcelresp_msg ( xcel$xcelresp_msg ),
    .xcelresp_val ( xcel$xcelresp_val )
  );

  // signal connections
  assign memreq_msg       = xcel$memreq_msg;
  assign memreq_val       = xcel$memreq_val;
  assign xcel$clk         = clk;
  assign xcel$memresp_msg = memresp_msg;
  assign xcel$memresp_val = memresp_val;
  assign xcel$reset       = reset;
  assign xcel$xcelreq_msg = xcelreq_msg;
  assign xcel$xcelreq_val = xcelreq_val;
  assign xcelresp_msg     = xcel$xcelresp_msg;
  assign xcelresp_val     = xcel$xcelresp_val;


  // PYMTL SOURCE:
  //
  // @s.combinational
  // def comb():
  //       
  //       s.xcel.memreq.rdy.value = ~s.memreq.rdy
  //       s.memresp.rdy.value = ~s.xcel.memresp.rdy
  //       
  //       s.xcel.xcelresp.rdy.value = ~s.xcelresp.rdy
  //       s.xcelreq.rdy.value = ~s.xcel.xcelreq.rdy

  // logic for comb()
  always @ (*) begin
    xcel$memreq_rdy = ~memreq_rdy;
    memresp_rdy = ~xcel$memresp_rdy;
    xcel$xcelresp_rdy = ~xcelresp_rdy;
    xcelreq_rdy = ~xcel$xcelreq_rdy;
  end


endmodule // MemcpySramXcelHLS_0xa89d07e0dbd52f9
`default_nettype wire

//-----------------------------------------------------------------------------
// MemcpySramXcelHLS_v_0x6620a72a117ce74d
//-----------------------------------------------------------------------------
// dump-vcd: True
// verilator-xinit: zeros
`default_nettype none
module MemcpySramXcelHLS_v_0x6620a72a117ce74d
(
  input  wire [   0:0] clk,
  output wire [ 109:0] memreq_msg,
  input  wire [   0:0] memreq_rdy,
  output wire [   0:0] memreq_val,
  input  wire [  79:0] memresp_msg,
  output wire [   0:0] memresp_rdy,
  input  wire [   0:0] memresp_val,
  input  wire [   0:0] reset,
  input  wire [ 159:0] xcelreq_msg,
  output wire [   0:0] xcelreq_rdy,
  input  wire [   0:0] xcelreq_val,
  output wire [  68:0] xcelresp_msg,
  input  wire [   0:0] xcelresp_rdy,
  output wire [   0:0] xcelresp_val
);

  // Imported Verilog source from:
  // /work/bits0/ka429/brg-git/new-craft/craft-pymtl-hls/sim/memcpy_sram_xcel/MemcpySramXcelHLS_v.v

  MemcpySramXcelHLS_v#(

  )  verilog_module
  (
    .clk           ( clk ),
    .memreq_busy   ( memreq_rdy ),
    .memreq_data   ( memreq_msg ),
    .memreq_vld    ( memreq_val ),
    .memresp_busy  ( memresp_rdy ),
    .memresp_data  ( memresp_msg ),
    .memresp_vld   ( memresp_val ),
    .rst           ( reset ),
    .xcelreq_busy  ( xcelreq_rdy ),
    .xcelreq_data  ( xcelreq_msg ),
    .xcelreq_vld   ( xcelreq_val ),
    .xcelresp_busy ( xcelresp_rdy ),
    .xcelresp_data ( xcelresp_msg ),
    .xcelresp_vld  ( xcelresp_val )
  );

endmodule // MemcpySramXcelHLS_v_0x6620a72a117ce74d
`default_nettype wire

//-----------------------------------------------------------------------------
// RoccXcelToXcelAdapter_0x469384a45dd936fe
//-----------------------------------------------------------------------------
// dump-vcd: False
// verilator-xinit: zeros
`default_nettype none
module RoccXcelToXcelAdapter_0x469384a45dd936fe
(
  input  wire [   0:0] clk,
  input  wire [   6:0] core_cmd_bits_inst_funct,
  input  wire [   6:0] core_cmd_bits_inst_opcode,
  input  wire [   4:0] core_cmd_bits_inst_rd,
  input  wire [   4:0] core_cmd_bits_inst_rs1,
  input  wire [   4:0] core_cmd_bits_inst_rs2,
  input  wire [   0:0] core_cmd_bits_inst_xd,
  input  wire [   0:0] core_cmd_bits_inst_xs1,
  input  wire [   0:0] core_cmd_bits_inst_xs2,
  input  wire [  63:0] core_cmd_bits_rs1,
  input  wire [  63:0] core_cmd_bits_rs2,
  output wire [   0:0] core_cmd_ready,
  input  wire [   0:0] core_cmd_valid,
  output wire [  63:0] core_resp_bits_data,
  output wire [   4:0] core_resp_bits_rd,
  input  wire [   0:0] core_resp_ready,
  output wire [   0:0] core_resp_valid,
  input  wire [   0:0] reset,
  output wire [ 159:0] xcel_req_msg,
  input  wire [   0:0] xcel_req_rdy,
  output wire [   0:0] xcel_req_val,
  input  wire [  68:0] xcel_resp_msg,
  output wire [   0:0] xcel_resp_rdy,
  input  wire [   0:0] xcel_resp_val
);

  // signal connections
  assign core_cmd_ready        = xcel_req_rdy;
  assign core_resp_bits_data   = xcel_resp_msg[63:0];
  assign core_resp_bits_rd     = xcel_resp_msg[68:64];
  assign core_resp_valid       = xcel_resp_val;
  assign xcel_req_msg[127:64]  = core_cmd_bits_rs1;
  assign xcel_req_msg[134:128] = core_cmd_bits_inst_opcode;
  assign xcel_req_msg[139:135] = core_cmd_bits_inst_rd;
  assign xcel_req_msg[140:140] = core_cmd_bits_inst_xs2;
  assign xcel_req_msg[141:141] = core_cmd_bits_inst_xs1;
  assign xcel_req_msg[142:142] = core_cmd_bits_inst_xd;
  assign xcel_req_msg[147:143] = core_cmd_bits_inst_rs1;
  assign xcel_req_msg[152:148] = core_cmd_bits_inst_rs2;
  assign xcel_req_msg[159:153] = core_cmd_bits_inst_funct;
  assign xcel_req_msg[63:0]    = core_cmd_bits_rs2;
  assign xcel_req_val          = core_cmd_valid;
  assign xcel_resp_rdy         = core_resp_ready;



endmodule // RoccXcelToXcelAdapter_0x469384a45dd936fe
`default_nettype wire

`line 1 "memcpy_sram_xcel/MemcpySramXcelHLS_v.v" 0
/* verilator lint_off CASEINCOMPLETE */
/* verilator lint_off WIDTH */
// Generated by stratus_hls 16.15-s100  (85557.011347)
// Sat Feb 18 00:53:59 2017
// from ../../sim/rocc/MemMsg8B.cc

`timescale 1ps / 1ps

      
module MemcpySramXcelHLS_v(clk, rst, xcelreq_busy, xcelreq_vld, xcelreq_data, xcelresp_busy, xcelresp_vld, xcelresp_data, memreq_busy, memreq_vld, memreq_data, memresp_busy, memresp_vld, memresp_data);

      input clk;
      input rst;
      input xcelresp_busy;
      input memreq_busy;
      input [159:0] xcelreq_data;
      input [79:0] memresp_data;
      input xcelreq_vld;
      input memresp_vld;
      output xcelreq_busy;
      output memresp_busy;
      output [68:0] xcelresp_data;
      reg [68:0] xcelresp_data;
      output [109:0] memreq_data;
      output xcelresp_vld;
      output memreq_vld;
      reg[4:0] global_state_next;
      reg[4:0] global_state;
      reg gs_ctrl3;
      reg gs_ctrl2;
      reg[2:0] gs_ctrl1;
      reg[2:0] gs_ctrl0;
      reg memreq_m_unacked_req;
      reg xcelresp_m_unacked_req;
      wire memcpy_sram_NotEQ_7Ux1U_1U_4_29_out1;
      reg[1:0] memcpy_sram_N_Mux_2_2_0_4_40_out1;
      /*signed*/wire memcpy_sram_Or_1Sx1U_1S_4_32_out1;
      reg s_reg_60;
      reg[31:0] memcpy_sram_N_Mux_32_2_1_4_49_out1;
      reg[31:0] memcpy_sram_N_Mux_32_2_1_4_46_out1;
      reg[1:0] memcpy_sram_N_Mux_2_2_0_4_42_out1;
      reg[1:0] s_reg_49;
      reg[4:0] memcpy_sram_N_MuxB_5_2_5_4_54_out1;
      reg[31:0] memcpy_sram_N_MuxB_32_2_4_4_55_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_23_out1;
      wire memcpy_sram_Not_1U_1U_4_22_out1;
      /*signed*/wire memcpy_sram_Or_1Sx1U_1S_4_19_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_7_out1;
      wire memcpy_sram_Not_1U_1U_4_6_out1;
      /*signed*/wire memcpy_sram_Or_1Sx1U_1S_4_3_out1;
      wire memcpy_sram_Not_1U_1U_4_36_out1;
      wire memcpy_sram_OrReduction_5U_1U_4_31_out1;
      wire[4:0] memcpy_sram_OrReduction_5U_1U_4_28_in1;
      reg s_reg_59;
      wire memcpy_sram_Equal_7Ux2U_1U_4_50_out1;
      wire memcpy_sram_Equal_7Ux1U_1U_4_37_out1;
      wire memcpy_sram_OrReduction_5U_1U_4_28_out1;
      reg s_reg_57;
      wire memcpy_sram_Equal_7Ux2U_1U_4_47_out1;
      wire memcpy_sram_Equal_5Ux1U_1U_4_44_out1;
      reg s_reg_48;
      wire memcpy_sram_And_1Sx1U_1U_4_56_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_51_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_48_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_45_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_38_out1;
      wire[6:0] memcpy_sram_Equal_7Ux1U_1U_4_27_in2;
      wire memcpy_sram_Equal_7Ux1U_1U_4_30_out1;
      wire memcpy_sram_Equal_7Ux1U_1U_4_27_out1;
      reg s_reg_56;
      wire memcpy_sram_OrReduction_5U_1U_4_39_out1;
      reg[4:0] s_reg_55;
      reg[31:0] s_reg_58;
      reg tsmc16_1rw_1024_64b_mbuffer_1_GWEN;
      reg tsmc16_1rw_1024_64b_mbuffer_1_CEN;
      reg[1:0] memcpy_sram_N_Mux_2_2_2_4_41_out1;
      reg[31:0] memcpy_sram_N_Mux_32_5_3_4_53_out1;
      reg[1:0] s_reg_61;
      reg[31:0] s_reg_50;
      reg[31:0] s_reg_51;
      wire[2:0] tsmc16_1rw_1024_64b_mbuffer_1_EMA;
      wire[1:0] tsmc16_1rw_1024_64b_mbuffer_1_EMAW;
      wire tsmc16_1rw_1024_64b_mbuffer_1_EMAS;
      wire tsmc16_1rw_1024_64b_mbuffer_1_RET1N;
      reg[31:0] memcpy_sram_N_Mux_32_2_1_4_52_out1;
      reg[4:0] s_reg_54;
      wire memcpy_sram_OrReduction_22U_1U_4_57_out1;
      wire[21:0] memcpy_sram_OrReduction_22U_1U_4_57_in1;
      wire memcpy_sram_LessThan_32Ux11U_1U_4_64_out1;
      reg[31:0] memcpy_sram_Add_32Ux32U_32U_4_60_in1;
      reg[28:0] s_reg_63;
      reg[31:0] memcpy_sram_Add_32Ux32U_32U_4_60_in2;
      wire memcpy_sram_LessThan_32Ux11U_1U_4_68_out1;
      reg[10:0] s_reg_53;
      wire[31:0] memcpy_sram_Add_32Ux32U_32U_4_60_out1;
      wire[63:0] tsmc16_1rw_1024_64b_mbuffer_1_Q;
      wire[9:0] tsmc16_1rw_1024_64b_mbuffer_1_A;
      reg[28:0] s_reg_62;
      reg[31:0] s_reg_52;
      /*signed*/wire[31:0] memcpy_sram_Sub_32Ux11U_32S_4_59_out1;
      reg[10:0] memcpy_sram_N_Mux_11_2_6_4_58_out1;
      reg memreq_m_req_m_trig_req;
      reg xcelresp_m_req_m_trig_req;
      reg memreq_m_req_m_prev_trig_req;
      reg xcelresp_m_req_m_prev_trig_req;
      wire memcpy_sram_Not_1U_1U_4_18_out1;
      wire memcpy_sram_Not_1U_1U_4_14_out1;
      wire[63:0] tsmc16_1rw_1024_64b_mbuffer_1_D;
      reg memresp_m_busy_req_0;
      reg xcelreq_m_busy_req_0;
      wire memcpy_sram_Not_1U_1U_4_8_out1;
      wire memcpy_sram_Not_1U_1U_4_24_out1;
      reg stall0;
      wire memcpy_sram_And_1Sx1U_1U_4_20_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_4_out1;
      reg memcpy_sram_N_Muxb_1_2_9_4_2_out1;
      reg memresp_m_unvalidated_req;
      reg memcpy_sram_N_Muxb_1_2_9_4_1_out1;
      reg xcelreq_m_unvalidated_req;
      reg[96:0] memreq_data_slice;
      wire memcpy_sram_Xor_1Ux1U_1U_4_15_out1;
      wire memcpy_sram_Xor_1Ux1U_1U_4_11_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_13_out1;
      wire memcpy_sram_And_1Sx1U_1U_4_17_out1;
      /*signed*/wire memcpy_sram_Or_1Sx1U_1S_4_16_out1;
      /*signed*/wire memcpy_sram_Or_1Sx1U_1S_4_12_out1;

         tsmc16_1rw_1024_64b tsmc16_1rw_1024_64b_mbuffer_1(
                               .CLK( clk ),
                               .STOV( tsmc16_1rw_1024_64b_mbuffer_1_EMAS ),
                               .EMA( tsmc16_1rw_1024_64b_mbuffer_1_EMA ),
                               .EMAW( tsmc16_1rw_1024_64b_mbuffer_1_EMAW ),
                               .EMAS( tsmc16_1rw_1024_64b_mbuffer_1_EMAS ),
                               .RET1N( tsmc16_1rw_1024_64b_mbuffer_1_RET1N ),
                               .GWEN( tsmc16_1rw_1024_64b_mbuffer_1_GWEN ),
                               .CEN( tsmc16_1rw_1024_64b_mbuffer_1_CEN ),
                               .D( tsmc16_1rw_1024_64b_mbuffer_1_D ),
                               .Q( tsmc16_1rw_1024_64b_mbuffer_1_Q ),
                               .A( tsmc16_1rw_1024_64b_mbuffer_1_A )
                             );

         // resource: mux_97bx2i
         // resource: regr_97b
         always @(posedge clk)
          begin :drive_memreq_data
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd14: begin
                     memreq_data_slice <= {{1'b0, memcpy_sram_Add_32Ux32U_32U_4_60_out1}, 64'd00000000000000000000};
                  end
                  
                  5'd19: begin
                     memreq_data_slice <= {1'b1, {memcpy_sram_Add_32Ux32U_32U_4_60_out1, tsmc16_1rw_1024_64b_mbuffer_1_Q}};
                  end
                  
               endcase

            end
         end

         // resource: mux_69bx2i
         // resource: regr_69b
         always @(posedge clk)
          begin :drive_xcelresp_data
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd10: begin
                     xcelresp_data <= {memcpy_sram_N_MuxB_5_2_5_4_54_out1, {32'b00000000000000000000000000000000, memcpy_sram_N_MuxB_32_2_4_4_55_out1}};
                  end
                  
                  5'd11: begin
                     if (memcpy_sram_And_1Sx1U_1U_4_56_out1) begin
                     end
                     else begin
                        case (memcpy_sram_N_Mux_11_2_6_4_58_out1) 

                           11'd0000: begin
                              if (32'd0000000000 != memcpy_sram_Sub_32Ux11U_32S_4_59_out1) begin
                                 xcelresp_data <= {40'b0000000000000000000000000000000000000000, memcpy_sram_Add_32Ux32U_32U_4_60_out1[28:0]};
                              end
                           end
                           
                           default: begin
                              xcelresp_data <= {40'b0000000000000000000000000000000000000000, memcpy_sram_Add_32Ux32U_32U_4_60_out1[28:0]};
                           end
                           
                        endcase

                     end
                  end
                  
                  5'd22: begin
                     case (memcpy_sram_N_Mux_11_2_6_4_58_out1) 

                        11'd0000: begin
                           if (32'd0000000000 != memcpy_sram_Sub_32Ux11U_32S_4_59_out1) begin
                              xcelresp_data <= {40'b0000000000000000000000000000000000000000, memcpy_sram_Add_32Ux32U_32U_4_60_out1[28:0]};
                           end
                        end
                        
                        default: begin
                           xcelresp_data <= {40'b0000000000000000000000000000000000000000, memcpy_sram_Add_32Ux32U_32U_4_60_out1[28:0]};
                        end
                        
                     endcase

                  end
                  
               endcase

            end
         end

         // resource: mux_1bx2i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_xcelreq_m_busy_req_0
            if (rst == 1'b1) begin
               xcelreq_m_busy_req_0 <= 1'd1;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state) 

                     5'd00: begin
                        xcelreq_m_busy_req_0 <= 1'd0;
                     end
                     
                     5'd01: begin
                        xcelreq_m_busy_req_0 <= 1'd1;
                     end
                     
                     5'd11: begin
                        if (memcpy_sram_And_1Sx1U_1U_4_56_out1) begin
                           xcelreq_m_busy_req_0 <= 1'd0;
                        end
                        else begin
                           if (11'd0000 == memcpy_sram_N_Mux_11_2_6_4_58_out1 && 32'd0000000000 == memcpy_sram_Sub_32Ux11U_32S_4_59_out1) begin
                              xcelreq_m_busy_req_0 <= 1'd0;
                           end
                        end
                     end
                     
                     5'd17: begin
                        if (!memcpy_sram_LessThan_32Ux11U_1U_4_64_out1 && (11'd0000 == s_reg_53 && 32'd0000000000 == s_reg_52)) begin
                           xcelreq_m_busy_req_0 <= 1'd0;
                        end
                     end
                     
                     5'd21: begin
                        if (!memcpy_sram_LessThan_32Ux11U_1U_4_68_out1 && 32'd0000000000 == s_reg_52) begin
                           xcelreq_m_busy_req_0 <= 1'd0;
                        end
                     end
                     
                     5'd22: begin
                        if (11'd0000 == memcpy_sram_N_Mux_11_2_6_4_58_out1 && 32'd0000000000 == memcpy_sram_Sub_32Ux11U_32S_4_59_out1) begin
                           xcelreq_m_busy_req_0 <= 1'd0;
                        end
                     end
                     
                  endcase

               end
            end
         end

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_xcelresp_m_req_m_trig_req
            if (rst == 1'b1) begin
               xcelresp_m_req_m_trig_req <= 1'd0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state) 

                     5'd10: begin
                        xcelresp_m_req_m_trig_req <= memcpy_sram_Not_1U_1U_4_14_out1;
                     end
                     
                  endcase

               end
            end
         end

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_memreq_m_req_m_trig_req
            if (rst == 1'b1) begin
               memreq_m_req_m_trig_req <= 1'd0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state) 

                     5'd14, 5'd19: begin
                        memreq_m_req_m_trig_req <= memcpy_sram_Not_1U_1U_4_18_out1;
                     end
                     
                  endcase

               end
            end
         end

         // resource: mux_1bx2i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_memresp_m_busy_req_0
            if (rst == 1'b1) begin
               memresp_m_busy_req_0 <= 1'd1;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state) 

                     5'd15, 5'd20: begin
                        memresp_m_busy_req_0 <= 1'd0;
                     end
                     
                     5'd16, 5'd21: begin
                        memresp_m_busy_req_0 <= 1'd1;
                     end
                     
                  endcase

               end
            end
         end

         // resource: mux_1bx5i
         always @(memcpy_sram_Not_1U_1U_4_8_out1 or memcpy_sram_And_1Sx1U_1U_4_13_out1 or memcpy_sram_And_1Sx1U_1U_4_17_out1 or memcpy_sram_Not_1U_1U_4_24_out1 or global_state)
          begin :drive_stall0
            case (global_state) 

               5'd01: begin
                  stall0 = memcpy_sram_Not_1U_1U_4_8_out1;
               end
               
               5'd11: begin
                  stall0 = memcpy_sram_And_1Sx1U_1U_4_13_out1;
               end
               
               5'd15, 5'd20: begin
                  stall0 = memcpy_sram_And_1Sx1U_1U_4_17_out1;
               end
               
               5'd16, 5'd21: begin
                  stall0 = memcpy_sram_Not_1U_1U_4_24_out1;
               end
               
               default: begin
                  stall0 = 1'b0;
               end
               
            endcase

         end

         // resource: mux_1bx2i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_s_reg_48
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd00, 5'd17, 5'd21, 5'd22: begin
                     s_reg_48 <= 1'b1;
                  end
                  
                  5'd11: begin
                     if (memcpy_sram_And_1Sx1U_1U_4_56_out1) begin
                        s_reg_48 <= memcpy_sram_And_1Sx1U_1U_4_56_out1;
                     end
                     else begin
                        s_reg_48 <= 1'b1;
                     end
                  end
                  
               endcase

            end
         end

         // resource: mux_2bx2i
         // resource: regr_2b
         always @(posedge clk)
          begin :drive_s_reg_49
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd00: begin
                     s_reg_49 <= 2'd0;
                  end
                  
                  5'd03: begin
                     s_reg_49 <= memcpy_sram_N_Mux_2_2_0_4_42_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_32bx2i
         // resource: regr_32b
         always @(posedge clk)
          begin :drive_s_reg_50
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd00: begin
                     s_reg_50 <= 32'd0000000000;
                  end
                  
                  5'd05: begin
                     s_reg_50 <= memcpy_sram_N_Mux_32_2_1_4_46_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_32bx2i
         // resource: regr_32b
         always @(posedge clk)
          begin :drive_s_reg_51
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd00: begin
                     s_reg_51 <= 32'd0000000000;
                  end
                  
                  5'd07: begin
                     s_reg_51 <= memcpy_sram_N_Mux_32_2_1_4_49_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_32bx3i
         // resource: regr_32b
         always @(posedge clk)
          begin :drive_s_reg_52
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd00: begin
                     s_reg_52 <= 32'd0000000000;
                  end
                  
                  5'd09: begin
                     s_reg_52 <= memcpy_sram_N_Mux_32_2_1_4_52_out1;
                  end
                  
                  5'd11: begin
                     if (memcpy_sram_And_1Sx1U_1U_4_56_out1) begin
                     end
                     else begin
                        s_reg_52 <= memcpy_sram_Sub_32Ux11U_32S_4_59_out1;
                     end
                  end
                  
                  5'd22: begin
                     s_reg_52 <= memcpy_sram_Sub_32Ux11U_32S_4_59_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_11bx2i
         // resource: regr_11b
         always @(posedge clk)
          begin :drive_s_reg_53
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_53 <= {4'b0000, xcelreq_data[159:153]};
                  end
                  
                  5'd11, 5'd22: begin
                     if (11'd0000 != memcpy_sram_N_Mux_11_2_6_4_58_out1) begin
                        s_reg_53 <= memcpy_sram_N_Mux_11_2_6_4_58_out1;
                     end
                  end
                  
               endcase

            end
         end

         // resource: regr_5b
         always @(posedge clk)
          begin :drive_s_reg_54
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_54 <= xcelreq_data[139:135];
                  end
                  
               endcase

            end
         end

         // resource: regr_5b
         always @(posedge clk)
          begin :drive_s_reg_55
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_55 <= xcelreq_data[152:148];
                  end
                  
               endcase

            end
         end

         // resource: mux_1bx3i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_s_reg_56
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_56 <= memcpy_sram_Equal_7Ux1U_1U_4_27_out1;
                  end
                  
                  5'd04: begin
                     s_reg_56 <= memcpy_sram_Equal_5Ux1U_1U_4_44_out1;
                  end
                  
                  5'd06: begin
                     s_reg_56 <= memcpy_sram_Equal_7Ux2U_1U_4_47_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_1bx3i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_s_reg_57
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_57 <= memcpy_sram_OrReduction_5U_1U_4_28_out1;
                  end
                  
                  5'd03: begin
                     s_reg_57 <= memcpy_sram_Equal_7Ux1U_1U_4_37_out1;
                  end
                  
                  5'd08: begin
                     s_reg_57 <= memcpy_sram_Equal_7Ux2U_1U_4_50_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_32bx4i
         // resource: regr_32b
         always @(posedge clk)
          begin :drive_s_reg_58
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd01: begin
                     s_reg_58 <= xcelreq_data[95:64];
                  end
                  
                  5'd09: begin
                     s_reg_58 <= memcpy_sram_N_Mux_32_5_3_4_53_out1;
                  end
                  
                  5'd11, 5'd22: begin
                     if (11'd0000 != memcpy_sram_N_Mux_11_2_6_4_58_out1) begin
                        s_reg_58 <= 32'd0000000000;
                     end
                  end
                  
                  5'd16, 5'd20: begin
                     s_reg_58 <= memcpy_sram_Add_32Ux32U_32U_4_60_out1;
                  end
                  
                  5'd17: begin
                     if (memcpy_sram_LessThan_32Ux11U_1U_4_64_out1) begin
                     end
                     else begin
                        if (11'd0000 != s_reg_53) begin
                           s_reg_58 <= 32'd0000000000;
                        end
                     end
                  end
                  
               endcase

            end
         end

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_s_reg_59
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd02: begin
                     s_reg_59 <= memcpy_sram_Equal_7Ux1U_1U_4_30_out1;
                  end
                  
               endcase

            end
         end

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_s_reg_60
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd02: begin
                     s_reg_60 <= memcpy_sram_Or_1Sx1U_1S_4_32_out1;
                  end
                  
               endcase

            end
         end

         // resource: regr_2b
         always @(posedge clk)
          begin :drive_s_reg_61
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd03: begin
                     s_reg_61 <= memcpy_sram_N_Mux_2_2_0_4_40_out1;
                  end
                  
               endcase

            end
         end

         // resource: mux_29bx2i
         // resource: regr_29b
         always @(posedge clk)
          begin :drive_s_reg_62
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd11: begin
                     if (11'd0000 != memcpy_sram_N_Mux_11_2_6_4_58_out1) begin
                        s_reg_62 <= 29'd000000000;
                     end
                  end
                  
                  5'd22: begin
                     if (11'd0000 != memcpy_sram_N_Mux_11_2_6_4_58_out1) begin
                        s_reg_62 <= xcelresp_data[28:0];
                     end
                  end
                  
               endcase

            end
         end

         // resource: regr_29b
         always @(posedge clk)
          begin :drive_s_reg_63
            if (stall0) begin
            end
            else begin
               case (global_state) 

                  5'd13, 5'd18: begin
                     s_reg_63 <= memcpy_sram_Add_32Ux32U_32U_4_60_out1[28:0];
                  end
                  
               endcase

            end
         end

         assign memcpy_sram_Equal_7Ux1U_1U_4_27_in2 = xcelreq_data[159:153];

         // resource: memcpy_sram_Equal_7Ux1U_1U_4  instance: memcpy_sram_Equal_7Ux1U_1U_4_27
         assign memcpy_sram_Equal_7Ux1U_1U_4_27_out1 = memcpy_sram_Equal_7Ux1U_1U_4_27_in2 == 7'd001;

         assign memcpy_sram_OrReduction_5U_1U_4_28_in1 = xcelreq_data[139:135];

         // resource: memcpy_sram_OrReduction_5U_1U_4  instance: memcpy_sram_OrReduction_5U_1U_4_28
         assign memcpy_sram_OrReduction_5U_1U_4_28_out1 = |memcpy_sram_OrReduction_5U_1U_4_28_in1;

         // resource: memcpy_sram_NotEQ_7Ux1U_1U_4  instance: memcpy_sram_NotEQ_7Ux1U_1U_4_29
         assign memcpy_sram_NotEQ_7Ux1U_1U_4_29_out1 = s_reg_53[6:0] != 7'd001;

         // resource: memcpy_sram_Equal_7Ux1U_1U_4  instance: memcpy_sram_Equal_7Ux1U_1U_4_30
         assign memcpy_sram_Equal_7Ux1U_1U_4_30_out1 = s_reg_53[6:0] == 7'd001;

         // resource: memcpy_sram_OrReduction_5U_1U_4  instance: memcpy_sram_OrReduction_5U_1U_4_31
         assign memcpy_sram_OrReduction_5U_1U_4_31_out1 = |s_reg_54;

         // resource: memcpy_sram_Or_1Sx1U_1S_4  instance: memcpy_sram_Or_1Sx1U_1S_4_32
         assign memcpy_sram_Or_1Sx1U_1S_4_32_out1 = memcpy_sram_NotEQ_7Ux1U_1U_4_29_out1 | memcpy_sram_OrReduction_5U_1U_4_31_out1;

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_36
         assign memcpy_sram_Not_1U_1U_4_36_out1 = !s_reg_57;

         // resource: memcpy_sram_Equal_7Ux1U_1U_4  instance: memcpy_sram_Equal_7Ux1U_1U_4_37
         assign memcpy_sram_Equal_7Ux1U_1U_4_37_out1 = s_reg_53[6:0] == 7'd001;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_38
         assign memcpy_sram_And_1Sx1U_1U_4_38_out1 = memcpy_sram_Equal_7Ux1U_1U_4_37_out1 & memcpy_sram_Not_1U_1U_4_36_out1;

         // resource: memcpy_sram_OrReduction_5U_1U_4  instance: memcpy_sram_OrReduction_5U_1U_4_39
         assign memcpy_sram_OrReduction_5U_1U_4_39_out1 = |s_reg_55;

         // resource: memcpy_sram_N_Mux_2_2_0_4
         always @(s_reg_49 or s_reg_58[1:0] or memcpy_sram_And_1Sx1U_1U_4_38_out1)
          begin :memcpy_sram_N_Mux_2_2_0_4_40
            if (memcpy_sram_And_1Sx1U_1U_4_38_out1) begin
               memcpy_sram_N_Mux_2_2_0_4_40_out1 = s_reg_58[1:0];
            end
            else begin
               memcpy_sram_N_Mux_2_2_0_4_40_out1 = s_reg_49;
            end
         end

         // resource: memcpy_sram_N_Mux_2_2_2_4
         always @(s_reg_49 or memcpy_sram_OrReduction_5U_1U_4_39_out1)
          begin :memcpy_sram_N_Mux_2_2_2_4_41
            if (memcpy_sram_OrReduction_5U_1U_4_39_out1) begin
               memcpy_sram_N_Mux_2_2_2_4_41_out1 = s_reg_49;
            end
            else begin
               memcpy_sram_N_Mux_2_2_2_4_41_out1 = 2'd0;
            end
         end

         // resource: memcpy_sram_N_Mux_2_2_0_4
         always @(s_reg_56 or memcpy_sram_N_Mux_2_2_0_4_40_out1 or memcpy_sram_N_Mux_2_2_2_4_41_out1)
          begin :memcpy_sram_N_Mux_2_2_0_4_42
            if (s_reg_56) begin
               memcpy_sram_N_Mux_2_2_0_4_42_out1 = memcpy_sram_N_Mux_2_2_0_4_40_out1;
            end
            else begin
               memcpy_sram_N_Mux_2_2_0_4_42_out1 = memcpy_sram_N_Mux_2_2_2_4_41_out1;
            end
         end

         // resource: memcpy_sram_Equal_5Ux1U_1U_4  instance: memcpy_sram_Equal_5Ux1U_1U_4_44
         assign memcpy_sram_Equal_5Ux1U_1U_4_44_out1 = s_reg_54 == 5'd01;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_45
         assign memcpy_sram_And_1Sx1U_1U_4_45_out1 = s_reg_57 & s_reg_56;

         // resource: memcpy_sram_N_Mux_32_2_1_4
         always @(s_reg_50 or s_reg_58 or memcpy_sram_And_1Sx1U_1U_4_45_out1)
          begin :memcpy_sram_N_Mux_32_2_1_4_46
            if (memcpy_sram_And_1Sx1U_1U_4_45_out1) begin
               memcpy_sram_N_Mux_32_2_1_4_46_out1 = s_reg_58;
            end
            else begin
               memcpy_sram_N_Mux_32_2_1_4_46_out1 = s_reg_50;
            end
         end

         // resource: memcpy_sram_Equal_7Ux2U_1U_4  instance: memcpy_sram_Equal_7Ux2U_1U_4_47
         assign memcpy_sram_Equal_7Ux2U_1U_4_47_out1 = s_reg_54 == 5'd02;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_48
         assign memcpy_sram_And_1Sx1U_1U_4_48_out1 = s_reg_59 & s_reg_56;

         // resource: memcpy_sram_N_Mux_32_2_1_4
         always @(s_reg_51 or s_reg_58 or memcpy_sram_And_1Sx1U_1U_4_48_out1)
          begin :memcpy_sram_N_Mux_32_2_1_4_49
            if (memcpy_sram_And_1Sx1U_1U_4_48_out1) begin
               memcpy_sram_N_Mux_32_2_1_4_49_out1 = s_reg_58;
            end
            else begin
               memcpy_sram_N_Mux_32_2_1_4_49_out1 = s_reg_51;
            end
         end

         // resource: memcpy_sram_Equal_7Ux2U_1U_4  instance: memcpy_sram_Equal_7Ux2U_1U_4_50
         assign memcpy_sram_Equal_7Ux2U_1U_4_50_out1 = s_reg_54 == 5'd03;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_51
         assign memcpy_sram_And_1Sx1U_1U_4_51_out1 = s_reg_59 & s_reg_57;

         // resource: memcpy_sram_N_Mux_32_2_1_4
         always @(s_reg_52 or s_reg_58 or memcpy_sram_And_1Sx1U_1U_4_51_out1)
          begin :memcpy_sram_N_Mux_32_2_1_4_52
            if (memcpy_sram_And_1Sx1U_1U_4_51_out1) begin
               memcpy_sram_N_Mux_32_2_1_4_52_out1 = s_reg_58;
            end
            else begin
               memcpy_sram_N_Mux_32_2_1_4_52_out1 = s_reg_52;
            end
         end

         // resource: memcpy_sram_N_Mux_32_5_3_4
         always @(s_reg_50 or s_reg_51 or s_reg_55 or s_reg_61 or memcpy_sram_N_Mux_32_2_1_4_52_out1)
          begin :memcpy_sram_N_Mux_32_5_3_4_53
            case (s_reg_55) 

               5'd00: begin
                  memcpy_sram_N_Mux_32_5_3_4_53_out1 = {30'b000000000000000000000000000000, s_reg_61};
               end
               
               5'd01: begin
                  memcpy_sram_N_Mux_32_5_3_4_53_out1 = s_reg_50;
               end
               
               5'd02: begin
                  memcpy_sram_N_Mux_32_5_3_4_53_out1 = s_reg_51;
               end
               
               5'd03: begin
                  memcpy_sram_N_Mux_32_5_3_4_53_out1 = memcpy_sram_N_Mux_32_2_1_4_52_out1;
               end
               
               default: begin
                  memcpy_sram_N_Mux_32_5_3_4_53_out1 = 32'd0000000000;
               end
               
            endcase

         end

         // resource: memcpy_sram_N_MuxB_5_2_5_4
         always @(s_reg_54 or s_reg_59)
          begin :memcpy_sram_N_MuxB_5_2_5_4_54
            if (s_reg_59) begin
               memcpy_sram_N_MuxB_5_2_5_4_54_out1 = 5'd00;
            end
            else begin
               memcpy_sram_N_MuxB_5_2_5_4_54_out1 = s_reg_54;
            end
         end

         // resource: memcpy_sram_N_MuxB_32_2_4_4
         always @(s_reg_58 or s_reg_59)
          begin :memcpy_sram_N_MuxB_32_2_4_4_55
            if (s_reg_59) begin
               memcpy_sram_N_MuxB_32_2_4_4_55_out1 = 32'd0000000000;
            end
            else begin
               memcpy_sram_N_MuxB_32_2_4_4_55_out1 = s_reg_58;
            end
         end

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_56
         assign memcpy_sram_And_1Sx1U_1U_4_56_out1 = s_reg_60 & s_reg_48;

         assign memcpy_sram_OrReduction_22U_1U_4_57_in1 = s_reg_52[31:10];

         // resource: memcpy_sram_OrReduction_22U_1U_4  instance: memcpy_sram_OrReduction_22U_1U_4_57
         assign memcpy_sram_OrReduction_22U_1U_4_57_out1 = |memcpy_sram_OrReduction_22U_1U_4_57_in1;

         // resource: memcpy_sram_N_Mux_11_2_6_4
         always @(s_reg_52[10:0] or memcpy_sram_OrReduction_22U_1U_4_57_out1)
          begin :memcpy_sram_N_Mux_11_2_6_4_58
            if (memcpy_sram_OrReduction_22U_1U_4_57_out1) begin
               memcpy_sram_N_Mux_11_2_6_4_58_out1 = 11'd1024;
            end
            else begin
               memcpy_sram_N_Mux_11_2_6_4_58_out1 = s_reg_52[10:0];
            end
         end

         // resource: memcpy_sram_Sub_32Ux11U_32S_4  instance: memcpy_sram_Sub_32Ux11U_32S_4_59
         assign memcpy_sram_Sub_32Ux11U_32S_4_59_out1 = s_reg_52 - {21'b000000000000000000000, memcpy_sram_N_Mux_11_2_6_4_58_out1};

         // resource: mux_32bx5i
         always @(xcelresp_data[28:0] or s_reg_58 or s_reg_63 or gs_ctrl0)
          begin :drive_memcpy_sram_Add_32Ux32U_32U_4_60_in2
            case (gs_ctrl0) 

               3'd1: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in2 = {3'b000, s_reg_58[28:0]};
               end
               
               3'd2: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in2 = {s_reg_63, 3'd0};
               end
               
               3'd3: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in2 = s_reg_58;
               end
               
               3'd4: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in2 = {3'b000, xcelresp_data[28:0]};
               end
               
               default: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in2 = 32'd0000000000;
               end
               
            endcase

         end

         // resource: mux_32bx5i
         always @(s_reg_50 or s_reg_51 or s_reg_62 or memcpy_sram_N_Mux_11_2_6_4_58_out1 or gs_ctrl1)
          begin :drive_memcpy_sram_Add_32Ux32U_32U_4_60_in1
            case (gs_ctrl1) 

               3'd1: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in1 = {3'b000, s_reg_62};
               end
               
               3'd2: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in1 = s_reg_50;
               end
               
               3'd3: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in1 = 32'd0000000001;
               end
               
               3'd4: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in1 = s_reg_51;
               end
               
               default: begin
                  memcpy_sram_Add_32Ux32U_32U_4_60_in1 = {21'b000000000000000000000, memcpy_sram_N_Mux_11_2_6_4_58_out1};
               end
               
            endcase

         end

         // resource: memcpy_sram_Add_32Ux32U_32U_4  instance: memcpy_sram_Add_32Ux32U_32U_4_60
         assign memcpy_sram_Add_32Ux32U_32U_4_60_out1 = memcpy_sram_Add_32Ux32U_32U_4_60_in2 + memcpy_sram_Add_32Ux32U_32U_4_60_in1;

         // resource: memcpy_sram_LessThan_32Ux11U_1U_4  instance: memcpy_sram_LessThan_32Ux11U_1U_4_64
         assign memcpy_sram_LessThan_32Ux11U_1U_4_64_out1 = s_reg_58 < {21'b000000000000000000000, s_reg_53};

         assign tsmc16_1rw_1024_64b_mbuffer_1_A = s_reg_58[9:0];

         assign tsmc16_1rw_1024_64b_mbuffer_1_D = memresp_data[63:0];

         // resource: mux_1bx2i
         always @(gs_ctrl2)
          begin :drive_tsmc16_1rw_1024_64b_mbuffer_1_GWEN
            if (gs_ctrl2) begin
               tsmc16_1rw_1024_64b_mbuffer_1_GWEN = 1'd0;
            end
            else begin
               tsmc16_1rw_1024_64b_mbuffer_1_GWEN = 1'd1;
            end
         end

         // resource: mux_1bx2i
         always @(gs_ctrl3)
          begin :drive_tsmc16_1rw_1024_64b_mbuffer_1_CEN
            if (gs_ctrl3) begin
               tsmc16_1rw_1024_64b_mbuffer_1_CEN = 1'd0;
            end
            else begin
               tsmc16_1rw_1024_64b_mbuffer_1_CEN = 1'd1;
            end
         end

         // resource: memcpy_sram_LessThan_32Ux11U_1U_4  instance: memcpy_sram_LessThan_32Ux11U_1U_4_68
         assign memcpy_sram_LessThan_32Ux11U_1U_4_68_out1 = s_reg_58 < {21'b000000000000000000000, s_reg_53};

         // resource: regr_5b
         always @(posedge clk)
          begin :drive_global_state
            if (rst == 1'b1) begin
               global_state <= 5'd00;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  global_state <= global_state_next;
               end
            end
         end

         // resource: mux_5bx5i
         always @(s_reg_52 or s_reg_53 or memcpy_sram_And_1Sx1U_1U_4_56_out1 or memcpy_sram_N_Mux_11_2_6_4_58_out1 or memcpy_sram_Sub_32Ux11U_32S_4_59_out1 or memcpy_sram_LessThan_32Ux11U_1U_4_64_out1 or memcpy_sram_LessThan_32Ux11U_1U_4_68_out1 or global_state)
          begin :drive_global_state_next
            case (global_state) 

               5'd11: begin
                  if (memcpy_sram_And_1Sx1U_1U_4_56_out1) begin
                     global_state_next = 5'd01;
                  end
                  else begin
                     case (memcpy_sram_N_Mux_11_2_6_4_58_out1) 

                        11'd0000: begin
                           case (memcpy_sram_Sub_32Ux11U_32S_4_59_out1) 

                              32'd0000000000: begin
                                 global_state_next = 5'd01;
                              end
                              
                              default: begin
                                 global_state_next = 5'd22;
                              end
                              
                           endcase

                        end
                        
                        default: begin
                           global_state_next = global_state + 5'd01;
                        end
                        
                     endcase

                  end
               end
               
               5'd17: begin
                  if (memcpy_sram_LessThan_32Ux11U_1U_4_64_out1) begin
                     global_state_next = 5'd12;
                  end
                  else begin
                     case (s_reg_53) 

                        11'd0000: begin
                           case (s_reg_52) 

                              32'd0000000000: begin
                                 global_state_next = 5'd01;
                              end
                              
                              default: begin
                                 global_state_next = 5'd22;
                              end
                              
                           endcase

                        end
                        
                        default: begin
                           global_state_next = global_state + 5'd01;
                        end
                        
                     endcase

                  end
               end
               
               5'd21: begin
                  if (memcpy_sram_LessThan_32Ux11U_1U_4_68_out1) begin
                     global_state_next = 5'd18;
                  end
                  else begin
                     case (s_reg_52) 

                        32'd0000000000: begin
                           global_state_next = 5'd01;
                        end
                        
                        default: begin
                           global_state_next = global_state + 5'd01;
                        end
                        
                     endcase

                  end
               end
               
               5'd22: begin
                  case (memcpy_sram_N_Mux_11_2_6_4_58_out1) 

                     11'd0000: begin
                        case (memcpy_sram_Sub_32Ux11U_32S_4_59_out1) 

                           32'd0000000000: begin
                              global_state_next = 5'd01;
                           end
                           
                           default: begin
                              global_state_next = 5'd22;
                           end
                           
                        endcase

                     end
                     
                     default: begin
                        global_state_next = 5'd12;
                     end
                     
                  endcase

               end
               
               default: begin
                  global_state_next = global_state + 5'd01;
               end
               
            endcase

         end

         // resource: mux_3bx5i
         // resource: regr_3b
         always @(posedge clk)
          begin :drive_gs_ctrl0
            if (rst == 1'b1) begin
               gs_ctrl0 <= 3'd0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state_next) 

                     5'd13, 5'd18: begin
                        gs_ctrl0 <= 3'd1;
                     end
                     
                     5'd14, 5'd19: begin
                        gs_ctrl0 <= 3'd2;
                     end
                     
                     5'd16, 5'd20: begin
                        gs_ctrl0 <= 3'd3;
                     end
                     
                     5'd22: begin
                        gs_ctrl0 <= 3'd4;
                     end
                     
                     default: begin
                        gs_ctrl0 <= 3'd0;
                     end
                     
                  endcase

               end
            end
         end

         // resource: mux_3bx5i
         // resource: regr_3b
         always @(posedge clk)
          begin :drive_gs_ctrl1
            if (rst == 1'b1) begin
               gs_ctrl1 <= 3'd0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state_next) 

                     5'd13, 5'd18: begin
                        gs_ctrl1 <= 3'd1;
                     end
                     
                     5'd14: begin
                        gs_ctrl1 <= 3'd2;
                     end
                     
                     5'd16, 5'd20: begin
                        gs_ctrl1 <= 3'd3;
                     end
                     
                     5'd19: begin
                        gs_ctrl1 <= 3'd4;
                     end
                     
                     default: begin
                        gs_ctrl1 <= 3'd0;
                     end
                     
                  endcase

               end
            end
         end

         // resource: mux_1bx2i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_gs_ctrl2
            if (rst == 1'b1) begin
               gs_ctrl2 <= 1'b0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state_next) 

                     5'd16: begin
                        gs_ctrl2 <= 1'b1;
                     end
                     
                     default: begin
                        gs_ctrl2 <= 1'b0;
                     end
                     
                  endcase

               end
            end
         end

         // resource: mux_1bx2i
         // resource: regr_1b
         always @(posedge clk)
          begin :drive_gs_ctrl3
            if (rst == 1'b1) begin
               gs_ctrl3 <= 1'b0;
            end
            else begin
               if (stall0) begin
               end
               else begin
                  case (global_state_next) 

                     5'd16, 5'd18: begin
                        gs_ctrl3 <= 1'b1;
                     end
                     
                     default: begin
                        gs_ctrl3 <= 1'b0;
                     end
                     
                  endcase

               end
            end
         end

         assign tsmc16_1rw_1024_64b_mbuffer_1_RET1N = 1'd1;

         assign xcelreq_busy = memcpy_sram_And_1Sx1U_1U_4_4_out1;

         // resource: memcpy_sram_Or_1Sx1U_1S_4  instance: memcpy_sram_Or_1Sx1U_1S_4_3
         assign memcpy_sram_Or_1Sx1U_1S_4_3_out1 = xcelreq_m_unvalidated_req | xcelreq_vld;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_4
         assign memcpy_sram_And_1Sx1U_1U_4_4_out1 = memcpy_sram_Or_1Sx1U_1S_4_3_out1 & xcelreq_m_busy_req_0;

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_6
         assign memcpy_sram_Not_1U_1U_4_6_out1 = !memcpy_sram_And_1Sx1U_1U_4_4_out1;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_7
         assign memcpy_sram_And_1Sx1U_1U_4_7_out1 = memcpy_sram_Not_1U_1U_4_6_out1 & xcelreq_vld;

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_8
         assign memcpy_sram_Not_1U_1U_4_8_out1 = !memcpy_sram_And_1Sx1U_1U_4_7_out1;

         assign tsmc16_1rw_1024_64b_mbuffer_1_EMAS = 1'd0;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_xcelreq_m_unvalidated_req
            if (rst == 1'b1) begin
               xcelreq_m_unvalidated_req <= 1'd1;
            end
            else begin
               xcelreq_m_unvalidated_req <= memcpy_sram_N_Muxb_1_2_9_4_1_out1;
            end
         end

         // resource: memcpy_sram_N_Muxb_1_2_9_4
         always @(xcelreq_vld or xcelreq_m_busy_req_0 or xcelreq_m_unvalidated_req)
          begin :memcpy_sram_N_Muxb_1_2_9_4_1
            if (xcelreq_m_busy_req_0) begin
               memcpy_sram_N_Muxb_1_2_9_4_1_out1 = xcelreq_m_unvalidated_req;
            end
            else begin
               memcpy_sram_N_Muxb_1_2_9_4_1_out1 = xcelreq_vld;
            end
         end

         assign tsmc16_1rw_1024_64b_mbuffer_1_EMAW = 2'd0;

         assign tsmc16_1rw_1024_64b_mbuffer_1_EMA = 3'd0;

         assign xcelresp_vld = memcpy_sram_Or_1Sx1U_1S_4_12_out1;

         // resource: memcpy_sram_Or_1Sx1U_1S_4  instance: memcpy_sram_Or_1Sx1U_1S_4_12
         assign memcpy_sram_Or_1Sx1U_1S_4_12_out1 = xcelresp_m_unacked_req | memcpy_sram_Xor_1Ux1U_1U_4_11_out1;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_xcelresp_m_unacked_req
            if (rst == 1'b1) begin
               xcelresp_m_unacked_req <= 1'd0;
            end
            else begin
               xcelresp_m_unacked_req <= memcpy_sram_And_1Sx1U_1U_4_13_out1;
            end
         end

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_13
         assign memcpy_sram_And_1Sx1U_1U_4_13_out1 = xcelresp_busy & xcelresp_vld;

         // resource: memcpy_sram_Xor_1Ux1U_1U_4  instance: memcpy_sram_Xor_1Ux1U_1U_4_11
         assign memcpy_sram_Xor_1Ux1U_1U_4_11_out1 = xcelresp_m_req_m_trig_req ^ xcelresp_m_req_m_prev_trig_req;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_xcelresp_m_req_m_prev_trig_req
            if (rst == 1'b1) begin
               xcelresp_m_req_m_prev_trig_req <= 1'd0;
            end
            else begin
               xcelresp_m_req_m_prev_trig_req <= xcelresp_m_req_m_trig_req;
            end
         end

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_14
         assign memcpy_sram_Not_1U_1U_4_14_out1 = !xcelresp_m_req_m_trig_req;

         assign memreq_vld = memcpy_sram_Or_1Sx1U_1S_4_16_out1;

         // resource: memcpy_sram_Or_1Sx1U_1S_4  instance: memcpy_sram_Or_1Sx1U_1S_4_16
         assign memcpy_sram_Or_1Sx1U_1S_4_16_out1 = memreq_m_unacked_req | memcpy_sram_Xor_1Ux1U_1U_4_15_out1;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_memreq_m_unacked_req
            if (rst == 1'b1) begin
               memreq_m_unacked_req <= 1'd0;
            end
            else begin
               memreq_m_unacked_req <= memcpy_sram_And_1Sx1U_1U_4_17_out1;
            end
         end

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_17
         assign memcpy_sram_And_1Sx1U_1U_4_17_out1 = memreq_busy & memreq_vld;

         // resource: memcpy_sram_Xor_1Ux1U_1U_4  instance: memcpy_sram_Xor_1Ux1U_1U_4_15
         assign memcpy_sram_Xor_1Ux1U_1U_4_15_out1 = memreq_m_req_m_trig_req ^ memreq_m_req_m_prev_trig_req;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_memreq_m_req_m_prev_trig_req
            if (rst == 1'b1) begin
               memreq_m_req_m_prev_trig_req <= 1'd0;
            end
            else begin
               memreq_m_req_m_prev_trig_req <= memreq_m_req_m_trig_req;
            end
         end

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_18
         assign memcpy_sram_Not_1U_1U_4_18_out1 = !memreq_m_req_m_trig_req;

         assign memresp_busy = memcpy_sram_And_1Sx1U_1U_4_20_out1;

         // resource: memcpy_sram_Or_1Sx1U_1S_4  instance: memcpy_sram_Or_1Sx1U_1S_4_19
         assign memcpy_sram_Or_1Sx1U_1S_4_19_out1 = memresp_m_unvalidated_req | memresp_vld;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_20
         assign memcpy_sram_And_1Sx1U_1U_4_20_out1 = memcpy_sram_Or_1Sx1U_1S_4_19_out1 & memresp_m_busy_req_0;

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_22
         assign memcpy_sram_Not_1U_1U_4_22_out1 = !memcpy_sram_And_1Sx1U_1U_4_20_out1;

         // resource: memcpy_sram_And_1Sx1U_1U_4  instance: memcpy_sram_And_1Sx1U_1U_4_23
         assign memcpy_sram_And_1Sx1U_1U_4_23_out1 = memcpy_sram_Not_1U_1U_4_22_out1 & memresp_vld;

         // resource: memcpy_sram_Not_1U_1U_4  instance: memcpy_sram_Not_1U_1U_4_24
         assign memcpy_sram_Not_1U_1U_4_24_out1 = !memcpy_sram_And_1Sx1U_1U_4_23_out1;

         // resource: regr_1b
         always @(posedge clk)
          begin :drive_memresp_m_unvalidated_req
            if (rst == 1'b1) begin
               memresp_m_unvalidated_req <= 1'd1;
            end
            else begin
               memresp_m_unvalidated_req <= memcpy_sram_N_Muxb_1_2_9_4_2_out1;
            end
         end

         // resource: memcpy_sram_N_Muxb_1_2_9_4
         always @(memresp_vld or memresp_m_busy_req_0 or memresp_m_unvalidated_req)
          begin :memcpy_sram_N_Muxb_1_2_9_4_2
            if (memresp_m_busy_req_0) begin
               memcpy_sram_N_Muxb_1_2_9_4_2_out1 = memresp_m_unvalidated_req;
            end
            else begin
               memcpy_sram_N_Muxb_1_2_9_4_2_out1 = memresp_vld;
            end
         end

         assign memreq_data = {{2'b00, memreq_data_slice[96]}, {8'd000, {memreq_data_slice[95:64], {3'd0, memreq_data_slice[63:0]}}}};


endmodule

