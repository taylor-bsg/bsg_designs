module Accel0(input clk, input reset,
    output io_cmd_ready,
    input  io_cmd_valid,
    input [6:0] io_cmd_bits_inst_funct,
    input [4:0] io_cmd_bits_inst_rs2,
    input [4:0] io_cmd_bits_inst_rs1,
    input  io_cmd_bits_inst_xd,
    input  io_cmd_bits_inst_xs1,
    input  io_cmd_bits_inst_xs2,
    input [4:0] io_cmd_bits_inst_rd,
    input [6:0] io_cmd_bits_inst_opcode,
    input [63:0] io_cmd_bits_rs1,
    input [63:0] io_cmd_bits_rs2,
    input  io_resp_ready,
    output io_resp_valid,
    output[4:0] io_resp_bits_rd,
    output[63:0] io_resp_bits_data,
    input  io_mem_req_ready,
    output io_mem_req_valid,
    output[39:0] io_mem_req_bits_addr,
    output[9:0] io_mem_req_bits_tag,
    output[4:0] io_mem_req_bits_cmd,
    output[2:0] io_mem_req_bits_typ,
    //output io_mem_req_bits_kill
    output io_mem_req_bits_phys,
    output[63:0] io_mem_req_bits_data,
    input  io_mem_resp_valid,
    input [39:0] io_mem_resp_bits_addr,
    input [9:0] io_mem_resp_bits_tag,
    input [4:0] io_mem_resp_bits_cmd,
    input [2:0] io_mem_resp_bits_typ,
    input [63:0] io_mem_resp_bits_data,
    input  io_mem_resp_bits_nack,
    input  io_mem_resp_bits_replay,
    input  io_mem_resp_bits_has_data,
    input [63:0] io_mem_resp_bits_data_word_bypass,
    input [63:0] io_mem_resp_bits_store_data,
    //input  io_mem_replay_next_valid
    //input [9:0] io_mem_replay_next_bits
    //input  io_mem_xcpt_ma_ld
    //input  io_mem_xcpt_ma_st
    //input  io_mem_xcpt_pf_ld
    //input  io_mem_xcpt_pf_st
    //output io_mem_invalidate_lr
    //input  io_mem_ordered
    output io_busy,
    input  io_s,
    output io_interrupt,
    input  io_autl_acquire_ready,
    output io_autl_acquire_valid,
    //output[25:0] io_autl_acquire_bits_addr_block
    //output[2:0] io_autl_acquire_bits_client_xact_id
    //output[1:0] io_autl_acquire_bits_addr_beat
    //output io_autl_acquire_bits_is_builtin_type
    //output[2:0] io_autl_acquire_bits_a_type
    //output[16:0] io_autl_acquire_bits_union
    //output[127:0] io_autl_acquire_bits_data
    output io_autl_grant_ready,
    input  io_autl_grant_valid,
    input [1:0] io_autl_grant_bits_addr_beat,
    input [2:0] io_autl_grant_bits_client_xact_id,
    input [3:0] io_autl_grant_bits_manager_xact_id,
    input  io_autl_grant_bits_is_builtin_type,
    input [3:0] io_autl_grant_bits_g_type,
    input [127:0] io_autl_grant_bits_data,
    //input  io_fpu_req_ready
    //output io_fpu_req_valid
    //output[4:0] io_fpu_req_bits_cmd
    //output io_fpu_req_bits_ldst
    //output io_fpu_req_bits_wen
    //output io_fpu_req_bits_ren1
    //output io_fpu_req_bits_ren2
    //output io_fpu_req_bits_ren3
    //output io_fpu_req_bits_swap12
    //output io_fpu_req_bits_swap23
    //output io_fpu_req_bits_single
    //output io_fpu_req_bits_fromint
    //output io_fpu_req_bits_toint
    //output io_fpu_req_bits_fastpipe
    //output io_fpu_req_bits_fma
    //output io_fpu_req_bits_div
    //output io_fpu_req_bits_sqrt
    //output io_fpu_req_bits_round
    //output io_fpu_req_bits_wflags
    //output[2:0] io_fpu_req_bits_rm
    //output[1:0] io_fpu_req_bits_typ
    //output[64:0] io_fpu_req_bits_in1
    //output[64:0] io_fpu_req_bits_in2
    //output[64:0] io_fpu_req_bits_in3
    //output io_fpu_resp_ready
    //input  io_fpu_resp_valid
    //input [64:0] io_fpu_resp_bits_data
    //input [4:0] io_fpu_resp_bits_exc
    input  io_exception,
    //input [11:0] io_csr_waddr
    //input [63:0] io_csr_wdata
    //input  io_csr_wen
    input  io_host_id
);

  wire bb_io_cmd_ready;
  wire bb_io_resp_valid;
  wire[4:0] bb_io_resp_bits_rd;
  wire[63:0] bb_io_resp_bits_data;
  wire bb_io_mem_req_valid;
  wire[39:0] bb_io_mem_req_bits_addr;
  wire[9:0] bb_io_mem_req_bits_tag;
  wire[4:0] bb_io_mem_req_bits_cmd;
  wire[2:0] bb_io_mem_req_bits_typ;
  wire[63:0] bb_io_mem_req_bits_data;
  wire bb_io_busy;


`ifndef SYNTHESIS
// synthesis translate_off
//  assign io_fpu_resp_ready = {1{$random}};
//  assign io_fpu_req_bits_in3 = {3{$random}};
//  assign io_fpu_req_bits_in2 = {3{$random}};
//  assign io_fpu_req_bits_in1 = {3{$random}};
//  assign io_fpu_req_bits_typ = {1{$random}};
//  assign io_fpu_req_bits_rm = {1{$random}};
//  assign io_fpu_req_bits_wflags = {1{$random}};
//  assign io_fpu_req_bits_round = {1{$random}};
//  assign io_fpu_req_bits_sqrt = {1{$random}};
//  assign io_fpu_req_bits_div = {1{$random}};
//  assign io_fpu_req_bits_fma = {1{$random}};
//  assign io_fpu_req_bits_fastpipe = {1{$random}};
//  assign io_fpu_req_bits_toint = {1{$random}};
//  assign io_fpu_req_bits_fromint = {1{$random}};
//  assign io_fpu_req_bits_single = {1{$random}};
//  assign io_fpu_req_bits_swap23 = {1{$random}};
//  assign io_fpu_req_bits_swap12 = {1{$random}};
//  assign io_fpu_req_bits_ren3 = {1{$random}};
//  assign io_fpu_req_bits_ren2 = {1{$random}};
//  assign io_fpu_req_bits_ren1 = {1{$random}};
//  assign io_fpu_req_bits_wen = {1{$random}};
//  assign io_fpu_req_bits_ldst = {1{$random}};
//  assign io_fpu_req_bits_cmd = {1{$random}};
//  assign io_fpu_req_valid = {1{$random}};
//  assign io_autl_acquire_bits_data = {4{$random}};
//  assign io_autl_acquire_bits_union = {1{$random}};
//  assign io_autl_acquire_bits_a_type = {1{$random}};
//  assign io_autl_acquire_bits_is_builtin_type = {1{$random}};
//  assign io_autl_acquire_bits_addr_beat = {1{$random}};
//  assign io_autl_acquire_bits_client_xact_id = {1{$random}};
//  assign io_autl_acquire_bits_addr_block = {1{$random}};
//  assign io_mem_invalidate_lr = {1{$random}};
//  assign io_mem_req_bits_kill = {1{$random}};
// synthesis translate_on
`endif
  assign io_autl_grant_ready = 1'h0;
  assign io_autl_acquire_valid = 1'h0;
  assign io_interrupt = 1'h0;
  assign io_busy = bb_io_busy;
  assign io_mem_req_bits_data = bb_io_mem_req_bits_data;
  assign io_mem_req_bits_phys = 1'h1;
  assign io_mem_req_bits_typ = bb_io_mem_req_bits_typ;
  assign io_mem_req_bits_cmd = bb_io_mem_req_bits_cmd;
  assign io_mem_req_bits_tag = bb_io_mem_req_bits_tag;
  assign io_mem_req_bits_addr = bb_io_mem_req_bits_addr;
  assign io_mem_req_valid = bb_io_mem_req_valid;
  assign io_resp_bits_data = bb_io_resp_bits_data;
  assign io_resp_bits_rd = bb_io_resp_bits_rd;
  assign io_resp_valid = bb_io_resp_valid;
  assign io_cmd_ready = bb_io_cmd_ready;
  RoccXcelWrapper bb(.clk(clk), .reset(reset),
       .io_cmd_ready( bb_io_cmd_ready ),
       .io_cmd_valid( io_cmd_valid ),
       .io_cmd_bits_inst_funct( io_cmd_bits_inst_funct ),
       .io_cmd_bits_inst_rs2( io_cmd_bits_inst_rs2 ),
       .io_cmd_bits_inst_rs1( io_cmd_bits_inst_rs1 ),
       .io_cmd_bits_inst_xd( io_cmd_bits_inst_xd ),
       .io_cmd_bits_inst_xs1( io_cmd_bits_inst_xs1 ),
       .io_cmd_bits_inst_xs2( io_cmd_bits_inst_xs2 ),
       .io_cmd_bits_inst_rd( io_cmd_bits_inst_rd ),
       .io_cmd_bits_inst_opcode( io_cmd_bits_inst_opcode ),
       .io_cmd_bits_rs1( io_cmd_bits_rs1 ),
       .io_cmd_bits_rs2( io_cmd_bits_rs2 ),
       .io_resp_ready( io_resp_ready ),
       .io_resp_valid( bb_io_resp_valid ),
       .io_resp_bits_rd( bb_io_resp_bits_rd ),
       .io_resp_bits_data( bb_io_resp_bits_data ),
       .io_mem_req_ready( io_mem_req_ready ),
       .io_mem_req_valid( bb_io_mem_req_valid ),
       .io_mem_req_bits_addr( bb_io_mem_req_bits_addr ),
       .io_mem_req_bits_tag( bb_io_mem_req_bits_tag ),
       .io_mem_req_bits_cmd( bb_io_mem_req_bits_cmd ),
       .io_mem_req_bits_typ( bb_io_mem_req_bits_typ ),
       //.io_mem_req_bits_kill(  )
       //.io_mem_req_bits_phys(  )
       .io_mem_req_bits_data( bb_io_mem_req_bits_data ),
       .io_mem_resp_valid( io_mem_resp_valid ),
       .io_mem_resp_bits_addr( io_mem_resp_bits_addr ),
       .io_mem_resp_bits_tag( io_mem_resp_bits_tag ),
       .io_mem_resp_bits_cmd( io_mem_resp_bits_cmd ),
       .io_mem_resp_bits_typ( io_mem_resp_bits_typ ),
       .io_mem_resp_bits_data( io_mem_resp_bits_data ),
       .io_mem_resp_bits_nack( io_mem_resp_bits_nack ),
       .io_mem_resp_bits_replay( io_mem_resp_bits_replay ),
       .io_mem_resp_bits_has_data( io_mem_resp_bits_has_data ),
       .io_mem_resp_bits_data_word_bypass( io_mem_resp_bits_data_word_bypass ),
       .io_mem_resp_bits_store_data( io_mem_resp_bits_store_data ),
       //.io_mem_replay_next_valid(  )
       //.io_mem_replay_next_bits(  )
       //.io_mem_xcpt_ma_ld(  )
       //.io_mem_xcpt_ma_st(  )
       //.io_mem_xcpt_pf_ld(  )
       //.io_mem_xcpt_pf_st(  )
       //.io_mem_invalidate_lr(  )
       //.io_mem_ordered(  )
       .io_busy( bb_io_busy )
  );
endmodule
