#------------------------------------------------------------
# Do NOT arbitrarily change the order of files. Some module
# and macro definitions may be needed by the subsequent files
#------------------------------------------------------------

set bsg_ip_cores_dir $::env(BSG_OUT_DIR)/bsg_ip_cores
set bsg_rocket_dir $::env(BSG_OUT_DIR)/bsg_rocket
set bsg_designs_dir $::env(BSG_OUT_DIR)/bsg_designs

set TESTING_SOURCE_FILES [join "
  $bsg_ip_cores_dir/bsg_misc/bsg_defines.v
  $bsg_ip_cores_dir/bsg_misc/bsg_dff.v
  $bsg_ip_cores_dir/bsg_misc/bsg_circular_ptr.v
  $bsg_ip_cores_dir/bsg_misc/bsg_gray_to_binary.v
  $bsg_ip_cores_dir/bsg_misc/bsg_binary_plus_one_to_gray.v
  $bsg_ip_cores_dir/bsg_misc/bsg_mux_one_hot.v
  $bsg_ip_cores_dir/bsg_misc/bsg_popcount.v
  $bsg_ip_cores_dir/bsg_misc/bsg_scan.v
  $bsg_ip_cores_dir/bsg_misc/bsg_rotate_right.v
  $bsg_ip_cores_dir/bsg_misc/bsg_thermometer_count.v
  $bsg_ip_cores_dir/bsg_misc/bsg_counter_up_down_variable.v
  $bsg_ip_cores_dir/bsg_misc/bsg_round_robin_arb.v
  $bsg_ip_cores_dir/bsg_misc/bsg_crossbar_o_by_i.v
  $bsg_ip_cores_dir/bsg_misc/bsg_counter_clear_up.v
  $bsg_ip_cores_dir/bsg_misc/bsg_decode.v
  $bsg_ip_cores_dir/bsg_misc/bsg_decode_with_v.v
  $bsg_ip_cores_dir/bsg_misc/bsg_wait_cycles.v
  $bsg_ip_cores_dir/bsg_misc/bsg_wait_after_reset.v
  $bsg_ip_cores_dir/bsg_test/bsg_nonsynth_clock_gen.v
  $bsg_ip_cores_dir/bsg_test/bsg_nonsynth_reset_gen.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_credit_counter.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_fifo.v
  $bsg_ip_cores_dir/bsg_async/bsg_async_ptr_gray.v
  $bsg_ip_cores_dir/bsg_async/bsg_launch_sync_sync.v
  $bsg_ip_cores_dir/bsg_async/bsg_sync_sync.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_1_to_n_tagged.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_1_to_n_tagged_fifo.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_round_robin_n_to_1.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel_in.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel_out.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_tunnel.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_flatten_2D_array.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_make_2D_array.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_round_robin_fifo_to_fifo.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_sbox.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_scatter_gather.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_two_fifo.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_channel_narrow.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_narrowed.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_tracker.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_small.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1r1w_pseudo_large.v
  $bsg_ip_cores_dir/bsg_dataflow/bsg_fifo_1rw_large.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w_synth.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1r1w.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync_synth.v
  $bsg_ip_cores_dir/bsg_mem/bsg_mem_1rw_sync.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_pkg.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_murn_gateway.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_in.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_front_side_bus_hop_out.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb.v
  $bsg_ip_cores_dir/bsg_fsb/bsg_fsb_node_trace_replay.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_channel_control_master.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_channel_control_master_master.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_assembler_in.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_assembler_out.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_channel_control_slave.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_input.v
  $bsg_ip_cores_dir/bsg_comm_link/bsg_source_sync_output.v
  $bsg_designs_dir/modules/bsg_guts/trace_replay/bsg_test_node_master.v
  $bsg_designs_dir/modules/bsg_guts_new/bsg_comm_link_fuser.v
  $bsg_designs_dir/modules/bsg_guts_new/bsg_comm_link_kernel.v
  $bsg_designs_dir/modules/bsg_guts_new/bsg_comm_link.v
  $bsg_rocket_dir/rockets/bison/generated-src/consts.Bsg1AccelVLSIConfig.vh
  $bsg_rocket_dir/rockets/bison/generated-src/bsg_fsb_master_rom.v
  $bsg_rocket_dir/modules/bsg_rocket_fsb/bsg_host.v
  $bsg_rocket_dir/modules/bsg_rocket_fsb/bsg_nasti_master_req.v
  $bsg_rocket_dir/modules/bsg_rocket_fsb/bsg_nasti_master_resp.v
  $bsg_rocket_dir/modules/bsg_rocket_fsb/bsg_nasti_master.v
  $bsg_rocket_dir/modules/bsg_rocket_fsb/bsg_fsb_to_rocket.v
  $bsg_rocket_dir/modules/bsg_rocket_debug/bsg_rocket_monitor.v
  $bsg_rocket_dir/modules/bsg_rocket_node/bsg_rocket_node_master.v
  $bsg_designs_dir/toplevels/bsg_two_bison_memcpy_sram/testing/v/bsg_gateway_chip.v
  $bsg_designs_dir/toplevels/bsg_two_bison_memcpy_sram/testing/v/bsg_zedboard_chip.v
  $bsg_rocket_dir/common/vsrc/test_bsg.v
  $bsg_rocket_dir/common/vsrc/rocketTestHarness.v
"]
