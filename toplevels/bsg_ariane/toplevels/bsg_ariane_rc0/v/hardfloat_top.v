
module hardfloat_top
 #(parameter exp_width_p   = 11 // 8 for single
   , parameter sig_width_p = 53 // 24 for single

   , parameter int_width_lp   = 64
   , parameter float_width_lp = exp_width_p + sig_width_p
   , parameter rec_width_lp   = 1 + exp_width_p + sig_width_p

   , parameter num_stages_p = 5
   )
  (input                         clk_i
   , input                       reset_i

   , input                       is_add_i
   , input                       is_sub_i
   , input                       is_mul_i
   , input                       signed_int_i
   , input                       is_fma_i
   , input [1:0]                 fma_op_i
   , input                       is_lt_i
   , input                       is_le_i
   , input                       is_eq_i
   , input                       is_min_i
   , input                       is_max_i
   , input                       is_i2f_i
   , input                       is_f2i_i

   , input [float_width_lp-1:0]  data0_i
   , input [float_width_lp-1:0]  data1_i
   , input [float_width_lp-1:0]  data2_i
   , input [2:0]                 frm_i

   , output [float_width_lp-1:0] data_o
   , output [4:0]                eflags_o
   );

  // Sets mode where tininess is detected before rounding. 
  wire [(`floatControlWidth - 1):0] control_li = 1'b1;

  // 
  // Recode the input data
  logic [rec_width_lp-1:0] recode0_li;
  fNToRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   in_recode0
    (.in(data0_i)
     ,.out(recode0_li)
     );

  logic [rec_width_lp-1:0] recode1_li;
  fNToRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   in_recode1
    (.in(data1_i)
     ,.out(recode1_li)
     );

  logic [rec_width_lp-1:0] recode2_li;
  fNToRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   in_recode2
    (.in(data2_i)
     ,.out(recode2_li)
     );

  // 
  // Do int to float / float to int conversion and classification
  logic [rec_width_lp-1:0] i2f_lo;
  logic [4:0] i2f_eflags_lo;
  iNToRecFN
   #(.intWidth(int_width_lp), .expWidth(exp_width_p), .sigWidth(sig_width_p))
   i2f
    (.control(control_li)
     ,.signedIn(signed_int_i)
     ,.in(data0_i)
     ,.roundingMode(frm_i)
     ,.out(i2f_lo)
     ,.exceptionFlags(i2f_eflags_lo)
     );

  logic [int_width_lp-1:0] f2i_lo;
  logic [2:0] f2i_eflags_lo;
  recFNToIN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p), .intWidth(int_width_lp))
   f2i
    (.control(control_li)
     ,.in(recode0_li)
     ,.roundingMode(frm_i)
     ,.signedOut(signed_int_i)
     ,.out(f2i_lo)
     ,.intExceptionFlags(f2i_eflags_lo)
     );

  logic is_nan_lo, is_inf_lo, is_zero_lo, sign_lo;
  recFNToRawFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   fclass
    (.in(recode0_li)
     ,.isNaN(is_nan_lo)
     ,.isInf(is_inf_lo)
     ,.isZero(is_zero_lo)
     ,.sign(sign_lo)
     /* We don't use the raw form, just the classification */
     ,.sExp()
     ,.sig()
     );

  logic is_sig_nan_lo;
  isSigNaNRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   fnan
    (.in(recode0_li)
     ,.isSigNaN(is_sig_nan_lo)
     );

  //
  // Arithmetic operations
  logic [rec_width_lp-1:0] addsub_lo;
  logic [4:0] addsub_eflags_lo;
  addRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   addsub
    (.control(control_li)
     ,.subOp(is_sub_i)
     ,.a(recode0_li)
     ,.b(recode1_li)
     ,.roundingMode(frm_i)
     ,.out(addsub_lo)
     ,.exceptionFlags(addsub_eflags_lo)
     );
  wire is_addsub_li = is_add_i | is_sub_i;
  
  logic [rec_width_lp-1:0] mul_lo;
  logic [4:0] mul_eflags_lo;
  mulRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   mul
    (.control(control_li)
     ,.a(recode0_li)
     ,.b(recode1_li)
     ,.roundingMode(frm_i)
     ,.out(mul_lo)
     ,.exceptionFlags(mul_eflags_lo)
     );

  logic [rec_width_lp-1:0] fma_lo;
  logic [4:0] fma_eflags_lo;
  mulAddRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   fma
    (.control(control_li)
     ,.op(fma_op_i)
     ,.a(recode0_li)
     ,.b(recode1_li)
     ,.c(recode2_li)
     ,.roundingMode(frm_i)
     ,.out(fma_lo)
     ,.exceptionFlags(fma_eflags_lo)
   );

  logic lt_lo, eq_lo, gt_lo, unordered_lo;
  logic [float_width_lp-1:0] compare_lo;
  logic [4:0] compare_eflags_lo;
  compareRecFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   compare
    (.a(recode0_li)
     ,.b(recode1_li)
     ,.signaling(is_sig_nan_lo)
     ,.lt(lt_lo)
     ,.eq(eq_lo)
     ,.gt(gt_lo)
     ,.unordered(unordered_lo)
     ,.exceptionFlags(compare_eflags_lo)
     );

  wire is_compare_li = is_lt_i | is_le_i | is_eq_i | is_min_i | is_max_i;
  always_comb
    unique case ({is_lt_i, is_le_i, is_eq_i, is_min_i, is_max_i})
      5'b10000: compare_lo = unordered_lo ? 1'b0 :  lt_lo;
      5'b01000: compare_lo = unordered_lo ? 1'b0 : ~gt_lo;
      5'b00100: compare_lo = unordered_lo ? 1'b0 :  eq_lo;
      /* Need to deal with NaN for min/max */
      5'b00010: compare_lo = lt_lo ? recode0_li : recode1_li;
      5'b00001: compare_lo = gt_lo ? recode0_li : recode1_li;
      default: compare_lo = '0;
    endcase

  logic [rec_width_lp-1:0] recode_lo;
  always_comb
    unique case ({is_addsub_li, is_mul_i, is_compare_li, is_i2f_i, is_f2i_i, is_fma_i})
      6'b100000: recode_lo = addsub_lo;
      6'b010000: recode_lo = mul_lo;
      6'b001000: recode_lo = compare_lo;
      6'b000100: recode_lo = i2f_lo;
      6'b000010: recode_lo = f2i_lo;
      6'b000001: recode_lo = fma_lo;
      default: recode_lo = '0;
    endcase

  logic [float_width_lp-1:0] data_lo;
  recFNToFN
   #(.expWidth(exp_width_p), .sigWidth(sig_width_p))
   out_recode
    (.in(recode_lo)
     ,.out(data_lo)
     );

  logic [num_stages_p:0][4:0]                eflags_pipe_r;
  logic [num_stages_p:0][float_width_lp-1:0] data_pipe_r;

  assign eflags_pipe_r[0] = '0;
  assign data_pipe_r  [0] = data_lo;
  always_ff @(posedge clk_i)
    for (integer i = 0; i < num_stages_p; i++)
      begin
        eflags_pipe_r[i+1] <= eflags_pipe_r[i];
        data_pipe_r  [i+1] <= data_pipe_r  [i];
      end
  assign data_o = data_pipe_r[num_stages_p];

endmodule

