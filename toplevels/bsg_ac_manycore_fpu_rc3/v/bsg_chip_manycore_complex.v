
`include "bsg_defines.v"
`include "bsg_noc_pkg.v"
`include "bsg_manycore_packet.vh"
`include "bsg_cache_dma_pkt.vh"

module bsg_chip_manycore_complex

  import bsg_tag_pkg::*;
  import bsg_noc_pkg::*;

#(parameter addr_width_p  = -1
, parameter data_width_p  = -1
, parameter num_tiles_x_p = -1
, parameter num_tiles_y_p = -1

, parameter dmem_size_p        = -1
, parameter icache_entries_p   = -1
, parameter icache_tag_width_p = -1

, parameter load_id_width_p       = -1
, parameter epa_byte_addr_width_p = -1

, parameter dram_ch_addr_width_p = -1
, parameter dram_ch_start_col_p  = -1

, parameter vcache_num_cache_p           = -1
, parameter vcache_ways_p                = -1
, parameter vcache_sets_p                = -1
, parameter vcache_block_size_in_words_p = -1
, parameter vcache_size_p                = -1

, parameter extra_io_rows_p = 1

, parameter x_cord_width_lp      = `BSG_SAFE_CLOG2(num_tiles_x_p)
, parameter y_cord_width_lp      = `BSG_SAFE_CLOG2(num_tiles_y_p + extra_io_rows_p)
, parameter byte_offset_width_lp = `BSG_SAFE_CLOG2(data_width_p>>3)

, parameter bsg_manycore_link_sif_width_lp = `bsg_manycore_link_sif_width(addr_width_p, data_width_p, x_cord_width_lp, y_cord_width_lp, load_id_width_p)
, parameter bsg_cache_dma_pkt_width_lp = `bsg_cache_dma_pkt_width(addr_width_p+byte_offset_width_lp-1)
)

( input  clk_i
, input  reset_i

, input  [bsg_manycore_link_sif_width_lp-1:0]  io_link_sif_i
, output [bsg_manycore_link_sif_width_lp-1:0]  io_link_sif_o

, output [vcache_num_cache_p-1:0][bsg_cache_dma_pkt_width_lp-1:0] dma_pkt_o
, output [vcache_num_cache_p-1:0]                                 dma_pkt_v_o
, input  [vcache_num_cache_p-1:0]                                 dma_pkt_yumi_i

, input  [vcache_num_cache_p-1:0][data_width_p-1:0] dma_data_i
, input  [vcache_num_cache_p-1:0]                   dma_data_v_i
, output [vcache_num_cache_p-1:0]                   dma_data_ready_o

, output [vcache_num_cache_p-1:0][data_width_p-1:0] dma_data_o
, output [vcache_num_cache_p-1:0]                   dma_data_v_o
, input  [vcache_num_cache_p-1:0]                   dma_data_yumi_i

);

  genvar i;

  `declare_bsg_manycore_link_sif_s(addr_width_p, data_width_p, x_cord_width_lp, y_cord_width_lp, load_id_width_p);

  bsg_manycore_link_sif_s [E:W][num_tiles_y_p-1:0] hor_link_sif_li;
  bsg_manycore_link_sif_s [E:W][num_tiles_y_p-1:0] hor_link_sif_lo;

  bsg_manycore_link_sif_s [S:N][num_tiles_x_p-1:0] ver_link_sif_li;
  bsg_manycore_link_sif_s [S:N][num_tiles_x_p-1:0] ver_link_sif_lo;

  bsg_manycore_link_sif_s [num_tiles_x_p-1:0] io_link_sif_li;
  bsg_manycore_link_sif_s [num_tiles_x_p-1:0] io_link_sif_lo;

  //////////////////////////////////////////////////
  //
  // BSG Manycore Instance
  //

  // Manycore instance
  bsg_manycore #(.addr_width_p(addr_width_p)
                ,.data_width_p(data_width_p)

                ,.dmem_size_p(dmem_size_p)
                ,.icache_entries_p(icache_entries_p)
                ,.icache_tag_width_p(icache_tag_width_p)
                ,.vcache_size_p(vcache_size_p)
                ,.vcache_block_size_in_words_p(vcache_block_size_in_words_p)

                ,.num_tiles_x_p(num_tiles_x_p)
                ,.num_tiles_y_p(num_tiles_y_p)

                ,.load_id_width_p(load_id_width_p)
                ,.epa_byte_addr_width_p(epa_byte_addr_width_p)

                ,.dram_ch_addr_width_p(dram_ch_addr_width_p)
                ,.dram_ch_start_col_p(dram_ch_start_col_p)

                ,.stub_w_p({num_tiles_y_p{1'b0}})
                ,.stub_e_p({num_tiles_y_p{1'b0}})
                ,.stub_n_p({num_tiles_x_p{1'b0}})
                ,.stub_s_p({num_tiles_x_p{1'b0}})
                )
    mc
      (.clk_i   ( clk_i )
      ,.reset_i ( reset_i )

      ,.hor_link_sif_i ( hor_link_sif_li )
      ,.hor_link_sif_o ( hor_link_sif_lo )

      ,.ver_link_sif_i ( ver_link_sif_li )
      ,.ver_link_sif_o ( ver_link_sif_lo )

      ,.io_link_sif_i ( io_link_sif_li )
      ,.io_link_sif_o ( io_link_sif_lo )
      );

  // IO links[0] go out of this module
  assign io_link_sif_li[0] = io_link_sif_i;
  assign io_link_sif_o = io_link_sif_lo[0];

  //////////////////////////////////////////////////
  //
  // Victim Cache
  //

  // Pipeline the reset by 2 flip flops (for 16nm layout)
  logic [vcache_num_cache_p-1:0] vc_reset_r, vc_reset_rr, vc_reset_rrr;

  always_ff @(posedge clk_i)
    begin
      vc_reset_r <= {vcache_num_cache_p{reset_i}};
      vc_reset_rr <= vc_reset_r;
      vc_reset_rrr <= vc_reset_rr;
    end

  for (i=0; i < vcache_num_cache_p; i++)
    begin: rof
      vcache #(.data_width_p(data_width_p)
              ,.addr_width_p(addr_width_p)
              ,.block_size_in_words_p(vcache_block_size_in_words_p)
              ,.sets_p(vcache_sets_p)
              ,.ways_p(vcache_ways_p)
              ,.x_cord_width_p(x_cord_width_lp)
              ,.y_cord_width_p(y_cord_width_lp)
              ,.load_id_width_p(load_id_width_p)
              )
        vc
          (.clk_i   ( clk_i   )
          ,.reset_i ( vc_reset_rrr[i] )

          // manycore side
          ,.my_x_i ( (x_cord_width_lp)'(i)                       )
          ,.my_y_i ( (y_cord_width_lp)'(num_tiles_y_p) )

          ,.link_sif_i ( ver_link_sif_lo[S][i] )
          ,.link_sif_o ( ver_link_sif_li[S][i] )
          
          // cache dma side
          ,.dma_pkt_o        ( dma_pkt_o[i]        )
          ,.dma_pkt_v_o      ( dma_pkt_v_o[i]      )
          ,.dma_pkt_yumi_i   ( dma_pkt_yumi_i[i]   )
          ,.dma_data_i       ( dma_data_i[i]       )
          ,.dma_data_v_i     ( dma_data_v_i[i]     )
          ,.dma_data_ready_o ( dma_data_ready_o[i] )
          ,.dma_data_o       ( dma_data_o[i]       )
          ,.dma_data_v_o     ( dma_data_v_o[i]     )
          ,.dma_data_yumi_i  ( dma_data_yumi_i[i]  )
          );
    end: rof

  //////////////////////////////////////////////////
  //
  // Link Tieoffs (io, N, S, W, E)
  //

  // io link tieoffs
  for (i=1; i < num_tiles_x_p; i++)
    begin: io
      bsg_manycore_link_sif_tieoff #(.addr_width_p(addr_width_p)
                                    ,.data_width_p(data_width_p)
                                    ,.x_cord_width_p(x_cord_width_lp)
                                    ,.y_cord_width_p(y_cord_width_lp)
                                    ,.load_id_width_p(load_id_width_p)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( reset_i ) // only used for print outs

          ,.link_sif_i ( io_link_sif_lo[i] )
          ,.link_sif_o ( io_link_sif_li[i] )
          );
    end: io

  // north-side link tieoffs
  for (i=0; i < num_tiles_x_p; i++)
    begin: north
      bsg_manycore_link_sif_tieoff #(.addr_width_p(addr_width_p)
                                    ,.data_width_p(data_width_p)
                                    ,.x_cord_width_p(x_cord_width_lp)
                                    ,.y_cord_width_p(y_cord_width_lp)
                                    ,.load_id_width_p(load_id_width_p)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( reset_i ) // only used for print outs

          ,.link_sif_i ( ver_link_sif_lo[N][i] )
          ,.link_sif_o ( ver_link_sif_li[N][i] )
          );
    end: north

  // south-side link tieoffs
  for (i=vcache_num_cache_p; i < num_tiles_x_p; i++)
    begin: south
      bsg_manycore_link_sif_tieoff #(.addr_width_p(addr_width_p)
                                    ,.data_width_p(data_width_p)
                                    ,.x_cord_width_p(x_cord_width_lp)
                                    ,.y_cord_width_p(y_cord_width_lp)
                                    ,.load_id_width_p(load_id_width_p)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( reset_i ) // only used for print outs

          ,.link_sif_i ( ver_link_sif_lo[S][i] )
          ,.link_sif_o ( ver_link_sif_li[S][i] )
          );
    end: south

  // west-side link tieoffs
  for (i=0; i < num_tiles_y_p; i++)
    begin: west
      bsg_manycore_link_sif_tieoff #(.addr_width_p(addr_width_p)
                                    ,.data_width_p(data_width_p)
                                    ,.x_cord_width_p(x_cord_width_lp)
                                    ,.y_cord_width_p(y_cord_width_lp)
                                    ,.load_id_width_p(load_id_width_p)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( reset_i ) // only used for print outs

          ,.link_sif_i ( hor_link_sif_lo[W][i] )
          ,.link_sif_o ( hor_link_sif_li[W][i] )
          );
    end: west

  // east-side link tieoffs
  for (i=0; i < num_tiles_y_p; i++)
    begin: east
      bsg_manycore_link_sif_tieoff #(.addr_width_p(addr_width_p)
                                    ,.data_width_p(data_width_p)
                                    ,.x_cord_width_p(x_cord_width_lp)
                                    ,.y_cord_width_p(y_cord_width_lp)
                                    ,.load_id_width_p(load_id_width_p)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( reset_i ) // only used for print outs

          ,.link_sif_i ( hor_link_sif_lo[E][i] )
          ,.link_sif_o ( hor_link_sif_li[E][i] )
          );
    end: east

endmodule

