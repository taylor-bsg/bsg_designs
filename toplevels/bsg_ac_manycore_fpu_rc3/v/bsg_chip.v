`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

//==============================================================================
//
// BSG CHIP
//
// This is the toplevel for the ASIC. This chip uses the UW BGA package found
// inside bsg_packaging/uw_bga. For physical design reasons, the input pins
// have been swizzled (ie. re-arranged) from their original meaning. We use the
// bsg_chip_swizzle_adapter in every ASIC to abstract away detail.
//

module bsg_chip

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

`include "bsg_pinout.v"
`include "bsg_iopads.v"

  `declare_bsg_manycore_link_sif_s(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp);
  `declare_bsg_ready_and_link_sif_s(ct_width_gp, bsg_ready_and_link_sif_s);
  `declare_bsg_cache_dma_pkt_s(manycore_addr_width_gp+manycore_byte_offset_width_gp-1);

  //////////////////////////////////////////////////
  //
  // BSG Tag Master Instance
  //

  // All tag lines from the btm
  bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;

  // Tag lines for clock generators
  bsg_tag_s       async_reset_tag_lines_lo;
  bsg_tag_s [2:0] osc_tag_lines_lo;
  bsg_tag_s [2:0] osc_trigger_tag_lines_lo;
  bsg_tag_s [2:0] ds_tag_lines_lo;
  bsg_tag_s [2:0] sel_tag_lines_lo;

  assign async_reset_tag_lines_lo = tag_lines_lo[0];
  assign osc_tag_lines_lo         = tag_lines_lo[3:1];
  assign osc_trigger_tag_lines_lo = tag_lines_lo[6:4];
  assign ds_tag_lines_lo          = tag_lines_lo[9:7];
  assign sel_tag_lines_lo         = tag_lines_lo[12:10];

  // Tag lines for io complex
  wire bsg_tag_s prev_link_io_tag_lines_lo   = tag_lines_lo[13];
  wire bsg_tag_s prev_link_core_tag_lines_lo = tag_lines_lo[14];
  wire bsg_tag_s prev_ct_core_tag_lines_lo   = tag_lines_lo[15];
  wire bsg_tag_s next_link_io_tag_lines_lo   = tag_lines_lo[16];
  wire bsg_tag_s next_link_core_tag_lines_lo = tag_lines_lo[17];
  wire bsg_tag_s next_ct_core_tag_lines_lo   = tag_lines_lo[18];
  
  // Tag lines for the manycore
  wire bsg_tag_s manycore_tag_lines_lo = tag_lines_lo[19];
  wire bsg_tag_s vcache_tag_lines_lo   = tag_lines_lo[20];
  
  // Tag lines for router networks
  bsg_tag_s                           network_a_router_core_tag_lines_lo;
  bsg_tag_s [vcache_num_cache_gp-1:0] network_b_router_core_tag_lines_lo;
  
  assign network_a_router_core_tag_lines_lo = tag_lines_lo[21];
  assign network_b_router_core_tag_lines_lo = tag_lines_lo[22+:vcache_num_cache_gp];

  // BSG tag master instance
  bsg_tag_master #(.els_p( tag_num_clients_gp )
                  ,.lg_width_p( tag_lg_max_payload_width_gp )
                  )
    btm
      (.clk_i      ( bsg_tag_clk_i_int )
      ,.data_i     ( bsg_tag_en_i_int ? bsg_tag_data_i_int : 1'b0 )
      ,.en_i       ( 1'b1 )
      ,.clients_r_o( tag_lines_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Clock Generator Power Domain
  //

  logic manycore_clk_lo;
  logic io_master_clk_lo;
  logic router_clk_lo;

  bsg_clk_gen_power_domain #(.num_clk_endpoint_p( clk_gen_num_endpoints_gp )
                            ,.ds_width_p( clk_gen_ds_width_gp )
                            ,.num_adgs_p( clk_gen_num_adgs_gp )
                            )
    clk_gen_pd
      (.async_reset_tag_lines_i ( async_reset_tag_lines_lo )
      ,.osc_tag_lines_i         ( osc_tag_lines_lo )
      ,.osc_trigger_tag_lines_i ( osc_trigger_tag_lines_lo )
      ,.ds_tag_lines_i          ( ds_tag_lines_lo )
      ,.sel_tag_lines_i         ( sel_tag_lines_lo )

      ,.ext_clk_i({ clk_C_i_int, clk_B_i_int, clk_A_i_int })

      ,.clk_o({ router_clk_lo, io_master_clk_lo, manycore_clk_lo })
      );
      
  // Route the clock signals off chip
  logic [1:0]  clk_out_sel;
  logic        clk_out;

  assign clk_out_sel[0] = sel_0_i_int;
  assign clk_out_sel[1] = sel_1_i_int;
  assign clk_o_int      = clk_out;

  bsg_mux #(.width_p   ( 1 )
           ,.els_p     ( 4 )
           ,.balanced_p( 1 )
           ,.harden_p  ( 1 )
           ) 
    clk_out_mux
      (.data_i( {1'b0, manycore_clk_lo, io_master_clk_lo, router_clk_lo} )
      ,.sel_i ( clk_out_sel )
      ,.data_o( clk_out )
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Client Instance
  //

  // Tag payload for manycore control signals
  typedef struct packed { 
      logic reset;
      logic [network_a_wh_cord_width_gp-1:0] cord;
  } manycore_tag_payload_s;

  // Tag payload for manycore control signals
  manycore_tag_payload_s manycore_tag_data_lo;
  logic                  manycore_tag_new_data_lo;

  bsg_tag_client #(.width_p( $bits(manycore_tag_data_lo) ), .default_p( 0 ))
    btc_manycore
      (.bsg_tag_i     ( manycore_tag_lines_lo )
      ,.recv_clk_i    ( manycore_clk_lo )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( manycore_tag_new_data_lo )
      ,.recv_data_r_o ( manycore_tag_data_lo )
      );
  
  // Tag payload for Vcache control signals
  typedef struct packed { 
      logic reset;
      logic [(tag_max_payload_width_gp-1)-1:0] cord;
  } vcache_tag_payload_s;

  vcache_tag_payload_s vcache_tag_data_lo;
  logic                vcache_tag_new_data_lo;
  
  bsg_tag_client #(.width_p( $bits(vcache_tag_data_lo) ), .default_p( 0 ))
    btc_vcache
      (.bsg_tag_i     ( vcache_tag_lines_lo )
      ,.recv_clk_i    ( manycore_clk_lo )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( vcache_tag_new_data_lo )
      ,.recv_data_r_o ( vcache_tag_data_lo )
      );

  //////////////////////////////////////////////////
  //
  // Swizzle Adapter for Comm Link IO Signals
  //

  logic         ci_clk_li;
  logic         ci_v_li;
  logic [8:0]   ci_data_li;
  logic         ci_tkn_lo;

  logic         co_clk_lo;
  logic         co_v_lo;
  logic [8:0]   co_data_lo;
  logic         co_tkn_li;

  logic         ci2_clk_li;
  logic         ci2_v_li;
  logic [8:0]   ci2_data_li;
  logic         ci2_tkn_lo;

  logic         co2_clk_lo;
  logic         co2_v_lo;
  logic [8:0]   co2_data_lo;
  logic         co2_tkn_li;

  bsg_chip_swizzle_adapter
    swizzle
      ( // IO Port Side
       .port_ci_clk_i   (ci_clk_i_int)
      ,.port_ci_v_i     (ci_v_i_int)
      ,.port_ci_data_i  ({ci_8_i_int, ci_7_i_int, ci_6_i_int, ci_5_i_int, ci_4_i_int, ci_3_i_int, ci_2_i_int, ci_1_i_int, ci_0_i_int})
      ,.port_ci_tkn_o   (ci_tkn_o_int)

      ,.port_ci2_clk_o  (ci2_clk_o_int)
      ,.port_ci2_v_o    (ci2_v_o_int)
      ,.port_ci2_data_o ({ci2_8_o_int, ci2_7_o_int, ci2_6_o_int, ci2_5_o_int, ci2_4_o_int, ci2_3_o_int, ci2_2_o_int, ci2_1_o_int, ci2_0_o_int})
      ,.port_ci2_tkn_i  (ci2_tkn_i_int)

      ,.port_co_clk_i   (co_clk_i_int)
      ,.port_co_v_i     (co_v_i_int)
      ,.port_co_data_i  ({co_8_i_int, co_7_i_int, co_6_i_int, co_5_i_int, co_4_i_int, co_3_i_int, co_2_i_int, co_1_i_int, co_0_i_int})
      ,.port_co_tkn_o   (co_tkn_o_int)

      ,.port_co2_clk_o  (co2_clk_o_int)
      ,.port_co2_v_o    (co2_v_o_int)
      ,.port_co2_data_o ({co2_8_o_int, co2_7_o_int, co2_6_o_int, co2_5_o_int, co2_4_o_int, co2_3_o_int, co2_2_o_int, co2_1_o_int, co2_0_o_int})
      ,.port_co2_tkn_i  (co2_tkn_i_int)

      // Chip (Guts) Side
      ,.guts_ci_clk_o  (ci_clk_li)
      ,.guts_ci_v_o    (ci_v_li)
      ,.guts_ci_data_o (ci_data_li)
      ,.guts_ci_tkn_i  (ci_tkn_lo)

      ,.guts_co_clk_i  (co_clk_lo)
      ,.guts_co_v_i    (co_v_lo)
      ,.guts_co_data_i (co_data_lo)
      ,.guts_co_tkn_o  (co_tkn_li)

      ,.guts_ci2_clk_o (ci2_clk_li)
      ,.guts_ci2_v_o   (ci2_v_li)
      ,.guts_ci2_data_o(ci2_data_li)
      ,.guts_ci2_tkn_i (ci2_tkn_lo)

      ,.guts_co2_clk_i (co2_clk_lo)
      ,.guts_co2_v_i   (co2_v_lo)
      ,.guts_co2_data_i(co2_data_lo)
      ,.guts_co2_tkn_o (co2_tkn_li)
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip IO Complex
  //

  logic network_a_router_reset_lo;
  bsg_ready_and_link_sif_s [1:0] network_a_rtr_links_li;
  bsg_ready_and_link_sif_s [1:0] network_a_rtr_links_lo;
  
  logic [vcache_num_cache_gp-1:0] network_b_router_reset_lo;
  logic [vcache_num_cache_gp-1:0][network_b_wh_cord_width_gp-1:0] network_b_router_cord_lo;
  bsg_ready_and_link_sif_s [vcache_num_cache_gp-1:0] network_b_rtr_links_li;
  bsg_ready_and_link_sif_s [vcache_num_cache_gp-1:0] network_b_rtr_links_lo;

  bsg_chip_io_complex_dual_network #(.link_width_p( link_width_gp )
                       ,.link_channel_width_p( link_channel_width_gp )
                       ,.link_num_channels_p( link_num_channels_gp )
                       ,.link_lg_fifo_depth_p( link_lg_fifo_depth_gp )
                       ,.link_lg_credit_to_token_decimation_p( link_lg_credit_to_token_decimation_gp )
                       ,.link_use_extra_data_bit_p( link_use_extra_data_bit_gp )

                       ,.ct_width_p( ct_width_gp )
                       ,.ct_num_in_p( ct_num_in_gp )
                       ,.ct_remote_credits_p( ct_remote_credits_gp )
                       ,.ct_use_pseudo_large_fifo_p( ct_use_pseudo_large_fifo_gp )
                       ,.ct_lg_credit_decimation_p( ct_lg_credit_decimation_gp )

                       ,.network_a_num_router_groups_p( 1 )
                       ,.network_a_num_in_p( 2 )
                       ,.network_a_wh_cord_markers_pos_p( { network_a_wh_cord_markers_pos_y_gp, network_a_wh_cord_markers_pos_x_gp } )
                       ,.network_a_wh_len_width_p( network_a_wh_len_width_gp )
                       ,.network_a_tag_cord_width_p( network_a_wh_cord_markers_pos_y_gp )
                       
                       ,.network_b_num_router_groups_p( vcache_num_cache_gp )
                       ,.network_b_num_in_p( 1 )
                       ,.network_b_wh_cord_markers_pos_p( { network_b_wh_cord_markers_pos_y_gp, network_b_wh_cord_markers_pos_x_gp } )
                       ,.network_b_wh_len_width_p( network_b_wh_len_width_gp )
                       ,.network_b_tag_cord_width_p( tag_max_payload_width_gp-1 )
                       ,.network_b_num_repeater_nodes_p( 4 )
                       )
    io_complex
      (.core_clk_i ( router_clk_lo    )
      ,.io_clk_i   ( io_master_clk_lo )

      ,.prev_link_io_tag_lines_i  ( prev_link_io_tag_lines_lo   )
      ,.prev_link_core_tag_lines_i( prev_link_core_tag_lines_lo )
      ,.prev_ct_core_tag_lines_i  ( prev_ct_core_tag_lines_lo   )
      
      ,.next_link_io_tag_lines_i  ( next_link_io_tag_lines_lo   )
      ,.next_link_core_tag_lines_i( next_link_core_tag_lines_lo )
      ,.next_ct_core_tag_lines_i  ( next_ct_core_tag_lines_lo   )

      ,.network_a_rtr_core_tag_lines_i( network_a_router_core_tag_lines_lo )
      ,.network_b_rtr_core_tag_lines_i( network_b_router_core_tag_lines_lo )
      
      ,.ci_clk_i   ( ci_clk_li )
      ,.ci_v_i     ( ci_v_li   )
      ,.ci_data_i  ( ci_data_li[link_channel_width_gp-1:0] )
      ,.ci_tkn_o   ( ci_tkn_lo )

      ,.co_clk_o   ( co_clk_lo )
      ,.co_v_o     ( co_v_lo   )
      ,.co_data_o  ( co_data_lo[link_channel_width_gp-1:0] )
      ,.co_tkn_i   ( co_tkn_li )

      ,.ci2_clk_i  ( ci2_clk_li )
      ,.ci2_v_i    ( ci2_v_li   )
      ,.ci2_data_i ( ci2_data_li[link_channel_width_gp-1:0] )
      ,.ci2_tkn_o  ( ci2_tkn_lo )

      ,.co2_clk_o  ( co2_clk_lo )
      ,.co2_v_o    ( co2_v_lo   )
      ,.co2_data_o ( co2_data_lo[link_channel_width_gp-1:0] )
      ,.co2_tkn_i  ( co2_tkn_li )
      
      ,.network_a_rtr_reset_o ( network_a_router_reset_lo )
      ,.network_a_rtr_cord_o  (                           )
      ,.network_a_rtr_links_i ( network_a_rtr_links_li    )
      ,.network_a_rtr_links_o ( network_a_rtr_links_lo    )

      ,.network_b_rtr_reset_o ( network_b_router_reset_lo )
      ,.network_b_rtr_cord_o  ( network_b_router_cord_lo  )
      ,.network_b_rtr_links_i ( network_b_rtr_links_li    )
      ,.network_b_rtr_links_o ( network_b_rtr_links_lo    )
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip Manycore
  //

  bsg_manycore_link_sif_s  link_sif_li;
  bsg_manycore_link_sif_s  link_sif_lo;

  bsg_cache_dma_pkt_s [vcache_num_cache_gp-1:0] dma_pkt_lo;
  logic               [vcache_num_cache_gp-1:0] dma_pkt_v_lo;
  logic               [vcache_num_cache_gp-1:0] dma_pkt_yumi_li;
  
  logic [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_li;
  logic [vcache_num_cache_gp-1:0]                             dma_data_v_li;
  logic [vcache_num_cache_gp-1:0]                             dma_data_ready_lo;
  
  logic [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_lo;
  logic [vcache_num_cache_gp-1:0]                             dma_data_v_lo;
  logic [vcache_num_cache_gp-1:0]                             dma_data_yumi_li;

  bsg_chip_manycore_complex #(.addr_width_p( manycore_addr_width_gp )
                             ,.data_width_p( manycore_data_width_gp )
                             ,.num_tiles_x_p( manycore_num_tiles_x_gp )
                             ,.num_tiles_y_p ( manycore_num_tiles_y_gp  )
                             
                             ,.dmem_size_p( manycore_dmem_size_gp )
                             ,.icache_entries_p( manycore_icache_entries_gp )
                             ,.icache_tag_width_p( manycore_icache_tag_width_gp )
                             
                             ,.load_id_width_p( manycore_load_id_width_gp )
                             ,.epa_byte_addr_width_p( manycore_epa_byte_addr_width_gp )
                             
                             ,.dram_ch_addr_width_p( manycore_dram_ch_addr_width_gp )
                             ,.dram_ch_start_col_p( manycore_dram_ch_start_col_gp )
                             
                             ,.vcache_num_cache_p( vcache_num_cache_gp )
                             ,.vcache_ways_p( vcache_ways_gp )
                             ,.vcache_sets_p( vcache_sets_gp )
                             ,.vcache_block_size_in_words_p( vcache_block_size_in_words_gp )
                             ,.vcache_size_p( vcache_size_gp )
                             )
    manycore_complex
      (.clk_i  ( manycore_clk_lo )
      ,.reset_i( manycore_tag_data_lo.reset )

      ,.io_link_sif_i( link_sif_li )
      ,.io_link_sif_o( link_sif_lo )

      ,.dma_pkt_o       ( dma_pkt_lo )
      ,.dma_pkt_v_o     ( dma_pkt_v_lo )
      ,.dma_pkt_yumi_i  ( dma_pkt_yumi_li )

      ,.dma_data_i      ( dma_data_li )
      ,.dma_data_v_i    ( dma_data_v_li )
      ,.dma_data_ready_o( dma_data_ready_lo )

      ,.dma_data_o      ( dma_data_lo )
      ,.dma_data_v_o    ( dma_data_v_lo )
      ,.dma_data_yumi_i ( dma_data_yumi_li )
      );


  //////////////////////////////////////////////////
  //
  // Adapter between manycore and IO complex
  //

  bsg_manycore_link_async_to_wormhole #(.addr_width_p( manycore_addr_width_gp )
                                       ,.data_width_p( manycore_data_width_gp )
                                       ,.load_id_width_p( manycore_load_id_width_gp )
                                       ,.x_cord_width_p( manycore_x_cord_width_gp )
                                       ,.y_cord_width_p( manycore_y_cord_width_gp )

                                       ,.flit_width_p( ct_width_gp )
                                       ,.dims_p( 1 )
                                       ,.cord_markers_pos_p({ network_a_wh_cord_markers_pos_y_gp, network_a_wh_cord_markers_pos_x_gp })
                                       ,.len_width_p( network_a_wh_len_width_gp )
                                       )
    manycore_wormhole_adapter
      (.mc_clk_i   ( manycore_clk_lo )
      ,.mc_reset_i ( manycore_tag_data_lo.reset )

      ,.mc_links_sif_i ( link_sif_lo )
      ,.mc_links_sif_o ( link_sif_li )
      
      ,.mc_dest_cord_i ( manycore_tag_data_lo.cord )

      ,.wh_clk_i   ( router_clk_lo )
      ,.wh_reset_i ( network_a_router_reset_lo )

      ,.wh_link_i ( network_a_rtr_links_lo )
      ,.wh_link_o ( network_a_rtr_links_li )
      );


  //////////////////////////////////////////////////
  //
  // Adapters between Vcache and IO complex
  //
  
  // Pipeline the reset by 3 flip flops
  logic [vcache_num_cache_gp-1:0] dma_reset_r, dma_reset_rr, dma_reset_rrr;

  always_ff @(posedge manycore_clk_lo)
    begin
      dma_reset_r <= {vcache_num_cache_gp{vcache_tag_data_lo.reset}};
      dma_reset_rr <= dma_reset_r;
      dma_reset_rrr <= dma_reset_rr;
    end
  
  for (i = 0; i < vcache_num_cache_gp; i++)
  begin
    bsg_cache_dma_async_to_wormhole #(.cache_addr_width_p   ( manycore_addr_width_gp+manycore_byte_offset_width_gp-1 )
                                     ,.data_width_p         ( manycore_data_width_gp )
                                     ,.block_size_in_words_p( vcache_block_size_in_words_gp )
                                     ,.flit_width_p         ( ct_width_gp )
                                     ,.dims_p               ( 1 )
                                     ,.cord_markers_pos_p   ( { network_b_wh_cord_markers_pos_y_gp, network_b_wh_cord_markers_pos_x_gp } )
                                     ,.len_width_p          ( network_b_wh_len_width_gp )
                                     )
    dma_to_wh
      (.dma_clk_i  ( manycore_clk_lo )
      ,.dma_reset_i( dma_reset_rrr[i] )
               
      ,.dma_pkt_i             ( dma_pkt_lo[i] )
      ,.dma_pkt_v_i           ( dma_pkt_v_lo[i] )
      ,.dma_pkt_yumi_o        ( dma_pkt_yumi_li[i] )
      
      ,.dma_data_i            ( dma_data_lo[i] )
      ,.dma_data_v_i          ( dma_data_v_lo[i] )
      ,.dma_data_yumi_o       ( dma_data_yumi_li[i] )
      
      ,.dma_return_pkt_o      (      )
      ,.dma_return_pkt_v_o    (      )
      ,.dma_return_pkt_ready_i( 1'b1 ) // Currently ignore dma return pkt
      
      ,.dma_data_o            ( dma_data_li[i] )
      ,.dma_data_v_o          ( dma_data_v_li[i] )
      ,.dma_data_ready_i      ( dma_data_ready_lo[i] )
      
      ,.dma_my_cord_i  ( network_b_router_cord_lo[i] )
      ,.dma_dest_cord_i( network_b_wh_cord_width_gp'(vcache_tag_data_lo.cord) )
      
      ,.wh_clk_i       ( router_clk_lo )
      ,.wh_reset_i     ( network_b_router_reset_lo[i] )
      
      ,.wh_link_i      ( network_b_rtr_links_lo[i] )
      ,.wh_link_o      ( network_b_rtr_links_li[i] )
      );
  end

endmodule

