// 
// bsg_wormhole_async_to_cache_dma.v
// 
// Paul Gao   06/2019
//  
// 

`include "bsg_cache_dma_pkt.vh"
`include "bsg_noc_links.vh"
`include "bsg_wormhole_router.vh"

module bsg_wormhole_async_to_cache_dma
  
 #(// cache dma configuration
   parameter cache_addr_width_p                 = "inv"
  ,parameter data_width_p                       = "inv"
  ,parameter block_size_in_words_p              = "inv"
  ,parameter num_cache_p                        = "inv"

  // wormhole network configuration
  ,parameter flit_width_p                       = "inv"
  ,parameter dims_p                             = 2
  ,parameter int cord_markers_pos_p[dims_p:0]   = '{5, 4, 0}
  ,parameter len_width_p                        = "inv"
  
  ,localparam cord_width_lp                     = cord_markers_pos_p[dims_p]
  ,localparam bsg_cache_dma_pkt_width_lp        = `bsg_cache_dma_pkt_width(cache_addr_width_p)
  ,localparam bsg_ready_and_link_sif_width_lp   = `bsg_ready_and_link_sif_width(flit_width_p)
  )
  
  (input                                                    dma_clk_i
  ,input                                                    dma_reset_i
  // Sending address and write_en               
  ,output logic [num_cache_p-1:0][bsg_cache_dma_pkt_width_lp-1:0] dma_pkt_o
  ,output logic [num_cache_p-1:0]                           dma_pkt_v_o
  ,input  [num_cache_p-1:0]                                 dma_pkt_yumi_i
  // Sending cache block                        
  ,output logic [num_cache_p-1:0][data_width_p-1:0]         dma_data_o
  ,output logic [num_cache_p-1:0]                           dma_data_v_o
  ,input  [num_cache_p-1:0]                                 dma_data_yumi_i
  // Receiving cache block
  ,input  [num_cache_p-1:0][data_width_p-1:0]               dma_data_i
  ,input  [num_cache_p-1:0]                                 dma_data_v_i
  ,output logic [num_cache_p-1:0]                           dma_data_ready_o
  // Wormhole coordinate IDs
  ,input  [cord_width_lp-1:0]                               dma_my_cord_i
  ,input  [cord_width_lp-1:0]                               dma_dest_cord_i

  ,input                                          wh_clk_i
  ,input                                          wh_reset_i
  // bsg noc links
  ,input  [bsg_ready_and_link_sif_width_lp-1:0]   wh_link_i
  ,output [bsg_ready_and_link_sif_width_lp-1:0]   wh_link_o
  );
  
  localparam lg_fifo_depth_lp = 3;
  genvar i;
  
  // Wires to async fifo
  logic dma_async_fifo_enq_li, dma_async_fifo_full_lo,
        dma_async_fifo_valid_lo, dma_async_fifo_deq_li;
  logic [flit_width_p-1:0] dma_async_fifo_data_li, dma_async_fifo_data_lo;


  /********************* Packet definition *********************/
  
  // Define cache DMA packet
  `declare_bsg_cache_dma_pkt_s(cache_addr_width_p);
  
  // Define wormhole header packet
  `declare_bsg_wormhole_router_header_s(cord_width_lp, len_width_p, bsg_wormhole_hdr_s);
  localparam hdr_width_lp = $bits(bsg_wormhole_hdr_s);
  
  typedef struct packed {
    logic [flit_width_p-hdr_width_lp-cord_width_lp-1-1:0] data;
    logic                                                 write_not_read;
    logic [cord_width_lp-1:0]                             src_cord;
    bsg_wormhole_hdr_s                                    hdr;
  } wormhole_hdr_flit_s;
  
  // each cache_dma_pkt / data word takes up 1 wormhole flit
  localparam addr_len_lp  = 1;
  localparam data_len_lp  = block_size_in_words_p;
  
  // Different wormhole packet length
  localparam fill_data_len_lp = data_len_lp;
  localparam evict_ack_len_lp = 0;
  
  
  /********************* Wormhole -> cache DMA *********************/
  
  localparam count_width_lp = `BSG_SAFE_CLOG2(data_len_lp);
  
  // send cache DMA packet
  bsg_cache_dma_pkt_s send_dma_pkt;
  assign send_dma_pkt = dma_async_fifo_data_lo[bsg_cache_dma_pkt_width_lp-1:0];
  for (i = 0; i < num_cache_p; i++)
  begin: rof0
    assign dma_pkt_o[i] = send_dma_pkt;
  end
  
  // send wormhole hdr flit
  wormhole_hdr_flit_s send_wormhole_hdr_flit;
  assign send_wormhole_hdr_flit = dma_async_fifo_data_lo;
  for (i = 0; i < num_cache_p; i++)
  begin: rof1
    assign dma_data_o[i] = dma_async_fifo_data_lo[data_width_p-1:0];
  end
  
  // State machine
  typedef enum logic [1:0] {
    SEND_RESET
   ,SEND_READY
   ,SEND_ADDR
   ,SEND_DATA
  } dma_state_e;
  
  dma_state_e dma_state_r, dma_state_n;
  logic [count_width_lp-1:0] count_r, count_n;
  logic [cord_width_lp-1:0] cord_r, cord_n;
  
  // FIXME: hardcoded mapping!
  logic [`BSG_SAFE_CLOG2(num_cache_p)-1:0] cache_sel_lo;
  assign cache_sel_lo = cord_r[`BSG_SAFE_CLOG2(num_cache_p)-1:0];
  
  always_ff @(posedge dma_clk_i)
  begin
    if (dma_reset_i)
      begin
        dma_state_r <= SEND_RESET;
        count_r <= '0;
        cord_r <= '0;
      end
    else
      begin
        dma_state_r <= dma_state_n;
        count_r <= count_n;
        cord_r <= cord_n;
      end
  end
  
  always_comb
  begin
    dma_state_n = dma_state_r;
    count_n = count_r;
    cord_n = cord_r;
    
    dma_pkt_v_o = '0;
    dma_data_v_o = '0;
    dma_async_fifo_deq_li = 1'b0;
    
    case (dma_state_r)
    SEND_RESET:
      begin
        dma_state_n = SEND_READY;
      end
    SEND_READY:
      begin
        if (dma_async_fifo_valid_lo)
          begin
            dma_async_fifo_deq_li = 1'b1;
            cord_n = send_wormhole_hdr_flit.src_cord;
            dma_state_n = SEND_ADDR;
          end
      end
    SEND_ADDR:
      begin
        if (dma_async_fifo_valid_lo)
          begin
            dma_pkt_v_o[cache_sel_lo] = 1'b1;
            if (dma_pkt_yumi_i[cache_sel_lo])
              begin
                dma_async_fifo_deq_li = 1'b1;
                dma_state_n           = (send_dma_pkt.write_not_read)? SEND_DATA : SEND_READY;
              end
          end
      end
    SEND_DATA:
      begin
        if (dma_async_fifo_valid_lo)
          begin
            dma_data_v_o[cache_sel_lo] = 1'b1;
            if (dma_data_yumi_i[cache_sel_lo])
              begin
                dma_async_fifo_deq_li = 1'b1;
                count_n = (count_r == data_len_lp-1)? '0 : count_r + 1'b1;
                dma_state_n = (count_r == data_len_lp-1)? SEND_READY : SEND_DATA;
              end
          end
      end
    default:
      begin
      end
    endcase
  end


  /********************* cache DMA -> Wormhole *********************/
  
  // State machine
  typedef enum logic [1:0] {
    RECEIVE_RESET
   ,RECEIVE_READY
   ,RECEIVE_DATA
  } receive_state_e;
  
  receive_state_e receive_state_r, receive_state_n;
  logic [count_width_lp-1:0] receive_count_r, receive_count_n;
  
  always_ff @(posedge dma_clk_i)
  begin
    if (dma_reset_i)
      begin
        receive_state_r <= RECEIVE_RESET;
        receive_count_r <= '0;
      end
    else
      begin
        receive_state_r <= receive_state_n;
        receive_count_r <= receive_count_n;
      end
  end
  
  logic [`BSG_SAFE_CLOG2(num_cache_p)-1:0] receive_cache_sel_lo;
  logic receive_cache_sel_v;
  
  if (num_cache_p == 1)
  begin: single_cache
    assign receive_cache_sel_lo = 1'b0;
    assign receive_cache_sel_v  = dma_data_v_i[0];
  end
  else
  begin: multi_cache
    bsg_encode_one_hot
   #(.width_p(num_cache_p)
    ) beoh
    (.i(dma_data_v_i)
    ,.addr_o(receive_cache_sel_lo)
    ,.v_o(receive_cache_sel_v)
    );
  end
  
  // FIXME: hardcoded mapping
  logic [cord_width_lp-1:0] cache_dest_cord_lo;
  assign cache_dest_cord_lo = 
        {dma_dest_cord_i[cord_width_lp-1:`BSG_SAFE_CLOG2(num_cache_p)], receive_cache_sel_lo};
  
  // receive wormhole hdr flit
  wormhole_hdr_flit_s receive_wormhole_hdr_flit;
  assign receive_wormhole_hdr_flit.data           = '0;
  assign receive_wormhole_hdr_flit.hdr.cord       = cache_dest_cord_lo;
  assign receive_wormhole_hdr_flit.src_cord       = dma_my_cord_i;
  assign receive_wormhole_hdr_flit.write_not_read = 1'b0;
  assign receive_wormhole_hdr_flit.hdr.len        = len_width_p'(fill_data_len_lp);
  
  always_comb
  begin
    receive_state_n = receive_state_r;
    receive_count_n = receive_count_r;
    
    dma_data_ready_o = '0;
    dma_async_fifo_enq_li = 1'b0;
    dma_async_fifo_data_li = '0;
    
    case (receive_state_r)
    RECEIVE_RESET:
      begin
        receive_state_n = RECEIVE_READY;
      end
    RECEIVE_READY:
      begin
        dma_async_fifo_data_li = receive_wormhole_hdr_flit;
        if (receive_cache_sel_v & ~dma_async_fifo_full_lo)
          begin
            dma_async_fifo_enq_li = 1'b1;
            receive_state_n = RECEIVE_DATA;
          end
      end
    RECEIVE_DATA:
      begin
        dma_async_fifo_data_li = flit_width_p'(dma_data_i[receive_cache_sel_lo]);
        if (~dma_async_fifo_full_lo)
          begin
            dma_data_ready_o = '1;
            if (receive_cache_sel_v)
            if (receive_cache_sel_v)
              begin
                dma_async_fifo_enq_li = 1'b1;
                receive_count_n = (receive_count_r == data_len_lp-1)? 
                                  '0 : receive_count_r + 1'b1;
                receive_state_n = (receive_count_r == data_len_lp-1)? 
                                  RECEIVE_READY : RECEIVE_DATA;
              end
          end
      end
    default:
      begin
      end
    endcase
  end
  
  
  /********************* Async fifo to wormhole link *********************/
  
  `declare_bsg_ready_and_link_sif_s(flit_width_p, bsg_ready_and_link_sif_s);
  bsg_ready_and_link_sif_s wh_link_i_cast, wh_link_o_cast;
  
  assign wh_link_i_cast = wh_link_i;
  assign wh_link_o      = wh_link_o_cast;
  
  logic  wh_async_fifo_full_lo;
  assign wh_link_o_cast.ready_and_rev = ~wh_async_fifo_full_lo;

  bsg_async_fifo
 #(.lg_size_p(lg_fifo_depth_lp)
  ,.width_p  (flit_width_p)
  ) wh_to_dma
  (.w_clk_i  (wh_clk_i)
  ,.w_reset_i(wh_reset_i)
  ,.w_enq_i  (wh_link_i_cast.v & wh_link_o_cast.ready_and_rev)
  ,.w_data_i (wh_link_i_cast.data)
  ,.w_full_o (wh_async_fifo_full_lo)

  ,.r_clk_i  (dma_clk_i)
  ,.r_reset_i(dma_reset_i)
  ,.r_deq_i  (dma_async_fifo_deq_li)
  ,.r_data_o (dma_async_fifo_data_lo)
  ,.r_valid_o(dma_async_fifo_valid_lo)
  );

  bsg_async_fifo
 #(.lg_size_p(lg_fifo_depth_lp)
  ,.width_p  (flit_width_p)
  ) dma_to_wh
  (.w_clk_i  (dma_clk_i)
  ,.w_reset_i(dma_reset_i)
  ,.w_enq_i  (dma_async_fifo_enq_li)
  ,.w_data_i (dma_async_fifo_data_li)
  ,.w_full_o (dma_async_fifo_full_lo)

  ,.r_clk_i  (wh_clk_i)
  ,.r_reset_i(wh_reset_i)
  ,.r_deq_i  (wh_link_o_cast.v & wh_link_i_cast.ready_and_rev)
  ,.r_data_o (wh_link_o_cast.data)
  ,.r_valid_o(wh_link_o_cast.v)
  );
  
endmodule