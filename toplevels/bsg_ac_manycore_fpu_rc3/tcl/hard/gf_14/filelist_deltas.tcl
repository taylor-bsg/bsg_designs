set basejump_stl_dir $::env(BASEJUMP_STL_DIR)

# list of files to replace
set HARD_SWAP_FILELIST [join "
  $basejump_stl_dir/hard/gf_14/bsg_mem/bsg_mem_1rw_sync.v
  $basejump_stl_dir/hard/gf_14/bsg_mem/bsg_mem_1rw_sync_mask_write_bit.v
  $basejump_stl_dir/hard/gf_14/bsg_mem/bsg_mem_1rw_sync_mask_write_byte.v
  $basejump_stl_dir/hard/gf_14/bsg_mem/bsg_mem_2r1w_sync.v
  $basejump_stl_dir/hard/gf_14/bsg_async/bsg_sync_sync.v
  $basejump_stl_dir/hard/gf_14/bsg_async/bsg_launch_sync_sync.v
  $basejump_stl_dir/hard/gf_14/bsg_misc/bsg_mux.v
  $basejump_stl_dir/hard/gf_14/bsg_clk_gen/bsg_clk_gen_osc.v
"]

set NETLIST_SOURCE_FILES [join "
  $basejump_stl_dir/hard/gf_14/bsg_clk_gen/bsg_rp_clk_gen_fine_delay_tuner.v
  $basejump_stl_dir/hard/gf_14/bsg_clk_gen/bsg_rp_clk_gen_coarse_delay_tuner.v
  $basejump_stl_dir/hard/gf_14/bsg_clk_gen/bsg_rp_clk_gen_atomic_delay_tuner.v
"]

set NEW_SVERILOG_SOURCE_FILES [join "
"]

