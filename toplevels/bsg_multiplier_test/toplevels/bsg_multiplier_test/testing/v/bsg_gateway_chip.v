
module bsg_gateway_chip;

  //////////////////////////////////////////////////
  //
  // Nonsynth Clock Generator(s)
  //

  logic clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(1000)) blackparrot_clk_gen (.o(clk));

  //////////////////////////////////////////////////
  //
  // Nonsynth Reset Generator(s)
  //

  logic reset;
  bsg_nonsynth_reset_gen #(.num_clocks_p(1),.reset_cycles_lo_p(10),.reset_cycles_hi_p(5))
    tag_reset_gen
      (.clk_i(clk)
      ,.async_reset_o(reset)
      );

  logic [63:0] in1, in2, out;
  bsg_chip DUT
   (.clk_i(clk)
    ,.reset_i(reset)

    ,.in1(in2)
    ,.in2(in1)
    ,.out(out)
    );

  initial
    begin
      in1 = 0;
      in2 = 0;
      @(negedge reset);
      for (integer i = 0; i < 10; i++)
        @(posedge clk);
      in1 = $random();
      in2 = $random();
      for (integer i = 0; i < 3; i++)
        @(posedge clk);
      in1 = $random();
      in2 = $random();
      for (integer i = 0; i < 3; i++)
        @(posedge clk);
      in1 = $random();
      in2 = $random();
      for (integer i = 0; i < 3; i++)
        @(posedge clk);
      $finish();
    end

  initial
    begin
      $vcdpluson;
      $vcdplusmemon;
      $vcdplusautoflushon;
    end

  initial
    begin
      $set_gate_level_monitoring("rtl_on");
      $set_toggle_region(DUT);
      $toggle_start();
    end

  final
    begin
      $toggle_stop();
      $toggle_report("run.saif", 1.0e-12, DUT);
    end

endmodule

