
module bsg_mem_2r1w_latch #(parameter width_p=-1
                            , parameter els_p=-1
                            , parameter addr_width_lp=`BSG_SAFE_CLOG2(els_p)
                            )
   (input                       w_clk_i
    , input                     w_reset_i

    , input                     w_v_i
    , input [addr_width_lp-1:0] w_addr_i
    , input [width_p-1:0]       w_data_i

    , input                      r0_v_i
    , input [addr_width_lp-1:0]  r0_addr_i
    , output logic [width_p-1:0] r0_data_o

    , input                      r1_v_i
    , input [addr_width_lp-1:0]  r1_addr_i
    , output logic [width_p-1:0] r1_data_o
    );

logic [width_p-1:0] mem [0:els_p-1];

logic [els_p-1:0] w_addr_onehot_lo;
logic w_addr_onehot_v_lo;
bsg_decode_with_v
 #(.num_out_p(els_p))
 waddr_decode
  (.i(w_addr_i)
   ,.v_i(w_v_i)
   ,.o(w_addr_onehot_lo)
   );

logic [width_p-1:0] wdata_r;
bsg_dff
 #(.width_p(width_p))
 wdata_reg
  (.clk_i(w_clk_i)

   ,.data_i(w_data_i)
   ,.data_o(wdata_r)
   );

for (genvar i = 0; i < els_p; i++)
  begin : latch
    bsg_dlatch
     #(.width_p(width_p), .i_know_this_is_a_bad_idea_p(1))
     mem_latch
      (.clk_i(w_clk_i & w_addr_onehot_lo[i])
       ,.data_i(wdata_r)

       ,.data_o(mem[i])
       );
  end

assign r0_data_o = mem[r0_addr_i];
assign r1_data_o = mem[r1_addr_i];

endmodule

