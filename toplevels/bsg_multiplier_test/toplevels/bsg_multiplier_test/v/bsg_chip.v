
//==============================================================================
//
// BSG CHIP
//
// This is the toplevel for the ASIC. This chip uses the UW BGA package found
// inside bsg_packaging/uw_bga. For physical design reasons, the input pins
// have been swizzled (ie. re-arranged) from their original meaning. We use the
// bsg_chip_swizzle_adapter in every ASIC to abstract away detail.
//

`include "bsg_defines.v"

module bsg_chip
(input logic clk_i
 , input logic reset_i

 , input [63:0] in1
 , input [63:0] in2

 , output [63:0] out
 );

  logic [63:0] mul_lo;

  assign mul_lo = in1 * in2;

  bsg_dff_chain
   #(.width_p(64), .num_stages_p(3))
   retime_chain
    (.clk_i(clk_i)

     ,.data_i(mul_lo)
     ,.data_o(out)
     );

endmodule

