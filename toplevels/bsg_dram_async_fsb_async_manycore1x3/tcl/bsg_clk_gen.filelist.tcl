#------------------------------------------------------------
# Do NOT arbitrarily change the order of files. Some module
# and macro definitions may be needed by the subsequent files
#------------------------------------------------------------

set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_designs_dir $::env(BSG_DESIGNS_DIR)
set bsg_designs_target_dir $::env(BSG_DESIGNS_TARGET_DIR)

set SVERILOG_SOURCE_FILES [join "
  $bsg_ip_cores_dir/bsg_misc/bsg_defines.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_pkg.v
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_clk_gen.v
  $bsg_ip_cores_dir/bsg_clk_gen/bsg_clk_gen_osc.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_client.v
  $bsg_ip_cores_dir/bsg_tag/bsg_tag_client_unsync.v
  $bsg_ip_cores_dir/bsg_misc/bsg_counter_clock_downsample.v
  $bsg_ip_cores_dir/bsg_misc/bsg_counter_clear_up.v
  $bsg_ip_cores_dir/bsg_misc/bsg_mux.v
  $bsg_ip_cores_dir/bsg_misc/bsg_mux2_gatestack.v
  $bsg_ip_cores_dir/bsg_misc/bsg_muxi2_gatestack.v
  $bsg_ip_cores_dir/bsg_misc/bsg_dff_gatestack.v
  $bsg_ip_cores_dir/bsg_misc/bsg_dff_reset_en.v
  $bsg_ip_cores_dir/bsg_misc/bsg_strobe.v
  $bsg_ip_cores_dir/bsg_misc/bsg_dff.v
  $bsg_ip_cores_dir/bsg_misc/bsg_xnor.v
  $bsg_ip_cores_dir/bsg_misc/bsg_nand.v
  $bsg_ip_cores_dir/bsg_misc/bsg_nor3.v
  $bsg_ip_cores_dir/bsg_misc/bsg_reduce.v
  $bsg_ip_cores_dir/bsg_misc/bsg_buf.v
  $bsg_ip_cores_dir/bsg_async/bsg_launch_sync_sync.v
"]
