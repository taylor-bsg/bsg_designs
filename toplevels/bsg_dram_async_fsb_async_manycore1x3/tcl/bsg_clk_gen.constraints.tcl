source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag_timing.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen_timing.tcl

set JTAG_CLK_PERIOD 6.0
set OSC_PERIOD_INT  1.8

if { ${analysis_type} == "bc_wc" } {
  set CORE_CLOCK_PERIOD         3.0 
  set FSB_CLOCK_PERIOD          2.5
  set DRLP_CLOCK_PERIOD         2.5 
  set OUTERSPACE_CLOCK_PERIOD   2.5
  set DFI_2X_CLOCK_PERIOD       5.0 
  set MASTER_IO_CLOCK_PERIOD    5.0
} elseif { ${analysis_type} == "single_typical" } {
  set CORE_CLOCK_PERIOD         3.0 
  set FSB_CLOCK_PERIOD          2.5 
  set DRLP_CLOCK_PERIOD         2.5 
  set OUTERSPACE_CLOCK_PERIOD   2.5 
  set DFI_2X_CLOCK_PERIOD       5.0 
  set MASTER_IO_CLOCK_PERIOD    5.0
}

#######################################################################################################################
#bsg_tag_clock_create bsg_tag_clk clk_i data_i en_i $JTAG_CLK_PERIOD 3.0
create_clock -period $JTAG_CLK_PERIOD -name bsg_tag_clk [get_ports *_tag_i\[clk\]]
set_clock_uncertainty [expr $JTAG_CLK_PERIOD * 3.0 / 100.0] [get_clocks bsg_tag_clk]
set_input_delay [expr $JTAG_CLK_PERIOD / 2] -clock bsg_tag_clk [remove_from_collection [get_ports *_tag_i\[*\]] [get_ports *_tag_i\[clk\]]]

bsg_clk_gen_clock_create "" bsg_clk bsg_tag_clk $OSC_PERIOD_INT $CORE_CLOCK_PERIOD 5
