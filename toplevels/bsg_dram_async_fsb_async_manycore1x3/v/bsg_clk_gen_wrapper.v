import bsg_tag_pkg::bsg_tag_s;
module bsg_clk_gen_wrapper #
  (parameter bsg_num_clk_endpoint_p      = 5
  ,parameter bsg_num_dram_clk_endpoint_p = 1
  ,parameter bsg_lines_per_endpoint_p    = 2)
  (input                                                             tag_clk_i
  ,input                                                             tag_data_i
  ,input                                                             tag_en_i
  ,input    [bsg_num_dram_clk_endpoint_p+bsg_num_clk_endpoint_p-1:0] async_reset_i
  ,input                                [bsg_num_clk_endpoint_p-1:0] clk_i
  ,input                              [2*bsg_num_clk_endpoint_p-1:0] sel_i
  ,output                               [bsg_num_clk_endpoint_p-1:0] clk_o
  ,input                        [$clog2(bsg_num_clk_endpoint_p)-1:0] obs_sel_i
  ,output                                                            obs_clk_o
  ,output                          [bsg_num_dram_clk_endpoint_p-1:0] dram_clk_2x_o
  ,output                          [bsg_num_dram_clk_endpoint_p-1:0] dram_clk_1x_o
  ,input  [bsg_lines_per_endpoint_p*bsg_num_dram_clk_endpoint_p-1:0] dly_clk_i
  ,output [bsg_lines_per_endpoint_p*bsg_num_dram_clk_endpoint_p-1:0] dly_clk_o);

`include "bsg_tag.vh"
   // for bsg_clk_gen v2, we need 3 bsg_tag endpoints
  localparam bsg_tags_per_endpoint_lp = 3;
  localparam bsg_tag_els_lp           = (bsg_num_clk_endpoint_p+bsg_num_dram_clk_endpoint_p)*bsg_tags_per_endpoint_lp;
  localparam bsg_ds_width_lp          = 8;
  localparam bsg_num_adgs_lp          = 1;

  `declare_bsg_clk_gen_osc_tag_payload_s(bsg_num_adgs_lp)
  `declare_bsg_clk_gen_ds_tag_payload_s(bsg_ds_width_lp)

  localparam bsg_tag_max_payload_length_lp    = `BSG_MAX($bits(bsg_clk_gen_osc_tag_payload_s),$bits(bsg_clk_gen_ds_tag_payload_s));
  localparam lg_bsg_tag_max_payload_length_lp = $clog2(bsg_tag_max_payload_length_lp+1);

  localparam bsg_obs_sel_width_lp = $clog2(bsg_num_clk_endpoint_p);
  localparam bsg_obs_clk_width_lp = 1<<bsg_obs_sel_width_lp;

  bsg_tag_s  [bsg_tag_els_lp-1:0] tags;

  wire [bsg_obs_clk_width_lp-1:0] obs_clk_i;

  genvar i;

  bsg_tag_master #
    (.els_p                     ( bsg_tag_els_lp                     )
    ,.lg_width_p                ( lg_bsg_tag_max_payload_length_lp   ))
  btm
    (.clk_i                     ( tag_clk_i                          )
    ,.data_i                    ( tag_data_i                         )
    ,.en_i                      ( tag_en_i                           )
    ,.clients_r_o               ( tags                               ));

  // core clock generator (bsg_tag ID's 0 and 1)
  generate
    for(i=0;i<bsg_num_clk_endpoint_p;i=i+1) begin: clk_gen
      //bsg_clk_gen #
      //  (.downsample_width_p    ( bsg_ds_width_lp                    )
      //  ,.num_adgs_p            ( bsg_num_adgs_lp                    )
      //  ,.version_p             ( 2                                  ))
      bsg_clk_gen
       # (.downsample_width_p    ( bsg_ds_width_lp                    )
        ,.num_adgs_p            ( bsg_num_adgs_lp                    )
        ,.version_p             ( 2                                  ))
      clk_gen_inst
        (.bsg_osc_tag_i         ( tags[bsg_tags_per_endpoint_lp*i+0] )
        ,.bsg_osc_trigger_tag_i ( tags[bsg_tags_per_endpoint_lp*i+2] )
        ,.bsg_ds_tag_i          ( tags[bsg_tags_per_endpoint_lp*i+1] )
        ,.async_osc_reset_i     ( async_reset_i[i]                   )
        ,.ext_clk_i             ( clk_i[i]                           )
        ,.select_i              ( sel_i[2*i+1:2*i]                   )
        ,.clk_o                 ( clk_o[i]                           ));
    end
  endgenerate

  generate
    for(i=0;i<bsg_obs_clk_width_lp;i=i+1) begin: obs_clk_mux
      assign obs_clk_i[i] = (i < bsg_num_clk_endpoint_p)? clk_o[i]: 1'b0;
    end
  endgenerate

  bsg_mux #
    (.width_p                   ( 1                    )
    ,.els_p                     ( bsg_obs_clk_width_lp )
    ,.balanced_p                ( 1                    )
    ,.harden_p                  ( 1                    ))
  mux_inst
    (.data_i                    ( obs_clk_i            )
    ,.sel_i                     ( obs_sel_i            )
    ,.data_o                    ( obs_clk_o            ));

  generate
    for(i=0;i<bsg_num_dram_clk_endpoint_p;i=i+1) begin: dram_clk_gen
      bsg_dram_clk_gen #
        (.num_lines_p           ( bsg_lines_per_endpoint_p )
        ,.downsample_width_p    ( bsg_ds_width_lp          )
        ,.num_adgs_p            ( bsg_num_adgs_lp          )
        ,.version_p             ( 2                        ))
      dram_clk_gen_inst
        (.bsg_osc_tag_i         ( tags[bsg_tags_per_endpoint_lp*(bsg_num_clk_endpoint_p+i)+0] )
        ,.bsg_osc_trigger_tag_i ( tags[bsg_tags_per_endpoint_lp*(bsg_num_clk_endpoint_p+i)+2] )
        ,.bsg_ds_tag_i          ( tags[bsg_tags_per_endpoint_lp*(bsg_num_clk_endpoint_p+i)+1] )
        ,.async_osc_reset_i     ( async_reset_i[bsg_num_clk_endpoint_p+i]                     )
        ,.osc_clk_o             ( dram_clk_2x_o[i] )
        ,.div_clk_o             ( dram_clk_1x_o[i] )
        ,.dly_clk_i             ( dly_clk_i[bsg_lines_per_endpoint_p*(i+1)-1:bsg_lines_per_endpoint_p*i] )
        ,.dly_clk_o             ( dly_clk_o[bsg_lines_per_endpoint_p*(i+1)-1:bsg_lines_per_endpoint_p*i] ));
    end
  endgenerate 

endmodule
