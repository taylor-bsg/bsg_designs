# This file is for setting simulation parameters that are particular to a design
# Generally speaking, very few things should be set here and they should be
# specific to the design; for instance CLOCK PERIODS


#################################################
# DRAM Parameter
VCS_OPTIONS += +define+den2048Mb
VCS_OPTIONS += +define+sg75
VCS_OPTIONS += +define+x16
VCS_OPTIONS += +define+FULL_MEM

# clocking this too fast runs into issues
# caused by the timescale of the TSMC cells
# being set to 1 ns

ifeq ($(BSG_VCS_RUN_TYPE),rtl_hard)
VCS_OPTIONS += +define+CORE_0_PERIOD=10000
VCS_OPTIONS += +define+CORE_1_PERIOD=10000

VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=9500
VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=9500

VCS_OPTIONS += +define+DFI_2X_0_PERIOD=5000
VCS_OPTIONS += +define+DFI_2X_1_PERIOD=5000

VCS_OPTIONS += +define+DRLP_0_PERIOD=5000
VCS_OPTIONS += +define+DRLP_1_PERIOD=5000

VCS_OPTIONS += +define+FSB_0_PERIOD=8000
VCS_OPTIONS += +define+FSB_1_PERIOD=8000

VCS_OPTIONS += +define+OUTERSPACE_0_PERIOD=8000
VCS_OPTIONS += +define+OUTERSPACE_1_PERIOD=8000

VCS_OPTIONS += +define+bsg_VANILLA
endif

ifeq ($(BSG_VCS_RUN_TYPE),rtl)
VCS_OPTIONS += +define+CORE_0_PERIOD=10000
VCS_OPTIONS += +define+CORE_1_PERIOD=10000

VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=9500
VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=9500

VCS_OPTIONS += +define+DFI_2X_0_PERIOD=5000
VCS_OPTIONS += +define+DFI_2X_1_PERIOD=5000

VCS_OPTIONS += +define+DRLP_0_PERIOD=5000
VCS_OPTIONS += +define+DRLP_1_PERIOD=5000

VCS_OPTIONS += +define+FSB_0_PERIOD=8000
VCS_OPTIONS += +define+FSB_1_PERIOD=8000

VCS_OPTIONS += +define+OUTERSPACE_0_PERIOD=8000
VCS_OPTIONS += +define+OUTERSPACE_1_PERIOD=8000

VCS_OPTIONS += +define+bsg_VANILLA
endif


ifeq ($(BSG_VCS_RUN_TYPE),post_synth)

VCS_OPTIONS += +define+CORE_0_PERIOD=12000
VCS_OPTIONS += +define+CORE_1_PERIOD=12000
VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=3000
VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=3000
VCS_OPTIONS += +define+DFI_2X_0_PERIOD=2500
VCS_OPTIONS += +define+DFI_2X_1_PERIOD=2500

VCS_OPTIONS += +define+DRLP_0_PERIOD=4000
VCS_OPTIONS += +define+DRLP_1_PERIOD=4000
VCS_OPTIONS += +define+bsg_VANILLA

VCS_OPTIONS += +define+FSB_0_PERIOD=2500
VCS_OPTIONS += +define+FSB_1_PERIOD=2500

VCS_OPTIONS += +define+OUTERSPACE_0_PERIOD=5000
VCS_OPTIONS += +define+OUTERSPACE_1_PERIOD=5000

endif


ifeq ($(BSG_VCS_RUN_TYPE),post_place_and_route)
# these correspond to the 5.5 ns core frequency
# and the 4.0 ns I/O frequency in constraints.tcl

# MIN PARAMETERS
ifeq ($(BSG_GATE_LEVEL_MODE),min)
VCS_OPTIONS += +define+CORE_0_PERIOD=1000
VCS_OPTIONS += +define+CORE_1_PERIOD=6000

VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=5500
VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=5500

VCS_OPTIONS += +define+DFI_2X_0_PERIOD=5000
VCS_OPTIONS += +define+DFI_2X_1_PERIOD=5000

VCS_OPTIONS += +define+DRLP_0_PERIOD=5000
VCS_OPTIONS += +define+DRLP_1_PERIOD=5000

VCS_OPTIONS += +define+FSB_0_PERIOD=8000
VCS_OPTIONS += +define+FSB_1_PERIOD=8000

VCS_OPTIONS += +define+OUTERSPACE_0_PERIOD=8000
VCS_OPTIONS += +define+OUTERSPACE_1_PERIOD=8000

# does not work  FIXME figure out why
#VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=4528
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=4528
endif

# MAX PARAMETERS
ifeq ($(BSG_GATE_LEVEL_MODE),max)
VCS_OPTIONS += +define+CORE_0_PERIOD=3000
VCS_OPTIONS += +define+CORE_1_PERIOD=3000

VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=5000
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=2000 Failed
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=2015 Failed
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=5000 Success
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=4000 Success
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=3000 Success
VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=5000


VCS_OPTIONS += +define+DFI_2X_0_PERIOD=5000
VCS_OPTIONS += +define+DFI_2X_1_PERIOD=5000

VCS_OPTIONS += +define+DRLP_0_PERIOD=2500
VCS_OPTIONS += +define+DRLP_1_PERIOD=2500

VCS_OPTIONS += +define+FSB_0_PERIOD=2500
VCS_OPTIONS += +define+FSB_1_PERIOD=2500

VCS_OPTIONS += +define+OUTERSPACE_0_PERIOD=2500
VCS_OPTIONS += +define+OUTERSPACE_1_PERIOD=2500

#doesn't work
#VCS_OPTIONS += +define+IO_MASTER_0_PERIOD=4560
#VCS_OPTIONS += +define+IO_MASTER_1_PERIOD=4560
endif

endif

###########################################################
#  Select the test mode
VCS_OPTIONS += +incdir+$(BSG_DESIGNS_TARGET_DIR)/testing

ifdef TEST_BSG_CLK_GEN
VCS_OPTIONS += +define+TEST_BSG_CLK_GEN=1
endif

ifdef TEST_OUTERSPACE_LOOPBACK
VCS_OPTIONS += +define+TEST_OUTERSPACE_LOOPBACK=1
endif

################################################################
# Generates the ELF file and the memory dump files.
TEST_CASE=bsg_dram_loopback_cache

ifdef UPDATE_ROMS
PROG_NAME=main
bsg_tiles_X =4
bsg_tiles_Y =4
make_opt =bsg_tiles_X=$(bsg_tiles_X)  bsg_tiles_Y=$(bsg_tiles_Y)
SPMD_DIR =$(BSG_MANYCORE_DIR)/software/spmd/$(TEST_CASE)

memory_dump: 
	make clean -C $(SPMD_DIR) $(make_opt)
	make $(PROG_NAME)_dmem.mem -C $(SPMD_DIR) $(make_opt)
	make $(PROG_NAME)_dram.mem -C $(SPMD_DIR) $(make_opt)							       
	ln -s $(SPMD_DIR)/$(PROG_NAME)_dmem.mem $(PROG_NAME)_dmem.mem  
	ln -s $(SPMD_DIR)/$(PROG_NAME)_dram.mem $(PROG_NAME)_dram.mem  
	ln -s $(SPMD_DIR)/$(PROG_NAME).riscv    $(PROG_NAME).riscv 
endif

include $(BSG_MANYCORE_DIR)/software/mk/Makefile.verilog.loader




