/**
 *  bsg_chip.v
 */

`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

module bsg_chip
  import bsg_chip_pkg::*;
  `include "bsg_pinout.v"
  `include "bsg_iopads.v"
 
  // tie off unused signals
  //
  `BSG_IO_TIELO_VEC_ONE(sdi_token_o_int,1);
  `BSG_IO_TIELO_VEC_ONE(sdi_token_o_int,2);
  `BSG_IO_TIELO_VEC_ONE(sdo_sclk_o_int,1);
  `BSG_IO_TIELO_VEC_ONE(sdo_ncmd_o_int,1);
  `BSG_IO_TIELO_VEC(sdo_B_data_o_int,8);
  `BSG_IO_TIELO(sdo_A_data_8_o_int);
  `BSG_IO_TIELO(sdo_C_data_8_o_int);
  `BSG_IO_TIELO(SMA_out_p_o_int);
  `BSG_IO_TIELO(SMA_out_n_o_int);
  `BSG_IO_TIELO(JTAG_TDO_o_int);
  `BSG_IO_TIELO(misc_R_3_o_int);

  // clock gens
  // 0: core
  // 1: io master
  // 2: drlp
  // 3: fsb
  // 4: outerspace
  // 5-6: DRAM
  //
  // clock signal sel
  // 00: osc_clk
  // 01: ds_clk
  // 10: ext_clk
  // 11: disable
  //
  logic [1:0] clk_gen_core_sel;
  logic [1:0] clk_gen_iom_sel;
  logic [1:0] clk_gen_drlp_sel;
  logic [1:0] clk_gen_fsb_sel;
  logic [1:0] clk_gen_op_sel;
  logic [9:0] sel_li; // clock select

  logic [4:0] clk_li; // external clocks
  logic [5:0] async_reset_li;  // reset signals from outside

  logic [4:0] clk_lo;
  logic core_clk_lo;
  logic iom_clk_lo;
  logic drlp_clk_lo;
  logic fsb_clk_lo;
  logic op_clk_lo;
  logic dfi_clk_2x_lo;
  logic dfi_clk_1x_lo;
  logic obs_clk_lo;
  logic [1:0] dqs_filter;
  logic [1:0] dqs_dly;

  logic tag_clk_li; // tag_master signals for clock_gen_wrapper
  logic tag_data_li;
  logic tag_en_li;

  assign clk_gen_core_sel = {misc_R_7_i_int, misc_R_6_i_int};
  assign clk_gen_iom_sel = {misc_L_7_i_int, misc_L_6_i_int};
  assign clk_gen_drlp_sel = {misc_T_2_i_int, misc_R_4_i_int};
  assign clk_gen_fsb_sel = {misc_L_0_i_int, misc_R_0_i_int};
  assign clk_gen_op_sel = {sdo_token_i_int[2], sdo_token_i_int[1]};

  assign sel_li = {
    clk_gen_op_sel,
    clk_gen_fsb_sel,
    clk_gen_drlp_sel,
    clk_gen_iom_sel,
    clk_gen_core_sel
  };

  assign clk_li = {
    sdo_token_i_int[3],
    misc_L_1_i_int,
    misc_L_5_i_int,
    PLL_CLK_i_int,
    misc_L_4_i_int
  };
  
  assign async_reset_li = {
    {4{JTAG_TRST_i_int}},
    misc_T_1_i_int,
    JTAG_TRST_i_int
  };

  assign core_clk_lo = clk_lo[0];
  assign iom_clk_lo = clk_lo[1];
  assign drlp_clk_lo = clk_lo[2];
  assign fsb_clk_lo = clk_lo[3];
  assign op_clk_lo = clk_lo[4];

  assign tag_clk_li = JTAG_TCK_i_int;
  assign tag_data_li = JTAG_TDI_i_int;
  assign tag_en_li = JTAG_TMS_i_int;

  bsg_clk_gen_wrapper #(
    .num_clk_endpoint_p(5)
    ,.num_dram_clk_endpoint_p(1)
    ,.lines_per_endpoint_p(2)
  ) clk_gen_wrapper_inst (
    .tag_clk_i(tag_clk_li)
    ,.tag_data_i(tag_data_li)
    ,.tag_en_i(tag_en_li)
    ,.async_reset_i(async_reset_li)
    ,.clk_i(clk_li)
    ,.sel_i(sel_li)
    ,.clk_o(clk_lo)
    ,.obs_clk_o(obs_clk_lo)
    ,.dram_clk_2x_o(dfi_clk_2x_lo)
    ,.dram_clk_1x_o(dfi_clk_1x_lo)
    //,.dly_clk_i(sdi_sclk_i_io_int)
    ,.dly_clk_i(dqs_filter)
    ,.dly_clk_o(dqs_dly)
  );

  assign misc_L_3_o_int = obs_clk_lo; // observe clock gen signals from outside!

  // dfi_clk mux
  //
  logic ext_dfi_clk_2x;
  logic ext_dfi_clk_1x;
  logic dfi_clk_sel;
  assign ext_dfi_clk_2x = misc_R_5_i_int;
  assign ext_dfi_clk_1x = misc_T_0_i_int;  
  assign dfi_clk_sel = misc_R_1_i_int;

  logic dfi_clk_2x_final;
  logic dfi_clk_1x_final;

  bsg_mux #(
    .width_p(2)
    ,.els_p(2)
    ,.balanced_p(1)
    ,.harden_p(1)
  ) dfi_clk_mux (
    .data_i({ext_dfi_clk_2x, ext_dfi_clk_1x, dfi_clk_2x_lo, dfi_clk_1x_lo})
    ,.sel_i(dfi_clk_sel)
    ,.data_o({dfi_clk_2x_final, dfi_clk_1x_final})
  );

  // bsg_chip_guts
  //
  wire [(dram_dfi_width_gp>>4)-1:0] dm_oen_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dm_o_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_p_oen_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_p_ien_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_p_o_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_p_i_li;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_n_oen_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_n_ien_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_n_o_lo;
  wire [(dram_dfi_width_gp>>4)-1:0] dqs_n_i_li;

  wire [(dram_dfi_width_gp>>1)-1:0] dq_oen_lo;
  wire [(dram_dfi_width_gp>>1)-1:0] dq_o_lo;
  wire [(dram_dfi_width_gp>>1)-1:0] dq_i_li;

  wire ddr_ck_p_lo;
  wire ddr_ck_n_lo;
  wire ddr_cke_lo;
  wire [2:0] ddr_ba_lo;     // this is the maximum width.
  wire [15:0] ddr_addr_lo;  // this is the maximum width.
  wire ddr_cs_n_lo;
  wire ddr_ras_n_lo;
  wire ddr_cas_n_lo;
  wire ddr_we_n_lo;
  wire ddr_reset_n_lo;
  wire ddr_odt_lo;


  localparam num_channels_lp = 1;
  logic [7:0] sdi_data_i_int_packed [num_channels_lp-1:0];
  logic [7:0] sdo_data_o_int_packed [num_channels_lp-1:0];
  assign sdi_data_i_int_packed[0] = sdi_A_data_i_int;
  assign sdo_A_data_o_int = sdo_data_o_int_packed[0];

  bsg_chip_guts #(
    .uniqueness_p(1)  // unused
    ,.enabled_at_start_vec_p(2'b11)
    ,.num_channels_p( num_channels_lp )
  ) guts (
    .core_clk_i(core_clk_lo)
    ,.async_reset_i(reset_i_int)
    ,.io_master_clk_i(iom_clk_lo)
    ,.drlp_clk_i(drlp_clk_lo)
    ,.fsb_clk_i(fsb_clk_lo)
    ,.op_clk_i(op_clk_lo)
    ,.dmc_rst_i(async_reset_li[0])
    // flip B and C input for PD
    ,.io_clk_tline_i(sdi_sclk_i_int[num_channels_lp-1:0])
    ,.io_valid_tline_i(sdi_ncmd_i_int[num_channels_lp-1:0])
    ,.io_data_tline_i(sdi_data_i_int_packed)
    ,.io_token_clk_tline_o(sdi_token_o_int[num_channels_lp-1:0])
    ,.im_clk_tline_o(sdo_sclk_o_int[num_channels_lp-1:0])
    ,.im_valid_tline_o(sdo_ncmd_o_int[num_channels_lp-1:0])
    ,.im_data_tline_o(sdo_data_o_int_packed)
    ,.token_clk_tline_i(sdo_token_i_int[num_channels_lp-1:0])
    ,.im_slave_reset_tline_r_o()    // unused by ASIC
    ,.fsb_reset_o()                 // post calibration reset

    // DDR interface signals
    ,.ddr_ck_p(ddr_ck_p_lo)
    ,.ddr_ck_n(ddr_ck_n_lo)
    ,.ddr_cke(ddr_cke_lo)
    ,.ddr_ba(ddr_ba_lo)
    ,.ddr_addr(ddr_addr_lo)
    ,.ddr_cs_n(ddr_cs_n_lo)
    ,.ddr_ras_n(ddr_ras_n_lo)
    ,.ddr_cas_n(ddr_cas_n_lo)
    ,.ddr_we_n(ddr_we_n_lo)
    ,.ddr_reset_n(ddr_reset_n_lo)   // TODO
    ,.ddr_odt(ddr_odt_lo)           // TODO

    ,.dm_oe_n(dm_oen_lo)
    ,.dm_o(dm_o_lo)
    ,.dqs_p_oe_n(dqs_p_oen_lo)
    ,.dqs_p_ie_n(dqs_p_ien_lo)
    ,.dqs_p_o(dqs_p_o_lo)
    ,.dqs_p_i(dqs_p_i_li)
    ,.dqs_n_oe_n(dqs_n_oen_lo)
    ,.dqs_n_ie_n(dqs_n_ien_lo)
    ,.dqs_n_o(dqs_n_o_lo)
    ,.dqs_n_i(dqs_n_i_li)
    ,.dq_oe_n(dq_oen_lo)
    ,.dq_o(dq_o_lo)
    ,.dq_i(dq_i_li)

    ,.dfi_clk_2x(dfi_clk_2x_final)
    ,.dfi_clk(dfi_clk_1x_final)
  );

  assign clk_1_p_o_int = ddr_ck_p_lo;
  assign clk_1_n_o_int = ddr_ck_n_lo;

  assign sdo_sclk_o_int[3] = ddr_cke_lo;
  assign sdo_sclk_o_int[2] = ddr_we_n_lo;

  assign sdi_token_o_int[3] = ddr_cs_n_lo;

  assign sdo_ncmd_o_int[2] = ddr_ras_n_lo;
  assign sdo_ncmd_o_int[3] = ddr_cas_n_lo;


  assign sdo_C_data_o_int = ddr_addr_lo[7:0];
  assign sdo_D_data_o_int = {ddr_ba_lo[1:0], ddr_addr_lo[13:8]};

  assign sdi_B_data_o_io_int = dq_o_lo[7:0];
  assign sdi_B_data_oen_io_int = dq_oen_lo[7:0];
  assign dq_i_li[7:0] = sdi_B_data_i_io_int;

  assign sdi_D_data_o_io_int = dq_o_lo[15:8];
  assign sdi_D_data_oen_io_int = dq_oen_lo[15:8];
  assign dq_i_li[15:8] = sdi_D_data_i_io_int;

  assign sdi_sclk_o_io_int = dqs_p_o_lo;
  assign sdi_sclk_oen_io_int = dqs_p_oen_lo;
  assign dqs_p_i_li = dqs_dly;

  assign sdi_ncmd_o_int = dm_o_lo;
  assign sdi_ncmd_oen_int = dm_oen_lo;

  assign dqs_filter[0] = dqs_p_ien_lo[0]? 1'b0: sdi_sclk_i_io_int[0];
  assign dqs_filter[1] = dqs_p_ien_lo[1]? 1'b0: sdi_sclk_i_io_int[1];

`include "bsg_pinout_end.v"
