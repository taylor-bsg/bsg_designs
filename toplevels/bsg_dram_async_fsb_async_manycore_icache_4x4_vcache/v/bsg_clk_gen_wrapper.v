/**
 *  bsg_clk_gen_wrapper.v
 */

module bsg_clk_gen_wrapper
  import bsg_tag_pkg::bsg_tag_s;
  #(parameter num_clk_endpoint_p=5
    ,parameter num_dram_clk_endpoint_p=1
    ,parameter lines_per_endpoint_p=2
  )
  (
    input tag_clk_i
    ,input tag_data_i
    ,input tag_en_i
    ,input [num_dram_clk_endpoint_p+num_clk_endpoint_p-1:0] async_reset_i
    ,input [num_clk_endpoint_p-1:0] clk_i
    ,input [num_clk_endpoint_p-1:0][1:0] sel_i
    ,output logic [num_clk_endpoint_p-1:0] clk_o
    ,output logic obs_clk_o
    ,output logic [num_dram_clk_endpoint_p-1:0] dram_clk_2x_o
    ,output logic [num_dram_clk_endpoint_p-1:0] dram_clk_1x_o
    ,input  [lines_per_endpoint_p*num_dram_clk_endpoint_p-1:0] dly_clk_i
    ,output logic [lines_per_endpoint_p*num_dram_clk_endpoint_p-1:0] dly_clk_o
  );

  `include "bsg_tag.vh"

  // for bsg_clk_gen v2, we need 3 bsg_tag endpoints
  // we also need one bsg_tag enpoints for obs clock sel.
  //
  localparam tags_per_endpoint_lp = 3;
  localparam tag_els_lp = (num_clk_endpoint_p+num_dram_clk_endpoint_p)*tags_per_endpoint_lp + 1;
  localparam ds_width_lp = 8;
  localparam num_adgs_lp = 1;

  // declaring payload struct
  //
  `declare_bsg_clk_gen_osc_tag_payload_s(num_adgs_lp)
  `declare_bsg_clk_gen_ds_tag_payload_s(ds_width_lp)

  // max payload length
  //
  localparam tag_max_payload_length_lp = `BSG_MAX($bits(bsg_clk_gen_osc_tag_payload_s),$bits(bsg_clk_gen_ds_tag_payload_s));
  localparam lg_tag_max_payload_length_lp = $clog2(tag_max_payload_length_lp+1);

  // observer clock
  //
  localparam obs_clk_width_lp = num_clk_endpoint_p + ((2+lines_per_endpoint_p)*num_dram_clk_endpoint_p);
  localparam obs_sel_width_lp = $clog2(obs_clk_width_lp);

  // tag master
  //
  bsg_tag_s [tag_els_lp-1:0] tags;

  bsg_tag_master #(
    .els_p(tag_els_lp)
    ,.lg_width_p(lg_tag_max_payload_length_lp)
  ) btm (
    .clk_i(tag_clk_i)
    ,.data_i(tag_data_i)
    ,.en_i(tag_en_i)
    ,.clients_r_o(tags)
  );

  // core clock generator
  //
  for (genvar i = 0; i < num_clk_endpoint_p; i++) begin
    bsg_clk_gen #(
      .downsample_width_p(ds_width_lp)
      ,.num_adgs_p(num_adgs_lp)
      ,.version_p(2)
    ) clk_gen_inst (
      .bsg_osc_tag_i(tags[tags_per_endpoint_lp*i+0])
      ,.bsg_osc_trigger_tag_i(tags[tags_per_endpoint_lp*i+2])
      ,.bsg_ds_tag_i(tags[tags_per_endpoint_lp*i+1])
      ,.async_osc_reset_i(async_reset_i[i])
      ,.ext_clk_i(clk_i[i])
      ,.select_i(sel_i[i])
      ,.clk_o(clk_o[i])
    );
  end
  
  // dram clock generator
  //
  for (genvar i = 0; i < num_dram_clk_endpoint_p; i++) begin
    bsg_dram_clk_gen #(
      .num_lines_p(lines_per_endpoint_p)
      ,.downsample_width_p(ds_width_lp)
      ,.num_adgs_p(num_adgs_lp)
      ,.version_p(2)
    ) dram_clk_gen_inst (
      .bsg_osc_tag_i(tags[tags_per_endpoint_lp*(num_clk_endpoint_p+i)+0])
      ,.bsg_osc_trigger_tag_i(tags[tags_per_endpoint_lp*(num_clk_endpoint_p+i)+2])
      ,.bsg_ds_tag_i(tags[tags_per_endpoint_lp*(num_clk_endpoint_p+i)+1])
      ,.async_osc_reset_i(async_reset_i[num_clk_endpoint_p+i])
      ,.osc_clk_o(dram_clk_2x_o[i])
      ,.div_clk_o(dram_clk_1x_o[i])
      ,.dly_clk_i(dly_clk_i[(lines_per_endpoint_p*i)+:lines_per_endpoint_p])
      ,.dly_clk_o(dly_clk_o[(lines_per_endpoint_p*i)+:lines_per_endpoint_p])
    );
  end

  // observer clock mux
  //
  logic [obs_clk_width_lp-1:0] obs_clk;
  logic [obs_sel_width_lp-1:0] obs_sel;

  for (genvar i = 0; i < num_clk_endpoint_p; i++) begin
    assign obs_clk[i] = clk_o[i];
  end

  for (genvar i = 0; i < num_dram_clk_endpoint_p; i++) begin
    assign obs_clk[num_clk_endpoint_p+(4*i)] = dram_clk_2x_o[i];
    assign obs_clk[num_clk_endpoint_p+(4*i)+1] = dram_clk_1x_o[i];
    assign obs_clk[num_clk_endpoint_p+(4*i)+2] = dly_clk_o[2*i];
    assign obs_clk[num_clk_endpoint_p+(4*i)+3] = dly_clk_o[(2*i)+1];
  end

  bsg_tag_client_unsync #(
    .width_p(obs_sel_width_lp)
  ) btc_unsync_obs_mux (
    .bsg_tag_i(tags[tag_els_lp-1])
    ,.data_async_r_o(obs_sel)
  );
  
  bsg_mux #(
    .width_p(1)
    ,.els_p(obs_clk_width_lp)
    ,.balanced_p(1)
    ,.harden_p(1)
  ) mux_inst (
    .data_i(obs_clk)
    ,.sel_i(obs_sel)
    ,.data_o(obs_clk_o)
  );
endmodule
