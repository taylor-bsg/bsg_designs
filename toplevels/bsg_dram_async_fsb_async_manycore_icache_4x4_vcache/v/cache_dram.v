`include "bsg_cache_pkt.vh"
`include "bsg_cache_dma_pkt.vh"

module cache_dram
  import bsg_chip_pkg::*;
  import bsg_dram_ctrl_pkg::*;
  #(parameter addr_width_p="inv"
    ,parameter data_width_p="inv"
    ,parameter x_cord_width_p="inv"
    ,parameter y_cord_width_p="inv"
    ,parameter num_links_p="inv"
    ,parameter sets_p="inv"
    ,parameter block_size_in_words_p="inv"
    ,parameter link_sif_width_lp=`bsg_manycore_link_sif_width(addr_width_p,data_width_p,x_cord_width_p,y_cord_width_p)
  )
  (
    input clk_i
    ,input reset_i
    ,input dmc_rst_i

    ,input [num_links_p-1:0][x_cord_width_p-1:0] my_x_i
    ,input [num_links_p-1:0][y_cord_width_p-1:0] my_y_i

    ,input [num_links_p-1:0][link_sif_width_lp-1:0] links_sif_i
    ,output logic [num_links_p-1:0][link_sif_width_lp-1:0] links_sif_o

    ,input dfi_clk
    ,input dfi_clk_2x

    ,output logic [(dram_dfi_width_gp>>4)-1:0] dm_oe_n
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dm_o
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_p_oe_n
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_p_ie_n
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_p_o
    ,input [(dram_dfi_width_gp>>4)-1:0] dqs_p_i
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_n_oe_n
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_n_ie_n
    ,output logic [(dram_dfi_width_gp>>4)-1:0] dqs_n_o
    ,input [(dram_dfi_width_gp>>4)-1:0] dqs_n_i

    ,output logic[(dram_dfi_width_gp>>1)-1:0] dq_oe_n
    ,output logic[(dram_dfi_width_gp>>1)-1:0] dq_o
    ,input [(dram_dfi_width_gp>>1)-1:0] dq_i

    ,output logic ddr_ck_p
    ,output logic ddr_ck_n
    ,output logic ddr_cke
    ,output logic [2:0] ddr_ba      //this is the maximum width.
    ,output logic [15:0] ddr_addr    //this is the maximum width.
    ,output logic ddr_cs_n
    ,output logic ddr_ras_n
    ,output logic ddr_cas_n
    ,output logic ddr_we_n
    ,output logic ddr_reset_n
    ,output logic ddr_odt
  );

  genvar i;

  // manycore links to cache
  //
  `declare_bsg_cache_pkt_s(addr_width_p+`BSG_SAFE_CLOG2(data_width_p>>3), data_width_p);
  bsg_cache_pkt_s [num_links_p-1:0] cache_pkt_lo;
  logic [num_links_p-1:0] link_to_cache_v_lo;
  logic [num_links_p-1:0] link_to_cache_ready_li;
  logic [num_links_p-1:0][data_width_p-1:0] link_to_cache_data_li;
  logic [num_links_p-1:0] link_to_cache_v_li;
  logic [num_links_p-1:0] link_to_cache_yumi_lo;
  logic [num_links_p-1:0] link_to_cache_v_we_li;

  for (i = 0; i < num_links_p; i++) begin
    bsg_manycore_link_to_cache #(
      .addr_width_p(addr_width_p)
      ,.data_width_p(data_width_p)
      ,.x_cord_width_p(x_cord_width_p)
      ,.y_cord_width_p(y_cord_width_p)
      ,.dram_addr_width_p(addr_width_p-1)
    ) link_to_cache (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
     
      ,.my_x_i(my_x_i[i])
      ,.my_y_i(my_y_i[i])
     
      ,.link_sif_i(links_sif_i[i])
      ,.link_sif_o(links_sif_o[i])

      ,.cache_pkt_o(cache_pkt_lo[i])
      ,.v_o(link_to_cache_v_lo[i])
      ,.ready_i(link_to_cache_ready_li[i])

      ,.data_i(link_to_cache_data_li[i])
      ,.v_i(link_to_cache_v_li[i])
      ,.yumi_o(link_to_cache_yumi_lo[i])

      ,.v_we_i(link_to_cache_v_we_li[i]) 
    );
  end

  // cache
  // 
  `declare_bsg_cache_dma_pkt_s(addr_width_p+`BSG_SAFE_CLOG2(data_width_p>>3));
  bsg_cache_dma_pkt_s [num_links_p-1:0] dma_pkt_lo;
  logic [num_links_p-1:0] dma_pkt_v_lo;
  logic [num_links_p-1:0] dma_pkt_yumi_li;

  logic [num_links_p-1:0][data_width_p-1:0] dma_data_li;
  logic [num_links_p-1:0] dma_data_v_li;
  logic [num_links_p-1:0] dma_data_ready_lo;

  logic [num_links_p-1:0][data_width_p-1:0] dma_data_lo;
  logic [num_links_p-1:0] dma_data_v_lo;
  logic [num_links_p-1:0] dma_data_yumi_li;

  for (i = 0; i < num_links_p; i++) begin: cache
    bsg_cache #(
      .data_width_p(data_width_p)
      ,.addr_width_p(addr_width_p+`BSG_SAFE_CLOG2(data_width_p>>3))
      ,.block_size_in_words_p(block_size_in_words_p)
      ,.sets_p(sets_p)
    ) cache (
      .clk_i(clk_i)
      ,.reset_i(reset_i)
      
      ,.cache_pkt_i(cache_pkt_lo[i])
      ,.v_i(link_to_cache_v_lo[i])
      ,.ready_o(link_to_cache_ready_li[i])
    
      ,.data_o(link_to_cache_data_li[i])
      ,.v_o(link_to_cache_v_li[i])
      ,.yumi_i(link_to_cache_yumi_lo[i])

      ,.v_we_o(link_to_cache_v_we_li[i])

      ,.dma_pkt_o(dma_pkt_lo[i])
      ,.dma_pkt_v_o(dma_pkt_v_lo[i])
      ,.dma_pkt_yumi_i(dma_pkt_yumi_li[i])

      ,.dma_data_i(dma_data_li[i])
      ,.dma_data_v_i(dma_data_v_li[i])
      ,.dma_data_ready_o(dma_data_ready_lo[i])

      ,.dma_data_o(dma_data_lo[i])
      ,.dma_data_v_o(dma_data_v_lo[i])
      ,.dma_data_yumi_i(dma_data_yumi_li[i])
    );
  end

  // cache to dram_ctrl
  //
  logic app_en;
  logic app_rdy;
  logic app_hi_pri;
  eAppCmd app_cmd;
  logic [dram_ctrl_awidth_gp-1:0] app_addr;
  logic app_wdf_wren;
  logic app_wdf_rdy;
  logic [dram_ctrl_dwidth_gp-1:0] app_wdf_data;
  logic [(dram_ctrl_dwidth_gp>>3)-1:0] app_wdf_mask;
  logic app_wdf_end;
  logic app_rd_data_valid;
  logic [dram_ctrl_dwidth_gp-1:0] app_rd_data;
  logic app_rd_data_end;
  logic app_ref_req;
  logic app_ref_ack;
  logic app_zq_req;
  logic app_zq_ack;
  logic init_calib_complete;
  logic app_sr_req;
  logic app_sr_ack;

  bsg_cache_to_dram_ctrl #(
    .data_width_p(data_width_p)
    ,.addr_width_p(addr_width_p+`BSG_SAFE_CLOG2(data_width_p>>3))
    ,.block_size_in_words_p(block_size_in_words_p)
    ,.burst_len_p(1)
    ,.burst_width_p(dram_ctrl_dwidth_gp)
    ,.num_cache_p(num_links_p)
    ,.dram_addr_width_p(dram_ctrl_awidth_gp)
  ) cache_to_dram_ctrl (
    .clk_i(clk_i)
    ,.reset_i(reset_i)

    ,.dma_pkt_i(dma_pkt_lo)
    ,.dma_pkt_v_i(dma_pkt_v_lo)
    ,.dma_pkt_yumi_o(dma_pkt_yumi_li)

    ,.dma_data_o(dma_data_li)
    ,.dma_data_v_o(dma_data_v_li)
    ,.dma_data_ready_i(dma_data_ready_lo)

    ,.dma_data_i(dma_data_lo)
    ,.dma_data_v_i(dma_data_v_lo)
    ,.dma_data_yumi_o(dma_data_yumi_li)

    ,.app_en_o(app_en)
    ,.app_rdy_i(app_rdy)
    ,.app_hi_pri_o(app_hi_pri)
    ,.app_cmd_o(app_cmd)
    ,.app_addr_o(app_addr)

    ,.app_wdf_wren_o(app_wdf_wren)
    ,.app_wdf_rdy_i(app_wdf_rdy)
    ,.app_wdf_data_o(app_wdf_data)
    ,.app_wdf_mask_o(app_wdf_mask)
    ,.app_wdf_end_o(app_wdf_end)

    ,.app_rd_data_valid_i(app_rd_data_valid)
    ,.app_rd_data_i(app_rd_data)
    ,.app_rd_data_end_i(app_rd_data_end)

    ,.app_ref_req_o(app_ref_req)
    ,.app_ref_ack_i(app_ref_ack)

    ,.app_zq_req_o(app_zq_req)
    ,.app_zq_ack_i(app_zq_ack)
    ,.init_calib_complete_i(init_calib_complete)

    ,.app_sr_req_o(app_sr_req)
    ,.app_sr_ack_i(app_sr_ack)
  );

  // dram_ctrl
  //  
  dmc #(
    .UI_ADDR_WIDTH(dram_ctrl_awidth_gp)
    ,.UI_DATA_WIDTH(dram_ctrl_dwidth_gp)
    ,.DFI_DATA_WIDTH(dram_dfi_width_gp)
  ) lpddr1_ctrl (
    .sys_rst(~dmc_rst_i)      // active low
    ,.app_addr(app_addr>>1) // half address
    ,.app_cmd(app_cmd)
    ,.app_en(app_en)
    ,.app_rdy(app_rdy)
    ,.app_wdf_wren(app_wdf_wren)
    ,.app_wdf_data(app_wdf_data)
    ,.app_wdf_mask(app_wdf_mask)
    ,.app_wdf_end(app_wdf_end)
    ,.app_wdf_rdy(app_wdf_rdy)
    ,.app_rd_data_valid(app_rd_data_valid)
    ,.app_rd_data(app_rd_data)
    ,.app_rd_data_end(app_rd_data_end)
    ,.app_ref_req(app_ref_req)
    ,.app_ref_ack(app_ref_ack)
    ,.app_zq_req(app_zq_req)
    ,.app_zq_ack(app_zq_ack)
    ,.app_sr_req(app_sr_req)
    ,.app_sr_active(app_sr_ack)
    ,.init_calib_complete(init_calib_complete)

    ,.ddr_ck_p(ddr_ck_p)
    ,.ddr_ck_n(ddr_ck_n)
    ,.ddr_cke(ddr_cke)
    ,.ddr_ba(ddr_ba)
    ,.ddr_addr(ddr_addr)
    ,.ddr_cs_n(ddr_cs_n)
    ,.ddr_ras_n(ddr_ras_n)
    ,.ddr_cas_n(ddr_cas_n)
    ,.ddr_we_n(ddr_we_n)
    ,.ddr_reset_n(ddr_reset_n)
    ,.ddr_odt(ddr_odt)

    ,.dm_oe_n(dm_oe_n)
    ,.dm_o(dm_o)
    ,.dqs_p_oe_n(dqs_p_oe_n)
    ,.dqs_p_ie_n(dqs_p_ie_n)
    ,.dqs_p_o(dqs_p_o)
    ,.dqs_p_i(dqs_p_i)
    ,.dqs_n_oe_n(dqs_n_oe_n)
    ,.dqs_n_ie_n(dqs_n_ie_n)
    ,.dqs_n_o(dqs_n_o)
    ,.dqs_n_i(dqs_n_i)
    ,.dq_oe_n(dq_oe_n)
    ,.dq_o(dq_o)
    ,.dq_i(dq_i)

    ,.ui_clk(clk_i)
    ,.ui_clk_sync_rst()
    ,.dfi_clk_2x(dfi_clk_2x)
    ,.dfi_clk(dfi_clk)
    ,.device_temp()
  );

endmodule

