module bsg_gateway_chip
# (parameter num_channels_p=4
  ,parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p
  ,parameter master_p=1
  ,parameter master_to_client_speedup_p=100
  ,parameter master_bypass_test_p=5'b11111)
  (input                     core_clk_i
  ,input                     io_master_clk_i
  ,input                     async_reset_i
  // in
  ,input                     v_i
  ,input  [ring_width_p-1:0] data_i
  ,output                    ready_o
  // out
  ,output                    v_o
  ,output [ring_width_p-1:0] data_o
  ,input                     yumi_i
  // comm_link ctrl
  ,output                       p_chip_reset_o
  ,output                       p_host_reset_o
  // comm_link in
  ,input   [num_channels_p-1:0] p_sdo_sclk_i
  ,input   [num_channels_p-1:0] p_sdo_ncmd_i
  ,input  [channel_width_p-1:0] p_sdo_A_data_i
  ,input  [channel_width_p-1:0] p_sdo_B_data_i
  ,input  [channel_width_p-1:0] p_sdo_C_data_i
  ,input  [channel_width_p-1:0] p_sdo_D_data_i
  ,output  [num_channels_p-1:0] p_sdo_token_o
  // comm_link out
  ,output  [num_channels_p-1:0] p_sdi_sclk_o
  ,output  [num_channels_p-1:0] p_sdi_ncmd_o
  ,output [channel_width_p-1:0] p_sdi_A_data_o
  ,output [channel_width_p-1:0] p_sdi_B_data_o
  ,output [channel_width_p-1:0] p_sdi_C_data_o
  ,output [channel_width_p-1:0] p_sdi_D_data_o
  ,input   [num_channels_p-1:0] p_sdi_token_i);

  // comm_link

  wire [channel_width_p-1:0] sdo_data_i_int_packed [num_channels_p-1:0];
  wire [channel_width_p-1:0] sdi_data_o_int_packed [num_channels_p-1:0];

  bsg_make_2D_array #
    (.width_p(channel_width_p)
    ,.items_p(num_channels_p))
  m2da
    (.i({p_sdo_D_data_i, p_sdo_C_data_i, p_sdo_B_data_i, p_sdo_A_data_i})
    ,.o(sdo_data_i_int_packed));

  // we swap B input and C input on both ASIC and Gateway to make physical design easier
  bsg_flatten_2D_array #
    (.width_p(channel_width_p),
    .items_p(num_channels_p))
  f2da
    (.i(sdi_data_o_int_packed)
    ,.o({p_sdi_D_data_o, p_sdi_B_data_o, p_sdi_C_data_o, p_sdi_A_data_o}));

  wire core_calib_done_r_lo;

  `define BSG_SWIZZLE_3120(a) { a[3],a[1],a[2],a[0] }

  bsg_comm_link #
    (.channel_width_p(channel_width_p)
    ,.core_channels_p(ring_bytes_p)
    ,.link_channels_p(num_channels_p)
    ,.master_p(master_p)
    ,.master_to_slave_speedup_p(master_to_client_speedup_p)
    ,.master_bypass_test_p(master_bypass_test_p))
  comm_link
    (.core_clk_i(core_clk_i)
    ,.io_master_clk_i(io_master_clk_i)

    // reset
    ,.async_reset_i(async_reset_i)

    // core ctrl
    ,.core_calib_done_r_o(core_calib_done_r_lo)

    // core in
    ,.core_valid_i(v_i)
    ,.core_data_i(data_i)
    ,.core_ready_o(ready_o)

    // core out
    ,.core_valid_o(v_o)
    ,.core_data_o(data_o)
    ,.core_yumi_i(yumi_i)

    // in from i/o
    ,.io_clk_tline_i(p_sdo_sclk_i)        // clk
    ,.io_valid_tline_i(p_sdo_ncmd_i)
    ,.io_data_tline_i(sdo_data_i_int_packed)
    ,.io_token_clk_tline_o(p_sdo_token_o) // clk

    // out to i/o
    ,.im_clk_tline_o(`BSG_SWIZZLE_3120(p_sdi_sclk_o))     // clk
    ,.im_valid_tline_o(`BSG_SWIZZLE_3120(p_sdi_ncmd_o))
    ,.im_data_tline_o(sdi_data_o_int_packed)
    ,.token_clk_tline_i(`BSG_SWIZZLE_3120(p_sdi_token_i)) // clk

    ,.im_slave_reset_tline_r_o(p_chip_reset_o));

  assign p_host_reset_o = ~core_calib_done_r_lo;

endmodule
