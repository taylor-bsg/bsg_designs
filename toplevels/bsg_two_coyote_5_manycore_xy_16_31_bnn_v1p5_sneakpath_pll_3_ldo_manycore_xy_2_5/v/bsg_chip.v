`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

module bsg_chip

  // pull in BSG Two's top-level module signature, and the definition of the pads
  `include "bsg_pinout.v"
  `include "bsg_iopads.v"

  //  _____  _      _          
  // |  __ \| |    | |         
  // | |__) | |    | |     ___ 
  // |  ___/| |    | |    / __|
  // | |    | |____| |____\__ \
  // |_|    |______|______|___/
                           
  logic core_clk,     io_master_clk,     manycore_clk;
  logic core_tst_clk, io_master_tst_clk, manycore_tst_clk;
  logic core_sdo,     io_master_sdo,     manycore_sdo;
  
  pll core_clk_pll
    (.in_clk_ref(misc_L_4_i_int)
    ,.in_chip_select(sdi_sclk_ex_i_int[1])
    ,.in_scn_clk(JTAG_TCK_i_int)
    ,.in_sdi(misc_L_6_i_int)
    ,.in_rstb(misc_L_7_i_int)
    ,.out_sdo(core_sdo)
    ,.out_clk_tst(core_tst_clk)
    ,.out_clk(core_clk));
  
  pll io_master_clk_pll
    (.in_clk_ref(PLL_CLK_i_int)
    ,.in_chip_select(misc_R_5_i_int)
    ,.in_scn_clk(JTAG_TCK_i_int)
    ,.in_sdi(misc_R_6_i_int)
    ,.in_rstb(misc_R_7_i_int)
    ,.out_sdo(io_master_sdo)
    ,.out_clk_tst(io_master_tst_clk)
    ,.out_clk(io_master_clk));

  pll manycore_clk_pll
    (.in_clk_ref(misc_L_5_i_int)
    ,.in_chip_select(misc_L_2_i_int)
    ,.in_scn_clk(JTAG_TCK_i_int)
    ,.in_sdi(misc_L_1_i_int)
    ,.in_rstb(misc_L_0_i_int)
    ,.out_sdo(manycore_sdo)
    ,.out_clk_tst(manycore_tst_clk)
    ,.out_clk(manycore_clk));

  assign sdi_tkn_ex_o_int[1]  = core_sdo;
  assign sdo_sclk_ex_o_int[2] = core_tst_clk;

  assign sdi_tkn_ex_o_int[3]  = io_master_sdo;
  assign sdo_sclk_ex_o_int[3] = io_master_tst_clk;

  assign sdi_tkn_ex_o_int[2]  = manycore_sdo;
  assign sdo_sclk_ex_o_int[1] = manycore_tst_clk;

  //  _____   _____     _______   _____   _      _____   ____  
  // |  __ \ / ____|   / /  __ \ / ____| | |    |  __ \ / __ \ 
  // | |  | | |       / /| |  | | |      | |    | |  | | |  | |
  // | |  | | |      / / | |  | | |      | |    | |  | | |  | |
  // | |__| | |____ / /  | |__| | |____  | |____| |__| | |__| |
  // |_____/ \_____/_/   |_____/ \_____| |______|_____/ \____/ 

  logic dc_dc_ldo_clk_ov4;
  logic dc_dc_ldo_enhh;
  logic dc_dc_ldo_enll;
  logic dc_dc_ldo_enh;
  logic dc_dc_ldo_enl;

  LDO_16nm_V0_TOP dc_dc_ldo
    (.CLKOV64(dc_dc_ldo_clk_ov4)
    ,.CLK_LOAD(misc_R_1_i_int)
    ,.CLK_REF(misc_R_2_i_int)
    ,.ENH(dc_dc_ldo_enh)
    ,.ENHH(dc_dc_ldo_enhh)
    ,.ENL(dc_dc_ldo_enl)
    ,.ENLL(dc_dc_ldo_enll)
    ,.EXT_SAM(misc_R_4_i_int)
    ,.PLL(SMA_in_p_i_int)
    ,.RSTIN(sdi_sclk_ex_i_int[0])
    ,.SPI_CLK(SMA_in_n_i_int)
    ,.SPI_IN(JTAG_TDI_i_int)
    ,.SPI_RST(clk_0_p_i_int)

    //----- analog pins -----//
    ,.VREFH(misc_T_1_i_int)
    ,.VREFL(misc_T_2_i_int)
    ,.VB_RING(JTAG_TMS_i_int)
    ,.VCAL_HH(misc_R_0_i_int)
    ,.VCAL_LL(misc_T_0_i_int)
    ,.VL(JTAG_TRST_i_int));

  assign sdo_sclk_ex_o_int[0] = dc_dc_ldo_clk_ov4;
  assign sdi_tkn_ex_o_int[0]  = dc_dc_ldo_enhh;
  assign SMA_out_p_o_int      = dc_dc_ldo_enll;
  assign SMA_out_n_o_int      = dc_dc_ldo_enh;
  assign JTAG_TDO_o_int       = dc_dc_ldo_enl;

  wire [7:0] sdi_data_i_int_packed [3:0];
  wire [7:0] sdo_data_o_int_packed [3:0];

  assign sdi_data_i_int_packed = { sdi_D_data_i_int, sdi_B_data_i_int, sdi_C_data_i_int, sdi_A_data_i_int };
  assign { sdo_D_data_o_int, sdo_C_data_o_int, sdo_B_data_o_int, sdo_A_data_o_int } = { >> {sdo_data_o_int_packed} };

  `define BSG_SWIZZLE_3120(a) { a[3],a[1],a[2],a[0] }

  //  ____   _____  _____    _____ _     _          _____       _       
  // |  _ \ / ____|/ ____|  / ____| |   (_)        / ____|     | |      
  // | |_) | (___ | |  __  | |    | |__  _ _ __   | |  __ _   _| |_ ___ 
  // |  _ < \___ \| | |_ | | |    | '_ \| | '_ \  | | |_ | | | | __/ __|
  // | |_) |____) | |__| | | |____| | | | | |_) | | |__| | |_| | |_\__ \
  // |____/|_____/ \_____|  \_____|_| |_|_| .__/   \_____|\__,_|\__|___/
  //                                      | |                           
  //                                      |_|                           

  bsg_chip_guts g
    (.core_clk_i(core_clk)
    ,.io_master_clk_i(io_master_clk)
    ,.manycore_clk_i(manycore_clk)
    ,.async_reset_i(reset_i_int)

    ,.io_clk_tline_i(`BSG_SWIZZLE_3120(sdi_sclk_i_int))
    ,.io_valid_tline_i(`BSG_SWIZZLE_3120(sdi_ncmd_i_int))
    ,.io_data_tline_i(sdi_data_i_int_packed)
    ,.io_token_clk_tline_o(`BSG_SWIZZLE_3120(sdi_token_o_int))

    ,.im_clk_tline_o(sdo_sclk_o_int)
    ,.im_valid_tline_o(sdo_ncmd_o_int)
    ,.im_data_tline_o(sdo_data_o_int_packed)
    ,.token_clk_tline_i(sdo_token_i_int)

    ,.im_slave_reset_tline_r_o()  // unused by ASIC

    ,.core_reset_o());            // post calibration reset

  `include "bsg_pinout_end.v"
