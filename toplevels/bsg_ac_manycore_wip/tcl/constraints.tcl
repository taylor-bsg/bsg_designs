source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_cdc.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_comm_link.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen.constraints.tcl

########################################
##
## Clock Setup
##

set router_clk_name      "router_clk"       ;# core clock (everything that isn't a part of another clock is on this domain)
set io_master_clk_name "io_master_clk"  ;# 2x clock for DDR IO paths (1x clock generated from this clock)
set manycore_clk_name  "manycore_clk"   ;# main clock running the manycore complex
set tag_clk_name       "tag_clk"

set router_clk_period_ps       2500.0 ;# 400 MHz
set router_clk_uncertainty_per 3.0
set router_clk_uncertainty_ps  [expr ${router_clk_period_ps}*(${router_clk_uncertainty_per}/100.0)]

set io_master_clk_period_ps       5000.0 ;# 200MHz
set io_master_clk_uncertainty_per 3.0
set io_master_clk_uncertainty_ps  [expr ${io_master_clk_period_ps}*(${io_master_clk_uncertainty_per}/100.0)]

set manycore_clk_period_ps       1000.0 ;# 1GHz
set manycore_clk_uncertainty_per 3.0
set manycore_clk_uncertainty_ps  [expr ${manycore_clk_period_ps}*(${manycore_clk_uncertainty_per}/100.0)]

set oscillator_period_ps       250.0 ;# Raw oscillator frequency
set oscillator_uncertainty_per 3.0
set oscillator_uncertainty_ps  [expr ${oscillator_period_ps}*(${oscillator_uncertainty_per}/100.0)]

set tag_clk_period_ps       10000.0 ;# 100 MHz
set tag_clk_uncertainty_per 3.0
set tag_clk_uncertainty_ps  [expr ${tag_clk_period_ps}*(${tag_clk_uncertainty_per}/100.0)]

########################################
##
## BSG Manycore Tile Constraints
##

if { ${DESIGN_NAME} == "bsg_manycore_tile" } {

  set clk_name           ${manycore_clk_name}
  set clk_period_ps      ${manycore_clk_period_ps}
  set clk_uncertainty_ps ${manycore_clk_uncertainty_ps}

  set input_delay_per  50.0
  set output_delay_per 50.0

  set input_delay_ps  [expr ${clk_period_ps}*(${input_delay_per}/100.0)]
  set output_delay_ps [expr ${clk_period_ps}*(${output_delay_per}/100.0)]

  set driving_lib_cell "SC7P5T_INVX2_SSC14SL"
  set load_lib_pin     "SC7P5T_INVX8_SSC14SL/A"

  # Reg2Reg
  create_clock -period ${clk_period_ps} -name ${clk_name} [get_ports clk_i]
  set_clock_uncertainty ${clk_uncertainty_ps} [get_clocks ${clk_name}]

  # In2Reg
  set all_input_pins [remove_from_collection [all_inputs] [get_ports clk_i]]
  set_driving_cell -no_design_rule -lib_cell ${driving_lib_cell} ${all_input_pins}
  set_input_delay ${input_delay_ps} -clock ${clk_name} ${all_input_pins}

  # Reg2Out
  set all_output_pins [all_outputs]
  set_load [load_of [get_lib_pin */${load_lib_pin}]] ${all_output_pins}
  set_output_delay ${output_delay_ps} -clock ${clk_name} ${all_output_pins}

  # False Paths
  set_false_path -from [get_ports my_*]

  # Flatten the whole design! (Will make reading timing paths and placement bounds impossible)
  #set_ungroup [get_designs *]

  set cells_to_derate [list]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~gf14_*"]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~IN12LP_*"]
  if { [sizeof $cells_to_derate] > 0 } {
    foreach_in_collection cell $cells_to_derate {
      set_timing_derate -cell_delay -early 0.97 $cell
      set_timing_derate -cell_delay -late  1.03 $cell
      set_timing_derate -cell_check -early 0.97 $cell
      set_timing_derate -cell_check -late  1.03 $cell
    }
  }
  #report_timing_derate


########################################
##
## VCache Constraints
##

} elseif { ${DESIGN_NAME} == "vcache" } {

  set clk_name           ${manycore_clk_name}
  set clk_period_ps      ${manycore_clk_period_ps}
  set clk_uncertainty_ps ${manycore_clk_uncertainty_ps}

  set input_delay_per  50.0
  set output_delay_per 50.0

  set input_delay_ps  [expr ${clk_period_ps}*(${input_delay_per}/100.0)]
  set output_delay_ps [expr ${clk_period_ps}*(${output_delay_per}/100.0)]

  set driving_lib_cell "SC7P5T_INVX2_SSC14SL"
  set load_lib_pin     "SC7P5T_INVX8_SSC14SL/A"

  # Reg2Reg
  create_clock -period ${clk_period_ps} -name ${clk_name} [get_ports clk_i]
  set_clock_uncertainty ${clk_uncertainty_ps} [get_clocks ${clk_name}]

  # In2Reg
  set all_input_pins [remove_from_collection [all_inputs] [get_ports clk_i]]
  set_driving_cell -no_design_rule -lib_cell ${driving_lib_cell} ${all_input_pins}
  set_input_delay ${input_delay_ps} -clock ${clk_name} ${all_input_pins}

  # Reg2Out
  set all_output_pins [all_outputs]
  set_load [load_of [get_lib_pin */${load_lib_pin}]] ${all_output_pins}
  set_output_delay ${output_delay_ps} -clock ${clk_name} ${all_output_pins}

  # False Paths
  set_false_path -from [get_ports my_*]

  # Flatten the whole design! (Will make reading timing paths and placement bounds impossible)
  #set_ungroup [get_designs *]

  set cells_to_derate [list]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~gf14_*"]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~IN12LP_*"]
  if { [sizeof $cells_to_derate] > 0 } {
    foreach_in_collection cell $cells_to_derate {
      set_timing_derate -cell_delay -early 0.97 $cell
      set_timing_derate -cell_delay -late  1.03 $cell
      set_timing_derate -cell_check -early 0.97 $cell
      set_timing_derate -cell_check -late  1.03 $cell
    }
  }
  #report_timing_derate

########################################
##
## Top-level Constraints
##

} elseif { ${DESIGN_NAME} == "bsg_chip" } {

  set_app_var timing_enable_multiple_clocks_per_reg true

  set_register_merging [get_cells -of [get_nets manycore_complex/vc_reset_r]]   false
  set_register_merging [get_cells -of [get_nets manycore_complex/mc/reset_i_r]] false

  bsg_tag_clock_create ${tag_clk_name} p_bsg_tag_clk_i p_bsg_tag_data_i p_bsg_tag_en_i ${tag_clk_period_ps} ${tag_clk_uncertainty_ps}

  bsg_clk_gen_clock_create clk_gen_pd/clk_gen[0].clk_gen_inst/ ${manycore_clk_name}  ${oscillator_period_ps} ${manycore_clk_period_ps}
  bsg_clk_gen_clock_create clk_gen_pd/clk_gen[1].clk_gen_inst/ ${io_master_clk_name} ${oscillator_period_ps} ${io_master_clk_period_ps}
  bsg_clk_gen_clock_create clk_gen_pd/clk_gen[2].clk_gen_inst/ ${router_clk_name}    ${oscillator_period_ps} ${router_clk_period_ps}

  set_clock_uncertainty ${manycore_clk_uncertainty_ps}  [get_clocks ${manycore_clk_name}]
  set_clock_uncertainty ${io_master_clk_uncertainty_ps} [get_clocks ${io_master_clk_name}]
  set_clock_uncertainty ${router_clk_uncertainty_ps}    [get_clocks ${router_clk_name}]
  
  create_clock -period ${oscillator_period_ps} -name manycore_clk_ext  [get_ports p_clk_A_i]
  create_clock -period ${oscillator_period_ps} -name io_master_clk_ext [get_ports p_clk_B_i]
  create_clock -period ${oscillator_period_ps} -name router_clk_ext    [get_ports p_clk_C_i]

  # Comm Link CH0
  #=================
  set ch0_in_clk_port  [get_ports p_ci_clk_i]
  set ch0_in_dv_port   [remove_from_collection [get_ports p_ci_*_i] $ch0_in_clk_port]
  set ch0_in_tkn_port  [get_ports p_ci_tkn_o]
  set ch0_out_clk_port [get_ports p_ci2_clk_o]
  set ch0_out_dv_port  [remove_from_collection [get_ports p_ci2_*_o] $ch0_out_clk_port]
  set ch0_out_tkn_port [get_ports p_ci2_tkn_i]
  
  bsg_comm_link_timing_constraints \
    ${io_master_clk_name}          \
    "a"                            \
    $ch0_in_clk_port               \
    $ch0_in_dv_port                \
    $ch0_in_tkn_port               \
    $ch0_out_clk_port              \
    $ch0_out_dv_port               \
    $ch0_out_tkn_port              \
    100                            \
    100

  # Comm Link CH1
  #=================
  set ch1_in_clk_port  [get_ports p_co_clk_i]
  set ch1_in_dv_port   [remove_from_collection [get_ports p_co_*_i] $ch1_in_clk_port]
  set ch1_in_tkn_port  [get_ports p_co_tkn_o]
  set ch1_out_clk_port [get_ports p_co2_clk_o]
  set ch1_out_dv_port  [remove_from_collection [get_ports p_co2_*_o] $ch1_out_clk_port]
  set ch1_out_tkn_port [get_ports p_co2_tkn_i]
  
  bsg_comm_link_timing_constraints \
    ${io_master_clk_name}          \
    "b"                            \
    $ch1_in_clk_port               \
    $ch1_in_dv_port                \
    $ch1_in_tkn_port               \
    $ch1_out_clk_port              \
    $ch1_out_dv_port               \
    $ch1_out_tkn_port              \
    100                            \
    100

  # CDC Paths
  #=================
  update_timing
  set clocks [all_clocks]
  foreach_in_collection launch_clk $clocks {
    if { [get_attribute $launch_clk is_generated] } {
      set launch_group [get_generated_clocks -filter "name==[get_attribute $launch_clk master_clock_name]"]
      append_to_collection launch_group [get_attribute $launch_clk master_clock]
    } else {
      set launch_group [get_generated_clocks -filter "name==[get_attribute $launch_clk name]"]
      append_to_collection launch_group $launch_clk
    }
    foreach_in_collection latch_clk [remove_from_collection $clocks $launch_group] {
      set_max_delay [get_attribute $launch_clk period] -from $launch_clk -to $latch_clk -ignore_clock_latency
      set_min_delay 0                                  -from $launch_clk -to $latch_clk -ignore_clock_latency
    }
  }

  # Ungrouping
  #=================
  set_ungroup [get_cells manycore_complex/mc/link]
  set_ungroup [get_cells manycore_complex/*.tieoff]
  set_ungroup [get_cells swizzle]

  set cells_to_derate [list]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~gf14_*"]
  append_to_collection cells_to_derate [get_cells -quiet -hier -filter "ref_name=~IN12LP_*"]
  if { [sizeof $cells_to_derate] > 0 } {
    foreach_in_collection cell $cells_to_derate {
      set_timing_derate -cell_delay -early 0.97 $cell
      set_timing_derate -cell_delay -late  1.03 $cell
      set_timing_derate -cell_check -early 0.97 $cell
      set_timing_derate -cell_check -late  1.03 $cell
    }
  }
  #report_timing_derate

########################################
##
## Unknown design...
##
} else {

  puts "BSG-error: No constraints found for design (${DESIGN_NAME})!"

}
