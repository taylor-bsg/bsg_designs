remove_individual_pin_constraints
remove_block_pin_constraints

set_app_options -name plan.pins.incremental -value false
set_app_options -name plan.pins.strict_alignment -value true
set_block_pin_constraints -allowed_layers {C4 C5 K1 K2 K3 K4}

#set master_instance [index_collection [get_cells -hier *__tile] 0]
#
#set link_width [expr [sizeof_collection [get_pins -of ${master_instance}  -filter "name=~link_in*"]] / 4]
#
#set block_llx [get_attribute $master_instance boundary_bounding_box.ll_x]
#set block_lly [get_attribute $master_instance boundary_bounding_box.ll_y]
#set block_width  [get_attribute $master_instance width]
#set block_height [get_attribute $master_instance height]
#
#
#
#set counter 2.48
#set counter2 3.968
#set odd 0
#for {set i [expr ${link_width}*0]} {$i < [expr ${link_width}*1]} {incr i} {
#  set pin_x [expr $block_llx]
#  set pin_y [expr $block_lly + $counter]
#  puts "$pin_x -- $pin_y"
#  set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_in[$i]"] -allowed_layers {C4} -location "$pin_x $pin_y"
#  set pin_x [expr $block_llx]
#  set pin_y [expr $block_lly + $counter2]
#  if { $odd } {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K1} -location "$pin_x $pin_y"
#  } else {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K3} -location "$pin_x $pin_y"
#  }
#  set counter [expr $counter + 0.160]
#  set counter2 [expr $counter2 + 0.128]
#  set odd [expr ($odd+1) % 2]
#}
#
#
#
#set counter 2.48
#set counter2 3.968
#set odd 0
#for {set i [expr ${link_width}*1]} {$i < [expr ${link_width}*2]} {incr i} {
#  set pin_x [expr $block_llx + $block_width]
#  set pin_y [expr $block_lly + $counter]
#  puts "$pin_x -- $pin_y"
#  set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_in[$i]"] -allowed_layers {C4} -location "$pin_x $pin_y"
#  set pin_x [expr $block_llx + $block_width]
#  set pin_y [expr $block_lly + $counter2]
#  if { $odd } {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K1} -location "$pin_x $pin_y"
#  } else {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K3} -location "$pin_x $pin_y"
#  }
#  set counter [expr $counter + 0.160]
#  set counter2 [expr $counter2 + 0.128]
#  set odd [expr ($odd+1) % 2]
#}
#
#
#set counter [expr $block_width - 2.24]
#set counter2 0
#set counter3 [expr $block_width - 2.24]
#set odd 0
#for {set i [expr ${link_width}*2]} {$i < [expr ${link_width}*3]} {incr i} {
#  set pin_x [expr $block_llx + $counter]
#  set pin_y [expr $block_lly]
#  set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_in[$i]"] -allowed_layers {C5} -location "$pin_x $pin_y"
#  set pin_x [expr $block_llx + $counter3]
#  set pin_y [expr $block_lly]
#  if { $odd } {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K2} -location "$pin_x $pin_y"
#  } else {
#    set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {K4} -location "$pin_x $pin_y"
#  }
#  set odd [expr ($odd+1) % 2]
#  incr counter2
#  if {$counter2 == 15} {
#    set counter [expr $counter - 1.12]
#    set counter2 0
#  } else {
#    set counter [expr $counter - 0.160]
#  }
#}
#
#
#
#for {set i [expr ${link_width}*3]} {$i < [expr ${link_width}*4]} {incr i} {
#  set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_in[$i]"] -allowed_layers {C5 K2 K4} -side 2
#  set_individual_pin_constraints -pins [get_pins -of ${master_instance} -filter "name==link_out[$i]"] -allowed_layers {C5 K2 K4} -side 2
#}
#
#
#
#set                  all_other_pins [get_pins -of ${master_instance} -filter "name=~clk_i"]
#append_to_collection all_other_pins [get_pins -of ${master_instance} -filter "name=~reset_i"]
#append_to_collection all_other_pins [get_pins -of ${master_instance} -filter "name=~my_*"]
#
#set_individual_pin_constraints -pins $all_other_pins -allowed_layers {C4 C5 K1 K2 K3 K4}

