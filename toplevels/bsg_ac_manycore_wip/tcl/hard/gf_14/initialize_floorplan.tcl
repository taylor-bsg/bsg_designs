
initialize_floorplan        \
  -control_type die         \
  -coincident_boundary true \
  -shape R                  \
  -side_ratio { 5 6 }       \
  -core_utilization 0.50    \
  -core_offset 15.0

