puts "BSG-info: Running script [info script]\n"

################################################################################
## Create boundary for the manycore tile
################################################################################

current_design bsg_manycore_tile

# Proc bound
set proc_bound [create_bound -name "proc" -type hard -boundary {{0.0000 27.7440} {210.6870 138.240}}]
add_to_bound ${proc_bound} [get_cells -hier -filter "full_name=~proc/*"]

# Rtr bound
set rtr_bound [create_bound -name "rtr" -type hard -boundary {{0.0000 0.0000} {241.920 0.0000} {241.920 138.240} {210.6870 138.240} {210.6870 27.7440} {0.0000 27.7440}}]
add_to_bound ${rtr_bound} [get_cells -hier -filter "full_name=~rtr/*"]

current_design ${DESIGN_NAME}

################################################################################
## Macro constraints for the manycore tile
################################################################################

set master_instance [index_collection [get_cells -hier *__tile] 0]

# Paths for macros
set icache_path "*/proc/h_z/vcore/icache0/imem_0/macro_mem"
set dmem_path   "*/proc/h_z/vcore/dmem/macro_mem"

# Calc keepout margin
set margin_x [expr 6*[unit_width]]
set margin_y [expr 1*[unit_height]]

# I-Cache Macro Mem
set icache_cell [get_cells -hier -of ${master_instance} -filter "full_name=~${icache_path}"]
create_keepout_margin -type hard -outer "${margin_x} ${margin_y} ${margin_x} ${margin_y}" ${icache_cell}
set_macro_relative_location -target_object ${icache_cell} -target_corner tl -target_orientation MY -anchor_object ${proc_bound} -anchor_corner tl -offset "${margin_x} -${margin_y}"

# D-Mem Macro Mem
set dmem_cell [get_cells -hier -of ${master_instance} -filter "full_name=~${dmem_path}"]
create_keepout_margin -type hard -outer "${margin_x} ${margin_y} ${margin_x} ${margin_y}" ${dmem_cell}
set_macro_relative_location -target_object ${dmem_cell} -target_corner tr -target_orientation R0 -anchor_object ${proc_bound} -anchor_corner tr -offset "-${margin_x} -${margin_y}"

################################################################################
## Macro constraints for the vcache
################################################################################

set master_instance [index_collection [get_cells -hier *__vc] 0]

# Paths for macros
set data_mem_path "*/cache/data_mem/macro_mem"
set stat_mem_path "*/cache/stat_mem/macro_mem"
set tag_mem_path  "*/cache/tag_mem/macro_mem"

# Calc keepout margin
set margin_x [expr 6*[unit_width]]
set margin_y [expr 1*[unit_height]]

# Data Macro Mem
set data_mem_cell [get_cells -hier -of ${master_instance} -filter "full_name=~${data_mem_path}"]
create_keepout_margin -type hard -outer "${margin_x} ${margin_y} ${margin_x} ${margin_y}" ${data_mem_cell}
set_macro_relative_location -target_object ${data_mem_cell} -target_corner bl -target_orientation MY -anchor_corner bl -offset "${margin_x} ${margin_y}"

# Stat + Tag Macro Memory Array
set stat_tag_mem_cell [get_cells -hier -of ${master_instance} -filter "full_name=~${stat_mem_path}||full_name=~${tag_mem_path}"]
set stat_tag_mem_array [create_macro_array ${stat_tag_mem_cell} -name "stat_tag_mem_array" -num_row 2 -num_col 1 -align right -orientation N -horizontal_channel 1]
create_keepout_margin -type hard -outer "${margin_x} ${margin_y} ${margin_x} ${margin_y}" ${stat_tag_mem_cell}
set_macro_relative_location -target_object ${stat_tag_mem_array} -target_corner br -target_orientation R0 -anchor_corner br -offset {0 0}

puts "BSG-info: Completed script [info script]\n"

