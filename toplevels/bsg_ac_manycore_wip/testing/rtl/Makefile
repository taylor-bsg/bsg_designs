.DEFAULT_GOAL=run

# Note: most variables that are file/dir paths are ?= because they can be
# overriden by the chip repo if this makefile is called from the chip
# infrastructure.

TOP_DIR            ?= $(shell git rev-parse --show-toplevel)
ROOT_DIR           ?= $(abspath $(TOP_DIR)/../)
BSG_DESIGNS_TARGET ?= $(notdir $(abspath ../../))

BSG_WORK_DIR := $(abspath ./)
BSG_OUT_DIR  := $(BSG_WORK_DIR)/out
BSG_OUT_SIM  := $(BSG_OUT_DIR)/simv

include $(ROOT_DIR)/bsg_cadenv/cadenv.mk

# Repository setup
export BSG_DESIGNS_DIR        ?= $(ROOT_DIR)/bsg_designs
export BSG_DESIGNS_TARGET_DIR ?= $(BSG_DESIGNS_DIR)/toplevels/$(BSG_DESIGNS_TARGET)
export BASEJUMP_STL_DIR       ?= $(ROOT_DIR)/basejump_stl
export BSG_PACKAGING_DIR      ?= $(ROOT_DIR)/bsg_packaging
export BSG_MANYCORE_DIR       ?= $(ROOT_DIR)/bsg_manycore
export BOARD_DIR              ?= $(ROOT_DIR)/board

export TESTING_BSG_DESIGNS_DIR        ?= $(BSG_OUT_DIR)/root/bsg_designs
export TESTING_BASEJUMP_STL_DIR       ?= $(BSG_OUT_DIR)/root/basejump_stl
export TESTING_BSG_PACKAGING_DIR      ?= $(BSG_OUT_DIR)/root/bsg_packaging
export TESTING_BSG_MANYCORE_DIR       ?= $(BSG_OUT_DIR)/root/bsg_manycore
export TESTING_BOARD_DIR              ?= $(BSG_OUT_DIR)/root/board
export TESTING_BSG_DESIGNS_TARGET_DIR ?= $(TESTING_BSG_DESIGNS_DIR)/toplevels/$(BSG_DESIGNS_TARGET)

export BSG_PACKAGE           ?=uw_bga
export BSG_PINOUT            ?=bsg_asic_cloud
export BSG_PACKAGING_FOUNDRY ?=gf_14_invecas_3p3v
export BSG_PADMAPPING        ?=default

########################################
## VCS OPTIONS
########################################

# Common VCS Options (will be used most of the time by all corners)
VCS_OPTIONS := -full64
VCS_OPTIONS += -notice
VCS_OPTIONS += -debug_pp
VCS_OPTIONS += -V
VCS_OPTIONS += +v2k
VCS_OPTIONS += -sverilog -assert svaext
VCS_OPTIONS += +noportcoerce
VCS_OPTIONS += +vc
VCS_OPTIONS += +vcs+loopreport
VCS_OPTIONS += -timescale=1ps/1ps
VCS_OPTIONS += -diag timescale 
VCS_OPTIONS += -o $(BSG_OUT_SIM)
VCS_OPTIONS += -Mdir=$(BSG_OUT_DIR)
VCS_OPTIONS += -top bsg_config bsg_config.v

########################################
## Chip and Testing Filelists and Liblists
########################################

BSG_TOP_SIM_MODULE = bsg_asic_cloud_pcb
BSG_CHIP_INSTANCE_PATH = bsg_asic_cloud_pcb.IC0.ASIC

VCS_OPTIONS += +define+BSG_TOP_SIM_MODULE=$(BSG_TOP_SIM_MODULE)
VCS_OPTIONS += +define+BSG_CHIP_INSTANCE_PATH=$(BSG_CHIP_INSTANCE_PATH)

export BSG_CHIP_LIBRARY_NAME = bsg_chip
export BSG_CHIP_FILELIST = $(BSG_OUT_DIR)/$(BSG_CHIP_LIBRARY_NAME).filelist
export BSG_CHIP_LIBRARY = $(BSG_OUT_DIR)/$(BSG_CHIP_LIBRARY_NAME).library

VCS_OPTIONS += +define+BSG_CHIP_LIBRARY_NAME=$(BSG_CHIP_LIBRARY_NAME)
VCS_OPTIONS += -f $(BSG_CHIP_FILELIST)
VCS_OPTIONS += -libmap $(BSG_CHIP_LIBRARY)

export BSG_DESIGNS_TESTING_LIBRARY_NAME = bsg_design_testing
export BSG_DESIGNS_TESTING_FILELIST = $(BSG_OUT_DIR)/$(BSG_DESIGNS_TESTING_LIBRARY_NAME).filelist
export BSG_DESIGNS_TESTING_LIBRARY = $(BSG_OUT_DIR)/$(BSG_DESIGNS_TESTING_LIBRARY_NAME).library

VCS_OPTIONS += +define+BSG_DESIGNS_TESTING_LIBRARY_NAME=$(BSG_DESIGNS_TESTING_LIBRARY_NAME)
VCS_OPTIONS += -f $(BSG_DESIGNS_TESTING_FILELIST)
VCS_OPTIONS += -libmap $(BSG_DESIGNS_TESTING_LIBRARY)

$(BSG_CHIP_FILELIST): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_CHIP_LIBRARY): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_DESIGNS_TESTING_FILELIST): $(BSG_DESIGNS_TESTING_LIBRARY)
$(BSG_DESIGNS_TESTING_LIBRARY): $(BSG_OUT_DIR)/root
	/usr/bin/tclsh bsg_config.tcl

########################################
## Trace Replay Roms
########################################

BSG_TRACE_FILES := $(notdir $(wildcard $(BSG_WORK_DIR)/../traces/*.tr))
BSG_TRACE_ROMS  := $(addprefix $(BSG_OUT_DIR)/,${BSG_TRACE_FILES:.tr=_rom.v})

$(BSG_OUT_DIR)/%_rom.v: $(BSG_WORK_DIR)/../traces/%.tr | $(BSG_OUT_DIR)
	$(BASEJUMP_STL_DIR)/bsg_mem/bsg_ascii_to_rom.py $< $*_rom > $@

VCS_OPTIONS += $(addprefix -v ,$(BSG_TRACE_ROMS))

########################################
## Manycore SPMD Loader
########################################

SPMD_PROG    := hello
BSG_SPMD_DIR := $(BSG_WORK_DIR)/../spmd/$(SPMD_PROG)

export bsg_global_X = 4
export bsg_global_Y = 5

export bsg_tiles_org_X = 0
export bsg_tiles_org_Y = 1

export bsg_tiles_X = $(bsg_global_X)
export bsg_tiles_Y = $(shell echo $(bsg_global_Y)-1 | bc)

VCS_OPTIONS += +define+NUM_CODE_SECTIONS=0
VCS_OPTIONS += +define+CODE_SECTIONS="0,0"

$(BSG_SPMD_DIR)/main.riscv: | $(BSG_SPMD_DIR)/config
	mkdir -p $(@D)
	$(MAKE) -C $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/ clean main.riscv
	cp -r $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/main.riscv $@

$(BSG_SPMD_DIR)/main_dmem.mem: | $(BSG_SPMD_DIR)/config
	mkdir -p $(@D)
	$(MAKE) -C $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/ clean main_dmem.mem
	cp -r $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/main_dmem.mem $@

$(BSG_SPMD_DIR)/main_dram.mem: | $(BSG_SPMD_DIR)/config
	mkdir -p $(@D)
	$(MAKE) -C $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/ clean main_dram.mem
	cp -r $(ROOT_DIR)/bsg_manycore/software/spmd/$(SPMD_PROG)/main_dram.mem $@

.PHONY: $(BSG_SPMD_DIR)/config
$(BSG_SPMD_DIR)/config:
	@echo "# This is the manycore setup when this program binary was compiled. If this"     > $@
	@echo "# doesn't match what you want, you should delete these binaries and recompile!" >> $@
	@echo ""                                                                               >> $@
	@echo "bsg_global_X = $(bsg_global_X)"                                                 >> $@
	@echo "bsg_global_Y = $(bsg_global_Y)"                                                 >> $@
	@echo ""                                                                               >> $@
	@echo "bsg_tiles_org_X = $(bsg_tiles_org_X)"                                           >> $@
	@echo "bsg_tiles_org_Y = $(bsg_tiles_org_Y)"                                           >> $@
	@echo ""                                                                               >> $@
	@echo "bsg_tiles_X = $(bsg_tiles_X)"                                                   >> $@
	@echo "bsg_tiles_Y = $(bsg_tiles_Y)"                                                   >> $@
	@echo ""                                                                               >> $@

########################################
## DRAM Definitions
########################################

VCS_OPTIONS += +define+den2048Mb+sg5+x16+FULL_MEM

########################################
## Run Targets
########################################

run: clean $(BSG_OUT_SIM)
	$(BSG_OUT_SIM) | tee -i $(BSG_OUT_DIR)/run.log

run-no-tee: clean $(BSG_OUT_SIM)
	$(BSG_OUT_SIM)

rerun: $(BSG_OUT_SIM)
	$(BSG_OUT_SIM) | tee -i $(BSG_OUT_DIR)/run.log

rerun-no-tee: $(BSG_OUT_SIM)
	$(BSG_OUT_SIM)

view:
	$(VCS_BIN)/dve -full64 -vpd vcdplus.vpd

build: $(BSG_OUT_SIM)
$(BSG_OUT_SIM): $(BSG_CHIP_FILELIST) $(BSG_CHIP_LIBRARY) $(BSG_DESIGNS_TESTING_FILELIST) $(BSG_DESIGNS_TESTING_LIBRARY) $(BSG_TRACE_ROMS) main.riscv main_dmem.mem main_dram.mem
	$(eval include $$(ROOT_DIR)/bsg_manycore/software/mk/Makefile.dimensions)
	$(eval include $$(ROOT_DIR)/bsg_manycore/software/mk/Makefile.verilog.loader)
	$(VCS) $(VCS_OPTIONS) | tee -i $(BSG_OUT_DIR)/build.log

main.riscv: $(BSG_SPMD_DIR)/main.riscv
	ln -nsf $^ $@

main_dmem.mem: $(BSG_SPMD_DIR)/main_dmem.mem
	ln -nsf $^ $@

main_dram.mem: $(BSG_SPMD_DIR)/main_dram.mem
	ln -nsf $^ $@

$(BSG_OUT_DIR)/root: | $(BSG_OUT_DIR)
	ln -nsf $(ROOT_DIR) $@

$(BSG_OUT_DIR):
	mkdir -p $@

clean:
	rm -rf $(BSG_OUT_DIR)
	rm -rf DVEfiles
	rm -rf stack.info.*
	rm -f  vc_hdrs.h
	rm -f  vcdplus.vpd
	rm -f  inter.vpd
	rm -f  ucli.key
	rm -f  main.riscv main_dmem.mem main_dram.mem

