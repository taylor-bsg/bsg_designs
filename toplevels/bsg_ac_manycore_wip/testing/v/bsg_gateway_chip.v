`timescale 1ps/1ps

`ifndef MANYCORE_CLK_PERIOD
  `define MANYCORE_CLK_PERIOD 5000.0
`endif

`ifndef IO_MASTER_CLK_PERIOD
  `define IO_MASTER_CLK_PERIOD 5000.0
`endif

`ifndef ROUTER_CLK_PERIOD
  `define ROUTER_CLK_PERIOD 5000.0
`endif

`ifndef TAG_CLK_PERIOD
  `define TAG_CLK_PERIOD 10000.0
`endif

module bsg_gateway_chip

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

`include "bsg_pinout_inverted.v"

  //////////////////////////////////////////////////
  //
  // Waveform Dump
  //

  initial
    begin
      $vcdpluson;
      $vcdplusmemon;
      $vcdplusautoflushon;
    end

  //////////////////////////////////////////////////
  //
  // Nonsynth Clock Generator(s)
  //

  logic manycore_clk;
  assign p_clk_A_o = manycore_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`MANYCORE_CLK_PERIOD)) manycore_clk_gen (.o(manycore_clk));

  logic io_master_clk;
  assign p_clk_B_o = io_master_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`IO_MASTER_CLK_PERIOD)) io_master_clk_gen (.o(io_master_clk));

  logic router_clk;
  assign p_clk_C_o = router_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`ROUTER_CLK_PERIOD)) router_clk_gen (.o(router_clk));

  logic tag_clk;
  assign p_bsg_tag_clk_o = tag_clk;
  bsg_nonsynth_clock_gen #(.cycle_time_p(`TAG_CLK_PERIOD)) tag_clk_gen (.o(tag_clk));

  //////////////////////////////////////////////////
  //
  // Nonsynth Reset Generator(s)
  //

  logic tag_reset;
  bsg_nonsynth_reset_gen #(.num_clocks_p(1),.reset_cycles_lo_p(10),.reset_cycles_hi_p(5))
    tag_reset_gen
      (.clk_i(tag_clk)
      ,.async_reset_o(tag_reset)
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Track Replay
  //

  localparam tag_trace_rom_addr_width_lp = 32;
  localparam tag_trace_rom_data_width_lp = 22;

  logic [tag_trace_rom_addr_width_lp-1:0] rom_addr_li;
  logic [tag_trace_rom_data_width_lp-1:0] rom_data_lo;

  logic [1:0] tag_trace_en_r_lo;
  logic       tag_trace_done_lo;

  // TAG TRACE ROM
  bsg_tag_boot_rom #(.width_p( tag_trace_rom_data_width_lp )
                    ,.addr_width_p( tag_trace_rom_addr_width_lp )
                    )
    tag_trace_rom
      (.addr_i( rom_addr_li )
      ,.data_o( rom_data_lo )
      );

  // TAG TRACE REPLAY
  bsg_tag_trace_replay #(.rom_addr_width_p( tag_trace_rom_addr_width_lp )
                        ,.rom_data_width_p( tag_trace_rom_data_width_lp )
                        ,.num_masters_p( 2 )
                        ,.num_clients_p( tag_num_clients_gp )
                        ,.max_payload_width_p( tag_max_payload_width_gp )
                        )
    tag_trace_replay
      (.clk_i   ( p_bsg_tag_clk_o )
      ,.reset_i ( tag_reset    )
      ,.en_i    ( 1'b1            )

      ,.rom_addr_o( rom_addr_li )
      ,.rom_data_i( rom_data_lo )

      ,.valid_i ( 1'b0 )
      ,.data_i  ( '0 )
      ,.ready_o ()

      ,.valid_o    ()
      ,.en_r_o     ( tag_trace_en_r_lo )
      ,.tag_data_o ( p_bsg_tag_data_o )
      ,.yumi_i     ( 1'b1 )

      ,.done_o  ( tag_trace_done_lo )
      ,.error_o ()
      ) ;

  assign p_bsg_tag_en_o = tag_trace_en_r_lo[0];

  //////////////////////////////////////////////////
  //
  // BSG Tag Master Instance
  //

  // All tag lines from the btm
  bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;

  // Rename the tag lines (copy from bsg_chip.v)
  wire  bsg_tag_s  prev_io_link_ctrl_tag_lines_lo   = tag_lines_lo[0];
  wire  bsg_tag_s  prev_core_link_ctrl_tag_lines_lo = tag_lines_lo[1];
  wire  bsg_tag_s  next_io_link_ctrl_tag_lines_lo   = tag_lines_lo[2];
  wire  bsg_tag_s  next_core_link_ctrl_tag_lines_lo = tag_lines_lo[3];
  wire  bsg_tag_s  router_ctrl_tag_lines_lo         = tag_lines_lo[4];
  wire  bsg_tag_s  manycore_ctrl_tag_lines_lo       = tag_lines_lo[5];

  // BSG tag master instance
  bsg_tag_master #(.els_p( tag_num_clients_gp )
                  ,.lg_width_p( tag_lg_max_payload_width_gp )
                  )
    btm
      (.clk_i      ( p_bsg_tag_clk_o )
      ,.data_i     ( tag_trace_en_r_lo[1] ? p_bsg_tag_data_o : 1'b0 )
      ,.en_i       ( 1'b1 )
      ,.clients_r_o( tag_lines_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Client Instance(s)
  //

  // Copy the manycore control tag from bsg_chip_manycore_complex.v

  // Tag payload for manycore control signals
  typedef struct packed { 
      logic reset;
      logic [wormhole_x_cord_width_gp-1:0] x;
      logic [wormhole_y_cord_width_gp-1:0] y;
  } tag_manycore_ctrl_payload_s;

  // tag payload for manycore control signals
  tag_manycore_ctrl_payload_s manycore_ctrl_data_lo;
  logic                       manycore_ctrl_new_data_lo;

  bsg_tag_client #(.width_p( $bits(manycore_ctrl_data_lo) ), .default_p( 0 ))
    btc_manycore_ctrl
      (.bsg_tag_i     ( manycore_ctrl_tag_lines_lo )
      ,.recv_clk_i    ( manycore_clk )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( manycore_ctrl_new_data_lo )
      ,.recv_data_r_o ( manycore_ctrl_data_lo )
      );

  //////////////////////////////////////////////////
  //
  // Commlink Swizzle
  //

  logic       ci_clk_li;
  logic       ci_v_li;
  logic [8:0] ci_data_li;
  logic       ci_tkn_lo;

  logic       co_clk_lo;
  logic       co_v_lo;
  logic [8:0] co_data_lo;
  logic       co_tkn_li;

  logic       ci2_clk_li;
  logic       ci2_v_li;
  logic [8:0] ci2_data_li;
  logic       ci2_tkn_lo;

  logic       co2_clk_lo;
  logic       co2_v_lo;
  logic [8:0] co2_data_lo;
  logic       co2_tkn_li;

  bsg_chip_swizzle_adapter
    swizzle
      (.port_ci_clk_i   (p_ci_clk_i)
      ,.port_ci_v_i     (p_ci_v_i)
      ,.port_ci_data_i  ({p_ci_8_i, p_ci_7_i, p_ci_6_i, p_ci_5_i, p_ci_4_i, p_ci_3_i, p_ci_2_i, p_ci_1_i, p_ci_0_i})
      ,.port_ci_tkn_o   (p_ci_tkn_o)

      ,.port_ci2_clk_o  (p_ci2_clk_o)
      ,.port_ci2_v_o    (p_ci2_v_o)
      ,.port_ci2_data_o ({p_ci2_8_o, p_ci2_7_o, p_ci2_6_o, p_ci2_5_o, p_ci2_4_o, p_ci2_3_o, p_ci2_2_o, p_ci2_1_o, p_ci2_0_o})
      ,.port_ci2_tkn_i  (p_ci2_tkn_i)

      ,.port_co_clk_i   (p_co_clk_i)
      ,.port_co_v_i     (p_co_v_i)
      ,.port_co_data_i  ({p_co_8_i, p_co_7_i, p_co_6_i, p_co_5_i, p_co_4_i, p_co_3_i, p_co_2_i, p_co_1_i, p_co_0_i})
      ,.port_co_tkn_o   (p_co_tkn_o)

      ,.port_co2_clk_o  (p_co2_clk_o)
      ,.port_co2_v_o    (p_co2_v_o)
      ,.port_co2_data_o ({p_co2_8_o, p_co2_7_o, p_co2_6_o, p_co2_5_o, p_co2_4_o, p_co2_3_o, p_co2_2_o, p_co2_1_o, p_co2_0_o})
      ,.port_co2_tkn_i  (p_co2_tkn_i)

      ,.guts_ci_clk_o  (ci_clk_li)
      ,.guts_ci_v_o    (ci_v_li)
      ,.guts_ci_data_o (ci_data_li)
      ,.guts_ci_tkn_i  (ci_tkn_lo)

      ,.guts_co_clk_i  (co_clk_lo)
      ,.guts_co_v_i    (co_v_lo)
      ,.guts_co_data_i (co_data_lo)
      ,.guts_co_tkn_o  (co_tkn_li)

      ,.guts_ci2_clk_o (ci2_clk_li)
      ,.guts_ci2_v_o   (ci2_v_li)
      ,.guts_ci2_data_o(ci2_data_li)
      ,.guts_ci2_tkn_i (ci2_tkn_lo)

      ,.guts_co2_clk_i (co2_clk_lo)
      ,.guts_co2_v_i   (co2_v_lo)
      ,.guts_co2_data_i(co2_data_lo)
      ,.guts_co2_tkn_o (co2_tkn_li)
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip IO Complex
  //

  logic router_reset_lo;

  bsg_ready_and_link_sif_s  fwd_links_li;
  bsg_ready_and_link_sif_s  fwd_links_lo;
  
  bsg_ready_and_link_sif_s  rev_links_li;
  bsg_ready_and_link_sif_s  rev_links_lo;

  bsg_chip_io_complex #(.link_channel_width_p( link_channel_width_gp )
                       ,.wormhole_packet_width_p( wormhole_packet_width_gp )
                       ,.wormhole_x_cord_width_p( wormhole_x_cord_width_gp )
                       ,.wormhole_y_cord_width_p( wormhole_y_cord_width_gp )
                       ,.wormhole_len_width_p( wormhole_len_width_gp )
                       ,.wormhole_reserved_width_p( wormhole_reserved_width_gp )
                       ,.wormhole_remote_credits_p( wormhole_remote_credits_gp )
                       ,.wormhole_max_payload_flits_p( wormhole_max_payload_flits_gp )
                       )
    io_complex
      (.core_clk_i ( router_clk )
      ,.io_clk_i   ( io_master_clk )

      ,.core_reset_o ( router_reset_lo )

      ,.prev_io_link_ctrl_tag_lines_i   ( prev_io_link_ctrl_tag_lines_lo )
      ,.prev_core_link_ctrl_tag_lines_i ( prev_core_link_ctrl_tag_lines_lo )
      ,.next_io_link_ctrl_tag_lines_i   ( next_io_link_ctrl_tag_lines_lo )
      ,.next_core_link_ctrl_tag_lines_i ( next_core_link_ctrl_tag_lines_lo )
      ,.router_ctrl_tag_lines_i         ( router_ctrl_tag_lines_lo )
      
      ,.ci_clk_i  ( ci_clk_li )
      ,.ci_v_i    ( ci_v_li )
      ,.ci_data_i ( ci_data_li[link_channel_width_gp-1:0] )
      ,.ci_tkn_o  ( ci_tkn_lo )

      ,.co_clk_o  ( co_clk_lo )
      ,.co_v_o    ( co_v_lo )
      ,.co_data_o ( co_data_lo[link_channel_width_gp-1:0] )
      ,.co_tkn_i  ( co_tkn_li )

      ,.ci2_clk_i  ( ci2_clk_li )
      ,.ci2_v_i    ( ci2_v_li )
      ,.ci2_data_i ( ci2_data_li[link_channel_width_gp-1:0] )
      ,.ci2_tkn_o  ( ci2_tkn_lo )

      ,.co2_clk_o  ( co2_clk_lo )
      ,.co2_v_o    ( co2_v_lo )
      ,.co2_data_o ( co2_data_lo[link_channel_width_gp-1:0] )
      ,.co2_tkn_i  ( co2_tkn_li )
      
      ,.fwd_links_i ( fwd_links_li )
      ,.fwd_links_o ( fwd_links_lo )

      ,.rev_links_i ( rev_links_li )
      ,.rev_links_o ( rev_links_lo )
      );

  //////////////////////////////////////////////////
  //
  // Manycore SPMD Loader
  //

  bsg_manycore_link_sif_s link_sif_li;
  bsg_manycore_link_sif_s link_sif_lo;

  bsg_manycore_spmd_tester
    manycore_spmd_tester
      (.clk_i   ( manycore_clk )
      ,.reset_i ( manycore_ctrl_data_lo.reset | ~tag_trace_done_lo )

      ,.links_sif_i ( link_sif_li )
      ,.links_sif_o ( link_sif_lo )

      ,.finish_o()
      ,.success_o()
      ,.timeout_o()
      );

  //////////////////////////////////////////////////
  //
  // Adapter between manycore and IO complex
  //

  bsg_manycore_link_async_to_wormhole #(.addr_width_p( manycore_addr_width_gp )
                                       ,.data_width_p( manycore_data_width_gp )
                                       ,.load_id_width_p( manycore_load_id_width_gp )
                                       ,.x_cord_width_p( manycore_x_cord_width_gp )
                                       ,.y_cord_width_p( manycore_y_cord_width_gp )
                                       ,.wormhole_width_p( wormhole_packet_width_gp )
                                       ,.wormhole_x_cord_width_p( wormhole_x_cord_width_gp )
                                       ,.wormhole_y_cord_width_p( wormhole_y_cord_width_gp )
                                       ,.wormhole_len_width_p( wormhole_len_width_gp )
                                       ,.wormhole_reserved_width_p( wormhole_reserved_width_gp )
                                       )
    manycore_wormhole_adapter
      (.manycore_clk_i   ( manycore_clk )
      ,.manycore_reset_i ( manycore_ctrl_data_lo.reset )
       
      ,.links_sif_i ( link_sif_lo )
      ,.links_sif_o ( link_sif_li )

      ,.clk_i   ( router_clk )
      ,.reset_i ( router_reset_lo )

      ,.dest_x_i (manycore_ctrl_data_lo.x )
      ,.dest_y_i (manycore_ctrl_data_lo.y )
       
      ,.link_i ({ fwd_links_lo, rev_links_lo })
      ,.link_o ({ fwd_links_li, rev_links_li })
      );

endmodule

