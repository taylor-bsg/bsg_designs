module bsg_manycore_spmd_tester

import bsg_chip_pkg::*;

( input  clk_i
, input  reset_i

, input   bsg_manycore_link_sif_s  links_sif_i
, output  bsg_manycore_link_sif_s  links_sif_o

, output logic finish_o
, output logic success_o
, output logic timeout_o
);

  //////////////////////////////////////////////////
  //
  // Cycle Counter
  //

  wire [39:0] cycle_count;

  bsg_cycle_counter #(.width_p( 40 )
                     ,.init_val_p( 0 )
                     )
    cc
      (.clk_i   ( clk_i       )
      ,.reset_i ( reset_i     )
      ,.ctr_r_o ( cycle_count )
      );

  //////////////////////////////////////////////////
  //
  //
  //

  bsg_manycore_packet_s  loader_data_lo;
  logic                  loader_v_lo;
  logic                  loader_ready_li;

  logic reset_r;
  always_ff @ (posedge clk_i) begin
    reset_r <= reset_i;
  end

  bsg_manycore_spmd_loader #(.icache_entries_num_p( manycore_icache_entries_gp )
                                     ,.num_rows_p( manycore_num_tiles_y_gp )
                                     ,.num_cols_p( manycore_num_tiles_x_gp )
                                     ,.data_width_p( manycore_data_width_gp )
                                     ,.addr_width_p( manycore_addr_width_gp )
                                     ,.load_id_width_p( manycore_load_id_width_gp )
                                     ,.epa_byte_addr_width_p( manycore_epa_byte_addr_width_gp )
                                     ,.x_cord_width_p( manycore_x_cord_width_gp )
                                     ,.y_cord_width_p( manycore_y_cord_width_gp )
                                     ,.dram_ch_num_p( vcache_num_cache_gp )
                                     ,.dram_ch_addr_width_p( 0 )
                                     ,.init_vcache_p( 0 )
                                     ,.vcache_entries_p( vcache_sets_gp )
                                     ,.vcache_ways_p( vcache_ways_gp )
                                     )
    spmd_loader
      (.clk_i   ( clk_i   )
      ,.reset_i ( reset_r )
   
      ,.my_x_i ( manycore_x_cord_width_gp'(0) )
      ,.my_y_i ( manycore_y_cord_width_gp'(0) )
   
      ,.data_o  ( loader_data_lo  )
      ,.v_o     ( loader_v_lo     )
      ,.ready_i ( loader_ready_li )
      );

  // we only set such a high number because we
  // know these packets can always be consumed
  // at the recipient and do not require any
  // forwarded traffic. for an accelerator
  // this would not be the case, and this
  // number must be set to the same as the
  // number of elements in the accelerator's
  // input fifo

  localparam max_cycles_lp = 1_000_000;
  localparam credits_lp = 10;

  wire pass_thru_ready_lo;

  wire [`BSG_SAFE_CLOG2(credits_lp+1)-1:0] creds;

  // hook up the ready signal if this is the SPMD loader
  // we handle credits here but could do it in the SPMD module too

  assign loader_ready_li = pass_thru_ready_lo & (|creds);

  bsg_nonsynth_manycore_monitor #(.x_cord_width_p( manycore_x_cord_width_gp )
                                 ,.y_cord_width_p( manycore_y_cord_width_gp )
                                 ,.addr_width_p( manycore_addr_width_gp )
                                 ,.data_width_p( manycore_data_width_gp )
                                 ,.load_id_width_p( manycore_load_id_width_gp )
                                 ,.channel_num_p( 0 )
                                 ,.max_cycles_p( max_cycles_lp )
                                 ,.pass_thru_p(1 )
                                 // for the SPMD loader we don't anticipate
                                 // any backwards flow control; but for an
                                 // accelerator, we must be much more careful about
                                 // setting this
                                 ,.pass_thru_max_out_credits_p( credits_lp )
                                 )
    bmm
      (.clk_i   ( clk_i   )
      ,.reset_i ( reset_r )

      ,.link_sif_i ( links_sif_i )
      ,.link_sif_o ( links_sif_o )

      ,.pass_thru_data_i        ( loader_data_lo                )
      ,.pass_thru_v_i           ( loader_v_lo & loader_ready_li )
      ,.pass_thru_ready_o       ( pass_thru_ready_lo            )
      ,.pass_thru_out_credits_o ( creds                         )
      ,.pass_thru_x_i           ( '0                            )
      ,.pass_thru_y_i           ( '0                            )

      ,.cycle_count_i ( cycle_count )
      ,.finish_o      ( finish_o    )
      ,.success_o     ( success_o   )
      ,.timeout_o     ( timeout_o   )
      );

endmodule
