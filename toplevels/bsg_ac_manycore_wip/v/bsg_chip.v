`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

//==============================================================================
//
// BSG CHIP
//
// This is the toplevel for the ASIC. This chip uses the UW BGA package found
// inside bsg_packaging/uw_bga. For physical design reasons, the input pins
// have been swizzled (ie. re-arranged) from their original meaning. We use the
// bsg_chip_swizzle_adapter in every ASIC to abstract away detail.
//

module bsg_chip

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

`include "bsg_pinout.v"
`include "bsg_iopads.v"

  //////////////////////////////////////////////////
  //
  // BSG Tag Master Instance
  //

  // All tag lines from the btm
  bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;

  // Rename the tag lines (determines the client ID!)
  wire bsg_tag_s prev_io_link_ctrl_tag_lines_lo   = tag_lines_lo[0];
  wire bsg_tag_s prev_core_link_ctrl_tag_lines_lo = tag_lines_lo[1];
  wire bsg_tag_s next_io_link_ctrl_tag_lines_lo   = tag_lines_lo[2];
  wire bsg_tag_s next_core_link_ctrl_tag_lines_lo = tag_lines_lo[3];
  wire bsg_tag_s router_ctrl_tag_lines_lo         = tag_lines_lo[4];
  wire bsg_tag_s manycore_ctrl_tag_lines_lo       = tag_lines_lo[5];

  wire bsg_tag_s async_reset_tag_lines_lo = tag_lines_lo[6];

  bsg_tag_s [2:0] osc_tag_lines_lo;
  bsg_tag_s [2:0] osc_trigger_tag_lines_lo;
  bsg_tag_s [2:0] ds_tag_lines_lo;
  bsg_tag_s [2:0] sel_tag_lines_lo;

  assign osc_tag_lines_lo         = tag_lines_lo[9:7];
  assign osc_trigger_tag_lines_lo = tag_lines_lo[12:10];
  assign ds_tag_lines_lo          = tag_lines_lo[15:13];
  assign sel_tag_lines_lo         = tag_lines_lo[18:16];

  // BSG tag master instance
  bsg_tag_master #(.els_p( tag_num_clients_gp )
                  ,.lg_width_p( tag_lg_max_payload_width_gp )
                  )
    btm
      (.clk_i      ( bsg_tag_clk_i_int )
      ,.data_i     ( bsg_tag_en_i_int ? bsg_tag_data_i_int : 1'b0 )
      ,.en_i       ( 1'b1 )
      ,.clients_r_o( tag_lines_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Clock Generator Power Domain
  //

  logic router_clk_lo;
  logic io_master_clk_lo;
  logic manycore_clk_lo;

  bsg_clk_gen_power_domain #(.num_clk_endpoint_p( clk_gen_num_endpoints_gp )
                            ,.ds_width_p( clk_gen_ds_width_gp )
                            ,.num_adgs_p( clk_gen_num_adgs_gp )
                            )
    clk_gen_pd
      (.async_reset_tag_lines_i ( async_reset_tag_lines_lo )
      ,.osc_tag_lines_i         ( osc_tag_lines_lo )
      ,.osc_trigger_tag_lines_i ( osc_trigger_tag_lines_lo )
      ,.ds_tag_lines_i          ( ds_tag_lines_lo )
      ,.sel_tag_lines_i         ( sel_tag_lines_lo )

      ,.ext_clk_i({ clk_C_i_int, clk_B_i_int, clk_A_i_int })

      ,.clk_o({ router_clk_lo, io_master_clk_lo, manycore_clk_lo })
      //,.clk_o()
      );

  //////////////////////////////////////////////////
  //
  // Swizzle Adapter for Comm Link IO Signals
  //

  logic         ci_clk_li;
  logic         ci_v_li;
  logic [8:0]   ci_data_li;
  logic         ci_tkn_lo;

  logic         co_clk_lo;
  logic         co_v_lo;
  logic [8:0]   co_data_lo;
  logic         co_tkn_li;

  logic         ci2_clk_li;
  logic         ci2_v_li;
  logic [8:0]   ci2_data_li;
  logic         ci2_tkn_lo;

  logic         co2_clk_lo;
  logic         co2_v_lo;
  logic [8:0]   co2_data_lo;
  logic         co2_tkn_li;

  bsg_chip_swizzle_adapter
    swizzle
      ( // IO Port Side
       .port_ci_clk_i   (ci_clk_i_int)
      ,.port_ci_v_i     (ci_v_i_int)
      ,.port_ci_data_i  ({ci_8_i_int, ci_7_i_int, ci_6_i_int, ci_5_i_int, ci_4_i_int, ci_3_i_int, ci_2_i_int, ci_1_i_int, ci_0_i_int})
      ,.port_ci_tkn_o   (ci_tkn_o_int)

      ,.port_ci2_clk_o  (ci2_clk_o_int)
      ,.port_ci2_v_o    (ci2_v_o_int)
      ,.port_ci2_data_o ({ci2_8_o_int, ci2_7_o_int, ci2_6_o_int, ci2_5_o_int, ci2_4_o_int, ci2_3_o_int, ci2_2_o_int, ci2_1_o_int, ci2_0_o_int})
      ,.port_ci2_tkn_i  (ci2_tkn_i_int)

      ,.port_co_clk_i   (co_clk_i_int)
      ,.port_co_v_i     (co_v_i_int)
      ,.port_co_data_i  ({co_8_i_int, co_7_i_int, co_6_i_int, co_5_i_int, co_4_i_int, co_3_i_int, co_2_i_int, co_1_i_int, co_0_i_int})
      ,.port_co_tkn_o   (co_tkn_o_int)

      ,.port_co2_clk_o  (co2_clk_o_int)
      ,.port_co2_v_o    (co2_v_o_int)
      ,.port_co2_data_o ({co2_8_o_int, co2_7_o_int, co2_6_o_int, co2_5_o_int, co2_4_o_int, co2_3_o_int, co2_2_o_int, co2_1_o_int, co2_0_o_int})
      ,.port_co2_tkn_i  (co2_tkn_i_int)

      // Chip (Guts) Side
      ,.guts_ci_clk_o  (ci_clk_li)
      ,.guts_ci_v_o    (ci_v_li)
      ,.guts_ci_data_o (ci_data_li)
      ,.guts_ci_tkn_i  (ci_tkn_lo)

      ,.guts_co_clk_i  (co_clk_lo)
      ,.guts_co_v_i    (co_v_lo)
      ,.guts_co_data_i (co_data_lo)
      ,.guts_co_tkn_o  (co_tkn_li)

      ,.guts_ci2_clk_o (ci2_clk_li)
      ,.guts_ci2_v_o   (ci2_v_li)
      ,.guts_ci2_data_o(ci2_data_li)
      ,.guts_ci2_tkn_i (ci2_tkn_lo)

      ,.guts_co2_clk_i (co2_clk_lo)
      ,.guts_co2_v_i   (co2_v_lo)
      ,.guts_co2_data_i(co2_data_lo)
      ,.guts_co2_tkn_o (co2_tkn_li)
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip IO Complex
  //

  logic router_reset_lo;

  bsg_ready_and_link_sif_s  fwd_links_li;
  bsg_ready_and_link_sif_s  fwd_links_lo;
  
  bsg_ready_and_link_sif_s  rev_links_li;
  bsg_ready_and_link_sif_s  rev_links_lo;

  bsg_chip_io_complex #(.link_channel_width_p( link_channel_width_gp )
                       ,.wormhole_packet_width_p( wormhole_packet_width_gp )
                       ,.wormhole_x_cord_width_p( wormhole_x_cord_width_gp )
                       ,.wormhole_y_cord_width_p( wormhole_y_cord_width_gp )
                       ,.wormhole_len_width_p( wormhole_len_width_gp )
                       ,.wormhole_reserved_width_p( wormhole_reserved_width_gp )
                       ,.wormhole_remote_credits_p( wormhole_remote_credits_gp )
                       ,.wormhole_max_payload_flits_p( wormhole_max_payload_flits_gp )
                       )
    io_complex
      (.core_clk_i ( router_clk_lo )
      ,.io_clk_i   ( io_master_clk_lo )

      ,.core_reset_o ( router_reset_lo )

      ,.prev_io_link_ctrl_tag_lines_i   ( prev_io_link_ctrl_tag_lines_lo )
      ,.prev_core_link_ctrl_tag_lines_i ( prev_core_link_ctrl_tag_lines_lo )
      ,.next_io_link_ctrl_tag_lines_i   ( next_io_link_ctrl_tag_lines_lo )
      ,.next_core_link_ctrl_tag_lines_i ( next_core_link_ctrl_tag_lines_lo )
      ,.router_ctrl_tag_lines_i         ( router_ctrl_tag_lines_lo )
      
      ,.ci_clk_i  ( ci_clk_li )
      ,.ci_v_i    ( ci_v_li )
      ,.ci_data_i ( ci_data_li[link_channel_width_gp-1:0] )
      ,.ci_tkn_o  ( ci_tkn_lo )

      ,.co_clk_o  ( co_clk_lo )
      ,.co_v_o    ( co_v_lo )
      ,.co_data_o ( co_data_lo[link_channel_width_gp-1:0] )
      ,.co_tkn_i  ( co_tkn_li )

      ,.ci2_clk_i  ( ci2_clk_li )
      ,.ci2_v_i    ( ci2_v_li )
      ,.ci2_data_i ( ci2_data_li[link_channel_width_gp-1:0] )
      ,.ci2_tkn_o  ( ci2_tkn_lo )

      ,.co2_clk_o  ( co2_clk_lo )
      ,.co2_v_o    ( co2_v_lo )
      ,.co2_data_o ( co2_data_lo[link_channel_width_gp-1:0] )
      ,.co2_tkn_i  ( co2_tkn_li )
      
      ,.fwd_links_i ( fwd_links_li )
      ,.fwd_links_o ( fwd_links_lo )

      ,.rev_links_i ( rev_links_li )
      ,.rev_links_o ( rev_links_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip Manycore
  //

  logic                                manycore_reset_lo;
  logic [wormhole_x_cord_width_gp-1:0] manycore_dest_x_lo;
  logic [wormhole_y_cord_width_gp-1:0] manycore_dest_y_lo;

  bsg_manycore_link_sif_s  link_sif_li;
  bsg_manycore_link_sif_s  link_sif_lo;

  bsg_cache_dma_pkt_s [vcache_num_cache_gp-1:0] dma_pkt_lo;
  logic               [vcache_num_cache_gp-1:0] dma_pkt_v_lo;
  logic               [vcache_num_cache_gp-1:0] dma_pkt_yumi_li;
  
  logic [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_li;
  logic [vcache_num_cache_gp-1:0]                             dma_data_v_li;
  logic [vcache_num_cache_gp-1:0]                             dma_data_ready_lo;
  
  logic [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_lo;
  logic [vcache_num_cache_gp-1:0]                             dma_data_v_lo;
  logic [vcache_num_cache_gp-1:0]                             dma_data_yumi_li;

  bsg_chip_manycore_complex
    manycore_complex
      (.clk_i ( manycore_clk_lo )

      ,.manycore_ctrl_tag_lines_i ( manycore_ctrl_tag_lines_lo )

      ,.reset_o ( manycore_reset_lo )
      ,.dest_x_o( manycore_dest_x_lo )
      ,.dest_y_o( manycore_dest_y_lo )

      ,.io_link_sif_i( link_sif_li )
      ,.io_link_sif_o( link_sif_lo )

      ,.dma_pkt_o      ( dma_pkt_lo )
      ,.dma_pkt_v_o    ( dma_pkt_v_lo )
      ,.dma_pkt_yumi_i ( dma_pkt_yumi_li )

      ,.dma_data_i       ( dma_data_li )
      ,.dma_data_v_i     ( dma_data_v_li )
      ,.dma_data_ready_o ( dma_data_ready_lo )

      ,.dma_data_o      ( dma_data_lo )
      ,.dma_data_v_o    ( dma_data_v_lo )
      ,.dma_data_yumi_i ( dma_data_yumi_li )
      );

  // Tie-off for dma signals
  assign dma_pkt_yumi_li  = dma_pkt_v_lo;
  assign dma_data_li      = '0;
  assign dma_data_v_li    = '0;
  assign dma_data_yumi_li = dma_data_v_lo;

  //////////////////////////////////////////////////
  //
  // Adapter between manycore and IO complex
  //

  bsg_manycore_link_async_to_wormhole #(.addr_width_p( manycore_addr_width_gp )
                                       ,.data_width_p( manycore_data_width_gp )
                                       ,.load_id_width_p( manycore_load_id_width_gp )
                                       ,.x_cord_width_p( manycore_x_cord_width_gp )
                                       ,.y_cord_width_p( manycore_y_cord_width_gp )
                                       ,.wormhole_width_p( wormhole_packet_width_gp )
                                       ,.wormhole_x_cord_width_p( wormhole_x_cord_width_gp )
                                       ,.wormhole_y_cord_width_p( wormhole_y_cord_width_gp )
                                       ,.wormhole_len_width_p( wormhole_len_width_gp )
                                       ,.wormhole_reserved_width_p( wormhole_reserved_width_gp )
                                       )
    manycore_wormhole_adapter
      (.manycore_clk_i   ( manycore_clk_lo )
      ,.manycore_reset_i ( manycore_reset_lo )

      ,.links_sif_i ( link_sif_lo )
      ,.links_sif_o ( link_sif_li )

      ,.clk_i   ( router_clk_lo )
      ,.reset_i ( router_reset_lo )

      ,.dest_x_i ( manycore_dest_x_lo )
      ,.dest_y_i ( manycore_dest_y_lo )

      ,.link_i ({ fwd_links_lo, rev_links_lo })
      ,.link_o ({ fwd_links_li, rev_links_li })
      );

endmodule

