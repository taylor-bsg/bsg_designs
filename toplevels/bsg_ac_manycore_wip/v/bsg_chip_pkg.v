`ifndef BSG_CHIP_PKG_V
`define BSG_CHIP_PKG_V

package bsg_chip_pkg;

  `include "bsg_defines.v"
  `include "bsg_manycore_packet.vh"
  `include "bsg_cache_dma_pkt.vh"

  //////////////////////////////////////////////////
  //
  // BSG CLOCK GENERATOR PARAMETERS
  //

  localparam clk_gen_num_endpoints_gp = 3;
  localparam clk_gen_ds_width_gp      = 6;
  localparam clk_gen_num_adgs_gp      = 1;

  //////////////////////////////////////////////////
  //
  // BSG MANYCORE PARAMETERS
  //

  localparam manycore_num_tiles_x_gp = 4;
  localparam manycore_num_tiles_y_gp = 5;

  localparam manycore_addr_width_gp = 28;
  localparam manycore_data_width_gp = 32;

  localparam manycore_dmem_size_gp        = 1024;
  localparam manycore_icache_entries_gp   = 1024;
  localparam manycore_icache_tag_width_gp = 12;

  localparam manycore_load_id_width_gp       = 12;
  localparam manycore_epa_byte_addr_width_gp = 18;

  localparam manycore_dram_ch_addr_width_gp = 27;
  localparam manycore_dram_ch_start_col_gp  = 0;

  localparam manycore_extra_io_rows_gp = 1;

  localparam manycore_x_cord_width_gp      = `BSG_SAFE_CLOG2(manycore_num_tiles_x_gp);
  localparam manycore_y_cord_width_gp      = `BSG_SAFE_CLOG2(manycore_num_tiles_y_gp + manycore_extra_io_rows_gp);
  localparam manycore_byte_offset_width_gp = `BSG_SAFE_CLOG2(manycore_data_width_gp>>3);

  localparam manycore_pkt_width_gp        = `bsg_manycore_packet_width(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp);
  localparam manycore_return_pkt_width_gp = `bsg_manycore_return_packet_width(manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_data_width_gp, manycore_load_id_width_gp);

  //////////////////////////////////////////////////
  //
  // VICTIM CACHE PARAMETERS
  //

  localparam vcache_num_cache_gp           = manycore_num_tiles_x_gp;
  localparam vcache_ways_gp                = 2;
  localparam vcache_sets_gp                = 2**8;
  localparam vcache_block_size_in_words_gp = 8;

  //////////////////////////////////////////////////
  //
  // BSG CHIP IO COMPLEX PARAMETERS
  //

  // The number of bits per ddr link channel
  localparam link_channel_width_gp = 8;

  // The number of full-duplex channels per ddr link
  localparam link_num_channel_gp = 1;

  // The number of in streams to the channel tunnel
  localparam channel_tunnel_num_in_gp = 2;

  // Number of bits per wormhole router flit
  localparam wormhole_packet_width_gp = 32; // num bits per flit

  // The width of the x and y coordinate fields in the wormhole router header
  // packet. This will determine the number of ASICs and the topography allowed
  // for chained ASICs on the PCB.
  localparam wormhole_x_cord_width_gp = `BSG_SAFE_CLOG2(16);
  localparam wormhole_y_cord_width_gp = `BSG_SAFE_CLOG2(2);

  // The number of credits each channel tunnel has (increase to reduce stalls)
  localparam wormhole_remote_credits_gp = 48;

  // The len_width is the number of bits used for the length field of the
  // wormhole header. The length is the number of additional flits for the
  // rest of the payload, and the len_width is the number of bits required to
  // represent the max length. This value is difficult to calculate because of
  // a cyclical dependancy (len_width can affect number of flits which can
  // affect len_width).
  //
  // Therefore we set the length manually and use its value to calculate all
  // other values. The len_width simply needs to be sufficiently large enough
  // to store the maximum length value. We can check this after calculating the
  // rest of the parameters therefore an assertion is performed to make sure
  // that the len_width is sufficiently large (inside bsg_chip_io_complex).
  localparam wormhole_len_width_gp = 2;

  // TODO: Explain this parameter
  localparam wormhole_reserved_width_gp = `BSG_SAFE_CLOG2(channel_tunnel_num_in_gp + 1);

  // The number of bits for the entire wormhole packet for a manycore request packet
  localparam wormhole_manycore_req_width_gp  = `bsg_wormhole_packet_width(wormhole_reserved_width_gp,wormhole_x_cord_width_gp,wormhole_y_cord_width_gp,wormhole_len_width_gp,manycore_pkt_width_gp);
  // The number of flits for the entire wormhole packet for a manycore request packet
  localparam wormhole_manycore_req_ratio_gp  = `BSG_CDIV(wormhole_manycore_req_width_gp, wormhole_packet_width_gp);
  // The number of bits for the entire wormhole packet for a manycore response packet
  localparam wormhole_manycore_resp_width_gp = `bsg_wormhole_packet_width(wormhole_reserved_width_gp,wormhole_x_cord_width_gp,wormhole_y_cord_width_gp,wormhole_len_width_gp,manycore_return_pkt_width_gp);
  // The number of flits for the entire wormhole packet for a manycore response packet
  localparam wormhole_manycore_resp_ratio_gp = `BSG_CDIV(wormhole_manycore_resp_width_gp, wormhole_packet_width_gp);

  // The max number of flits for any wormhole packet
  localparam wormhole_max_ratio_gp  = `BSG_MAX(wormhole_manycore_req_ratio_gp, wormhole_manycore_resp_ratio_gp);

  // The max number of flits that only contain the payload (no header data)
  localparam wormhole_max_payload_flits_gp   = wormhole_max_ratio_gp-1;

  //////////////////////////////////////////////////
  //
  // BSG CHIP TAG PARAMETERS AND STRUCTS
  //

  // Total number of clients the master will be driving.
  localparam tag_num_clients_gp = 19;

  localparam tag_max_payload_width_in_io_complex_gp       = (wormhole_x_cord_width_gp + wormhole_y_cord_width_gp +1);
  localparam tag_max_payload_width_in_manycore_complex_gp = (wormhole_x_cord_width_gp + wormhole_y_cord_width_gp +1);
  localparam tag_max_payload_width_in_clk_gen_pd_gp       = `BSG_MAX(clk_gen_ds_width_gp+1, clk_gen_num_adgs_gp+4);

  localparam tag_max_payload_width_gp = `BSG_MAX(tag_max_payload_width_in_io_complex_gp
                                        , `BSG_MAX(tag_max_payload_width_in_manycore_complex_gp
                                        , `BSG_MAX(tag_max_payload_width_in_clk_gen_pd_gp
                                        , 0)));

  // The number of bits required to represent the max payload width
  localparam tag_lg_max_payload_width_gp = `BSG_SAFE_CLOG2(tag_max_payload_width_gp + 1);

  //////////////////////////////////////////////////
  //
  // Interface Struct Declarations
  //

  `declare_bsg_manycore_link_sif_s(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp);
  `declare_bsg_manycore_packet_s(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp);
  `declare_bsg_ready_and_link_sif_s(wormhole_packet_width_gp, bsg_ready_and_link_sif_s);
  `declare_bsg_cache_dma_pkt_s(manycore_addr_width_gp+manycore_byte_offset_width_gp-1);

endpackage // bsg_chip_pkg

`endif // BSG_CHIP_PKG_V

