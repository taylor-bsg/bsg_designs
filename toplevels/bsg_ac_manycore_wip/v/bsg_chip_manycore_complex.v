`include "bsg_noc_pkg.v"

module bsg_chip_manycore_complex

  import bsg_tag_pkg::*;
  import bsg_noc_pkg::*;
  import bsg_chip_pkg::*;

( input  clk_i

, input  bsg_tag_s  manycore_ctrl_tag_lines_i

, output logic                                reset_o
, output logic [wormhole_x_cord_width_gp-1:0] dest_x_o
, output logic [wormhole_y_cord_width_gp-1:0] dest_y_o

, input  bsg_manycore_link_sif_s  io_link_sif_i
, output bsg_manycore_link_sif_s  io_link_sif_o

, output bsg_cache_dma_pkt_s [vcache_num_cache_gp-1:0] dma_pkt_o
, output                     [vcache_num_cache_gp-1:0] dma_pkt_v_o
, input                      [vcache_num_cache_gp-1:0] dma_pkt_yumi_i

, input  [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_i
, input  [vcache_num_cache_gp-1:0]                             dma_data_v_i
, output [vcache_num_cache_gp-1:0]                             dma_data_ready_o

, output [vcache_num_cache_gp-1:0][manycore_data_width_gp-1:0] dma_data_o
, output [vcache_num_cache_gp-1:0]                             dma_data_v_o
, input  [vcache_num_cache_gp-1:0]                             dma_data_yumi_i

);

  genvar i;

  // Tag payload for manycore control signals
  typedef struct packed { 
      logic reset;
      logic [wormhole_x_cord_width_gp-1:0] x;
      logic [wormhole_y_cord_width_gp-1:0] y;
  } tag_manycore_ctrl_payload_s;

  // tag payload for manycore control signals
  tag_manycore_ctrl_payload_s manycore_ctrl_data_lo;
  logic                       manycore_ctrl_new_data_lo;

  bsg_manycore_link_sif_s [E:W][manycore_num_tiles_y_gp-1:0] hor_link_sif_li;
  bsg_manycore_link_sif_s [E:W][manycore_num_tiles_y_gp-1:0] hor_link_sif_lo;

  bsg_manycore_link_sif_s [S:N][manycore_num_tiles_x_gp-1:0] ver_link_sif_li;
  bsg_manycore_link_sif_s [S:N][manycore_num_tiles_x_gp-1:0] ver_link_sif_lo;

  bsg_manycore_link_sif_s [manycore_num_tiles_x_gp-1:0] io_link_sif_li;
  bsg_manycore_link_sif_s [manycore_num_tiles_x_gp-1:0] io_link_sif_lo;

  //////////////////////////////////////////////////
  //
  // BSG Tag Client(s)
  //

  bsg_tag_client #(.width_p( $bits(manycore_ctrl_data_lo) ), .default_p( 0 ))
    btc_manycore_ctrl
      (.bsg_tag_i     ( manycore_ctrl_tag_lines_i )
      ,.recv_clk_i    ( clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( manycore_ctrl_new_data_lo )
      ,.recv_data_r_o ( manycore_ctrl_data_lo )
      );

  assign reset_o  = manycore_ctrl_data_lo.reset;
  assign dest_x_o = manycore_ctrl_data_lo.x;
  assign dest_y_o = manycore_ctrl_data_lo.y;

  //////////////////////////////////////////////////
  //
  // BSG Manycore Instance
  //

  // Manycore instance
  bsg_manycore #(.addr_width_p(manycore_addr_width_gp)
                ,.data_width_p(manycore_data_width_gp)

                ,.dmem_size_p(manycore_dmem_size_gp)
                ,.icache_entries_p(manycore_icache_entries_gp)
                ,.icache_tag_width_p(manycore_icache_tag_width_gp)

                ,.num_tiles_x_p(manycore_num_tiles_x_gp)
                ,.num_tiles_y_p(manycore_num_tiles_y_gp)

                ,.load_id_width_p(manycore_load_id_width_gp)
                ,.epa_byte_addr_width_p(manycore_epa_byte_addr_width_gp)

                ,.dram_ch_addr_width_p(manycore_dram_ch_addr_width_gp)
                ,.dram_ch_start_col_p(manycore_dram_ch_start_col_gp)

                ,.stub_w_p({manycore_num_tiles_y_gp{1'b0}})
                ,.stub_e_p({manycore_num_tiles_y_gp{1'b0}})
                ,.stub_n_p({manycore_num_tiles_x_gp{1'b0}})
                ,.stub_s_p({manycore_num_tiles_x_gp{1'b0}})
                )
    mc
      (.clk_i   ( clk_i )
      ,.reset_i ( manycore_ctrl_data_lo.reset )

      ,.hor_link_sif_i ( hor_link_sif_li )
      ,.hor_link_sif_o ( hor_link_sif_lo )

      ,.ver_link_sif_i ( ver_link_sif_li )
      ,.ver_link_sif_o ( ver_link_sif_lo )

      ,.io_link_sif_i ( io_link_sif_li )
      ,.io_link_sif_o ( io_link_sif_lo )
      );

  // IO links[0] go out of this module
  assign io_link_sif_li[0] = io_link_sif_i;
  assign io_link_sif_o = io_link_sif_lo[0];

  //////////////////////////////////////////////////
  //
  // Victim Cache
  //

  // Pipeline the reset by 2 flip flops (for 16nm layout)
  logic [vcache_num_cache_gp-1:0] vc_reset_r, vc_reset_rr;

  always_ff @(posedge clk_i)
    begin
      vc_reset_r <= {vcache_num_cache_gp{manycore_ctrl_data_lo.reset}};
      vc_reset_rr <= vc_reset_r;
    end

  for (i=0; i < vcache_num_cache_gp; i++)
    begin: rof
      vcache #(.data_width_p(manycore_data_width_gp)
              ,.addr_width_p(manycore_addr_width_gp)
              ,.block_size_in_words_p(vcache_block_size_in_words_gp)
              ,.sets_p(vcache_sets_gp)
              ,.ways_p(vcache_ways_gp)
              ,.x_cord_width_p(manycore_x_cord_width_gp)
              ,.y_cord_width_p(manycore_y_cord_width_gp)
              ,.load_id_width_p(manycore_load_id_width_gp)
              )
        vc
          (.clk_i   ( clk_i   )
          ,.reset_i ( vc_reset_rr[i] )

          // manycore side
          ,.my_x_i ( (manycore_x_cord_width_gp)'(i)                       )
          ,.my_y_i ( (manycore_y_cord_width_gp)'(manycore_num_tiles_y_gp) )

          ,.link_sif_i ( ver_link_sif_lo[S][i] )
          ,.link_sif_o ( ver_link_sif_li[S][i] )
          
          // cache dma side
          ,.dma_pkt_o        ( dma_pkt_o[i]        )
          ,.dma_pkt_v_o      ( dma_pkt_v_o[i]      )
          ,.dma_pkt_yumi_i   ( dma_pkt_yumi_i[i]   )
          ,.dma_data_i       ( dma_data_i[i]       )
          ,.dma_data_v_i     ( dma_data_v_i[i]     )
          ,.dma_data_ready_o ( dma_data_ready_o[i] )
          ,.dma_data_o       ( dma_data_o[i]       )
          ,.dma_data_v_o     ( dma_data_v_o[i]     )
          ,.dma_data_yumi_i  ( dma_data_yumi_i[i]  )
          );
    end: rof

  //////////////////////////////////////////////////
  //
  // Link Tieoffs (io, N, S, W, E)
  //

  // io link tieoffs
  for (i=1; i < manycore_num_tiles_x_gp; i++)
    begin: io
      bsg_manycore_link_sif_tieoff #(.addr_width_p(manycore_addr_width_gp)
                                    ,.data_width_p(manycore_data_width_gp)
                                    ,.x_cord_width_p(manycore_x_cord_width_gp)
                                    ,.y_cord_width_p(manycore_y_cord_width_gp)
                                    ,.load_id_width_p(manycore_load_id_width_gp)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( manycore_ctrl_data_lo.reset )

          ,.link_sif_i ( io_link_sif_lo[i] )
          ,.link_sif_o ( io_link_sif_li[i] )
          );
    end: io

  // north-side link tieoffs
  for (i=0; i < manycore_num_tiles_x_gp; i++)
    begin: north
      bsg_manycore_link_sif_tieoff #(.addr_width_p(manycore_addr_width_gp)
                                    ,.data_width_p(manycore_data_width_gp)
                                    ,.x_cord_width_p(manycore_x_cord_width_gp)
                                    ,.y_cord_width_p(manycore_y_cord_width_gp)
                                    ,.load_id_width_p(manycore_load_id_width_gp)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( manycore_ctrl_data_lo.reset )

          ,.link_sif_i ( ver_link_sif_lo[N][i] )
          ,.link_sif_o ( ver_link_sif_li[N][i] )
          );
    end: north

  // south-side link tieoffs
  for (i=vcache_num_cache_gp; i < manycore_num_tiles_x_gp; i++)
    begin: south
      bsg_manycore_link_sif_tieoff #(.addr_width_p(manycore_addr_width_gp)
                                    ,.data_width_p(manycore_data_width_gp)
                                    ,.x_cord_width_p(manycore_x_cord_width_gp)
                                    ,.y_cord_width_p(manycore_y_cord_width_gp)
                                    ,.load_id_width_p(manycore_load_id_width_gp)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( manycore_ctrl_data_lo.reset )

          ,.link_sif_i ( ver_link_sif_lo[S][i] )
          ,.link_sif_o ( ver_link_sif_li[S][i] )
          );
    end: south

  // west-side link tieoffs
  for (i=0; i < manycore_num_tiles_y_gp; i++)
    begin: west
      bsg_manycore_link_sif_tieoff #(.addr_width_p(manycore_addr_width_gp)
                                    ,.data_width_p(manycore_data_width_gp)
                                    ,.x_cord_width_p(manycore_x_cord_width_gp)
                                    ,.y_cord_width_p(manycore_y_cord_width_gp)
                                    ,.load_id_width_p(manycore_load_id_width_gp)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( manycore_ctrl_data_lo.reset )

          ,.link_sif_i ( hor_link_sif_lo[W][i] )
          ,.link_sif_o ( hor_link_sif_li[W][i] )
          );
    end: west

  // east-side link tieoffs
  for (i=0; i < manycore_num_tiles_y_gp; i++)
    begin: east
      bsg_manycore_link_sif_tieoff #(.addr_width_p(manycore_addr_width_gp)
                                    ,.data_width_p(manycore_data_width_gp)
                                    ,.x_cord_width_p(manycore_x_cord_width_gp)
                                    ,.y_cord_width_p(manycore_y_cord_width_gp)
                                    ,.load_id_width_p(manycore_load_id_width_gp)
                                    )
        tieoff
          (.clk_i   ( clk_i   )
          ,.reset_i ( manycore_ctrl_data_lo.reset )

          ,.link_sif_i ( hor_link_sif_lo[E][i] )
          ,.link_sif_o ( hor_link_sif_li[E][i] )
          );
    end: east

endmodule

