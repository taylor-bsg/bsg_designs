
////////////////////////////////////////////////////////////////////////////////
//
// bsg_chip_io_complex
//
// This is the chip's main IO logic. This IO complex has 2 routers for forward
// packets (where the chip is acting as a master node) and reverse packets
// (where the chip is responding to another master node). The packets from both
// routers get merged via channel tunnels that then go into the DDR links.
//

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BSG CHIP IO COMPLEX DATAPATH                                                                                                  //
//                                    +-----+                                         +-----+                                    //
//        +-----------------+         |  C  |       +-----------+                     |  C  |         +-----------------+        //
//        |                 |         |  H  |       |           |                     |  H  |         |                 |        //
//  /---  |  PREV UPSTREAM  | <--+    |  A  | <===> W  FWD RTR  E <=================> |  A  |    +--> |  NEXT UPSTREAM  |  ---\  //
//  \---  |    DDR LINK     |    |    |  N  |       |           |                     |  N  |    |    |    DDR LINK     |  ---/  //
//        |                 |    |    |  N  |       +-----P-----+                     |  N  |    |    |                 |        //
//        +-----------------+    +--- |  E  |             ^                           |  E  | ---+    +-----------------+        //
//                                    |  L  |             |       +-----------+       |  L  |                                    //
//        +-----------------+    +--> |     |             |       |           |       |     | <--+    +-----------------+        //
//        |                 |    |    |  T  | <=================> W  REV RTR  E <===> |  T  |    |    |                 |        //
//  ---\  | PREV DOWNSTREAM |    |    |  U  |             |       |           |       |  U  |    |    | NEXT DOWNSTREAM |  /---  //
//  ---/  |    DDR LINK     | ---+    |  N  |             |       +-----P-----+       |  N  |    +--- |    DDR LINK     |  \---  //
//        |                 |         |  N  |             |             ^             |  N  |         |                 |        //
//        +-----------------+         |  E  |             |             |             |  E  |         +-----------------+        //
//                                    |  L  |             |             |             |  L  |                                    //
//                                    +-----+             V             V             +-----+                                    //
//                                                                                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//
// Tag Reset Sequence
//
// We have 5 tag clients all required to get the io out of reset properly. It
// is important to follow the seqence:
//
//  1. Assert reset for all synchronous reset (ie. not the async_token_reset)
//    1a. You should set the router x and y at the same
//    1b. You should make sure all async_token_reset are low
//  time as you assert reset.
//  2. Assert all async_token_reset
//  3. Deassert all async_token_reset
//  4. Deassert all io up_link_reset*
//  5. Deassert all io down_link_reset
//  6. Deassert all core up_link_reset**
//  7. Deassert all core down_link_reset**
//  8. Deassert router reset
//
// Note: "all" means prev and next, however if there is nothing attached to the
// prev or next links, you should keep that link in reset.
//
// * - After you take the up links out of reset, there are some number of
// cycles for the comm link clocks to start generating. These comm link clocks
// are what the io_down_link_reset get's synchronized to so it may be required
// to wait a few cycles to make sure the clock start up.
//
// ** - The order of these shouldn't matter
//

module bsg_chip_io_complex

import bsg_tag_pkg::*;
import bsg_noc_pkg::*;

#(parameter link_channel_width_p = -1

, parameter wormhole_packet_width_p = -1
, parameter wormhole_x_cord_width_p = -1
, parameter wormhole_y_cord_width_p = -1
, parameter wormhole_len_width_p = -1
, parameter wormhole_reserved_width_p = -1
, parameter wormhole_remote_credits_p = -1
, parameter wormhole_max_payload_flits_p = -1

, parameter bsg_ready_and_link_sif_width_lp = `bsg_ready_and_link_sif_width(wormhole_packet_width_p)
)

( input  core_clk_i
, input  io_clk_i

, output logic core_reset_o

// This module uses 5 tag clients
, input bsg_tag_s  prev_io_link_ctrl_tag_lines_i     // 3 bits
, input bsg_tag_s  prev_core_link_ctrl_tag_lines_i   // 2 bits
, input bsg_tag_s  next_io_link_ctrl_tag_lines_i     // 3 bits
, input bsg_tag_s  next_core_link_ctrl_tag_lines_i   // 2 bits
, input bsg_tag_s  router_ctrl_tag_lines_i           // (wh_x_cord_width + wh_y_cord_width + 1) bits

// comm link connection to next chip
, input                             ci_clk_i
, input                             ci_v_i
, input  [link_channel_width_p-1:0] ci_data_i
, output                            ci_tkn_o

, output                            co_clk_o
, output                            co_v_o
, output [link_channel_width_p-1:0] co_data_o
, input                             co_tkn_i

// comm link connection to prev chip
, input                             ci2_clk_i
, input                             ci2_v_i
, input  [link_channel_width_p-1:0] ci2_data_i
, output                            ci2_tkn_o

, output                            co2_clk_o
, output                            co2_v_o
, output [link_channel_width_p-1:0] co2_data_o
, input                             co2_tkn_i

// forward router links (we are master)
, input   [bsg_ready_and_link_sif_width_lp-1:0]  fwd_links_i
, output  [bsg_ready_and_link_sif_width_lp-1:0]  fwd_links_o

// reverse router links (we are client)
, input   [bsg_ready_and_link_sif_width_lp-1:0]  rev_links_i
, output  [bsg_ready_and_link_sif_width_lp-1:0]  rev_links_o
);

  // Tag payload for io link control signals
  typedef struct packed { 
      logic up_link_reset;
      logic down_link_reset;
      logic async_token_reset;
  } tag_io_link_ctrl_payload_s;

  // Tag payload for core link control signals
  typedef struct packed { 
      logic up_link_reset;
      logic down_link_reset;
  } tag_core_link_ctrl_payload_s;

  // Tag payload for router control signals
  typedef struct packed { 
      logic reset;
      logic [wormhole_x_cord_width_p-1:0] x;
      logic [wormhole_y_cord_width_p-1:0] y;
  } tag_router_ctrl_payload_s;

  // tag payload for prev link io clk control signals
  tag_io_link_ctrl_payload_s prev_io_link_ctrl_data_lo;
  logic                      prev_io_link_ctrl_new_data_lo;

  // tag payload for prev link core clk control signals
  tag_core_link_ctrl_payload_s prev_core_link_ctrl_data_lo;
  logic                        prev_core_link_ctrl_new_data_lo;

  // tag payload for next link io clk control signals
  tag_io_link_ctrl_payload_s next_io_link_ctrl_data_lo;
  logic                      next_io_link_ctrl_new_data_lo;

  // tag payload for next link core clk control signals
  tag_core_link_ctrl_payload_s next_core_link_ctrl_data_lo;
  logic                        next_core_link_ctrl_new_data_lo;

  // tag payload for router control signals
  tag_router_ctrl_payload_s router_ctrl_data_lo;
  logic                     router_ctrl_new_data_lo;

  // downstream_io_link_reset_i syncronized to the ci and ci2 comm link clocks
  logic  ci_link_reset_lo;
  logic  ci2_link_reset_lo;

  // prev_link -> prev_ct
  logic                               prev_link_v_lo;
  logic [wormhole_packet_width_p-1:0] prev_link_data_lo;
  logic                               prev_ct_ready_lo;

  // prev_ct -> prev_link
  logic                               prev_ct_v_lo;
  logic [wormhole_packet_width_p-1:0] prev_ct_data_lo;
  logic                               prev_link_ready_lo;

  // next_link -> next_ct
  logic                               next_link_v_lo;
  logic [wormhole_packet_width_p-1:0] next_link_data_lo;
  logic                               next_ct_ready_lo;

  // next_ct -> next_link
  logic                               next_ct_v_lo;
  logic [wormhole_packet_width_p-1:0] next_ct_data_lo;
  logic                               next_link_ready_lo;

  // declare the bsg_ready_and_link_sif_s struct
  `declare_bsg_ready_and_link_sif_s(wormhole_packet_width_p, bsg_ready_and_link_sif_s);

  // forward wormhole router links (P leaves this module)
  bsg_ready_and_link_sif_s [E:P] fwd_rtr_link_li;
  bsg_ready_and_link_sif_s [E:P] fwd_rtr_link_lo;

  assign fwd_rtr_link_li[P] = fwd_links_i;
  assign fwd_links_o = fwd_rtr_link_lo[P];

  // reverse wormhole router links (P leaves this module)
  bsg_ready_and_link_sif_s [E:P] rev_rtr_link_li;
  bsg_ready_and_link_sif_s [E:P] rev_rtr_link_lo;

  assign rev_rtr_link_li[P] = rev_links_i;
  assign rev_links_o = rev_rtr_link_lo[P];

  //////////////////////////////////////////////////
  //
  // BSG Tag Client(s)
  //

  bsg_tag_client #(.width_p( $bits(prev_io_link_ctrl_data_lo) ), .default_p( 0 ))
    btc_prev_io_link_ctrl
      (.bsg_tag_i     ( prev_io_link_ctrl_tag_lines_i )
      ,.recv_clk_i    ( io_clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( prev_io_link_ctrl_new_data_lo )
      ,.recv_data_r_o ( prev_io_link_ctrl_data_lo )
      );

  bsg_tag_client #(.width_p( $bits(prev_core_link_ctrl_data_lo) ), .default_p( 0 ))
    btc_prev_core_link_ctrl
      (.bsg_tag_i     ( prev_core_link_ctrl_tag_lines_i )
      ,.recv_clk_i    ( core_clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( prev_core_link_ctrl_new_data_lo )
      ,.recv_data_r_o ( prev_core_link_ctrl_data_lo )
      );

  bsg_tag_client #(.width_p( $bits(next_io_link_ctrl_data_lo) ), .default_p( 0 ))
    btc_next_io_link_ctrl
      (.bsg_tag_i     ( next_io_link_ctrl_tag_lines_i )
      ,.recv_clk_i    ( io_clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( next_io_link_ctrl_new_data_lo )
      ,.recv_data_r_o ( next_io_link_ctrl_data_lo )
      );

  bsg_tag_client #(.width_p( $bits(next_core_link_ctrl_data_lo) ), .default_p( 0 ))
    btc_next_core_link_ctrl
      (.bsg_tag_i     ( next_core_link_ctrl_tag_lines_i )
      ,.recv_clk_i    ( core_clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( next_core_link_ctrl_new_data_lo )
      ,.recv_data_r_o ( next_core_link_ctrl_data_lo )
      );

  bsg_tag_client #(.width_p( $bits(router_ctrl_data_lo) ), .default_p( 0 ))
    btc_router_ctrl
      (.bsg_tag_i     ( router_ctrl_tag_lines_i )
      ,.recv_clk_i    ( core_clk_i )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( router_ctrl_new_data_lo )
      ,.recv_data_r_o ( router_ctrl_data_lo )
      );

  assign core_reset_o = router_ctrl_data_lo.reset;  // used outside this module

  //////////////////////////////////////////////////
  //
  // BSG Link DDR (To Previous Chip)
  //

  // UPSTREAM
  bsg_link_ddr_upstream #(.width_p( wormhole_packet_width_p )
                         ,.channel_width_p( link_channel_width_p )
                         ,.num_channels_p( 1 )
                         ,.lg_fifo_depth_p( 6 )
                         ,.lg_credit_to_token_decimation_p( 3 )
                         )
    prev_uplink
      (.core_clk_i        ( core_clk_i )
      ,.core_link_reset_i ( prev_core_link_ctrl_data_lo.up_link_reset )

      ,.core_data_i  ( prev_ct_data_lo )
      ,.core_valid_i ( prev_ct_v_lo )
      ,.core_ready_o ( prev_link_ready_lo )

      ,.io_clk_i            ( io_clk_i )
      ,.io_link_reset_i     ( prev_io_link_ctrl_data_lo.up_link_reset )
      ,.async_token_reset_i ( prev_io_link_ctrl_data_lo.async_token_reset )

      ,.io_clk_r_o   ( co2_clk_o )
      ,.io_data_r_o  ( co2_data_o )
      ,.io_valid_r_o ( co2_v_o )
      ,.token_clk_i  ( co2_tkn_i )
      );

  // DOWNSTREAM
  bsg_sync_sync #(.width_p( 1 ))
    prev_downlink_io_reset_sync_sync
      (.oclk_i      ( ci2_clk_i )
      ,.iclk_data_i ( prev_io_link_ctrl_data_lo.down_link_reset )
      ,.oclk_data_o ( ci2_link_reset_lo )
      );

  bsg_link_ddr_downstream #(.width_p( wormhole_packet_width_p )
                           ,.channel_width_p( link_channel_width_p )
                           ,.num_channels_p( 1 )
                           ,.lg_fifo_depth_p( 6 )
                           ,.lg_credit_to_token_decimation_p( 3 )
                           )
    prev_downlink
      (.core_clk_i        ( core_clk_i )
      ,.core_link_reset_i ( prev_core_link_ctrl_data_lo.down_link_reset )

      ,.io_link_reset_i ( ci2_link_reset_lo )

      ,.core_data_o  ( prev_link_data_lo )
      ,.core_valid_o ( prev_link_v_lo )
      ,.core_yumi_i  ( prev_link_v_lo & prev_ct_ready_lo )

      ,.io_clk_i       ( ci2_clk_i )
      ,.io_data_i      ( ci2_data_i )
      ,.io_valid_i     ( ci2_v_i )
      ,.core_token_r_o ( ci2_tkn_o )
      );

  //////////////////////////////////////////////////
  //
  // BSG Channel Tunnel Wormhole (To Previous Chip Link)
  //

  bsg_channel_tunnel_wormhole #(.width_p( wormhole_packet_width_p )
                               ,.x_cord_width_p( wormhole_x_cord_width_p )
                               ,.y_cord_width_p( wormhole_y_cord_width_p )
                               ,.len_width_p( wormhole_len_width_p )
                               ,.reserved_width_p( wormhole_reserved_width_p )
                               ,.remote_credits_p( wormhole_remote_credits_p )
                               ,.max_payload_flits_p( wormhole_max_payload_flits_p )
                               ,.num_in_p( 2 )
                               ,.lg_credit_decimation_p( 3 )
                               )
    prev_ct
      (.clk_i   ( core_clk_i )
      ,.reset_i ( router_ctrl_data_lo.reset )

      ,.multi_v_i     ( prev_link_v_lo )
      ,.multi_data_i  ( prev_link_data_lo )
      ,.multi_ready_o ( prev_ct_ready_lo )

      ,.multi_v_o    ( prev_ct_v_lo )
      ,.multi_data_o ( prev_ct_data_lo )
      ,.multi_yumi_i ( prev_ct_v_lo & prev_link_ready_lo )

      ,.link_i ({ fwd_rtr_link_lo[W], rev_rtr_link_lo[W] })
      ,.link_o ({ fwd_rtr_link_li[W], rev_rtr_link_li[W] })
      );

  //////////////////////////////////////////////////
  //
  // BSG Wormhole Router (Chip acting as master node)
  //

  bsg_wormhole_router #(.width_p( wormhole_packet_width_p )
                       ,.x_cord_width_p( wormhole_x_cord_width_p )
                       ,.y_cord_width_p( wormhole_y_cord_width_p )
                       ,.len_width_p( wormhole_len_width_p )
                       ,.reserved_width_p( wormhole_reserved_width_p )
                       ,.enable_2d_routing_p( 1'b0 )
                       ,.stub_in_p( 3'b000 )
                       ,.stub_out_p( 3'b000 )
                       )
    fwd_rtr
      (.clk_i   ( core_clk_i )
      ,.reset_i ( router_ctrl_data_lo.reset )

      ,.my_x_i ( router_ctrl_data_lo.x )
      ,.my_y_i ( router_ctrl_data_lo.y )

      ,.link_i ( fwd_rtr_link_li )
      ,.link_o ( fwd_rtr_link_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Wormhole Router (Chip acting as client node)
  //

  bsg_wormhole_router #(.width_p( wormhole_packet_width_p )
                       ,.x_cord_width_p( wormhole_x_cord_width_p )
                       ,.y_cord_width_p( wormhole_y_cord_width_p )
                       ,.len_width_p( wormhole_len_width_p )
                       ,.reserved_width_p( wormhole_reserved_width_p )
                       ,.enable_2d_routing_p( 1'b0 )
                       ,.stub_in_p( 3'b000 )
                       ,.stub_out_p( 3'b000 )
                       )
    rev_rtr
      (.clk_i   ( core_clk_i )
      ,.reset_i ( router_ctrl_data_lo.reset )

      ,.my_x_i ( router_ctrl_data_lo.x )
      ,.my_y_i ( router_ctrl_data_lo.y )

      ,.link_i ( rev_rtr_link_li )
      ,.link_o ( rev_rtr_link_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Channel Tunnel Wormhole (To Next Chip Link)
  //

  bsg_channel_tunnel_wormhole #(.width_p( wormhole_packet_width_p )
                               ,.x_cord_width_p( wormhole_x_cord_width_p )
                               ,.y_cord_width_p( wormhole_y_cord_width_p )
                               ,.len_width_p( wormhole_len_width_p )
                               ,.reserved_width_p( wormhole_reserved_width_p )
                               ,.remote_credits_p( wormhole_remote_credits_p )
                               ,.max_payload_flits_p( wormhole_max_payload_flits_p )
                               ,.num_in_p( 2 )
                               ,.lg_credit_decimation_p( 3 )
                               )
    next_ct
      (.clk_i   ( core_clk_i )
      ,.reset_i ( router_ctrl_data_lo.reset )

      ,.multi_v_i     ( next_link_v_lo )
      ,.multi_data_i  ( next_link_data_lo )
      ,.multi_ready_o ( next_ct_ready_lo )

      ,.multi_v_o    ( next_ct_v_lo )
      ,.multi_data_o ( next_ct_data_lo )
      ,.multi_yumi_i ( next_ct_v_lo & next_link_ready_lo )

      ,.link_i ({ fwd_rtr_link_lo[E], rev_rtr_link_lo[E] })
      ,.link_o ({ fwd_rtr_link_li[E], rev_rtr_link_li[E] })
      );

  //////////////////////////////////////////////////
  //
  // BSG Link DDR (To Next Chip)
  //

  // UPSTREAM
  bsg_link_ddr_upstream #(.width_p( wormhole_packet_width_p )
                         ,.channel_width_p( link_channel_width_p )
                         ,.num_channels_p( 1 )
                         ,.lg_fifo_depth_p( 6 )
                         ,.lg_credit_to_token_decimation_p( 3 )
                         )
    next_uplink
      (.core_clk_i        ( core_clk_i )
      ,.core_link_reset_i ( next_core_link_ctrl_data_lo.up_link_reset )

      ,.core_valid_i ( next_ct_v_lo )
      ,.core_data_i  ( next_ct_data_lo )
      ,.core_ready_o ( next_link_ready_lo )

      ,.io_clk_i            ( io_clk_i )
      ,.io_link_reset_i     ( next_io_link_ctrl_data_lo.up_link_reset )
      ,.async_token_reset_i ( next_io_link_ctrl_data_lo.async_token_reset )

      ,.io_clk_r_o   ( co_clk_o )
      ,.io_data_r_o  ( co_data_o )
      ,.io_valid_r_o ( co_v_o )
      ,.token_clk_i  ( co_tkn_i )
      );

  // DOWNSTREAM
  bsg_sync_sync #(.width_p( 1 ))
    next_downlink_io_reset_sync_sync
      (.oclk_i      ( ci_clk_i )
      ,.iclk_data_i ( next_io_link_ctrl_data_lo.down_link_reset )
      ,.oclk_data_o ( ci_link_reset_lo )
      );

  bsg_link_ddr_downstream #(.width_p( wormhole_packet_width_p )
                           ,.channel_width_p( link_channel_width_p )
                           ,.num_channels_p( 1 )
                           ,.lg_fifo_depth_p( 6 )
                           ,.lg_credit_to_token_decimation_p( 3 )
                           )
    next_downlink
      (.core_clk_i        ( core_clk_i )
      ,.core_link_reset_i ( next_core_link_ctrl_data_lo.down_link_reset )

      ,.io_link_reset_i ( ci_link_reset_lo )

      ,.core_valid_o ( next_link_v_lo )
      ,.core_data_o  ( next_link_data_lo )
      ,.core_yumi_i  ( next_link_v_lo & next_ct_ready_lo )

      ,.io_clk_i       ( ci_clk_i )
      ,.io_data_i      ( ci_data_i )
      ,.io_valid_i     ( ci_v_i )
      ,.core_token_r_o ( ci_tkn_o )
      );

endmodule

