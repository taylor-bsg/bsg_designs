
proc bsg_place_macro { name x_origin y_origin orientation {x_cord 0} {y_cord 0} } {
    set obj [get_cells -all $name]
    set_attribute -quiet $obj origin [list $x_origin $y_origin ]
    set_attribute -quiet $obj orientation $orientation
    set_attribute -quiet $obj is_placed true
    set_attribute -quiet $obj is_fixed true
    set_attribute -quiet $obj is_soft_fixed false
    set_attribute -quiet $obj eco_status eco_reset

    set macro_bbox [get_attribute $obj bbox]
    set macro_ll_x [lindex [lindex $macro_bbox 0] 0]
    set macro_ll_y [lindex [lindex $macro_bbox 0] 1]
    set macro_ur_x [lindex [lindex $macro_bbox 1] 0]
    set macro_ur_y [lindex [lindex $macro_bbox 1] 1]

    set pr_blk_pad 10.0
    create_route_guide \
      -name preroute_block_$name \
      -coordinate [list [expr $macro_ll_x - $pr_blk_pad] [expr $macro_ll_y - $pr_blk_pad] [expr $macro_ur_x + $pr_blk_pad] [expr $macro_ur_y + $pr_blk_pad]] \
      -no_preroute_layers {METAL1} \
      -no_snap

    set place_blk_pad 10.0
		create_placement_blockage \
      -coordinate [list [expr $macro_ll_x - $place_blk_pad] [expr $macro_ll_y - $place_blk_pad] [expr $macro_ur_x + $place_blk_pad] [expr $macro_ll_y]] \
      -name "${name}_x_${x_cord}_y_${y_cord}_bottom_blockage"
		create_placement_blockage \
      -coordinate [list [expr $macro_ll_x - $place_blk_pad] [expr $macro_ur_y] [expr $macro_ur_x + $place_blk_pad] [expr $macro_ur_y + $place_blk_pad]] \
      -name "${name}_x_${x_cord}_y_${y_cord}_top_blockage"
		create_placement_blockage \
      -coordinate [list [expr $macro_ll_x - $place_blk_pad] [expr $macro_ll_y - $place_blk_pad] [expr $macro_ll_x] [expr $macro_ur_y + $place_blk_pad]] \
      -name "${name}_x_${x_cord}_y_${y_cord}_left_blockage"
		create_placement_blockage \
      -coordinate [list [expr $macro_ur_x] [expr $macro_ll_y - $place_blk_pad] [expr $macro_ur_x + $place_blk_pad] [expr $macro_ur_y + $place_blk_pad]] \
      -name "${name}_x_${x_cord}_y_${y_cord}_right_blockage"
}

proc bsg_place_rocket { prefix x_pos y_pos } {

    set index 0
    foreach x [collection_to_list -no_braces -name_only [get_flat_cells  ${prefix}/RocketTile/icache/icache/T*/mem/macro_mem]] {
        bsg_place_macro $x $x_pos [expr $y_pos + 340*$index] N
        set index [expr $index + 1]
    }

    set index 0
    foreach x [collection_to_list -no_braces -name_only [get_flat_cells  ${prefix}/RocketTile/dcache/data/T*/mem/macro_mem]] {
        bsg_place_macro $x [expr $x_pos + 2250 + 600*($index & 1)] [expr $y_pos + 840*($index >> 1)] N
        set index [expr $index + 1]
    }


    # *icache/tag_array/mem/macro_mem*
    bsg_place_macro ${prefix}/RocketTile/icache/icache/tag_array/mem/macro_mem [expr $x_pos + 1250] [expr 200 + $y_pos] N

    # *dcache/meta/tag_arr/mem/macro_mem*
    bsg_place_macro ${prefix}/RocketTile/dcache/meta/tag_arr/mem/macro_mem [expr $y_pos + 1750] [expr 200 + $y_pos] N

}

proc bsg_place_rocket_tunnel { prefix } {
    # *mem_1srw/macro_mem
    bsg_place_macro ${prefix}/bcti/b1_ntf/rof_0__psdlrg_fifo/big1p/mem_1srw/macro_mem 2270 448 W
    bsg_place_macro ${prefix}/bcti/b1_ntf/rof_1__psdlrg_fifo/big1p/mem_1srw/macro_mem 2618 800 E
}

bsg_place_rocket g/n_0__clnt/r2f/rocket 800 700
bsg_place_rocket_tunnel g/n_0__clnt/r2f/tunnel

#create_placement_blockage -coordinate {{449.710 1380.140} {930.850 1420.460}} -name L1_data_blockage

set_dont_touch_placement [all_macro_cells]




