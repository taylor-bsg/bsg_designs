
set xDim 1

if {$xDim == 1} {
    bsg_place_comm_link_rotate 2950 [expr $bsg_max_y-250] 2950 $bsg_min_y 1525 [expr $bsg_max_y-250] 1525 $bsg_min_y \
       	                       3475 [expr $bsg_max_y-95]  3475 $bsg_min_y 1074 [expr $bsg_max_y-95]  1074 $bsg_min_y \
                               [expr $bsg_max_x-350] 3383 [expr $bsg_max_x-350] 2030 $bsg_min_x 3383 $bsg_min_x 2030 \
  	                       1969.94 2648.78 2777.77 3479.05
} else {
    bsg_place_comm_link_rotate 2950 [expr $bsg_max_y-250] 3500 $bsg_min_y 1525 [expr $bsg_max_y-250] 1015 $bsg_min_y \
       	                       3475 [expr $bsg_max_y-95]  3950 $bsg_min_y 1074 [expr $bsg_max_y-95]  564 $bsg_min_y \
                               [expr $bsg_max_x-350] 2785 [expr $bsg_max_x-350] 2030 $bsg_min_x 2785 $bsg_min_x 2030 \
  	                       1969.94 2648.78 2777.77 3479.05

#    bsg_place_comm_link_rotate 2950 [expr $bsg_max_y-250] 3300 $bsg_min_y 1525 [expr $bsg_max_y-250] 1125 $bsg_min_y \
#       	                       3475 [expr $bsg_max_y-95]  3750 $bsg_min_y 1074 [expr $bsg_max_y-95]  674 $bsg_min_y \
#                               [expr $bsg_max_x-350] 3383 [expr $bsg_max_x-350] 2030 $bsg_min_x 3383 $bsg_min_x 2030 \
#  	                       1969.94 2648.78 2777.77 3479.05

}

#bsg_place_comm_link_guts 2150 2150 1
bsg_place_comm_link_guts 2150 2100 0

#bsg_place_comm_link 2880 2300 2880 2300 \
#                    1810 1810 2510 2510 \
#                    1969.94 2648.78 2777.77 3479.05



# bound the channel tunnel

if {$xDim == 1} {
    bsg_bound_creator_abs g/n_0_*_clnt/r2f/tunnel 2274 432 2616 782
} else {
    bsg_bound_creator_abs g/n_0_*_clnt/r2f/tunnel 2074 432 2906 760
}

#bsg_bound_creator g/comm_link/fsb/fsb_node_0 1930 1990 1800 450

# PLL Voltage Area Movebounds
#
#bsg_place_clk_gen clk_gen_core_inst 515 2554
#bsg_place_clk_gen clk_gen_iom_inst  515 2438.2
