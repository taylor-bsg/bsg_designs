`ifndef BSG_MANYCORE_PKG_V
`define BSG_MANYCORE_PKG_V

package bsg_chip_pkg;
   // 180: 1024x32 --> 278K um^2  -> 270 um^2 per word
   //      2048x32 --> 478K um^2  -> 233 um^2 per word (16 percent more efficient)
   
   // for both bsg_test_node_master and bsg_test_node_client
   parameter bank_size_gp          = 1024; // in words
   parameter bank_num_gp           = 1;    // number of banks

//   parameter imem_size_gp          = 2048; // in words (e.g. instructions)
   parameter imem_size_gp          = 1024; // in words (e.g. instructions)
   parameter addr_width_gp         = 20;
   parameter data_width_gp         = 32;
   parameter hetero_type_vec_gp    = 0;
   parameter fsb_remote_credits_gp = 128;

   //The number of tiles
   `ifdef bsg_tiles_X
        parameter num_tiles_x_gp        =  `bsg_tiles_X ;
   `else
        parameter num_tiles_x_gp        =  2 ;
   `endif

   `ifdef bsg_tiles_Y
        parameter num_tiles_y_gp        =  `bsg_tiles_Y ;
   `else
        parameter num_tiles_y_gp        =  5 ;
   `endif

   //the number of the rocket cores
   `ifdef bsg_rocc_num
        parameter num_rockets_gp         = `bsg_rocc_num ;
   `else
        parameter num_rockets_gp         = 2 ;
   `endif

   // for bsg_test_node_master
   parameter tile_id_ptr_gp  = -1;
   parameter max_cycles_gp   = 500000;

   parameter mem_size_gp     = (bank_num_gp*bank_size_gp+imem_size_gp)*4; // fixme: calc from bank size and num?
    
   `ifdef bsg_rocc_num
        parameter rocc_num_gp      = `bsg_rocc_num ;
   `else
        parameter rocc_num_gp      = 2 ;
   `endif

   //The distribution of the rocc interface.
   //1. Non-zero value is the index of the rocc interface,
   //   starting from 1.
   //2. for example, {32'h0000_2010}
   //   indicates there are two rocc interface, their x_cords are 1 and 3.
   //3. rocc_num_p must not bigger than tiles_x
   `ifdef bsg_rocc_dist_vec
        parameter rocc_dist_vec_gp = `bsg_rocc_dist_vec ;
   `else
        parameter rocc_dist_vec_gp = 64'h0000_0000_0000_0021 ;
   `endif
endpackage

`endif
