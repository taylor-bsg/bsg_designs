#------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#------------------------------------------------------------
# File: include.tcl
#
# Authors: Luis Vega - lvgutierrez@eng.ucsd.edu
#------------------------------------------------------------

set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_packaging_dir $::env(BSG_PACKAGING_DIR)

set SVERILOG_INCLUDE_PATHS [join "
  $bsg_ip_cores_dir/bsg_misc
  $bsg_packaging_dir/ucsd_bga_332/pinouts/bsg_one/verilog
  $bsg_packaging_dir/common/verilog
  $bsg_packaging_dir/common/foundry/portable/verilog
"]
