#-------------------------------------------------------------------------------
# University of California, San Diego - Bespoke Systems Group
#-------------------------------------------------------------------------------
# File: constraints.tcl
#
# Author: Luis Vega
#-------------------------------------------------------------------------------

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_timing_constraint.tcl

bsg_chip_timing_constraint \
  -package ucsd_bsg_332 \
  -reset_port [get_ports p_reset_i] \
  -core_clk_port [get_ports p_misc_L_i[3]] \
  -core_clk_name core_clk \
  -core_clk_period 3.5 \
  -master_io_clk_port [get_ports p_PLL_CLK_i] \
  -master_io_clk_name master_io_clk \
  -master_io_clk_period 2.35
