//
// here is a simple example of a gateway node that drives the ASIC
// it uses the trace replay facility, which is a general purpose
// way of testing node that uses a text file compiled into binary
//

module bsg_frame_gateway #(ring_width_p="inv"
                        ,nodes_p="inv"
                        ,bsg_fsb_in_s_width_lp      = `bsg_fsb_in_s_width (ring_width_lp)
                        ,bsg_fsb_out_s_width_lp     = `bsg_fsb_out_s_width(ring_width_lp)
                        )
   (input clk_i
    ,input  [nodes_p-1:0][bsg_fsb_in_s_width_lp -1:0] fsb_i
    ,output [nodes_p-1:0][bsg_fsb_out_s_width_lp-1:0] fsb_o
    );

   // set this parameter to be the size of the rom

   localparam rom_addr_width_lp = 6;

   localparam rom_data_width_lp = ring_width_p+4;

   logic [rom_addr_width_lp-1:0] rom_addr_lo;
   logic [rom_data_width_lp-1:0] rom_data_li;

   bsg_frame_trace_replay #(.ring_width_p(ring_width_p)
                            ,.rom_addr_width_p(6)
                            ) tr
   (.clk_i
    ,.fsb_i(fsb_i[0])
    ,.fsb_o(fsb_o[0])

    ,.rom_addr_o(rom_addr_lo)
    ,.rom_data_i(rom_addr_li)

    ,.done_o    ()
    ,.error_o   ()
    );

   bsg_rom_frame_gateway #( .width_p     (rom_data_width_lp)
                          ,.addr_width_p(rom_addr_width_lp)
                          ) rom
     (.addr_i(rom_addr_lo)
      ,.data_o(rom_data_li)
      );

endmodule
