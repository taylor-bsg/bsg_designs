// here is a simple example of an ASIC that instantiates a single loopback node

module bsg_frame_asic #(ring_width_p="inv"
                        ,nodes_p="inv"

                        ,bsg_fsb_in_s_width_lp      = `bsg_fsb_in_s_width (ring_width_lp)
                        ,bsg_fsb_out_s_width_lp     = `bsg_fsb_out_s_width(ring_width_lp)
                        )
   (input clk_i
    ,input  [nodes_p-1:0][bsg_fsb_in_s_width_lp -1:0] fsb_i
    ,output [nodes_p-1:0][bsg_fsb_out_s_width_lp-1:0] fsb_o
    );

   bsg_frame_loopback #(.ring_width_p(ring_width_p)) bfl
   (.clk_i
    ,.fsb_i(fsb_i[0])
    ,.fsb_o(fsb_o[0])
    );

endmodule
