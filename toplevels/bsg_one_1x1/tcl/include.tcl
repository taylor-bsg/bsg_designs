set bsg_ip_cores_dir $::env(BSG_IP_CORES_DIR)
set bsg_raw_dir $::env(BSG_RAW_DIR)
set bsg_packaging_dir $::env(BSG_PACKAGING_DIR)
set bsg_designware_dir $::env(BSG_DESIGNWARE_DIR)

set SVERILOG_INCLUDE_PATHS [join "
  $bsg_raw_dir/crudo/src/chip
  $bsg_raw_dir/crudo/src/chip/tile/switch/static
  $bsg_raw_dir/crudo/src/chip/tile/proc/mainpipe
  $bsg_raw_dir/crudo/src/interfaces
  $bsg_raw_dir/crudo/src/parts/verilog
  $bsg_raw_dir/crudo/src/toplevels/common/noc_fifo
  $bsg_raw_dir/crudo/flow/synopsys
  $bsg_ip_cores_dir/common
  $bsg_ip_cores_dir/bsg_misc
  $bsg_packaging_dir/ucsd_bga_332/pinouts/bsg_one/verilog
  $bsg_packaging_dir/common/verilog
  $bsg_packaging_dir/common/foundry/portable/verilog
  $bsg_designware_dir
"]
