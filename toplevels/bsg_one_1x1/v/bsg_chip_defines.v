//------------------------------------------------------------
// HowToUse: read and elaborate this file first to have some
// critical macros defined.
//
// Revision history:
// * [09-22-2014] MBT created bsg_chip_defines
// * [06-27-2016] LV refactored bsg_chip_defines
//------------------------------------------------------------

`define SMEM_ADDR_WIDTH_ACTUAL 7

`define TILE_MAX_X 1
`define TILE_MAX_Y 1
`define SYS_TYPE   0
`define LG_IMEM_WORDS 12
