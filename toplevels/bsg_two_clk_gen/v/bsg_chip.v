
`include "bsg_padmapping.v"

`include "bsg_iopad_macros.v"

module bsg_chip

  import bsg_tag_pkg::bsg_tag_s;

// pull in BSG Two's top-level module signature, and the definition of the pads
`include "bsg_pinout.v"

`include "bsg_iopads.v"

// **********************************************************************
// BEGIN BSG CLK GENS
//

   logic [1:0]  clk_gen_iom_sel;
   logic [1:0]  clk_gen_core_sel;
   logic        clk_gen_iom_async_reset;
   logic        clk_gen_core_async_reset;

   // all of these should be shielded
   assign clk_gen_iom_sel[0]  = misc_L_6_i_int;
   assign clk_gen_iom_sel[1]  = misc_L_7_i_int;
   assign clk_gen_core_sel[0] = misc_R_6_i_int;
   assign clk_gen_core_sel[1] = misc_R_7_i_int;
   assign clk_gen_iom_async_reset  = sdo_tkn_ex_i_int[2];
   assign clk_gen_core_async_reset = JTAG_TRST_i_int;

`include "bsg_tag.vh"

   localparam bsg_tag_els_lp  = 4;
   localparam bsg_ds_width_lp = 8;
   localparam bsg_num_adgs_lp = 1;

   `declare_bsg_clk_gen_osc_tag_payload_s(bsg_num_adgs_lp)
   `declare_bsg_clk_gen_ds_tag_payload_s(bsg_ds_width_lp)

   localparam bsg_tag_max_payload_length_lp
     = `BSG_MAX($bits(bsg_clk_gen_osc_tag_payload_s),$bits(bsg_clk_gen_ds_tag_payload_s));

   localparam lg_bsg_tag_max_payload_length_lp = $clog2(bsg_tag_max_payload_length_lp+1);

   bsg_tag_s [bsg_tag_els_lp-1:0] tags;

   bsg_tag_master #(.els_p(bsg_tag_els_lp)
                    ,.lg_width_p(lg_bsg_tag_max_payload_length_lp)
                    ) btm
     (.clk_i       (JTAG_TCK_i_int)
      ,.data_i     (JTAG_TDI_i_int)
      ,.en_i       (JTAG_TMS_i_int) // shield
      ,.clients_r_o(tags)
      );

   // Clock signals coming out of clock generators
   logic core_clk;
   logic iom_clk;

   // core clock generator (bsg_tag ID's 0 and 1)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ) clk_gen_core_inst
     (.bsg_osc_tag_i(tags[0])
      ,.bsg_ds_tag_i(tags[1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i(misc_L_4_i_int)  // probably should be identified as clock
      ,.select_i (clk_gen_core_sel)
      ,.clk_o    (core_clk)
      );

   // to reduce iteration time, we only instantiate one clock generator
   if (0)
     begin
   // io clock generator (bsg_tag ID's 2 and 3)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ) clk_gen_iom_inst
     (.bsg_osc_tag_i(tags[2])
      ,.bsg_ds_tag_i(tags[3])
      ,.async_osc_reset_i(clk_gen_iom_async_reset)
      ,.ext_clk_i(PLL_CLK_i_int) // probably should be identified as clock
      ,.select_i (clk_gen_iom_sel)
      ,.clk_o    (iom_clk)
      );
     end

   // Route the clock signals off chip to see life in the chip!
   logic [1:0]  clk_out_sel;
   logic        clk_out;

   assign clk_out_sel[0] = sdo_tkn_ex_i_int[3]; // shield
   assign clk_out_sel[1] = misc_R_5_i_int;      // shield
   assign sdi_tkn_ex_o_int[3] = clk_out;        // shield

   bsg_mux #(.width_p    (1)
             ,.els_p     (4)
             ,.balanced_p(1)
             ,.harden_p  (1)
             ) clk_out_mux_inst
     // being able to not output clock is a good idea
     // for noise
     (.data_i({1'b0,1'b0,iom_clk,core_clk})
      ,.sel_i(clk_out_sel)
      ,.data_o(clk_out)
      );

`include "bsg_pinout_end.v"
