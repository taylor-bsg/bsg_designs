source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag_timing.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen_timing.tcl

set JTAG_CLK_PERIOD 10.0
set OSC_PERIOD_INT 1.8

# create clock for bsg_tag, using pin p_JTAG_TCK_i, 10 ns period, with 3.0 percent clock uncertainty
# or 0.30 ns (5 percent uncertainty results in a lot of excess gates being inserted to satisfy hold times)

bsg_tag_clock_create bsg_tag_clk p_JTAG_TCK_i p_JTAG_TDI_i p_JTAG_TMS_i $JTAG_CLK_PERIOD 3.0

# create clocks for bsg_clk_gen,
#   - using clk_gen_core_inst as the path for the oscillator in the netlist.
#   - using osc_clk as the name
#   - using 1.6 ns as the (guaranteed not to exceed) period
#   - using 5% as the uncertainty
#

bsg_clk_gen_clock_create clk_gen_core_inst osc_core_clk bsg_tag_clk $OSC_PERIOD_INT $OSC_PERIOD_INT 5

# create a clock for the incoming external clock
create_clock -period $OSC_PERIOD_INT -name ext_clk p_misc_L_4_i

# create a clock for the outgoing clock that we can observe
create_clock -period $OSC_PERIOD_INT -name clk_mux clk_out_mux_inst/data_o

#
# keep the edges sharp on point-to-point clocks that tend to have long wires
# the practical effect of this is that it will insert a buffer every 0.5 mm of wire
#

set_max_transition 0.10 clk_mux
set_max_transition 0.10 ext_clk

# get_attribute ext_clk max_transition

source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_timing_constraint.tcl
