set bsg_ip_cores_dir $::env(BSG_OUT_DIR)/bsg_ip_cores
set bsg_rocket_dir $::env(BSG_OUT_DIR)/bsg_rocket
set bsg_manycore_dir $::env(BSG_OUT_DIR)/bsg_manycore

set TESTING_INCLUDE_PATHS [join "
  $bsg_ip_cores_dir/bsg_misc
  $bsg_ip_cores_dir/bsg_fsb
  $bsg_rocket_dir/modules/bsg_rocket_fsb
  $bsg_rocket_dir/rockets/coyote/generated-src
  $bsg_manycore_dir/v
"]
