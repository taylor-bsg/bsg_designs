`include "bsg_rocket_pkg.vh"
`include "bsg_fsb_pkg.v"

module bsg_zedboard_chip
  import bsg_rocket_pkg::*;
  import bsg_fsb_pkg::*;
# (parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p
  ,parameter nodes_p=2)
  (input                        clk_i
  ,input                        reset_i
  ,output                       boot_done_o
  // host in
  ,input                        host_valid_i
  ,input             bsg_host_t host_data_i
  ,output                       host_ready_o
  // host out
  ,output                       host_valid_o
  ,output            bsg_host_t host_data_o
  ,input                        host_ready_i
  // aw out
  ,output                       nasti_aw_valid_o
  ,output       bsg_nasti_a_pkt nasti_aw_data_o
  ,input                        nasti_aw_ready_i
  // w out
  ,output                       nasti_w_valid_o
  ,output       bsg_nasti_w_pkt nasti_w_data_o
  ,input                        nasti_w_ready_i
  // b in
  ,input                        nasti_b_valid_i
  ,input        bsg_nasti_b_pkt nasti_b_data_i
  ,output                       nasti_b_ready_o
  // ar out
  ,output                       nasti_ar_valid_o
  ,output       bsg_nasti_a_pkt nasti_ar_data_o
  ,input                        nasti_ar_ready_i
  // r in
  ,input                        nasti_r_valid_i
  ,input        bsg_nasti_r_pkt nasti_r_data_i
  ,output                       nasti_r_ready_o
  // in
  ,input                        v_i
  ,input     [ring_width_p-1:0] data_i
  ,output                       yumi_o
  // out
  ,output                       v_o
  ,output    [ring_width_p-1:0] data_o
  ,input                        ready_i);

  // boot done
  wire boot_done;

  // into nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_A;
  wire [ring_width_p-1:0] core_node_data_A [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_ready_A;

  // into nodes (control)
  wire [nodes_p-1:0]      core_node_en_r_lo;
  wire [nodes_p-1:0]      core_node_reset_r_lo;

  // out of nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_B;
  wire [ring_width_p-1:0] core_node_data_B [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_yumi_B;

`ifndef bsg_active_fsb_index
  localparam dest_id_lp = 0;
`else
  localparam dest_id_lp = `bsg_active_fsb_index;
`endif

generate
if (dest_id_lp == 5)
  begin
    bsg_manycore_node_master #
      (.client_id_p(dest_id_lp)
      ,.ring_width_p(ring_width_p))
    mstr
      (.clk_i(clk_i)
      // ctrl
      ,.reset_i(~boot_done)
      ,.en_i(core_node_en_r_lo[0])
      // in
      ,.v_i(core_node_v_A[0])
      ,.data_i(core_node_data_A[0])
      ,.ready_o(core_node_ready_A[0])
      // out
      ,.v_o(core_node_v_B[0])
      ,.data_o(core_node_data_B[0])
      ,.yumi_i(core_node_yumi_B[0]));
  end
else
  begin
    bsg_rocket_node_master #
      (.dest_id_p(dest_id_lp))
    mstr
      (.clk_i(clk_i)
      // ctrl
      ,.reset_i(core_node_reset_r_lo[0] & (~boot_done))
      ,.en_i(core_node_en_r_lo[0])
      // in
      ,.v_i(core_node_v_A[0])
      ,.data_i(core_node_data_A[0])
      ,.ready_o(core_node_ready_A[0])
      // out
      ,.v_o(core_node_v_B[0])
      ,.data_o(core_node_data_B[0])
      ,.yumi_i(core_node_yumi_B[0])
      // host in
      ,.host_valid_i(host_valid_i)
      ,.host_data_i(host_data_i)
      ,.host_ready_o(host_ready_o)
      // host out
      ,.host_valid_o(host_valid_o)
      ,.host_data_o(host_data_o)
      ,.host_ready_i(host_ready_i)
      // aw out
      ,.nasti_aw_valid_o(nasti_aw_valid_o)
      ,.nasti_aw_data_o(nasti_aw_data_o)
      ,.nasti_aw_ready_i(nasti_aw_ready_i)
      // w out
      ,.nasti_w_valid_o(nasti_w_valid_o)
      ,.nasti_w_data_o(nasti_w_data_o)
      ,.nasti_w_ready_i(nasti_w_ready_i)
      // b in
      ,.nasti_b_valid_i(nasti_b_valid_i)
      ,.nasti_b_data_i(nasti_b_data_i)
      ,.nasti_b_ready_o(nasti_b_ready_o)
      // ar out
      ,.nasti_ar_valid_o(nasti_ar_valid_o)
      ,.nasti_ar_data_o(nasti_ar_data_o)
      ,.nasti_ar_ready_i(nasti_ar_ready_i)
      // r in
      ,.nasti_r_valid_i(nasti_r_valid_i)
      ,.nasti_r_data_i(nasti_r_data_i)
      ,.nasti_r_ready_o(nasti_r_ready_o));
  end
endgenerate

  // boot node

  bsg_test_node_master #
    (.ring_width_p(80))
  boot
    (.clk_i(clk_i)
    ,.reset_i(core_node_reset_r_lo[1])
    // control
    ,.en_i(core_node_en_r_lo[1])
    ,.done_o(boot_done)
    // out
    ,.v_o(core_node_v_B[1])
    ,.data_o(core_node_data_B[1])
    ,.yumi_i(core_node_yumi_B[1])
    // not used
    ,.v_i()
    ,.data_i()
    ,.ready_o());

  assign boot_done_o = boot_done;

  // fsb

  // fsb in
  wire                    core_cl_valid_lo;
  wire [ring_width_p-1:0] core_cl_data_lo;
  wire                    core_fsb_yumi_lo;

  // fsb out
  wire                    core_fsb_valid_lo;
  wire [ring_width_p-1:0] core_fsb_data_lo;
  wire                    core_cl_ready_lo;

  bsg_fsb #
    (.width_p(ring_width_p)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p({nodes_p{1'b0}})
    // if master, enable at startup so that it can drive things
    ,.enabled_at_start_vec_p({nodes_p{1'b1}}))
  fsb
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    // node ctrl
    ,.node_reset_r_o(core_node_reset_r_lo)
    ,.node_en_r_o(core_node_en_r_lo)
    // node in
    ,.node_v_i(core_node_v_B)
    ,.node_data_i(core_node_data_B)
    ,.node_yumi_o(core_node_yumi_B)
    // node out
    ,.node_v_o(core_node_v_A)
    ,.node_data_o(core_node_data_A)
    ,.node_ready_i(core_node_ready_A)
    // asm in
    ,.asm_v_i(v_i)
    ,.asm_data_i({5'd0, data_i[74:0]})
    ,.asm_yumi_o(yumi_o)
    // asm out
    ,.asm_v_o(v_o)
    ,.asm_data_o(data_o)
    ,.asm_ready_i(ready_i));

endmodule
