module DC_DC_LDO (input  LOADSHIN
                 ,input  VREFH
                 ,input  VREFL
                 ,output ENHH
                 ,output ENLL
                 ,output ENH
                 ,output ENL
                 ,input  CLK_SEL
                 ,input  RSTIN
                 ,input  SHIN
                 ,input  LOADSHCLK
                 ,input  LOADCLK
                 ,input  SHCLK
                 ,input  SHRST
                 ,input  CLK_RST
                 ,input  BIAS50
                 ,output CLKOV4
                 ,input  VBRING
                 ,input  BPH_DIFF
                 ,input  BNH_DIFF
                 ,input  BPL_DIFF
                 ,input  BNL_DIFF);

/**
 *
 * +-------------+
 * |             |
 * |             |
 * |  BLACK BOX  |
 * |             |
 * |             |
 * +-------------+
 *
 **/

endmodule
