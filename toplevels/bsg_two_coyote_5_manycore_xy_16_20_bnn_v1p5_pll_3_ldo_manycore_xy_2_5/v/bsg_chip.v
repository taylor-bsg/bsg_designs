
`include "bsg_padmapping.v"
`include "bsg_iopad_macros.v"

module bsg_chip

  // pull in BSG Two's top-level module signature, and the definition of the pads
  `include "bsg_pinout.v"
  `include "bsg_iopads.v"

  //  _____  _      _          
  // |  __ \| |    | |         
  // | |__) | |    | |     ___ 
  // |  ___/| |    | |    / __|
  // | |    | |____| |____\__ \
  // |_|    |______|______|___/
                           
  logic core_clk, io_master_clk, manycore_clk;
  
  PLL core_clk_pll
    (.in_clk_ref(misc_L_4_i_int)
    ,.in_chip_select()
    ,.in_scn_clk()
    ,.in_sdi()
    ,.in_rstb()
    ,.out_sdo()
    ,.out_clk_tst()
    ,.out_clk(core_clk));
  
  PLL io_master_clk_pll
    (.in_clk_ref(PLL_CLK_i_int)
    ,.in_chip_select()
    ,.in_scn_clk()
    ,.in_sdi()
    ,.in_rstb()
    ,.out_sdo()
    ,.out_clk_tst()
    ,.out_clk(io_master_clk));
  
  PLL manycore_clk_pll
    (.in_clk_ref(misc_L_5_i_int)
    ,.in_chip_select()
    ,.in_scn_clk()
    ,.in_sdi()
    ,.in_rstb()
    ,.out_sdo()
    ,.out_clk_tst()
    ,.out_clk(manycore_clk));

  wire [7:0] sdi_data_i_int_packed [3:0];
  wire [7:0] sdo_data_o_int_packed [3:0];

  assign sdi_data_i_int_packed = { sdi_D_data_i_int, sdi_B_data_i_int, sdi_C_data_i_int, sdi_A_data_i_int };
  assign { sdo_D_data_o_int, sdo_C_data_o_int, sdo_B_data_o_int, sdo_A_data_o_int } = { >> {sdo_data_o_int_packed} };

  `define BSG_SWIZZLE_3120(a) { a[3],a[1],a[2],a[0] }

  //  ____   _____  _____    _____ _     _          _____       _       
  // |  _ \ / ____|/ ____|  / ____| |   (_)        / ____|     | |      
  // | |_) | (___ | |  __  | |    | |__  _ _ __   | |  __ _   _| |_ ___ 
  // |  _ < \___ \| | |_ | | |    | '_ \| | '_ \  | | |_ | | | | __/ __|
  // | |_) |____) | |__| | | |____| | | | | |_) | | |__| | |_| | |_\__ \
  // |____/|_____/ \_____|  \_____|_| |_|_| .__/   \_____|\__,_|\__|___/
  //                                      | |                           
  //                                      |_|                           

  bsg_chip_guts g
    (.core_clk_i(core_clk)
    ,.io_master_clk_i(io_master_clk)
    ,.manycore_clk_i(manycore_clk)
    ,.async_reset_i(reset_i_int)

    ,.io_clk_tline_i(`BSG_SWIZZLE_3120(sdi_sclk_i_int))
    ,.io_valid_tline_i(`BSG_SWIZZLE_3120(sdi_ncmd_i_int))
    ,.io_data_tline_i(sdi_data_i_int_packed)
    ,.io_token_clk_tline_o(`BSG_SWIZZLE_3120(sdi_token_o_int))

    ,.im_clk_tline_o(sdo_sclk_o_int)
    ,.im_valid_tline_o(sdo_ncmd_o_int)
    ,.im_data_tline_o(sdo_data_o_int_packed)
    ,.token_clk_tline_i(sdo_token_i_int)

    ,.im_slave_reset_tline_r_o()  // unused by ASIC

    ,.core_reset_o());            // post calibration reset

  `include "bsg_pinout_end.v"
