source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag_timing.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen_timing.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_dram_ctrl_timing_constraint.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_nodram_timing_constraint.tcl

#####################################################################################################################
#   TIMING SETTINGS
set JTAG_CLK_PERIOD 6.0
set OSC_PERIOD_INT 1.8

if { ${analysis_type} == "bc_wc" } {
  set CORE_CLOCK_PERIOD         1.2 
  set FSB_CLOCK_PERIOD          2.5
  set DRLP_CLOCK_PERIOD         4.0 
  set OUTERSPACE_CLOCK_PERIOD   1.2
  set DFI_2X_CLOCK_PERIOD       2.5 
  set MASTER_IO_CLOCK_PERIOD    2.0
} elseif { ${analysis_type} == "single_typical" } {
  set CORE_CLOCK_PERIOD         1.2 
  set FSB_CLOCK_PERIOD          2.5 
  set DRLP_CLOCK_PERIOD         4.0 
  set OUTERSPACE_CLOCK_PERIOD   1.2 
  set DFI_2X_CLOCK_PERIOD       2.5 
  set MASTER_IO_CLOCK_PERIOD    2.0
}

set DRAM_IO_SKEW                0.5   
#######################################################################################################################
bsg_tag_clock_create bsg_tag_clk p_JTAG_TCK_i p_JTAG_TDI_i p_JTAG_TMS_i $JTAG_CLK_PERIOD 3.0
bsg_clk_gen_clock_create clk_gen_core_inst   core_clk      bsg_tag_clk $OSC_PERIOD_INT $CORE_CLOCK_PERIOD       5
bsg_clk_gen_clock_create clk_gen_fsb_inst    fsb_clk       bsg_tag_clk $OSC_PERIOD_INT $FSB_CLOCK_PERIOD        5
bsg_clk_gen_clock_create clk_gen_drlp_inst   drlp_clk      bsg_tag_clk $OSC_PERIOD_INT $DRLP_CLOCK_PERIOD       5
bsg_clk_gen_clock_create clk_gen_op_inst     op_clk        bsg_tag_clk $OSC_PERIOD_INT $OUTERSPACE_CLOCK_PERIOD 5
bsg_clk_gen_clock_create clk_gen_dfi_2x_inst dfi_2x_clk    bsg_tag_clk $OSC_PERIOD_INT $DFI_2X_CLOCK_PERIOD     5
bsg_clk_gen_clock_create clk_gen_iom_inst    master_io_clk bsg_tag_clk $OSC_PERIOD_INT $MASTER_IO_CLOCK_PERIOD  5
# Create the generated clock from downsample
create_generated_clock  -name           dfi_clk                                                         \
                        -source         [get_attribute [get_clocks dfi_2x_clk] sources]                 \
                        -divide_by      2                                                               \
                        [get_pins       dfi_clk_ds/clk_r_o_reg/Q]

create_clock -period $OSC_PERIOD_INT -name ext_clk              p_misc_L_4_i
create_clock -period $OSC_PERIOD_INT -name fsb_ext_clk          p_misc_L_1_i
create_clock -period $OSC_PERIOD_INT -name drlp_ext_clk         p_misc_L_5_i
create_clock -period $OSC_PERIOD_INT -name op_ext_clk           p_sdo_token_i[3]
create_clock -period $OSC_PERIOD_INT -name dfi_2x_ext_clk       p_misc_R_1_i
create_clock -period $OSC_PERIOD_INT -name clk_mux              clk_out_mux_inst/data_o


set_max_transition 0.10 clk_mux
set_max_transition 0.10 ext_clk
set_max_transition 0.10 dfi_2x_ext_clk

##########################################################################################
#          DRAM CLOCK GENERATION
# Setup the dram ports
set dqs0_port  [get_ports "p_sdi_sclk_io[0]"]
set dqs1_port  [get_ports "p_sdi_sclk_io[1]"]
set dm0_port   [get_ports "p_sdi_ncmd_o[0]" ]
set dm1_port   [get_ports "p_sdi_ncmd_o[1]" ]
set dq0_ports  [get_ports "p_sdi_B_data_io*" ]
set dq1_ports  [get_ports "p_sdi_D_data_io*" ]
set ck_p_port  [get_ports "p_clk_1_p_o"      ]
set ck_n_port  [get_ports "p_clk_1_n_o"      ]

set                 ctrl_ports  [get_ports  "p_sdi_token_o[3]"     ] 
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_sclk_o[3]"      ]
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_sclk_o[2]"      ]
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_ncmd_o[3]"      ]
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_ncmd_o[2]"      ]
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_C_data_o*"      ]
add_to_collection  $ctrl_ports  [get_ports  "p_sdo_D_data_o*"      ]
 
bsg_dram_ctrl_timing_constraint        \
               $DRAM_IO_SKEW           \
               core_clk                \
               dfi_clk                 \
               dfi_2x_clk              \
               $ck_p_port             \
               $ck_n_port             \
               $dqs0_port              \
               $dqs1_port              \
               $dm0_port               \
               $dm1_port               \
               $dq0_ports              \
               $dq1_ports              \
               $ctrl_ports             \

#
#          END DRAM CLOCK GENERATION
##########################################################################################

bsg_chip_nodram_timing_constraint              \
  -package ucsd_bsg_332                 \
  -reset_port [get_ports p_reset_i]     \
  -core_clk_port clk_gen_core_inst/mux_inst/macro.b1_i/stack_b0/ZN \
  -core_clk_name core_clk               \
  -core_clk_period ${CORE_CLOCK_PERIOD} \
  -master_io_clk_port clk_gen_iom_inst/mux_inst/macro.b1_i/stack_b0/ZN        \
  -master_io_clk_name master_io_clk     \
  -master_io_clk_period ${MASTER_IO_CLOCK_PERIOD} \
  -create_core_clk 0                              \
  -create_master_clk 0                            \
  -input_cell_rise_fall_difference    [expr 1.37 - 1.15] \
  -output_cell_rise_fall_difference_A 0.8   \
  -output_cell_rise_fall_difference_B 0.67  \
  -output_cell_rise_fall_difference_C 0.17  \
  -output_cell_rise_fall_difference_D 0.34 

#####################################################################################################################
#   OUTSPACE CLOCK TREE
set_clock_latency -0.786 [get_pins -filter "name == clock" -of_objects [get_cells -hierarchical *OUTERSPACE_TOP*]]
