`include "bsg_padmapping.v"

`include "bsg_iopad_macros.v"

module bsg_chip

   import bsg_tag_pkg::bsg_tag_s;
   import bsg_chip_pkg::*;

// pull in BSG Two's top-level module signature, and the definition of the pads
`include "bsg_pinout.v"
//----------------------------------------------------------------------------
// Following are the pin mapping to the signals
//
//   output p_clk_0_p_i
// , output p_clk_0_n_i
// , output p_clk_1_p_o               ==> ddr_clk_p
// , output p_clk_1_n_o               ==> ddr_clk_n
// , output p_SMA_in_p_i   , output p_SMA_in_n_i
// , output p_SMA_out_p_o  , output p_SMA_out_n_o

// , input   p_PLL_CLK_i                ==> clk_gen_iom_inst.ext_clk_i
// , output [1:0] p_sdi_sclk_io         ==> { dqs[1], dqs[0] }
// , input  [1:0] p_sdi_sclk_i          ==> {  --, bsg_chip_guts.io_clk_tline_i}
//
// , output [1:0] p_sdi_ncmd_o          ==> { dm[1],  dm[0] }
// , input  [1:0] p_sdi_ncmd_i          ==> {  --, bsg_chip_guts.io_valid_tline_i}
//
// , input  [7:0] p_sdi_A_data_i        ==>  bsg_chip_guts.io_data_tline_i
//
// , inout  [7:0] p_sdi_B_data_io       ==> dq[0]
// , input  [7:0] p_sdi_C_data_i
// , inout  [7:0] p_sdi_D_data_io       ==> dq[1]
//
// , output [3:0] p_sdi_token_o         ==> {ddr_cs, --, --, bsg_chip_guts.io_token_clk_tline_o}
// , output [3:0] p_sdo_sclk_o          ==> {ddr_cke, ddr_we, --, bsg_chip_guts.im_clk_tline_o}
// , output [3:0] p_sdo_ncmd_o          ==> {ddr_cas, ddr_ras, --, bsg_chip_guts.im_valid_tline_o}
// , output [7:0] p_sdo_A_data_o        ==>  bsg_chip_guts.im_data_tline_o
//
// , output [7:0] p_sdo_B_data_o
// , output [7:0] p_sdo_C_data_o        ==> ddr_addr[7:0]
// , output [7:0] p_sdo_D_data_o        ==> {ddr_ba[1:0], ddr_addr[13:8]}
//
// , output p_sdo_A_data_8_o
// , output p_sdo_C_data_8_o
// , input  [3:0] p_sdo_token_i         ==> {clk_gen_op_inst.ext_clk_i,
//                                           clk_gen_op_sel[1],
//                                           clk_gen_op_sel[0],
//                                           bsg_chip_guts.token_clk_tline_i }
// , input  p_misc_T_0_i                ==> clk_out_sel[0]               ##
// , input  p_misc_T_1_i                ==> clk_gen_iom_async_reset      ##
// , input  p_misc_T_2_i                ==> clk_gen_drlp_sel[1]
//
// , input  p_misc_L_7_i                ==> clk_gen_iom_sel[1]
// , input  p_misc_R_7_i                ==> clk_gen_core_sel[1]
// , input  p_misc_L_6_i                ==> clk_gen_iom_sel[0]
// , input  p_misc_R_6_i                ==> clk_gen_core_sel[0]
// , input  p_misc_L_5_i                ==> clk_gen_drlp_inst.ext_clk_i
// , input  p_misc_R_5_i                ==> clk_out_sel[1]
// , input  p_misc_L_4_i                ==> clk_gen_core_inst.ext_clk_i
// , input  p_misc_R_4_i                ==> clk_gen_drlp_sel[0]
//
// , output p_misc_L_3_o                ==> clk_out                      ##
// , output p_misc_R_3_o
// , input  p_misc_L_2_i                ==> clk_gen_dfi_2x_sel[1]
// , input  p_misc_R_2_i                ==> clk_gen_dfi_2x_sel[0]
// , input  p_misc_L_1_i                ==> clk_gen_fsb_inst.ext_clk_i
// , input  p_misc_R_1_i                ==> clk_gen_dfi_2x_inst.ext_clk_i
// , input  p_misc_L_0_i                ==> clk_gen_fsb_sel[1]
// , input  p_misc_R_0_i                ==> clk_gen_fsb_sel[0]
//
// , input  p_reset_i                   ==> bsg_chip_guts.async_reset_i
//
// , input  p_JTAG_TMS_i                ==> bsg_tag_master.en_i
// , input  p_JTAG_TDI_i                ==> bsg_tag_master.data_i
// , input  p_JTAG_TCK_i                ==> bsg_tag_master.clk_i
// , input  p_JTAG_TRST_i               ==> clk_gen_core_async_reset
// , output p_JTAG_TDO_o
//

// End UCSD BGA 332 PAD Definitions
// **********************************************************************

`include "bsg_iopads.v"

  // MBT: copied directly from bsg_two_loopback_clk_gen
  // as a reminder that these must now be set by bsg_two
  // pinouts.

   `BSG_IO_TIEHI(misc_R_3_oen_int)      ;
   `BSG_IO_TIEHI(sdo_A_data_8_oen_int)  ;
   `BSG_IO_TIEHI(sdo_C_data_8_oen_int)  ;
   `BSG_IO_TIEHI(JTAG_TDO_oen_int)      ;

   assign sdi_token_o_int[1]  = 1'b0    ;
   assign sdi_token_o_int[2]  = 1'b0    ;
   assign sdo_sclk_o_int[1]   = 1'b0    ;
   assign sdo_ncmd_o_int[1]   = 1'b0    ;

   assign sdo_B_data_o_int           = 8'b0;
   assign sdo_A_data_8_o_int         = 1'b0;
   assign sdo_C_data_8_o_int         = 1'b0;
   assign SMA_out_p_o_int            = 1'b0;
   assign SMA_out_n_o_int            = 1'b0;
   assign JTAG_TDO_o_int             = 1'b0;

// **********************************************************************
// BEGIN BSG CLK GENS
//

  wire      dfi_clk_li    ;
  wire      dfi_clk_2x_li ;
  wire      drlp_clk_lo   ;
  wire      fsb_clk_lo    ;
  wire      op_clk_lo     ;

  wire [1:0]  clk_gen_iom_sel           ={ misc_L_7_i_int, misc_L_6_i_int };
  wire [1:0]  clk_gen_core_sel          ={ misc_R_7_i_int, misc_R_6_i_int };
  wire [1:0]  clk_gen_dfi_2x_sel        ={ misc_L_2_i_int, misc_R_2_i_int };
  wire [1:0]  clk_gen_drlp_sel          ={ misc_T_2_i_int, misc_R_4_i_int };
  wire [1:0]  clk_gen_fsb_sel           ={ misc_L_0_i_int, misc_R_0_i_int };
  wire [1:0]  clk_gen_op_sel            ={ sdo_token_i_int[2], sdo_token_i_int[1] };

  wire clk_gen_iom_async_reset          = misc_T_1_i_int;
  wire clk_gen_core_async_reset         = JTAG_TRST_i_int;



`include "bsg_tag.vh"

   // for bsg_clk_gen v2, we need 3 bsg_tag endpoints
   localparam bsg_tags_per_endpoint_lp = 3;
   localparam bsg_tag_els_lp  = 6*bsg_tags_per_endpoint_lp;
   localparam bsg_ds_width_lp = 8;
   localparam bsg_num_adgs_lp = 1;

   `declare_bsg_clk_gen_osc_tag_payload_s(bsg_num_adgs_lp)
   `declare_bsg_clk_gen_ds_tag_payload_s(bsg_ds_width_lp)

   localparam bsg_tag_max_payload_length_lp
     = `BSG_MAX($bits(bsg_clk_gen_osc_tag_payload_s),$bits(bsg_clk_gen_ds_tag_payload_s));

   localparam lg_bsg_tag_max_payload_length_lp = $clog2(bsg_tag_max_payload_length_lp+1);

   bsg_tag_s [bsg_tag_els_lp-1:0] tags;

   bsg_tag_master #(.els_p(bsg_tag_els_lp)
                    ,.lg_width_p(lg_bsg_tag_max_payload_length_lp)
                    ) btm
     (.clk_i       (JTAG_TCK_i_int)
      ,.data_i     (JTAG_TDI_i_int)
      ,.en_i       (JTAG_TMS_i_int) // shield
      ,.clients_r_o(tags)
      );

   // Clock signals coming out of clock generators
   logic core_clk_lo;
   logic iom_clk_lo;

   // core clock generator (bsg_tag ID's 0 and 1)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_core_inst
     (.bsg_osc_tag_i         (tags[0])
      ,.bsg_osc_trigger_tag_i(tags[2])
      ,.bsg_ds_tag_i         (tags[1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i(misc_L_4_i_int)  // probably should be identified as clock
      ,.select_i (clk_gen_core_sel)
      ,.clk_o    (core_clk_lo)
      );

   // io clock generator (bsg_tag ID's 2 and 3)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_iom_inst
     (.bsg_osc_tag_i         (tags[1*bsg_tags_per_endpoint_lp+0])
      ,.bsg_osc_trigger_tag_i(tags[1*bsg_tags_per_endpoint_lp+2])
      ,.bsg_ds_tag_i         (tags[1*bsg_tags_per_endpoint_lp+1])
      ,.async_osc_reset_i(clk_gen_iom_async_reset)
      ,.ext_clk_i(PLL_CLK_i_int) // probably should be identified as clock
      ,.select_i (clk_gen_iom_sel)
      ,.clk_o    (iom_clk_lo)
      );

   // dfi_2x clock generator (bsg_tag ID's 4 and 5)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_dfi_2x_inst
     (.bsg_osc_tag_i         (tags[2*bsg_tags_per_endpoint_lp+0])
      ,.bsg_osc_trigger_tag_i(tags[2*bsg_tags_per_endpoint_lp+2])
      ,.bsg_ds_tag_i         (tags[2*bsg_tags_per_endpoint_lp+1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i(misc_R_1_i_int) // probably should be identified as clock
      ,.select_i (clk_gen_dfi_2x_sel)
      ,.clk_o    (dfi_clk_2x_li)
      );
   // drlp clock generator (bsg_tag ID's 6 and 7)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_drlp_inst
     (.bsg_osc_tag_i         (tags[3*bsg_tags_per_endpoint_lp+0])
      ,.bsg_osc_trigger_tag_i(tags[3*bsg_tags_per_endpoint_lp+2])
      ,.bsg_ds_tag_i         (tags[3*bsg_tags_per_endpoint_lp+1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i(misc_L_5_i_int) // probably should be identified as clock
      ,.select_i (clk_gen_drlp_sel)
      ,.clk_o    (drlp_clk_lo)
      );

   // fsb clock generator (bsg_tag ID's 8 and 9)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_fsb_inst
     (.bsg_osc_tag_i         (tags[4*bsg_tags_per_endpoint_lp+0])
      ,.bsg_osc_trigger_tag_i(tags[4*bsg_tags_per_endpoint_lp+2])
      ,.bsg_ds_tag_i         (tags[4*bsg_tags_per_endpoint_lp+1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i( misc_L_1_i_int) // probably should be identified as clock
      ,.select_i (clk_gen_fsb_sel)
      ,.clk_o    (fsb_clk_lo)
      );

   // outerspace clock generator (bsg_tag ID's 10 and 11)
   bsg_clk_gen #(.downsample_width_p(bsg_ds_width_lp)
                 ,.num_adgs_p(bsg_num_adgs_lp)
                 ,.version_p(2)
                 ) clk_gen_op_inst
     (.bsg_osc_tag_i         (tags[5*bsg_tags_per_endpoint_lp+0])
      ,.bsg_osc_trigger_tag_i(tags[5*bsg_tags_per_endpoint_lp+2])
      ,.bsg_ds_tag_i         (tags[5*bsg_tags_per_endpoint_lp+1])
      ,.async_osc_reset_i(clk_gen_core_async_reset)
      ,.ext_clk_i( sdo_token_i_int[3] ) // probably should be identified as clock
      ,.select_i ( clk_gen_op_sel)
      ,.clk_o    ( op_clk_lo)
      );


   // Route the clock signals off chip to see life in the chip!
   logic [1:0]  clk_out_sel;
   logic        clk_out;

   assign clk_out_sel[0] = misc_T_0_i_int ; // shield
   assign clk_out_sel[1] = misc_R_5_i_int;      // shield
   assign misc_L_3_o_int = clk_out;        // shield

   bsg_mux #(.width_p    (1)
             ,.els_p     (4)
             ,.balanced_p(1)
             ,.harden_p  (1)
             ) clk_out_mux_inst
     // being able to not output clock is a good idea
     // for noise; can also be used to see if chip is alive

     (.data_i({1'b1,1'b0,iom_clk_lo,core_clk_lo})
      ,.sel_i(clk_out_sel)
      ,.data_o(clk_out)
      );

// **********************************************************************
// BEGIN BSG GUTS
//
// Put this last because the previous lines define the wires that are inputs.
//
  //----------------------------------------------------------------
  //  Memory contoller

  wire  [(dram_dfi_width_gp>>4)-1:0] dm_oen_lo    ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dm_o_lo       ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_p_oen_lo ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_p_o_lo    ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_p_i_li    ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_n_oen_lo ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_n_o_lo    ;
  wire  [(dram_dfi_width_gp>>4)-1:0] dqs_n_i_li    ;

  wire  [(dram_dfi_width_gp>>1)-1:0] dq_oen_lo    ;
  wire  [(dram_dfi_width_gp>>1)-1:0] dq_o_lo       ;
  wire  [(dram_dfi_width_gp>>1)-1:0] dq_i_li       ;

  wire                               ddr_ck_p_lo   ;
  wire                               ddr_ck_n_lo   ;
  wire                               ddr_cke_lo    ;
  wire                        [2:0]  ddr_ba_lo     ; //this is the maximum width.
  wire                       [15:0]  ddr_addr_lo   ; //this is the maximum width.
  wire                               ddr_cs_n_lo   ;
  wire                               ddr_ras_n_lo  ;
  wire                               ddr_cas_n_lo  ;
  wire                               ddr_we_n_lo   ;
  wire                               ddr_reset_n_lo;
  wire                               ddr_odt_lo    ;


   localparam num_channels_lp = 1;
   wire [7:0] sdi_data_i_int_packed [num_channels_lp-1:0];
   wire [7:0] sdo_data_o_int_packed [num_channels_lp-1:0];

   assign sdi_data_i_int_packed[0] = sdi_A_data_i_int           ;
   assign sdo_A_data_o_int         = sdo_data_o_int_packed[0]   ;

   bsg_chip_guts #(.uniqueness_p(1)
              ,.enabled_at_start_vec_p(2'b11)
              ,.num_channels_p( num_channels_lp )
              ) g
     (  .core_clk_i           (core_clk_lo    )
       ,.async_reset_i        (reset_i_int    )
       ,.io_master_clk_i      (iom_clk_lo     )
       ,.drlp_clk_i           (drlp_clk_lo    )
       ,.fsb_clk_i            (fsb_clk_lo     )
       ,.op_clk_i             (op_clk_lo      )
      // flip B and C input for PD
       ,.io_clk_tline_i       ( sdi_sclk_i_int[num_channels_lp-1:0]  )
       ,.io_valid_tline_i     ( sdi_ncmd_i_int[num_channels_lp-1:0]  )
       ,.io_data_tline_i      ( sdi_data_i_int_packed  )
       ,.io_token_clk_tline_o ( sdi_token_o_int[num_channels_lp-1:0] )
       ,.im_clk_tline_o       ( sdo_sclk_o_int[num_channels_lp-1:0] )
       ,.im_valid_tline_o     ( sdo_ncmd_o_int[num_channels_lp-1:0] )
       ,.im_data_tline_o      ( sdo_data_o_int_packed  )
       ,.token_clk_tline_i    ( sdo_token_i_int[num_channels_lp-1:0])
       ,.im_slave_reset_tline_r_o()             // unused by ASIC
       ,.fsb_reset_o            ()             // post calibration reset

       // DDR interface signals
       ,.ddr_ck_p   (  ddr_ck_p_lo   )
       ,.ddr_ck_n   (  ddr_ck_n_lo   )
       ,.ddr_cke    (  ddr_cke_lo    )
       ,.ddr_ba     (  ddr_ba_lo     )
       ,.ddr_addr   (  ddr_addr_lo   )
       ,.ddr_cs_n   (  ddr_cs_n_lo   )
       ,.ddr_ras_n  (  ddr_ras_n_lo  )
       ,.ddr_cas_n  (  ddr_cas_n_lo  )
       ,.ddr_we_n   (  ddr_we_n_lo   )
       ,.ddr_reset_n(  ddr_reset_n_lo)  //TODO
       ,.ddr_odt    (  ddr_odt_lo    )  //TODO

       ,.dm_oe_n    (  dm_oen_lo     )
       ,.dm_o       (  dm_o_lo       )
       ,.dqs_p_oe_n (  dqs_p_oen_lo  )
       ,.dqs_p_o    (  dqs_p_o_lo    )
       ,.dqs_p_i    (  dqs_p_i_li    )
       ,.dqs_n_oe_n (  dqs_n_oen_lo  )
       ,.dqs_n_o    (  dqs_n_o_lo    )
       ,.dqs_n_i    (  dqs_n_i_li    )
       ,.dq_oe_n    (  dq_oen_lo     )
       ,.dq_o       (  dq_o_lo       )
       ,.dq_i       (  dq_i_li       )

       ,.dfi_clk_2x      ( dfi_clk_2x_li     )
       ,.dfi_clk         ( dfi_clk_li        )
       );

  // Emulate the bi-directional pins for DM, DQ and DQS
  wire  [(dram_dfi_width_gp>>4)-1:0] dm_pin       ;

  for(i=0;  i<(dram_dfi_width_gp>>4);  i=i+1) begin: dqs_dm_pin_ass
        assign dm_pin   [i]     = dm_oen_lo      [i] ? 1'bz: dm_o_lo   [i];
  end

  //generate the clock for the DFI
  // bsg_nonsynth_clock_gen #(.cycle_time_p( `CORE_0_PERIOD/2 ))  dfi_gen_clk_2x  (.o( dfi_clk_2x_li ));
  bsg_counter_clock_downsample #(.width_p(2)) dfi_clk_ds
    (.clk_i( dfi_clk_2x_li)
    ,.reset_i ( reset_i_int)
    ,.val_i( 2'b0 )
    ,.clk_r_o( dfi_clk_li )
    );

  // Now we can connect the dram signal to the PINs
  assign clk_1_p_o_int          = ddr_ck_p_lo   ;
  assign clk_1_n_o_int          = ddr_ck_n_lo   ;

  assign sdo_sclk_o_int[3]      = ddr_cke_lo    ;
  assign sdo_sclk_o_int[2]      = ddr_we_n_lo   ;

  assign sdi_token_o_int[3]     = ddr_cs_n_lo   ;

  assign sdo_ncmd_o_int[2]      = ddr_ras_n_lo  ;
  assign sdo_ncmd_o_int[3]      = ddr_cas_n_lo  ;


  assign sdo_C_data_o_int       = ddr_addr_lo[7:0] ;
  assign sdo_D_data_o_int       = { ddr_ba_lo[1:0], ddr_addr_lo[13:8] };

  assign sdi_B_data_o_io_int    = dq_o_lo  [7:0];
  assign sdi_B_data_oen_io_int  = dq_oen_lo[7:0];
  assign dq_i_li[7:0]           = sdi_B_data_i_io_int;

  assign sdi_D_data_o_io_int   = dq_o_lo  [15:8]  ;
  assign sdi_D_data_oen_io_int = dq_oen_lo[15:8]  ;
  assign dq_i_li[15:8]         = sdi_D_data_i_io_int;

  assign sdi_sclk_o_io_int     = dqs_p_o_lo      ;
  assign sdi_sclk_oen_io_int   = dqs_p_oen_lo    ;
  assign dqs_p_i_li            = sdi_sclk_i_io_int;

  assign sdi_ncmd_o_int        = dm_o_lo       ;
  assign sdi_ncmd_oen_int      = dm_oen_lo     ;
`include "bsg_pinout_end.v"
