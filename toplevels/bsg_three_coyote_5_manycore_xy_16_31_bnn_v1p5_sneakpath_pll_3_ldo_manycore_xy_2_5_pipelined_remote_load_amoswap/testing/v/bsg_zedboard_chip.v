`include "bsg_rocket_pkg.vh"
`include "bsg_fsb_pkg.v"

module bsg_zedboard_chip
  import bsg_rocket_pkg::*;
  import bsg_fsb_pkg::*;
# (parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p
  ,parameter num_rockets_gp=5
  ,parameter nodes_p=num_rockets_gp+2
  ,parameter dcdc_fsb_idx_lp = bsg_chip_pkg::fsb_idx_dcdc_manycore_gp
  ,parameter boot_fsb_idx_lp = 6)// 5-rocket-nodes +  dcdc-manycore + 1-fsb-boot-node
  (input                        clk_i
  ,input                        reset_i
  ,output                       boot_done_o
  // host in
  ,input   [num_rockets_gp-1:0] host_valid_i
  ,input             bsg_host_t host_data_i [num_rockets_gp-1:0]
  ,output  [num_rockets_gp-1:0] host_ready_o
  // host out
  ,output  [num_rockets_gp-1:0] host_valid_o
  ,output            bsg_host_t host_data_o [num_rockets_gp-1:0]
  ,input   [num_rockets_gp-1:0] host_ready_i
  // aw out
  ,output                       [num_rockets_gp-1:0] nasti_aw_valid_o
  ,output       bsg_nasti_a_pkt nasti_aw_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_aw_ready_i
  // w out
  ,output                       [num_rockets_gp-1:0] nasti_w_valid_o
  ,output       bsg_nasti_w_pkt nasti_w_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_w_ready_i
  // b in
  ,input                        [num_rockets_gp-1:0] nasti_b_valid_i
  ,input        bsg_nasti_b_pkt nasti_b_data_i [num_rockets_gp-1:0]
  ,output                       [num_rockets_gp-1:0] nasti_b_ready_o
  // ar out
  ,output                       [num_rockets_gp-1:0] nasti_ar_valid_o
  ,output       bsg_nasti_a_pkt nasti_ar_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_ar_ready_i
  // r in
  ,input                        [num_rockets_gp-1:0] nasti_r_valid_i
  ,input        bsg_nasti_r_pkt nasti_r_data_i [num_rockets_gp-1:0]
  ,output                       [num_rockets_gp-1:0] nasti_r_ready_o
  // in
  ,input                        v_i
  ,input     [ring_width_p-1:0] data_i
  ,output                       yumi_o
  // out
  ,output                       v_o
  ,output    [ring_width_p-1:0] data_o
  ,input                        ready_i);

  // boot done
  wire boot_done;

  // into nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_A;
  wire [ring_width_p-1:0] core_node_data_A [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_ready_A;

  // into nodes (control)
  wire [nodes_p-1:0]      core_node_en_r_lo;
  wire [nodes_p-1:0]      core_node_reset_r_lo;

  // out of nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_B;
  wire [ring_width_p-1:0] core_node_data_B [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_yumi_B;
  // rocket nodes master(node[0-4])
  `ifdef bsg_enable_replay
    //master output
    wire                    from_master_v;
    wire [ring_width_p-1:0] from_master_data ;
    wire                    from_master_ready;

    //master input
    wire                    to_master_v;
    wire [ring_width_p-1:0] to_master_data;
    wire                    to_master_ready;
  `endif

  genvar i;
  localparam rc_fsb_idx_start = dcdc_fsb_idx_lp + 1;
  localparam rc_fsb_idx_end   = rc_fsb_idx_start + num_rockets_gp -1;

  for (i = rc_fsb_idx_start; i <= rc_fsb_idx_end; i=i+1)
   begin: n
    // bsg_tr_core_id is an macro defined in command line.
    // check out the makefile.
     if ( i == `bsg_tr_core_id ) begin
`ifdef bsg_enable_replay
        bsg_test_node_MITM #
		(.ring_width_p(ring_width_p)
		,.enable_MITM_stimulus_p(1)
		,.enable_MITM_response_p(1)   )
		MITM
        ( .clk_i   ( clk_i )
         ,.reset_i ( core_node_reset_r_lo   [i ] | (~boot_done) )

         // control
         ,.en_i    ( core_node_en_r_lo      [i ] )
         ,.done_o  (                      )
         ,.error_o (                      )

         // in
         ,.v_i      (core_node_v_A          [i ])
         ,.data_i   (core_node_data_A       [i ])
         ,.ready_o  (core_node_ready_A      [i ])
         // out
         ,.v_o      (core_node_v_B          [i ])
         ,.data_o   (core_node_data_B       [i ])
         ,.yumi_i   (core_node_yumi_B       [i ])


         // input from master
         , .from_master_v_i    ( from_master_v     )
         , .from_master_data_i ( from_master_data  )
         , .from_master_ready_o( from_master_ready )

         // output to master
         , .to_master_v_o       ( to_master_v     )
         , .to_master_data_o    ( to_master_data  )
         , .to_master_yumi_i    ( to_master_v & to_master_ready    )
         );

`endif
          bsg_rocket_node_master #
            (.dest_id_p(i))
          mstr
            (.clk_i(clk_i)
            // ctrl
            ,.reset_i               (core_node_reset_r_lo   [i ] & (~boot_done))
            ,.en_i                  (core_node_en_r_lo      [i ])

       `ifdef bsg_enable_replay
            // in
            ,.v_i       (to_master_v)
            ,.data_i    (to_master_data)
            ,.ready_o   (to_master_ready  )
            // out
            ,.v_o       (from_master_v)
            ,.data_o    (from_master_data)
            ,.yumi_i    (from_master_v & from_master_ready)
       `else
            // in
            ,.v_i                   (core_node_v_A          [i ])
            ,.data_i                (core_node_data_A       [i ])
            ,.ready_o               (core_node_ready_A      [i ])
            // out
            ,.v_o                   (core_node_v_B          [i ])
            ,.data_o                (core_node_data_B       [i ])
            ,.yumi_i                (core_node_yumi_B       [i ])
       `endif
       // host in
       ,.host_valid_i               (host_valid_i           [i - rc_fsb_idx_start])
       ,.host_data_i                (host_data_i            [i - rc_fsb_idx_start])
       ,.host_ready_o               (host_ready_o           [i - rc_fsb_idx_start])
       // host out
       ,.host_valid_o               (host_valid_o           [i - rc_fsb_idx_start])
       ,.host_data_o                (host_data_o            [i - rc_fsb_idx_start])
       ,.host_ready_i               (host_ready_i           [i - rc_fsb_idx_start])
       // aw out
       ,.nasti_aw_valid_o           (nasti_aw_valid_o       [i - rc_fsb_idx_start])
       ,.nasti_aw_data_o            (nasti_aw_data_o        [i - rc_fsb_idx_start])
       ,.nasti_aw_ready_i           (nasti_aw_ready_i       [i - rc_fsb_idx_start])
       // w out
       ,.nasti_w_valid_o            (nasti_w_valid_o        [i - rc_fsb_idx_start])
       ,.nasti_w_data_o             (nasti_w_data_o         [i - rc_fsb_idx_start])
       ,.nasti_w_ready_i            (nasti_w_ready_i        [i - rc_fsb_idx_start])
       // b in
       ,.nasti_b_valid_i            (nasti_b_valid_i        [i - rc_fsb_idx_start])
       ,.nasti_b_data_i             (nasti_b_data_i         [i - rc_fsb_idx_start])
       ,.nasti_b_ready_o            (nasti_b_ready_o        [i - rc_fsb_idx_start])
       // ar out
       ,.nasti_ar_valid_o           (nasti_ar_valid_o       [i - rc_fsb_idx_start])
       ,.nasti_ar_data_o            (nasti_ar_data_o        [i - rc_fsb_idx_start])
       ,.nasti_ar_ready_i           (nasti_ar_ready_i       [i - rc_fsb_idx_start])
       // r in
       ,.nasti_r_valid_i            (nasti_r_valid_i        [i - rc_fsb_idx_start])
       ,.nasti_r_data_i             (nasti_r_data_i         [i - rc_fsb_idx_start])
       ,.nasti_r_ready_o            (nasti_r_ready_o        [i - rc_fsb_idx_start]));

    `ifdef bsg_enable_trace
       bsg_rocket_node_tracer #(.dest_id_p(i)
                                )
       i_tracer(
         .clk_i ( clk_i)
        ,.reset_i   ( core_node_reset_r_lo  [i ] & (~boot_done) )
        ,.en_i      ( core_node_en_r_lo     [i ]     )

        ,.v_i_i     ( core_node_v_A         [i ]     )
        ,.data_i_i  ( core_node_data_A      [i ]     )
        ,.ready_o_i ( core_node_ready_A     [i ]     )

        ,.v_o_i     ( core_node_v_B         [i ]     )
        ,.data_o_i  ( core_node_data_B      [i ]     )
        ,.yumi_i_i  ( core_node_yumi_B      [i ]     )
       );
    `endif
  end else begin
          bsg_rocket_node_master #
            (.dest_id_p(i))
          mstr
            (.clk_i(clk_i)
            // ctrl
            ,.reset_i               (core_node_reset_r_lo   [i ] & (~boot_done))
            ,.en_i                  (core_node_en_r_lo      [i ])

           // in
            ,.v_i                   (core_node_v_A          [i ])
            ,.data_i                (core_node_data_A       [i ])
            ,.ready_o               (core_node_ready_A      [i ])
            // out
            ,.v_o                   (core_node_v_B          [i ])
            ,.data_o                (core_node_data_B       [i ])
            ,.yumi_i                (core_node_yumi_B       [i ])
            // host in
            ,.host_valid_i          (host_valid_i           [i - rc_fsb_idx_start])
            ,.host_data_i           (host_data_i            [i - rc_fsb_idx_start])
            ,.host_ready_o          (host_ready_o           [i - rc_fsb_idx_start])
            // host out
            ,.host_valid_o          (host_valid_o           [i - rc_fsb_idx_start])
            ,.host_data_o           (host_data_o            [i - rc_fsb_idx_start])
            ,.host_ready_i          (host_ready_i           [i - rc_fsb_idx_start])
            // aw out
            ,.nasti_aw_valid_o      (nasti_aw_valid_o       [i - rc_fsb_idx_start])
            ,.nasti_aw_data_o       (nasti_aw_data_o        [i - rc_fsb_idx_start])
            ,.nasti_aw_ready_i      (nasti_aw_ready_i       [i - rc_fsb_idx_start])
            // w out
            ,.nasti_w_valid_o       (nasti_w_valid_o        [i - rc_fsb_idx_start])
            ,.nasti_w_data_o        (nasti_w_data_o         [i - rc_fsb_idx_start])
            ,.nasti_w_ready_i       (nasti_w_ready_i        [i - rc_fsb_idx_start])
            // b in
            ,.nasti_b_valid_i       (nasti_b_valid_i        [i - rc_fsb_idx_start])
            ,.nasti_b_data_i        (nasti_b_data_i         [i - rc_fsb_idx_start])
            ,.nasti_b_ready_o       (nasti_b_ready_o        [i - rc_fsb_idx_start])
            // ar out
            ,.nasti_ar_valid_o      (nasti_ar_valid_o       [i - rc_fsb_idx_start])
            ,.nasti_ar_data_o       (nasti_ar_data_o        [i - rc_fsb_idx_start])
            ,.nasti_ar_ready_i      (nasti_ar_ready_i       [i - rc_fsb_idx_start])
            // r in
            ,.nasti_r_valid_i       (nasti_r_valid_i        [i - rc_fsb_idx_start])
            ,.nasti_r_data_i        (nasti_r_data_i         [i - rc_fsb_idx_start])
            ,.nasti_r_ready_o       (nasti_r_ready_o        [i - rc_fsb_idx_start]));
  end  //end if ( i == 0)
end //end for ( int i=0 ... )


  `ifdef bsg_enable_dcdc
  //the DC/DC manycore node master
  bsg_manycore_node_master #
    (.client_id_p( dcdc_fsb_idx_lp)
    ,.ring_width_p(ring_width_p))
  mstr
    (.clk_i(clk_i)
    // ctrl
    ,.reset_i(~boot_done )
    ,.en_i(core_node_en_r_lo[ dcdc_fsb_idx_lp])
    // in
    ,.v_i(core_node_v_A[ dcdc_fsb_idx_lp])
    ,.data_i(core_node_data_A[ dcdc_fsb_idx_lp])
    ,.ready_o(core_node_ready_A[ dcdc_fsb_idx_lp])
    // out
    ,.v_o(core_node_v_B[ dcdc_fsb_idx_lp])
    ,.data_o(core_node_data_B[ dcdc_fsb_idx_lp])
    ,.yumi_i(core_node_yumi_B[ dcdc_fsb_idx_lp]));

  `else
   assign  core_node_ready_A [ dcdc_fsb_idx_lp ] = 1'b1 ;
   assign  core_node_v_B     [ dcdc_fsb_idx_lp ] = 1'b0 ;
   assign  core_node_data_B  [ dcdc_fsb_idx_lp ] =  'b0 ;

  `endif

  // fsb boot node (node[6])
  bsg_test_node_master #
    (.ring_width_p(80))
  boot
    (.clk_i(clk_i)
    ,.reset_i(core_node_reset_r_lo[boot_fsb_idx_lp])
    // control
    ,.en_i(core_node_en_r_lo[boot_fsb_idx_lp])
    ,.done_o(boot_done)
    // out
    ,.v_o(core_node_v_B[boot_fsb_idx_lp])
    ,.data_o(core_node_data_B[boot_fsb_idx_lp])
    ,.yumi_i(core_node_yumi_B[boot_fsb_idx_lp])
    // not used
    ,.v_i()
    ,.data_i()
    ,.ready_o());

  assign boot_done_o = boot_done;

  // fsb

  bsg_fsb #
    (.width_p(ring_width_p)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p({nodes_p{1'b0}})
    // if master, enable at startup so that it can drive things
    ,.enabled_at_start_vec_p({nodes_p{1'b1}}))
  fsb
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    // node ctrl
    ,.node_reset_r_o(core_node_reset_r_lo)
    ,.node_en_r_o(core_node_en_r_lo)
    // node in
    ,.node_v_i(core_node_v_B)
    ,.node_data_i(core_node_data_B)
    ,.node_yumi_o(core_node_yumi_B)
    // node out
    ,.node_v_o(core_node_v_A)
    ,.node_data_o(core_node_data_A)
    ,.node_ready_i(core_node_ready_A)
    // asm in
    ,.asm_v_i(v_i)
    ,.asm_data_i(data_i)
    ,.asm_yumi_o(yumi_o)
    // asm out
    ,.asm_v_o(v_o)
    ,.asm_data_o(data_o)
    ,.asm_ready_i(ready_i));

endmodule
