`ifndef __BSG_CHIP_PKG_V__
`define __BSG_CHIP_PKG_V__

package bsg_chip_pkg;

  parameter fsb_nodes_gp                     = 6;

  //assign the  **ROCC index NOT FSB index** to manycore and BNN
  //BYTE0 is the index for the first rocc in manycore.
  //BYTE3 is the index for the last rocc in manycore.
  parameter rocc_idx_rocket_manycore_vect_gp  = 16'h4_3_1_0;
  parameter rocc_idx_rocket_bnn_gp            = 2;

  parameter fsb_idx_dcdc_manycore_gp         = 0;

  parameter num_rockets_gp   = 5 ;
  parameter rocc_num_gp      = num_rockets_gp;
  parameter num_mc_rockets_gp= num_rockets_gp -1 ;

  // For bsg_test_node_master
  parameter tile_id_ptr_gp   = -1;
  parameter max_cycles_gp    = 500000;

endpackage // bsg_chip_pkg

`endif // __BSG_CHIP_PKG_V__
