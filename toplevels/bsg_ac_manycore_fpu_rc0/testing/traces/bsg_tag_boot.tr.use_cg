# PRO-TIP: Set editior syntax to bash (or sh)

################################################################################
# DESIGN SPECIFIC TAG INFO
#
# Number | Description            | Width
# -------+------------------------+-------
#     0  | Clkgen Async Reset     | 1-bit
#   3-1  | Clkgen Oscillator      | 5-bits
#   6-4  | Clkgen Osc Trigger     | 1-bit
#   9-7  | Clkgen Downsampoler    | 7-bits
# 12-10  | Clkgen Clock Select    | 2-bits
#    13  | Prev Link IO Control   | 3-bits
#    14  | Prev Link CORE Control | 2-bits
#    15  | Prev CT CORE Control   | 2-bits
#    16  | Next Link IO Control   | 3-bits
#    17  | Next Link CORE Control | 2-bits
#    18  | Next CT CORE Control   | 2-bits
#    19  | Router Control         | 5-bits
#
# Number of masters = 2
# Number of clients = 20
# Max data width = 7
#
# Packet lengths
# <4b>___<2b>___<5b>_1b_<3b>___<7b>
# <4b>___<18b>
#
# There are 2 masters (ASIC and GW) but they are replicated systems. The lsb of
# the masterEn is the ASIC and the msb is the GW. The GW will be given WHID 0,0
# and the ASIC will be given WHID 1,0. Therefore, we want to keep the prev link
# of the GW in reset and the next link of the ASIC in reset.
#

################################################################################
# Tag Trace Replay Packet Format
#
# M = number of masters
# N = max(1, clog2(#_of_tag_clients))
# D = max(client_1_width, client_2_width, ..., client_n_width)
# L = clog2(D+1)
#
# |<    4-bits    >|< M-bits >|< N-bits >|<     1-bit    >|< L-bits >|< D-bits >|
# +----------------+----------+----------+----------------+----------+----------+
# | replay command | masterEn |  nodeID  | data_not_reset |  length  |   data   |
# +----------------+----------+----------+----------------+----------+----------+
#
# Replay Commands
#   0 = 0000 = Wait a cycle
#   1 = 0001 = Send data
#   2 = 0010 = Receive data
#   3 = 0011 = Assert done_o ouput signal
#   4 = 0100 = End test (calls $finish)
#   5 = 0101 = Wait for cycle_counter == 0
#   6 = 0110 = Initialize cycle_counter with a 16 bit number

################################################################################
#
# RESET BSG TAG MASTER
#
# First, we must reset the bsg_tag_master. To do this, we send a 1, then we
# send a bunch of 0's! By a bunch, the exact amount is (2^clog2(N+1+L+D))+1

# Send a full 0 packet to all masters
0001___11___00000_0_000___0000000

# Wait ~32 cycles
0110___000000000000100000
0101___000000000000000000

################################################################################
#  ___  ___   ___ _____ ___ _____ ___    _   ___    ___ _    _  _____ ___ _  _
# | _ )/ _ \ / _ \_   _/ __|_   _| _ \  /_\ | _ \  / __| |  | |/ / __| __| \| |
# | _ \ (_) | (_) || | \__ \ | | |   / / _ \|  _/ | (__| |__| ' < (_ | _|| .` |
# |___/\___/ \___/ |_| |___/ |_| |_|_\/_/ \_\_|    \___|____|_|\_\___|___|_|\_|
#
################################################################################

################################################################################
#
# RESET BSG TAG CLIENTS
#
# Next, we should reset each client node. To do this we send a packet
# that has all 1's for data, and has data_not_reset=0. The nodeID should
# be the ID of the client we are reseting, and length should be the
# corrent length of the packet. We should send this packet to each client.

#SEND  en   id=0  r l=1
0001___01___00000_0_001___0000001
#SEND  en   id=1  r l=5
0001___01___00001_0_101___0011111
#SEND  en   id=2  r l=5
0001___01___00010_0_101___0011111
#SEND  en   id=3  r l=5
0001___01___00011_0_101___0011111
#SEND  en   id=4  r l=1
0001___01___00100_0_001___0000001
#SEND  en   id=5  r l=1
0001___01___00101_0_001___0000001
#SEND  en   id=6  r l=1
0001___01___00110_0_001___0000001
#SEND  en   id=7  r l=7
0001___01___00111_0_111___1111111
#SEND  en   id=8  r l=7
0001___01___01000_0_111___1111111
#SEND  en   id=9  r l=7
0001___01___01001_0_111___1111111
#SEND  en   id=10 r l=2
0001___01___01010_0_010___0000011
#SEND  en   id=11 r l=2
0001___01___01011_0_010___0000011
#SEND  en   id=12 r l=2
0001___01___01100_0_010___0000011

################################################################################
#
# START CONFIGURATION
#
# The bsg tag network is now live! We can begin our configuration.

### Set osc triggers low

#SEND  en   id=4  d l=1   {trigger}
0001___01___00100_1_001___0000000
#SEND  en   id=5  d l=1   {trigger}
0001___01___00101_1_001___0000000
#SEND  en   id=6  d l=1   {trigger}
0001___01___00110_1_001___0000000

### Program the raw oscillators speed

#SEND  en   id=1  d l=5   {adt, cdt, fdt}
0001___01___00001_1_101___0000000
#SEND  en   id=2  d l=5   {adt, cdt, fdt}
0001___01___00010_1_101___0000000
#SEND  en   id=3  d l=5   {adt, cdt, fdt}
0001___01___00011_1_101___0000000

### Trigger oscillators

#SEND  en   id=4  d l=1   {trigger}
0001___01___00100_1_001___0000001
0001___01___00100_1_001___0000000
#SEND  en   id=5  d l=1   {trigger}
0001___01___00101_1_001___0000001
0001___01___00101_1_001___0000000
#SEND  en   id=6  d l=1   {trigger}
0001___01___00110_1_001___0000001
0001___01___00110_1_001___0000000

### Async clk-gen reset to get things moving

#SEND  en   id=0  d l=1   {async_reset}
0001___01___00000_1_001___0000000
0001___01___00000_1_001___0000001
0001___01___00000_1_001___0000000

### Set downsamples and reset

#SEND  en   id=7 d l=7   {ds_val, reset}
0001___01___00111_1_111___0000001
0001___01___00111_1_111___0000000
#SEND  en   id=8  d l=7   {ds_val, reset}
0001___01___01000_1_111___0000001
0001___01___01000_1_111___0000000
#SEND  en   id=9  d l=7   {ds_val, reset}
0001___01___01001_1_111___0000001
0001___01___01001_1_111___0000000

### Select the output clock (0=raw osc, 1=ds osc, 2=ext, 3=off)

#SEND  en   id=10 d l=2   {clk_select}
0001___01___01010_1_010___0000001
#SEND  en   id=11 d l=2   {clk_select}
0001___01___01011_1_010___0000001
#SEND  en   id=12 d l=2   {clk_select}
0001___01___01100_1_010___0000001

################################################################################
#  ___  ___   ___ _____ ___ _____ ___    _   ___   ___ ___     ___ ___  __  __ ___ _    _____  __
# | _ )/ _ \ / _ \_   _/ __|_   _| _ \  /_\ | _ \ |_ _/ _ \   / __/ _ \|  \/  | _ \ |  | __\ \/ /
# | _ \ (_) | (_) || | \__ \ | | |   / / _ \|  _/  | | (_) | | (_| (_) | |\/| |  _/ |__| _| >  <
# |___/\___/ \___/ |_| |___/ |_| |_|_\/_/ \_\_|   |___\___/   \___\___/|_|  |_|_| |____|___/_/\_\
#
################################################################################

################################################################################
#
# RESET BSG TAG CLIENTS
#
# Next, we should reset each client node. To do this we send a packet
# that has all 1's for data, and has data_not_reset=0. The nodeID should
# be the ID of the client we are reseting, and length should be the
# corrent length of the packet. We should send this packet to each client.

#SEND  en   id=13 r l=3
0001___11___01101_0_011___0000111
#SEND  en   id=14 r l=2
0001___11___01110_0_010___0000011
#SEND  en   id=15 r l=2
0001___11___01111_0_010___0000011
#SEND  en   id=16 r l=3
0001___11___10000_0_011___0000111
#SEND  en   id=17 r l=2
0001___11___10001_0_010___0000011
#SEND  en   id=18 r l=2
0001___11___10010_0_010___0000011
#SEND  en   id=19 r l=5
0001___11___10011_0_101___0011111

#SEND  en   id=20 r l=5
0001___11___10100_0_101___0011111

################################################################################
#
# START CONFIGURATION
#
# The bsg tag network is now live! We can begin our configuration.

### STEP 1: INITIALIZE EVERYTHING

# Reset both ASIC and GW Prev Link IO Control
#SEND  en   id=13 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___11___01101_1_011___0000110

# Reset both ASIC and GW Prev Link CORE Control
#SEND  en   id=14 d l=2   {up_link_reset, down_link_reset}
0001___11___01110_1_010___0000011

# Reset both ASIC and GW Prev CT CORE Control
#SEND  en   id=15 d l=2   {reset, fifo_reset}
0001___11___01111_1_010___0000011

# Reset both ASIC and GW Next Link IO Control
#SEND  en   id=16 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___11___10000_1_011___0000110

# Reset both ASIC and GW Next Link CORE Control
#SEND  en   id=17 d l=2   {up_link_reset, down_link_reset}
0001___11___10001_1_010___0000011

# Reset both ASIC and GW Next CT CORE Control
#SEND  en   id=18 d l=2   {reset, fifo_reset}
0001___11___10010_1_010___0000011

# Reset ASIC Router Control and set cord to 1
#SEND  en   id=19 d l=5   {reset, cord}
0001___01___10011_1_101___0010001

# Reset GW Router Control and set cord to 0
#SEND  en   id=19 d l=5   {reset, cord}
0001___10___10011_1_101___0010000

# Reset ASIC Manycore Control and set dest cord to 0
#SEND  en   id=20 d l=5   {reset, cord}
0001___01___10100_1_101___0010000

# Reset GW Manycore Control and set dest cord to 1
#SEND  en   id=20 d l=5   {reset, cord}
0001___10___10100_1_101___0010001

### STEP 2: Perform async token resets

# Async token reset for ASIC Prev IO Link
#SEND  en   id=13 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___01___01101_1_011___0000111
0001___01___01101_1_011___0000110

# Assert async token reset for GW Next IO Link
#SEND  en   id=16 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___11___10000_1_011___0000111
0001___11___10000_1_011___0000110

### STEP 3: De-assert Upstream IO Links reset

# De-assert upstream reset for ASIC Prev IO Link
#SEND  en   id=13 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___01___01101_1_011___0000010

# De-assert upstream reset for GW Next IO Link
#SEND  en   id=16 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___10___10000_1_011___0000010

### STEP 4: De-assert Downstream IO Links reset

# De-assert downstream reset for ASIC Prev IO Link
#SEND  en   id=13 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___01___01101_1_011___0000000

# De-assert downstream reset for GW Next IO Link
#SEND  en   id=16 d l=3   {up_link_reset, down_link_reset, async_token_reset}
0001___10___10000_1_011___0000000

### STEP 5/6: De-assert Upstream/Downstream CORE Links reset

# De-assert upstream/downstream reset for ASIC Prev CORE Link
#SEND  en   id=14 d l=2   {up_link_reset, down_link_reset}
0001___01___01110_1_010___0000000

# De-assert upstream/downstream reset for GW Next CORE Link
#SEND  en   id=17 d l=2   {up_link_reset, down_link_reset}
0001___10___10001_1_010___0000000

### STEP 7: De-assert CT reset and fifo reset

# De-assert reset and fifo_reset for ASIC Next CT CORE Control
#SEND  en   id=15 d l=2   {reset, fifo_reset}
0001___01___01111_1_010___0000000

# De-assert reset and fifo_reset for GW Next CT CORE Control
#SEND  en   id=18 d l=2   {reset, fifo_reset}
0001___10___10010_1_010___0000000

### STEP 8: De-assert Router reset

# De-assert reset for ASIC Router Control
#SEND  en   id=19 d l=5   {reset, cord}
0001___01___10011_1_101___0000001

# De-assert reset for GW Router Control
#SEND  en   id=19 d l=5   {reset, cord}
0001___10___10011_1_101___0000000

# De-assert reset for ASIC Manycore
#SEND  en   id=20 d l=5   {reset, cord}
0001___01___10100_1_101___0000000

# De-assert reset for GW Manycore
#SEND  en   id=20 d l=5   {reset, cord}
0001___10___10100_1_101___0000001

# Wait ~64 cycles for TAG traffic to propagate to destination
0110___000000000001000000
0101___000000000000000000

################################################################################
#
# Done!
#
# Configuration is complete and we are out of reset. We should indicate we are
# done to allow the next part of the testbench to come alive.

# Assert done_o
0011___000000000000000000

