
remove_individual_pin_constraints
remove_block_pin_constraints

#set_app_options -name plan.pins.incremental -value false
#set_app_options -name plan.pins.strict_alignment -value true
set_block_pin_constraints -allowed_layers {C4 C5 K1 K2 K3 K4}

set master_tile   [index_collection [get_cells -hier *__tile] 0]
set master_vcache [index_collection [get_cells -hier *__vc] 0]

set link_width [expr [sizeof_collection [get_pins -of ${master_tile}  -filter "name=~link_in*"]] / 4]

set tile_llx    [get_attribute $master_tile boundary_bounding_box.ll_x]
set tile_lly    [get_attribute $master_tile boundary_bounding_box.ll_y]
set tile_width  [get_attribute $master_tile width]
set tile_height [get_attribute $master_tile height]

set tile_left   ${tile_llx}
set tile_right  [expr ${tile_llx}+${tile_width}]
set tile_bottom ${tile_lly}
set tile_top    [expr ${tile_lly}+${tile_height}]

set vcache_llx    [get_attribute $master_vcache boundary_bounding_box.ll_x]
set vcache_lly    [get_attribute $master_vcache boundary_bounding_box.ll_y]
set vcache_width  [get_attribute $master_vcache width]
set vcache_height [get_attribute $master_vcache height]

set vcache_left   ${vcache_llx}
set vcache_right  [expr ${vcache_llx}+${vcache_width}]
set vcache_bottom ${vcache_lly}
set vcache_top    [expr ${vcache_lly}+${vcache_height}]

set start_y 2.48
for {set i [expr ${link_width}*0]} {$i < [expr ${link_width}*1]} {incr i} {
  set layer "C4"
  set real_y [expr ${tile_lly}+${start_y}]
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_in[$i]"]                       -allowed_layers "$layer" -location "${tile_left}  ${real_y}"
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_out[[expr $i+${link_width}]]"] -allowed_layers "$layer" -location "${tile_right} ${real_y}"
  set start_y [expr $start_y + 0.160]
}

set odd     0
set start_y 3.968
for {set i [expr ${link_width}*0]} {$i < [expr ${link_width}*1]} {incr i} {
  if { $odd } {
    set layer "K1"
  } else {
    set layer "K3"
  }
  set real_y [expr ${tile_lly}+${start_y}]
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_out[$i]"]                   -allowed_layers "$layer" -location "${tile_left}  ${real_y}"
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_in[[expr $i+$link_width]]"] -allowed_layers "$layer" -location "${tile_right} ${real_y}"
  set start_y [expr $start_y + 0.128]
  set odd [expr ($odd+1) % 2]
}

set count   0
set start_x 140.160
for {set i [expr ${link_width}*2]} {$i < [expr ${link_width}*3]} {incr i} {
  set layer "C5"
  set real_x [expr ${tile_llx}+${start_x}]
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_in[$i]"]                     -allowed_layers "$layer" -location "${real_x} ${tile_top}"
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_out[[expr $i+$link_width]]"] -allowed_layers "$layer" -location "${real_x} ${tile_bottom}"
  set start_x [expr $start_x + 0.160]
  incr count
  if {$count == 13} {
    set start_x [expr $start_x + 1.280]
    set count 0
  }
}

for {set i [expr ${link_width}*2]} {$i < [expr ${link_width}*3]} {incr i} {
  set layer "C5"
  set real_x [expr ${tile_llx}+${start_x}]
  puts "name==link_in[[expr $i+$link_width]] --> $real_x"
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_out[$i]"]                   -allowed_layers "$layer" -location "${real_x} ${tile_top}"
  set_individual_pin_constraints -pins [get_pins -of ${master_tile} -filter "name==link_in[[expr $i+$link_width]]"] -allowed_layers "$layer" -location "${real_x} ${tile_bottom}"
  set start_x [expr $start_x + 0.160]
  incr count
  if {$count == 13} {
    set start_x [expr $start_x + 1.280]
    set count 0
  }
}

set count   0
set start_x 140.160
for {set i [expr ${link_width}*0]} {$i < [expr ${link_width}*1]} {incr i} {
  set layer "C5"
  set real_x [expr ${vcache_llx}+${start_x}]
  set_individual_pin_constraints -pins [get_pins -of ${master_vcache} -filter "name==link_sif_i[$i]"] -allowed_layers "$layer" -location "${real_x} ${vcache_top}"
  set start_x [expr $start_x + 0.160]
  incr count
  if {$count == 13} {
    set start_x [expr $start_x + 1.280]
    set count 0
  }
}

for {set i [expr ${link_width}*0]} {$i < [expr ${link_width}*1]} {incr i} {
  set layer "C5"
  set real_x [expr ${vcache_llx}+${start_x}]
  set_individual_pin_constraints -pins [get_pins -of ${master_vcache} -filter "name==link_sif_o[$i]"] -allowed_layers "$layer" -location "${real_x} ${vcache_top}"
  set start_x [expr $start_x + 0.160]
  incr count
  if {$count == 13} {
    set start_x [expr $start_x + 1.280]
    set count 0
  }
}

set count   0
set start_x 140.160
foreach_in_collection pin [get_pins -of ${master_vcache} -filter "name=~dma_*"] {
  set layer "C5"
  set real_x [expr ${vcache_llx}+${start_x}]
  set_individual_pin_constraints -pins $pin -allowed_layers "$layer" -location "${real_x} ${vcache_bottom}"
  set start_x [expr $start_x + 0.160]
  incr count
  if {$count == 13} {
    set start_x [expr $start_x + 1.280]
    set count 0
  }
}

set                  all_other_pins [get_pins -of ${master_tile} -filter "name=~clk_i"]
append_to_collection all_other_pins [get_pins -of ${master_tile} -filter "name=~reset_i"]
append_to_collection all_other_pins [get_pins -of ${master_tile} -filter "name=~my_*"]

set start_y 40.88
foreach_in_collection pin $all_other_pins {
  set layer "C4"
  set real_y [expr ${tile_lly}+${start_y}]
  set_individual_pin_constraints -pins $pin -allowed_layers "$layer" -location "${tile_right} ${real_y}"
  set start_y [expr $start_y + 0.160]
}

set                  all_other_pins [get_pins -of ${master_vcache} -filter "name=~clk_i"]
append_to_collection all_other_pins [get_pins -of ${master_vcache} -filter "name=~reset_i"]
append_to_collection all_other_pins [get_pins -of ${master_vcache} -filter "name=~my_*"]

set start_y 98.48
foreach_in_collection pin $all_other_pins {
  set layer "C4"
  set real_y [expr ${vcache_lly}+${start_y}]
  set_individual_pin_constraints -pins $pin -allowed_layers "$layer" -location "${vcache_right} ${real_y}"
  set start_y [expr $start_y + 0.160]
}

