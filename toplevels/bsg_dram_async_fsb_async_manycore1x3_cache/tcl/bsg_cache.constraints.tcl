create_clock -period 2.0 -name CLK [get_ports clk_i]

set_output_delay 1.0 [all_outputs] -clock [get_clocks CLK] -max
set_output_delay 0.0 [all_outputs] -clock [get_clocks CLK] -min

set_input_delay  1.0 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -max
set_input_delay  0.0 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -min

set_max_delay 2.2 -from [remove_from_collection [all_inputs] [get_ports clk_i]] -to [all_outputs]
set_min_delay 0.0 -from [remove_from_collection [all_inputs] [get_ports clk_i]] -to [all_outputs]

