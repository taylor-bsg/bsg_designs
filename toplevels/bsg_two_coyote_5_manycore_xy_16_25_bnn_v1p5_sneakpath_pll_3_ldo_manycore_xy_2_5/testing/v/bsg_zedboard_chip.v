`include "bsg_rocket_pkg.vh"
`include "bsg_fsb_pkg.v"

module bsg_zedboard_chip
  import bsg_rocket_pkg::*;
  import bsg_fsb_pkg::*;
# (parameter channel_width_p=8
  ,parameter ring_bytes_p=10
  ,parameter ring_width_p=ring_bytes_p*channel_width_p
  ,parameter num_rockets_gp=5
  ,parameter nodes_p=num_rockets_gp+2
  ,parameter dcdc_fsb_idx_lp = 5
  ,parameter boot_fsb_idx_lp = 6)// 5-rocket-nodes +  dcdc-manycore + 1-fsb-boot-node
  (input                        clk_i
  ,input                        reset_i
  ,output                       boot_done_o
  // host in
  ,input   [num_rockets_gp-1:0] host_valid_i
  ,input             bsg_host_t host_data_i [num_rockets_gp-1:0]
  ,output  [num_rockets_gp-1:0] host_ready_o
  // host out
  ,output  [num_rockets_gp-1:0] host_valid_o
  ,output            bsg_host_t host_data_o [num_rockets_gp-1:0]
  ,input   [num_rockets_gp-1:0] host_ready_i
  // aw out
  ,output                       [num_rockets_gp-1:0] nasti_aw_valid_o
  ,output       bsg_nasti_a_pkt nasti_aw_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_aw_ready_i
  // w out
  ,output                       [num_rockets_gp-1:0] nasti_w_valid_o
  ,output       bsg_nasti_w_pkt nasti_w_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_w_ready_i
  // b in
  ,input                        [num_rockets_gp-1:0] nasti_b_valid_i
  ,input        bsg_nasti_b_pkt nasti_b_data_i [num_rockets_gp-1:0]
  ,output                       [num_rockets_gp-1:0] nasti_b_ready_o
  // ar out
  ,output                       [num_rockets_gp-1:0] nasti_ar_valid_o
  ,output       bsg_nasti_a_pkt nasti_ar_data_o [num_rockets_gp-1:0]
  ,input                        [num_rockets_gp-1:0] nasti_ar_ready_i
  // r in
  ,input                        [num_rockets_gp-1:0] nasti_r_valid_i
  ,input        bsg_nasti_r_pkt nasti_r_data_i [num_rockets_gp-1:0]
  ,output                       [num_rockets_gp-1:0] nasti_r_ready_o
  // in
  ,input                        v_i
  ,input     [ring_width_p-1:0] data_i
  ,output                       yumi_o
  // out
  ,output                       v_o
  ,output    [ring_width_p-1:0] data_o
  ,input                        ready_i);

  // boot done
  wire boot_done;

  // into nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_A;
  wire [ring_width_p-1:0] core_node_data_A [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_ready_A;

  // into nodes (control)
  wire [nodes_p-1:0]      core_node_en_r_lo;
  wire [nodes_p-1:0]      core_node_reset_r_lo;

  // out of nodes (fsb interface)
  wire [nodes_p-1:0]      core_node_v_B;
  wire [ring_width_p-1:0] core_node_data_B [nodes_p-1:0];
  wire [nodes_p-1:0]      core_node_yumi_B;
  // rocket nodes master(node[0-4])
  genvar i;

  for (i = 0; i < num_rockets_gp; i=i+1)
   begin: n
     bsg_rocket_node_master #
       (.dest_id_p(i))
     mstr
       (.clk_i(clk_i)
       // ctrl
       ,.reset_i(core_node_reset_r_lo[i] & (~boot_done))
       ,.en_i(core_node_en_r_lo[i])
       // in
       ,.v_i(core_node_v_A[i])
       ,.data_i(core_node_data_A[i])
       ,.ready_o(core_node_ready_A[i])
       // out
       ,.v_o(core_node_v_B[i])
       ,.data_o(core_node_data_B[i])
       ,.yumi_i(core_node_yumi_B[i])
       // host in
       ,.host_valid_i(host_valid_i[i])
       ,.host_data_i(host_data_i[i])
       ,.host_ready_o(host_ready_o[i])
       // host out
       ,.host_valid_o(host_valid_o[i])
       ,.host_data_o(host_data_o[i])
       ,.host_ready_i(host_ready_i[i])
       // aw out
       ,.nasti_aw_valid_o(nasti_aw_valid_o[i])
       ,.nasti_aw_data_o(nasti_aw_data_o[i])
       ,.nasti_aw_ready_i(nasti_aw_ready_i[i])
       // w out
       ,.nasti_w_valid_o(nasti_w_valid_o[i])
       ,.nasti_w_data_o(nasti_w_data_o[i])
       ,.nasti_w_ready_i(nasti_w_ready_i[i])
       // b in
       ,.nasti_b_valid_i(nasti_b_valid_i[i])
       ,.nasti_b_data_i(nasti_b_data_i[i])
       ,.nasti_b_ready_o(nasti_b_ready_o[i])
       // ar out
       ,.nasti_ar_valid_o(nasti_ar_valid_o[i])
       ,.nasti_ar_data_o(nasti_ar_data_o[i])
       ,.nasti_ar_ready_i(nasti_ar_ready_i[i])
       // r in
       ,.nasti_r_valid_i(nasti_r_valid_i[i])
       ,.nasti_r_data_i(nasti_r_data_i[i])
       ,.nasti_r_ready_o(nasti_r_ready_o[i]));
  end 


  `ifdef bsg_enable_dcdc
  //the DC/DC manycore node master
  bsg_manycore_node_master #
    (.client_id_p( dcdc_fsb_idx_lp)
    ,.ring_width_p(ring_width_p))
  mstr
    (.clk_i(clk_i)
    // ctrl
    ,.reset_i(~boot_done )
    ,.en_i(core_node_en_r_lo[ dcdc_fsb_idx_lp])
    // in
    ,.v_i(core_node_v_A[ dcdc_fsb_idx_lp])
    ,.data_i(core_node_data_A[ dcdc_fsb_idx_lp])
    ,.ready_o(core_node_ready_A[ dcdc_fsb_idx_lp])
    // out
    ,.v_o(core_node_v_B[ dcdc_fsb_idx_lp])
    ,.data_o(core_node_data_B[ dcdc_fsb_idx_lp])
    ,.yumi_i(core_node_yumi_B[ dcdc_fsb_idx_lp]));
  
  `else
   assign  core_node_ready_A [ dcdc_fsb_idx_lp ] = 1'b1 ;
   assign  core_node_v_B     [ dcdc_fsb_idx_lp ] = 1'b0 ;
   assign  core_node_data_B  [ dcdc_fsb_idx_lp ] =  'b0 ;

  `endif

  // fsb boot node (node[6])
  bsg_test_node_master #
    (.ring_width_p(80))
  boot
    (.clk_i(clk_i)
    ,.reset_i(core_node_reset_r_lo[boot_fsb_idx_lp])
    // control
    ,.en_i(core_node_en_r_lo[boot_fsb_idx_lp])
    ,.done_o(boot_done)
    // out
    ,.v_o(core_node_v_B[boot_fsb_idx_lp])
    ,.data_o(core_node_data_B[boot_fsb_idx_lp])
    ,.yumi_i(core_node_yumi_B[boot_fsb_idx_lp])
    // not used
    ,.v_i()
    ,.data_i()
    ,.ready_o());

  assign boot_done_o = boot_done;

  // fsb

  bsg_fsb #
    (.width_p(ring_width_p)
    ,.nodes_p(nodes_p)
    ,.snoop_vec_p({nodes_p{1'b0}})
    // if master, enable at startup so that it can drive things
    ,.enabled_at_start_vec_p({nodes_p{1'b1}}))
  fsb
    (.clk_i(clk_i)
    ,.reset_i(reset_i)
    // node ctrl
    ,.node_reset_r_o(core_node_reset_r_lo)
    ,.node_en_r_o(core_node_en_r_lo)
    // node in
    ,.node_v_i(core_node_v_B)
    ,.node_data_i(core_node_data_B)
    ,.node_yumi_o(core_node_yumi_B)
    // node out
    ,.node_v_o(core_node_v_A)
    ,.node_data_o(core_node_data_A)
    ,.node_ready_i(core_node_ready_A)
    // asm in
    ,.asm_v_i(v_i)
    ,.asm_data_i(data_i)
    ,.asm_yumi_o(yumi_o)
    // asm out
    ,.asm_v_o(v_o)
    ,.asm_data_o(data_o)
    ,.asm_ready_i(ready_i));

endmodule
