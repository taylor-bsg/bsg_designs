module  outerspace_top(
	clock, 
	reset, 
	fsb2fsb_ctrl_data, 
	fsb2fsb_ctrl_valid, 
	fsb2fsb_ctrl_ready, 
	fsb_ctrl2fsb_data, 
	fsb_ctrl2fsb_valid, 
	fsb_ctrl2fsb_yumi);

   input clock;
   input reset;
   input [79:0] fsb2fsb_ctrl_data;
   input fsb2fsb_ctrl_valid;
   input fsb2fsb_ctrl_ready;
   output [79:0] fsb_ctrl2fsb_data;
   output fsb_ctrl2fsb_valid;
   output fsb_ctrl2fsb_yumi;

        assign fsb_ctrl2fsb_yumi = 1'b0;
        assign fsb_ctrl2fsb_valid= 1'b0;
        assign fsb_ctrl2fsb_data = 80'(0);
endmodule

