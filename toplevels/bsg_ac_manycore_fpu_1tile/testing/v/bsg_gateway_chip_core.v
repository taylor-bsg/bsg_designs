`timescale 1ps/1ps

module bsg_gateway_chip_core

import bsg_tag_pkg::*;
import bsg_chip_pkg::*;

 #(parameter  num_asic_p = 1
  ,localparam bsg_manycore_link_sif_width_lp = `bsg_manycore_link_sif_width(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp)
  )

  (// Manycore clk and reset
   input                   mc_clk_i
  ,output [num_asic_p-1:0] mc_reset_o
  ,output                  init_done_o
  
  ,input                   io_master_clk_i
  ,input                   router_clk_i
  ,input                   tag_clk_i

  // Manycore links
  ,input  [num_asic_p-1:0][bsg_manycore_link_sif_width_lp-1:0] mc_links_sif_i
  ,output [num_asic_p-1:0][bsg_manycore_link_sif_width_lp-1:0] mc_links_sif_o
  
  // comm link connection to next chip
  ,input  [link_num_channels_gp-1:0]      ci_clk_i
  ,input  [link_num_channels_gp-1:0]      ci_v_i
  ,input  [link_num_channels_gp-1:0][8:0] ci_data_i
  ,output [link_num_channels_gp-1:0]      ci_tkn_o
  
  ,output [link_num_channels_gp-1:0]      ci2_clk_o
  ,output [link_num_channels_gp-1:0]      ci2_v_o
  ,output [link_num_channels_gp-1:0][8:0] ci2_data_o
  ,input  [link_num_channels_gp-1:0]      ci2_tkn_i
  
  // comm link connection to prev chip
  ,input  [link_num_channels_gp-1:0]      co_clk_i
  ,input  [link_num_channels_gp-1:0]      co_v_i
  ,input  [link_num_channels_gp-1:0][8:0] co_data_i
  ,output [link_num_channels_gp-1:0]      co_tkn_o
  
  ,output [link_num_channels_gp-1:0]      co2_clk_o
  ,output [link_num_channels_gp-1:0]      co2_v_o
  ,output [link_num_channels_gp-1:0][8:0] co2_data_o
  ,input  [link_num_channels_gp-1:0]      co2_tkn_i
  
  // Control signals
  ,output                  tag_clk_o
  ,output [1:0]            tag_en_o
  ,output                  tag_data_o
  
  ,output                  clk_a_o
  ,output                  clk_b_o
  ,output                  clk_c_o
  );

  `declare_bsg_manycore_link_sif_s(manycore_addr_width_gp, manycore_data_width_gp, manycore_x_cord_width_gp, manycore_y_cord_width_gp, manycore_load_id_width_gp);
  `declare_bsg_ready_and_link_sif_s(ct_width_gp, bsg_ready_and_link_sif_s);
  `declare_bsg_cache_dma_pkt_s(manycore_addr_width_gp+manycore_byte_offset_width_gp-1);


  logic manycore_clk;
  assign manycore_clk = mc_clk_i;
  assign clk_a_o = manycore_clk;

  logic io_master_clk;
  assign io_master_clk = io_master_clk_i;
  assign clk_b_o = io_master_clk;

  logic router_clk;
  assign router_clk = router_clk_i;
  assign clk_c_o = router_clk;

  logic tag_clk;
  assign tag_clk = tag_clk_i;
  assign tag_clk_o = tag_clk;

  //////////////////////////////////////////////////
  //
  // Nonsynth Reset Generator(s)
  //

  logic tag_reset;
  bsg_nonsynth_reset_gen #(.num_clocks_p(1),.reset_cycles_lo_p(10),.reset_cycles_hi_p(5))
    tag_reset_gen
      (.clk_i(tag_clk)
      ,.async_reset_o(tag_reset)
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Track Replay
  //

  localparam tag_trace_rom_addr_width_lp = 32;
  localparam tag_trace_rom_data_width_lp = 22;

  logic [tag_trace_rom_addr_width_lp-1:0] rom_addr_li;
  logic [tag_trace_rom_data_width_lp-1:0] rom_data_lo;

  logic [1:0] tag_trace_en_r_lo;
  logic       tag_trace_done_lo;

  // TAG TRACE ROM
  bsg_tag_boot_rom #(.width_p( tag_trace_rom_data_width_lp )
                    ,.addr_width_p( tag_trace_rom_addr_width_lp )
                    )
    tag_trace_rom
      (.addr_i( rom_addr_li )
      ,.data_o( rom_data_lo )
      );

  // TAG TRACE REPLAY
  bsg_tag_trace_replay #(.rom_addr_width_p( tag_trace_rom_addr_width_lp )
                        ,.rom_data_width_p( tag_trace_rom_data_width_lp )
                        ,.num_masters_p( 2 )
                        ,.num_clients_p( tag_num_clients_gp )
                        ,.max_payload_width_p( tag_max_payload_width_gp )
                        )
    tag_trace_replay
      (.clk_i   ( tag_clk )
      ,.reset_i ( tag_reset    )
      ,.en_i    ( 1'b1            )

      ,.rom_addr_o( rom_addr_li )
      ,.rom_data_i( rom_data_lo )

      ,.valid_i ( 1'b0 )
      ,.data_i  ( '0 )
      ,.ready_o ()

      ,.valid_o    ()
      ,.en_r_o     ( tag_trace_en_r_lo )
      ,.tag_data_o ( tag_data_o        )
      ,.yumi_i     ( 1'b1 )

      ,.done_o  ( tag_trace_done_lo )
      ,.error_o ()
      ) ;

  assign tag_en_o[0] = tag_trace_en_r_lo[0];
  
  bsg_launch_sync_sync #(.width_p(1)) done_blss
   (.iclk_i       ( tag_clk )
    ,.iclk_reset_i( 1'b0 )
    ,.oclk_i      ( manycore_clk )
    ,.iclk_data_i ( tag_trace_done_lo )
    ,.iclk_data_o (  )
    ,.oclk_data_o ( init_done_o )
    );

  //////////////////////////////////////////////////
  //
  // BSG Tag Master Instance (Copied from ASIC)
  //

  // All tag lines from the btm
  bsg_tag_s [tag_num_clients_gp-1:0] tag_lines_lo;

  // // Tag lines for clock generators
  // bsg_tag_s       async_reset_tag_lines_lo;
  // bsg_tag_s [2:0] osc_tag_lines_lo;
  // bsg_tag_s [2:0] osc_trigger_tag_lines_lo;
  // bsg_tag_s [2:0] ds_tag_lines_lo;
  // bsg_tag_s [2:0] sel_tag_lines_lo;

  // assign async_reset_tag_lines_lo = tag_lines_lo[0];
  // assign osc_tag_lines_lo         = tag_lines_lo[3:1];
  // assign osc_trigger_tag_lines_lo = tag_lines_lo[6:4];
  // assign ds_tag_lines_lo          = tag_lines_lo[9:7];
  // assign sel_tag_lines_lo         = tag_lines_lo[12:10];

  // Tag lines for io complex
  wire bsg_tag_s prev_link_io_tag_lines_lo   = tag_lines_lo[13];
  wire bsg_tag_s prev_link_core_tag_lines_lo = tag_lines_lo[14];
  wire bsg_tag_s prev_ct_core_tag_lines_lo   = tag_lines_lo[15];
  wire bsg_tag_s next_link_io_tag_lines_lo   = tag_lines_lo[16];
  wire bsg_tag_s next_link_core_tag_lines_lo = tag_lines_lo[17];
  wire bsg_tag_s next_ct_core_tag_lines_lo   = tag_lines_lo[18];
  wire bsg_tag_s router_core_tag_lines_lo    = tag_lines_lo[19];
  
  // Tag lines for the manycore
  wire bsg_tag_s manycore_tag_lines_lo = tag_lines_lo[20];

  // BSG tag master instance
  bsg_tag_master #(.els_p( tag_num_clients_gp )
                  ,.lg_width_p( tag_lg_max_payload_width_gp )
                  )
    btm
      (.clk_i      ( tag_clk )
      ,.data_i     ( tag_trace_en_r_lo[1] ? tag_data_o : 1'b0 )
      ,.en_i       ( 1'b1 )
      ,.clients_r_o( tag_lines_lo )
      );

  //////////////////////////////////////////////////
  //
  // BSG Tag Client Instance (Copied from ASIC)
  //

  // Tag payload for manycore control signals
  typedef struct packed { 
      logic reset;
      logic [wh_cord_width_gp-1:0] cord;
  } manycore_tag_payload_s;

  // Tag payload for manycore control signals
  manycore_tag_payload_s manycore_tag_data_lo;
  logic                  manycore_tag_new_data_lo;

  bsg_tag_client #(.width_p( $bits(manycore_tag_data_lo) ), .default_p( 0 ))
    btc_manycore
      (.bsg_tag_i     ( manycore_tag_lines_lo )
      ,.recv_clk_i    ( manycore_clk )
      ,.recv_reset_i  ( 1'b0 )
      ,.recv_new_r_o  ( manycore_tag_new_data_lo )
      ,.recv_data_r_o ( manycore_tag_data_lo )
      );

  //////////////////////////////////////////////////
  //
  // Commlink Swizzle
  //

  logic       ci_clk_li;
  logic       ci_v_li;
  logic [8:0] ci_data_li;
  logic       ci_tkn_lo;

  logic       co_clk_lo;
  logic       co_v_lo;
  logic [8:0] co_data_lo;
  logic       co_tkn_li;

  logic       ci2_clk_li;
  logic       ci2_v_li;
  logic [8:0] ci2_data_li;
  logic       ci2_tkn_lo;

  logic       co2_clk_lo;
  logic       co2_v_lo;
  logic [8:0] co2_data_lo;
  logic       co2_tkn_li;

  bsg_chip_swizzle_adapter
    swizzle
      (.port_ci_clk_i   (ci_clk_i)
      ,.port_ci_v_i     (ci_v_i)
      ,.port_ci_data_i  (ci_data_i)
      ,.port_ci_tkn_o   (ci_tkn_o)

      ,.port_ci2_clk_o  (ci2_clk_o)
      ,.port_ci2_v_o    (ci2_v_o)
      ,.port_ci2_data_o (ci2_data_o)
      ,.port_ci2_tkn_i  (ci2_tkn_i)

      ,.port_co_clk_i   (co_clk_i)
      ,.port_co_v_i     (co_v_i)
      ,.port_co_data_i  (co_data_i)
      ,.port_co_tkn_o   (co_tkn_o)

      ,.port_co2_clk_o  (co2_clk_o)
      ,.port_co2_v_o    (co2_v_o)
      ,.port_co2_data_o (co2_data_o)
      ,.port_co2_tkn_i  (co2_tkn_i)

      ,.guts_ci_clk_o  (ci_clk_li)
      ,.guts_ci_v_o    (ci_v_li)
      ,.guts_ci_data_o (ci_data_li)
      ,.guts_ci_tkn_i  (ci_tkn_lo)

      ,.guts_co_clk_i  (co_clk_lo)
      ,.guts_co_v_i    (co_v_lo)
      ,.guts_co_data_i (co_data_lo)
      ,.guts_co_tkn_o  (co_tkn_li)

      ,.guts_ci2_clk_o (ci2_clk_li)
      ,.guts_ci2_v_o   (ci2_v_li)
      ,.guts_ci2_data_o(ci2_data_li)
      ,.guts_ci2_tkn_i (ci2_tkn_lo)

      ,.guts_co2_clk_i (co2_clk_lo)
      ,.guts_co2_v_i   (co2_v_lo)
      ,.guts_co2_data_i(co2_data_lo)
      ,.guts_co2_tkn_o (co2_tkn_li)
      );

  //////////////////////////////////////////////////
  //
  // BSG Chip IO Complex
  //

  logic router_reset_lo;

  bsg_ready_and_link_sif_s [ct_num_in_gp-1:0] rtr_links_li;
  bsg_ready_and_link_sif_s [ct_num_in_gp-1:0] rtr_links_lo;

  bsg_chip_io_complex #(.num_router_groups_p( 1 )

                       ,.link_width_p( link_width_gp )
                       ,.link_channel_width_p( link_channel_width_gp )
                       ,.link_num_channels_p( link_num_channels_gp )
                       ,.link_lg_fifo_depth_p( link_lg_fifo_depth_gp )
                       ,.link_lg_credit_to_token_decimation_p( link_lg_credit_to_token_decimation_gp )

                       ,.ct_width_p( ct_width_gp )
                       ,.ct_num_in_p( ct_num_in_gp )
                       ,.ct_remote_credits_p( ct_remote_credits_gp )
                       ,.ct_use_pseudo_large_fifo_p( ct_use_pseudo_large_fifo_gp )
                       ,.ct_lg_credit_decimation_p( ct_lg_credit_decimation_gp )

                       ,.wh_cord_markers_pos_p({wh_cord_markers_pos_b_gp, wh_cord_markers_pos_a_gp})
                       ,.wh_len_width_p( wh_len_width_gp )
                       )
    io_complex
      (.core_clk_i ( router_clk )
      ,.io_clk_i   ( io_master_clk )

      ,.prev_link_io_tag_lines_i( prev_link_io_tag_lines_lo )
      ,.prev_link_core_tag_lines_i( prev_link_core_tag_lines_lo )
      ,.prev_ct_core_tag_lines_i( prev_ct_core_tag_lines_lo )
      
      ,.next_link_io_tag_lines_i( next_link_io_tag_lines_lo )
      ,.next_link_core_tag_lines_i( next_link_core_tag_lines_lo )
      ,.next_ct_core_tag_lines_i( next_ct_core_tag_lines_lo )

      ,.rtr_core_tag_lines_i( router_core_tag_lines_lo )
      
      ,.ci_clk_i  ( ci_clk_li )
      ,.ci_v_i    ( ci_v_li )
      ,.ci_data_i ( ci_data_li[link_channel_width_gp-1:0] )
      ,.ci_tkn_o  ( ci_tkn_lo )

      ,.co_clk_o  ( co_clk_lo )
      ,.co_v_o    ( co_v_lo )
      ,.co_data_o ( co_data_lo[link_channel_width_gp-1:0] )
      ,.co_tkn_i  ( co_tkn_li )

      ,.ci2_clk_i  ( ci2_clk_li )
      ,.ci2_v_i    ( ci2_v_li )
      ,.ci2_data_i ( ci2_data_li[link_channel_width_gp-1:0] )
      ,.ci2_tkn_o  ( ci2_tkn_lo )

      ,.co2_clk_o  ( co2_clk_lo )
      ,.co2_v_o    ( co2_v_lo )
      ,.co2_data_o ( co2_data_lo[link_channel_width_gp-1:0] )
      ,.co2_tkn_i  ( co2_tkn_li )
      
      ,.rtr_links_i ( rtr_links_li )
      ,.rtr_links_o ( rtr_links_lo )

      ,.rtr_reset_o ( router_reset_lo )
      ,.rtr_cord_o  ()
      );


  //////////////////////////////////////////////////
  //
  // Adapter between manycore and IO complex
  //

  bsg_manycore_link_async_to_wormhole #(.addr_width_p( manycore_addr_width_gp )
                                       ,.data_width_p( manycore_data_width_gp )
                                       ,.load_id_width_p( manycore_load_id_width_gp )
                                       ,.x_cord_width_p( manycore_x_cord_width_gp )
                                       ,.y_cord_width_p( manycore_y_cord_width_gp )

                                       ,.flit_width_p( ct_width_gp )
                                       ,.dims_p( 1 )
                                       ,.cord_markers_pos_p({ wh_cord_markers_pos_b_gp, wh_cord_markers_pos_a_gp })
                                       ,.len_width_p( wh_len_width_gp )
                                       )
    manycore_wormhole_adapter
      (.mc_clk_i   ( manycore_clk )
      ,.mc_reset_i ( manycore_tag_data_lo.reset )

      ,.mc_links_sif_i ( mc_links_sif_i )
      ,.mc_links_sif_o ( mc_links_sif_o )
      
      ,.mc_dest_cord_i ( manycore_tag_data_lo.cord )

      ,.wh_clk_i   ( router_clk )
      ,.wh_reset_i ( router_reset_lo )
      
      ,.wh_link_i ( rtr_links_lo )
      ,.wh_link_o ( rtr_links_li )
      );
    
    assign mc_reset_o = manycore_tag_data_lo.reset;

endmodule

