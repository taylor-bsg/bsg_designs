#=========================================================================
# vcs.mk.include
#=========================================================================
# Design-specific includes for VCS simulation.
#
# This includes additional VCS options and VCS dependencies needed for
# Rocket simulation.
#
# Note: This Makefile include should be included by the certus_chip repo's
# VCS testing scripts. The contents here are based on bsg_rocket's
# testing/rtl Makefile.
#
# Date   : March  9, 2017
# Author : Christopher Torng
#

#-------------------------------------------------------------------------
# Defines
#-------------------------------------------------------------------------
# Some defines used in the bsg_rocket Makefile

MODEL  := Top
CONFIG := Bsg1AccelVLSIConfig
TB     := rocketTestHarness

TIMEOUT_CYCLES ?= 100000000

# Some repos are huge and take hours to build, but they never change. We
# share these big repos in the dreslinski-lab group directory here.
# Currently, only bsg_riscv is shared.

MICHIGAN_CERTUS_COMMON=/n/trenton/v/dreslinskilab/certus/certus_common

# We need a recent install of GCC for the Rocket C++ testing sources.
# These are installed locally in the certus_common group directory.

CC  = $(MICHIGAN_CERTUS_COMMON)/installs/bin/gcc
CXX = $(MICHIGAN_CERTUS_COMMON)/installs/bin/g++
CXXFLAGS := -O1

# Directory paths

export RISCV       = $(MICHIGAN_CERTUS_COMMON)/bsg_riscv/riscv-install
export BSG_OUT_DIR = $(VCS_COMMON_RESULTS_DIR)

export BSG_ROCKET_COYOTE_GENERATEDSRC_DIR = $(BSG_ROCKET_DIR)/rockets/coyote/generated-src
export BSG_ROCKET_COMMON_DIR              = $(BSG_ROCKET_DIR)/common

# C++ testbench sources

sim_csrcs = \
	$(BSG_RISCV_DIR)/rocket-chip/csrc/vcs_main.$(TB).cc \
	$(BSG_RISCV_DIR)/rocket-chip/csrc/mm.cc \
	$(BSG_RISCV_DIR)/rocket-chip/csrc/mm_dramsim2.cc \

# Set up top-level module bsg_config
#
# Note: These vars override the defaults in the certus_chip VCS scripts

BSG_TOP_SIM_MODULE     = rocketTestHarness
BSG_CHIP_INSTANCE_PATH = rocketTestHarness.dut.chip

#-------------------------------------------------------------------------
# Generated files
#-------------------------------------------------------------------------
# Here are the rules to generate files needed for Rocket simulation. Most
# of these come directly from bsg_rocket's testing Makefile.

# DRAMSim2 init file
#
# The C++ testbench is hardcoded to expect this in the current working
# directory, so we need to link it here instead of the results directory.

dramsim2_ini:
	ln -s $(BSG_RISCV_DIR)/rocket-chip/emulator/dramsim2_ini $@

# DRAMSim2

DRAMSIM_OBJS := $(patsubst %.cpp,%.o,$(wildcard $(BSG_RISCV_DIR)/rocket-chip/dramsim2/*.cpp))

$(DRAMSIM_OBJS): %.o: %.cpp
	$(CXX) $(CXXFLAGS) -DNO_STORAGE -DNO_OUTPUT -Dmain=nomain -c -o $@ $<

$(BSG_OUT_DIR)/libdramsim.a: $(DRAMSIM_OBJS) $(BSG_OUT_DIR)
	ar rcs $@ $(DRAMSIM_OBJS)

# Constants Header Files

params_file   = $(BSG_ROCKET_COYOTE_GENERATEDSRC_DIR)/$(MODEL).$(CONFIG).prm
consts_header = $(BSG_OUT_DIR)/consts.$(CONFIG).h

$(consts_header): $(params_file) $(BSG_OUT_DIR)
	echo "#ifndef __CONST_H__" > $@
	echo "#define __CONST_H__" >> $@
	sed -r 's/\(([A-Za-z0-9_]+),([A-Za-z0-9_]+)\)/#define \1 \2/' $< >> $@
	echo "#define TBFRAG \"$(MODEL).$(CONFIG).tb.cpp\"" >> $@
	echo "#endif // __CONST_H__" >> $@

# Sram generation

sram_library_file = $(BSG_ROCKET_COYOTE_GENERATEDSRC_DIR)/sram.v

$(sram_library_file): $(BSG_ROCKET_COYOTE_GENERATEDSRC_DIR)/$(MODEL).$(CONFIG).conf
	$(BSG_ROCKET_COMMON_DIR)/scripts/bsg_vlsi_mem_gen $< > $@

# Config file for initializing registers (using +vcs+initreg+config)
#
# IMPORTANT: Here is where we initialize all registers in the design's
# netlist to random 0/1 values. This is done to avoid X-propagation bugs
# in uninitialized registers during gate-level simulation. These bugs are
# almost always not real design bugs (i.e., they would not be errors if
# the X's were replaced with 0's and 1's in real silicon). So we
# initialize the registers here and then run with different seeds to catch
# the real design bugs.

vcs_initreg_config = $(BSG_OUT_DIR)/vcs_initreg_config

$(vcs_initreg_config): $(BSG_OUT_DIR)
	@echo "module bsg_mem_1rw_sync_synth random" > $@
	@echo "module bsg_mem_1rw_sync_mask_write_bit_synth random" >> $@
	@echo "module bsg_mem_1r1w_sync_mask_write_bit_synth random" >> $@
	@echo "modtree $(DESIGN_NAME) 0 random" >> $@

# FSB rom generation
#
# The FSB master ROM is written to bsg_rocket. Normally we try to keep all
# generated files in BSG_OUT_DIR though.

bsg_fsb_trace_in     = $(BSG_DESIGNS_DIR)/modules/bsg_guts/trace_replay/testing/bsg_fsb_master_rom.rocket.trace.in
bsg_fsb_master_rom_v = $(BSG_ROCKET_COYOTE_GENERATEDSRC_DIR)/bsg_fsb_master_rom.v

$(bsg_fsb_master_rom_v):
	$(BSG_IP_CORES_DIR)/bsg_mem/bsg_ascii_to_rom.py $(bsg_fsb_trace_in) bsg_fsb_master_rom > $@

#-------------------------------------------------------------------------
# Design-specific VCS options
#-------------------------------------------------------------------------
# Here we add the extra VCS options from bsg_rocket's Makefile that are
# not already in certus_chip's common VCS options.

VCS_DESIGN_OPTIONS += -line +lint=all,noVCDE,noONGS,noUI -error=PCWM-L -quiet
VCS_DESIGN_OPTIONS += +rad +vc+list
VCS_DESIGN_OPTIONS += -v2k_generate

VCS_DESIGN_OPTIONS += +define+PRINTF_COND=$(TB).verbose
VCS_DESIGN_OPTIONS += $(RISCV)/lib/libfesvr.so
VCS_DESIGN_OPTIONS += $(BSG_OUT_DIR)/libdramsim.a # Generated
VCS_DESIGN_OPTIONS += +vcs+initreg+config+$(vcs_initreg_config) # Generated
VCS_DESIGN_OPTIONS += $(sim_csrcs)

# C++ testbench options
#
# Rocket uses a C++ testbench for more flexible testing. We include all
# the relevant files here.

VCS_DESIGN_OPTIONS += -e vcs_main
VCS_DESIGN_OPTIONS += -CC "-I$(VCS_HOME)/include"
VCS_DESIGN_OPTIONS += -CC "-I$(RISCV)/include"
VCS_DESIGN_OPTIONS += -CC "-I$(BSG_RISCV_DIR)/rocket-chip/dramsim2"
VCS_DESIGN_OPTIONS += -CC "-I./dramsim2_ini" # Generated
VCS_DESIGN_OPTIONS += -CC "-std=c++11"
VCS_DESIGN_OPTIONS += -CC "-Wl,-rpath,$(RISCV)/lib"
VCS_DESIGN_OPTIONS += -CC "-include $(consts_header)" # Generated
VCS_DESIGN_OPTIONS += -CC "-include $(BSG_ROCKET_COYOTE_GENERATEDSRC_DIR)/$(MODEL).$(CONFIG).scr_map.h"

# Turn off asserts (SEE NOTE)
#
# ctorng: When simulating Rocket designs, we get tons of SRAM address
# collision assertion failures. BSG says to ignore these and keep going,
# since they do not affect functionality. But it prints out thousands of
# these. To avoid these printouts for now, we disable asserts.
#
# When this is fixed, we should turn asserts back on.

VCS_DESIGN_OPTIONS += -assert disable

#-------------------------------------------------------------------------
# Additional design-specific VCS dependencies
#-------------------------------------------------------------------------
# These are the additional VCS dependencies from bsg_rocket's Makefile

VCS_DESIGN_DEPS += dramsim2_ini                # Generated
VCS_DESIGN_DEPS += $(BSG_OUT_DIR)/libdramsim.a # Generated
VCS_DESIGN_DEPS += $(consts_header)            # Generated
VCS_DESIGN_DEPS += $(vcs_initreg_config)       # Generated
VCS_DESIGN_DEPS += $(sram_library_file)        # Generated
VCS_DESIGN_DEPS += $(bsg_fsb_master_rom_v)     # Generated

VCS_DESIGN_DEPS += $(BSG_OUT_DIR)/bsg_ip_cores # Generated
VCS_DESIGN_DEPS += $(BSG_OUT_DIR)/bsg_designs  # Generated
VCS_DESIGN_DEPS += $(BSG_OUT_DIR)/bsg_rocket   # Generated

#-------------------------------------------------------------------------
# Additional design-specific VCS run options
#-------------------------------------------------------------------------

VCS_DESIGN_RUN_OPTIONS += -q
VCS_DESIGN_RUN_OPTIONS += +ntb_random_seed_automatic +dramsim +verbose
VCS_DESIGN_RUN_OPTIONS += +max-cycles=$(TIMEOUT_CYCLES)

# This option was only used if running simv_debug in bsg_rocket's Makefile

#VCS_DESIGN_RUN_OPTIONS += +define+DEBUG

#-------------------------------------------------------------------------
# Misc rules
#-------------------------------------------------------------------------

$(BSG_OUT_DIR)/bsg_ip_cores: $(BSG_OUT_DIR)
	-ln -s $(BSG_IP_CORES_DIR) $(dir $@)

$(BSG_OUT_DIR)/bsg_designs: $(BSG_OUT_DIR)
	-ln -s $(BSG_DESIGNS_DIR) $(dir $@)

$(BSG_OUT_DIR)/bsg_rocket: $(BSG_OUT_DIR)
	-ln -s $(BSG_ROCKET_DIR) $(dir $@)

#extra_clean_targets = vc_hdrs.h csrc ucli.key DVEfiles

#-------------------------------------------------------------------------
# Assembly tests
#-------------------------------------------------------------------------
# Search the asm_test_dir for *.hex files. Use a template to generate run
# targets for each assembly test found.

asm_test_dir  = $(RISCV)/riscv64-unknown-elf/share/riscv-tests/isa
asm_tests     = $(wildcard $(asm_test_dir)/*.hex)
num_asm_tests = $(shell ls -1 $(asm_test_dir)/*.hex | wc -l)

# VCS Assembly Test Template
#
# Generate targets for:
#
# - $1: each assembly test (the path to it)
# - $2: this is just the assembly test's name
# - $3: each VCS step (e.g., RTL, POSTSYN_FF, etc.)

define vcs_run_asm

# Run the assembly test on the simulator

$$(VCS_$3_RESULTS_DIR)/run-asm-$2.log:
	$$(VCS_$3_SIMV) $$(VCS_RUN_OPTIONS) $$(VCS_DESIGN_RUN_OPTIONS) +loadmem=$1 2>&1 | tee $$@

# Create alias target to help run an individual assembly test

VCS_$3_ASM_$2: $$(VCS_$3_RESULTS_DIR)/run-asm-$2.log

# Gather all assembly tests for this VCS step

VCS_$3_ASM_ALL += $$(VCS_$3_RESULTS_DIR)/run-asm-$2.log

endef

# Call template for each test, for each step

$(foreach test, $(asm_tests), \
  $(foreach step, $(VCS_STEPS), \
    $(eval $(call vcs_run_asm,$(test),$(basename $(notdir $(test))),$(step)))))

#-------------------------------------------------------------------------
# Assembly tests visible targets
#-------------------------------------------------------------------------
# Each of these targets runs all of the asm tests for a specific VCS step.
# They also print a summary of the status of all tests.

# Template for running each VCS step + printing the summary
#
# $1: The vcs step (e.g., RTL, RTL_HARD, POSTSYN_FF, etc.)

define aggregate_asm_tests

# Notes:
#
# - $$(shell echo "$1" | tr A-Z a-z)
#     - This converts the VCS step name to lowercase
#
# - We grep for pass and fail and dump to the summary log.
# - For summary stats, count number of passes + total number of tests

vcs_$$(shell echo "$1" | tr A-Z a-z)_asm: $$(VCS_$1_ASM_ALL)
	@echo
	@echo ---------------- $1 Assembly Test Results ----------------
	@( grep --color=never -h "\*\*\* PASS" $$(VCS_$1_RESULTS_DIR)/run-asm-*.log; \
	   grep --color=never -h "\*\*\* FAIL" $$(VCS_$1_RESULTS_DIR)/run-asm-*.log; \
	   ) | tee $$(VCS_$1_RESULTS_DIR)/summary-asm.log
	@( echo -n "Total passing   : " ; grep PASS $$(VCS_$1_RESULTS_DIR)/summary-asm.log | wc -l; \
	   echo    "Total num tests : $$(num_asm_tests)";                            \
	   echo    "Summary file    : $$(VCS_$1_RESULTS_DIR)/summary-asm.log";       \
	   ) | tee -a $$(VCS_$1_RESULTS_DIR)/summary-asm.log

endef

$(foreach step, $(VCS_STEPS), $(eval $(call aggregate_asm_tests,$(step))))

# Targets that clean the asm test logs

vcs_rtl_asm_clean:
	rm -rf $(VCS_RTL_RESULTS_DIR)/run-asm-*.log

vcs_rtl_hard_asm_clean:
	rm -rf $(VCS_RTL_HARD_RESULTS_DIR)/run-asm-*.log

vcs_postsyn_ff_asm_clean:
	rm -rf $(VCS_POSTSYN_FF_RESULTS_DIR)/run-asm-*.log

vcs_postsyn_sdf_asm_clean:
	rm -rf $(VCS_POSTSYN_SDF_RESULTS_DIR)/run-asm-*.log

vcs_postapr_ff_asm_clean:
	rm -rf $(VCS_POSTAPR_FF_RESULTS_DIR)/run-asm-*.log

vcs_postapr_sdf_asm_clean:
	rm -rf $(VCS_POSTAPR_SDF_RESULTS_DIR)/run-asm-*.log

#-------------------------------------------------------------------------
# BNN tests
#-------------------------------------------------------------------------
# Find BNN *.hex files. Use a template to generate run targets for each
# test found.

bnn_test_dir  = $(BSG_ROCKET_DIR)/common/benchmark
bnn_tests     = $(wildcard $(bnn_test_dir)/*.hex)
num_bnn_tests = $(shell ls -1 $(bnn_test_dir)/*.hex | wc -l)

# VCS BNN Test Template
#
# Generate targets for:
#
# - $1: each bnn test (the path to it)
# - $2: this is just the bnn test's name
# - $3: each VCS step (e.g., RTL, POSTSYN_FF, etc.)

define vcs_run_bnn

# Run the bnn test on the simulator
#
# Note: We redirect stderr to /dev/null to avoid printing the lengthy
# instruction dump.

$$(VCS_$3_RESULTS_DIR)/run-bnn-$2.log:
	$$(VCS_$3_SIMV) $$(VCS_RUN_OPTIONS) $$(VCS_DESIGN_RUN_OPTIONS) +loadmem=$1 2>/dev/null | tee $$@

# Create alias target to help run an individual test

VCS_$3_BNN_$2: $$(VCS_$3_RESULTS_DIR)/run-bnn-$2.log

# Gather all assembly tests for this VCS step

VCS_$3_BNN_ALL += $$(VCS_$3_RESULTS_DIR)/run-bnn-$2.log

endef

# Call template for each test, for each step

$(foreach test, $(bnn_tests), \
  $(foreach step, $(VCS_STEPS), \
    $(eval $(call vcs_run_bnn,$(test),$(basename $(notdir $(test))),$(step)))))

#-------------------------------------------------------------------------
# BNN tests visible targets
#-------------------------------------------------------------------------
# Each of these targets runs all of the bnn tests for a specific VCS step.
# They also print a summary of the status of all tests.

# Template for running each VCS step + printing the summary
#
# $1: The vcs step (e.g., RTL, RTL_HARD, POSTSYN_FF, etc.)

define aggregate_bnn_tests

# Notes:
#
# - $$(shell echo "$1" | tr A-Z a-z)
#     - This converts the VCS step name to lowercase
#
# - We grep for pass and fail and dump to the summary log.
# - For summary stats, count number of passes + total number of tests

vcs_$$(shell echo "$1" | tr A-Z a-z)_bnn: $$(VCS_$1_BNN_ALL)
	@echo
	@echo ---------------- $1 Assembly Test Results ----------------
	@( grep --color=never -h "\*\*\* PASS" $$(VCS_$1_RESULTS_DIR)/run-bnn-*.log; \
	   grep --color=never -h "\*\*\* FAIL" $$(VCS_$1_RESULTS_DIR)/run-bnn-*.log; \
	   ) | tee $$(VCS_$1_RESULTS_DIR)/summary-bnn.log
	@( echo -n "Total passing   : " ; grep PASS $$(VCS_$1_RESULTS_DIR)/summary-bnn.log | wc -l; \
	   echo    "Total num tests : $$(num_bnn_tests)";                            \
	   echo    "Summary file    : $$(VCS_$1_RESULTS_DIR)/summary-bnn.log";       \
	   ) | tee -a $$(VCS_$1_RESULTS_DIR)/summary-bnn.log

endef

$(foreach step, $(VCS_STEPS), $(eval $(call aggregate_bnn_tests,$(step))))

# Targets that clean the bnn test logs

vcs_rtl_bnn_clean:
	rm -rf $(VCS_RTL_RESULTS_DIR)/run-bnn-*.log

vcs_rtl_hard_bnn_clean:
	rm -rf $(VCS_RTL_HARD_RESULTS_DIR)/run-bnn-*.log

vcs_postsyn_ff_bnn_clean:
	rm -rf $(VCS_POSTSYN_FF_RESULTS_DIR)/run-bnn-*.log

vcs_postsyn_sdf_bnn_clean:
	rm -rf $(VCS_POSTSYN_SDF_RESULTS_DIR)/run-bnn-*.log

vcs_postapr_ff_bnn_clean:
	rm -rf $(VCS_POSTAPR_FF_RESULTS_DIR)/run-bnn-*.log

vcs_postapr_sdf_bnn_clean:
	rm -rf $(VCS_POSTAPR_SDF_RESULTS_DIR)/run-bnn-*.log

