`include "bsg_manycore_packet.vh"

//
// this instantiates a bsg_manycore array and also
// a converter between the south side of the manycore array
// and the FSB network.

module  fsb_mesh_node_stub
   import bsg_noc_pkg::*; // {P=0, W, E, N, S}
   import bsg_fsb_pkg::*;
   import bsg_chip_pkg::*;
   import bsg_dram_ctrl_pkg::*;
  #(parameter ring_width_p="inv"
    , parameter master_p="inv"
    , parameter master_id_p="inv"
    , parameter client_id_p="inv"
    )

  (  input clk_i
   , input reset_i
   , input drlp_clk_i

   // control
   , input en_i   // FIXME unused

   // input channel
   , input  v_i
   , input [ring_width_p-1:0] data_i
   , output ready_o

   // output channel
   , output v_o
   , output [ring_width_p-1:0] data_o
   , input yumi_i   // late

   //----------------------------------------------------------------
   //  Memory contoller
   , input                              dfi_clk
   , input                              dfi_clk_2x

   , output[(dram_dfi_width_gp>>4)-1:0] dm_oe_n
   , output[(dram_dfi_width_gp>>4)-1:0] dm_o
   , output[(dram_dfi_width_gp>>4)-1:0] dqs_p_oe_n
   , output[(dram_dfi_width_gp>>4)-1:0] dqs_p_o
   , input [(dram_dfi_width_gp>>4)-1:0] dqs_p_i
   , output[(dram_dfi_width_gp>>4)-1:0] dqs_n_oe_n
   , output[(dram_dfi_width_gp>>4)-1:0] dqs_n_o
   , input [(dram_dfi_width_gp>>4)-1:0] dqs_n_i

   , output[(dram_dfi_width_gp>>1)-1:0] dq_oe_n
   , output[(dram_dfi_width_gp>>1)-1:0] dq_o
   , input [(dram_dfi_width_gp>>1)-1:0] dq_i

   , output                             ddr_ck_p
   , output                             ddr_ck_n
   , output                             ddr_cke
   , output                      [2:0]  ddr_ba      //this is the maximum width.
   , output                     [15:0]  ddr_addr    //this is the maximum width.
   , output                             ddr_cs_n
   , output                             ddr_ras_n
   , output                             ddr_cas_n
   , output                             ddr_we_n
   , output                             ddr_reset_n
   , output                             ddr_odt
   );

   assign v_o     = 1'b0;
   assign data_o  = ring_width_p'(0);
   assign ready_o = 1'b1;

   assign  dm_oe_n      = 1'b1;
   assign  dm_o         = 2'b1;
   assign  dqs_p_oe_n   = 2'b11;
   assign  dqs_p_o      = 2'b11;
   assign  dqs_n_oe_n   = 2'b11;
   assign  dqs_n_o      = 2'b11;

   assign  dq_oe_n      = 16'hFFFF;
   assign  dq_o         = 16'hFFFF;

   assign  ddr_ck_p     = 1'b1;
   assign  ddr_ck_n     = 1'b1;
   assign  ddr_cke      = 1'b1;
   assign  ddr_ba       = 3'b1;
   assign  ddr_addr     = 16'b1;
   assign  ddr_cs_n     = 1'b1;
   assign  ddr_ras_n    = 1'b1;
   assign  ddr_cas_n    = 1'b1;
   assign  ddr_we_n     = 1'b1;
   assign  ddr_reset_n  = 1'b1;
   assign  ddr_odt      = 1'b1;

endmodule

