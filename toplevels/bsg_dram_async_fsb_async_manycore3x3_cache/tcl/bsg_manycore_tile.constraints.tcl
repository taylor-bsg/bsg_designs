create_clock -period 1.0 -name CLK [get_ports clk_i]

set_output_delay 0.5 [get_ports link_out*] -clock [get_clocks CLK] -max
set_output_delay 0.0 [get_ports link_out*] -clock [get_clocks CLK] -min

set_input_delay  0.5 [get_ports link_in*]  -clock [get_clocks CLK] -max
set_input_delay  0.0 [get_ports link_in*]  -clock [get_clocks CLK] -min
set_input_delay  0.5 [get_ports reset_i]   -clock [get_clocks CLK] -max
set_input_delay  0.0 [get_ports reset_i]   -clock [get_clocks CLK] -min

set_max_delay 1.2 -from [get_ports link_in*] -to [get_ports link_out*]
set_min_delay 0.0 -from [get_ports link_in*] -to [get_ports link_out*]

set_driving_cell -lib_cell INVD1BWP -pin ZN [all_inputs]
set_load 0.01 [all_outputs]

set_false_path -from [get_ports my_x_i*]
set_false_path -from [get_ports my_y_i*]

