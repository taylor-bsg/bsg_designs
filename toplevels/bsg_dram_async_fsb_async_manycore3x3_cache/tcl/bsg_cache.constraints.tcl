create_clock -period 1.0 -name CLK [get_ports clk_i]

set_output_delay 0.5 [all_outputs] -clock [get_clocks CLK] -max
set_output_delay 0.0 [all_outputs] -clock [get_clocks CLK] -min

set_input_delay  0.5 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -max
set_input_delay  0.0 [remove_from_collection [all_inputs] [get_ports clk_i]] -clock [get_clocks CLK] -min

set_max_delay 1.2 -from [remove_from_collection [all_inputs] [get_ports clk_i]] -to [all_outputs]
set_min_delay 0.0 -from [remove_from_collection [all_inputs] [get_ports clk_i]] -to [all_outputs]

set_driving_cell -lib_cell INVD1BWP -pin ZN [all_inputs]
set_load 0.01 [all_outputs]
