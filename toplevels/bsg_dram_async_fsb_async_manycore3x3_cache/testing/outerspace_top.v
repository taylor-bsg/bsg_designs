module  outerspace_top(
	clock, 
	reset, 
	fsb2fsb_ctrl_data, 
	fsb2fsb_ctrl_valid, 
	fsb2fsb_ctrl_ready, 
	fsb_ctrl2fsb_data, 
	fsb_ctrl2fsb_valid, 
	fsb_ctrl2fsb_yumi);

   input clock;
   input reset;
   input [79:0] fsb2fsb_ctrl_data;
   output fsb_ctrl2fsb_valid;
   output fsb_ctrl2fsb_yumi;

   input fsb2fsb_ctrl_valid;
   input fsb2fsb_ctrl_ready;
   output [79:0] fsb_ctrl2fsb_data;

        assign fsb_ctrl2fsb_yumi = fsb2fsb_ctrl_ready  //output channel is not full 
                                 & fsb2fsb_ctrl_valid; //input packet valid

        assign fsb_ctrl2fsb_valid= fsb2fsb_ctrl_valid; //out packet
        assign fsb_ctrl2fsb_data = fsb2fsb_ctrl_data;  //output_data
endmodule

