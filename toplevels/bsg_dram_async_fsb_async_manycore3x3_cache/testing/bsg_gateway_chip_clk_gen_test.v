/**
 *  bsg_gateway_chip_clk_gen_test.v
 */

module bsg_gateway_chip
  `include "bsg_pinout_inverted.v"

  localparam core_0_period_lp = `CORE_0_PERIOD;
  localparam core_1_period_lp = `CORE_1_PERIOD;
  localparam io_master_0_period_lp = `IO_MASTER_0_PERIOD;
  localparam io_master_1_period_lp = `IO_MASTER_1_PERIOD;
  localparam dfi_2x_0_period_lp = `DFI_2X_0_PERIOD;
  localparam dfi_2x_1_period_lp = `DFI_2X_1_PERIOD;
  localparam drlp_0_period_lp = `DRLP_0_PERIOD;
  localparam drlp_1_period_lp = `DRLP_1_PERIOD;
  localparam fsb_0_period_lp = `FSB_0_PERIOD;
  localparam fsb_1_period_lp = `FSB_1_PERIOD;
  localparam op_0_period_lp = `OUTERSPACE_0_PERIOD;
  localparam op_1_period_lp = `OUTERSPACE_1_PERIOD;

  localparam channel_width_lp = 8;
  localparam num_channels_lp  = 1;

  localparam nodes_lp = 2;

  initial begin
    $vcdpluson;
    $vcdplusmemon;
    $vcdplusautoflushon;
  end

  // reset generator for local module
  //
  logic gateway_core_clk, asic_core_clk;
  logic gateway_io_master_clk, asic_io_master_clk;
  logic dfi_2x_clk, dfi_1x_clk;
  logic drlp_clk;
  logic fsb_clk;
  logic op_clk;

  logic [4:0] reset_gen_clk_li;
  assign reset_gen_clk_li = {
    gateway_core_clk,
    gateway_io_master_clk,
    asic_core_clk,
    asic_io_master_clk,
    dfi_2x_clk
  };

  logic [1:0] async_reset_lo;
  bsg_nonsynth_reset_gen #(
    .num_clocks_p(5)
    ,.reset_cycles_lo_p(250)
    ,.reset_cycles_hi_p(10)
  ) reset_gen0 (
    .clk_i(reset_gen_clk_li)
    ,.async_reset_o(async_reset_lo[0])
  );

  bsg_nonsynth_reset_gen #(
    .num_clocks_p(5)
    ,.reset_cycles_lo_p(1000)
    ,.reset_cycles_hi_p(10)
  ) reset_gen1 (
    .clk_i(reset_gen_clk_li)
    ,.async_reset_o(async_reset_lo[1])
  );

  // external clk gen
  //
  bsg_nonsynth_clock_gen #(
    .cycle_time_p(core_0_period_lp)
  ) gateway_core_gen_clk (
    .o(gateway_core_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(core_1_period_lp)
  ) asic_core_gen_clk (
    .o(asic_core_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(io_master_0_period_lp)
  ) gateway_master_gen_clk (
    .o(gateway_io_master_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(io_master_1_period_lp)
  ) asic_master_gen_clk (
    .o(asic_io_master_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(drlp_1_period_lp)
  ) drlp_gen_clk (
    .o(drlp_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(fsb_1_period_lp)
  ) fsb_gen_clk (
    .o(fsb_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(op_1_period_lp)
  ) op_gen_clk (
    .o(op_clk)
  );

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(dfi_2x_1_period_lp)
  ) dfi_2x_gen_clk (
    .o(dfi_2x_clk)
  );

  bsg_counter_clock_downsample #(
    .width_p(2)
  ) dfi_1x_ds (
    .clk_i(dfi_2x_clk)
    ,.reset_i(async_reset_lo[0])
    ,.val_i(2'b0)
    ,.clk_r_o(dfi_1x_clk)
  );

  // assign ext clk pins
  //
  assign p_misc_L_4_o = asic_core_clk;
  assign p_PLL_CLK_o  = asic_io_master_clk;
  assign p_misc_L_5_o = drlp_clk;
  assign p_misc_R_5_o = dfi_2x_clk;
  assign p_misc_L_1_o = fsb_clk;
  assign p_sdo_token_o[3] = op_clk;
  assign p_misc_T_0_o = dfi_1x_clk;

  // assign ext clk sel pins
  //
  assign {p_misc_R_7_o, p_misc_R_6_o} = 2'b00; // core_clk_sel
  assign {p_misc_L_7_o, p_misc_L_6_o} = 2'b00; // io_master_clk_sel
  assign {p_misc_T_2_o, p_misc_R_4_o} = 2'b00; // drlp_clk_sel
  assign {p_misc_L_0_o, p_misc_R_0_o} = 2'b00; // fsb_clk_sel
  assign p_sdo_token_o[2:1] = 2'b00; // op_clk_sel
  
  assign p_misc_R_1_o = 1'b0; // dfi_clk_sel
  assign p_misc_L_2_o = 1'b0;

  // bsg_guts
  //
  logic [7:0] sdo_data_i_int_packed [num_channels_lp-1:0];
  logic [7:0] sdi_data_o_int_packed [num_channels_lp-1:0];
  assign sdo_data_i_int_packed[0] = p_sdo_A_data_i;
  assign p_sdi_A_data_o = sdi_data_o_int_packed[0];

  bsg_guts #(
    .num_channels_p(num_channels_lp)
    ,.master_p(1)
    ,.master_to_client_speedup_p(100)
    ,.master_bypass_test_p(5'b11111)
    ,.enabled_at_start_vec_p({(nodes_lp){2'b11}})
    ,.nodes_p(nodes_lp)
  ) guts (
    .core_clk_i(gateway_core_clk)
    ,.async_reset_i(async_reset_lo[0])
    ,.io_master_clk_i(gateway_io_master_clk)
    ,.io_clk_tline_i(p_sdo_sclk_i[num_channels_lp-1:0])
    ,.io_valid_tline_i(p_sdo_ncmd_i[num_channels_lp-1:0])
    ,.io_data_tline_i(sdo_data_i_int_packed)
    ,.io_token_clk_tline_o(p_sdo_token_o[num_channels_lp-1:0])
    ,.im_clk_tline_o(p_sdi_sclk_o[num_channels_lp-1:0])
    ,.im_valid_tline_o(p_sdi_ncmd_o[num_channels_lp-1:0])
    ,.im_data_tline_o(sdi_data_o_int_packed)
    ,.token_clk_tline_i(p_sdi_token_i[num_channels_lp-1:0])
    ,.im_slave_reset_tline_r_o(p_reset_o)
    ,.core_reset_o()
  );

  // bsg_tag_trace_replay
  //

  logic tr_clk;
  logic tr_rst;
  logic [15:0] rom_addr;
  logic [22:0] rom_data;
  logic tr_valid_lo;
  logic tr_data_lo;
  logic tr_done_lo;
  logic tr_error_lo;

  bsg_nonsynth_clock_gen #(
    .cycle_time_p(10000)
  ) tr_clk_gen (
    .o(tr_clk)
  );

  bsg_nonsynth_reset_gen #(
    .num_clocks_p(1)
    ,.reset_cycles_lo_p(10)
    ,.reset_cycles_hi_p(10)
  ) tr_reset_gen (
    .clk_i(tr_clk)
    ,.async_reset_o(tr_rst)
  );
  
  bsg_tag_trace_replay #(
    .rom_addr_width_p(16)
    ,.rom_data_width_p(23)
    ,.num_clients_p(19)
    ,.max_payload_width_p(9)
  ) tag_trace_replay (
    .clk_i(tr_clk)
    ,.reset_i(tr_rst)
    ,.en_i(1'b1)
    ,.rom_addr_o(rom_addr)
    ,.rom_data_i(rom_data)

    ,.valid_i(1'b0)
    ,.data_i(9'b0)
    ,.ready_o()

    ,.valid_o(tr_valid_lo)
    ,.tag_data_o(tr_data_lo)
    ,.yumi_i(tr_valid_lo)

    ,.done_o(tr_done_lo)
    ,.error_o(tr_error_lo)
  );

  bsg_trace_rom #(
    .width_p(23)
    ,.addr_width_p(16)
  ) tr_rom (
    .addr_i(rom_addr)
    ,.data_o(rom_data)
  );

  assign p_JTAG_TMS_o = tr_valid_lo;
  assign p_JTAG_TDI_o = tr_data_lo;
  assign p_JTAG_TCK_o = tr_clk;

  // unused signals
  //
  wire _unused1 = p_JTAG_TDO_i;
  wire _unused2 = p_misc_R_3_i;

  assign p_JTAG_TRST_o = |async_reset_lo;
  assign p_misc_T_1_o = |async_reset_lo;

  // DRAM model
  //
  mobile_ddr dram_model (
    .Clk(p_clk_1_p_i)
    ,.Clk_n(p_clk_1_n_i)
    ,.Cke(p_sdo_sclk_i[3])
    ,.We_n(p_sdo_sclk_i[2])
    ,.Cs_n(p_sdi_token_i[3])
    ,.Ras_n(p_sdo_ncmd_i[2])
    ,.Cas_n(p_sdo_ncmd_i[3])
    ,.Addr({p_sdo_D_data_i[5:0], p_sdo_C_data_i[7:0]})
    ,.Ba(p_sdo_D_data_i[7:6])
    ,.Dq({p_sdi_D_data_io, p_sdi_B_data_io})
    ,.Dqs(p_sdi_sclk_io)
    ,.Dm(p_sdi_ncmd_i)
  );

  // obs clk
  //
  wire obs_clk = p_misc_L_3_i;

endmodule
