source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_tag.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_chip_cdc.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_comm_link.constraints.tcl
source -echo -verbose $::env(BSG_DESIGNS_DIR)/toplevels/common/bsg_clk_gen.constraints.tcl

# 1 GHZ with 20 ps uncertainty
set clk_name           "clk"
set clk_period_ps      500.0
set clk_uncertainty_ps 20.0

set input_delay_per  20.0
set output_delay_per 20.0

set input_delay_ps  [expr ${clk_period_ps}*(${input_delay_per}/100.0)]
set output_delay_ps [expr ${clk_period_ps}*(${output_delay_per}/100.0)]

set driving_lib_cell "SC7P5T_INVX2_SSC14SL"
set load_lib_pin     "SC7P5T_INVX8_SSC14SL/A"

# Reg2Reg
create_clock -period ${clk_period_ps} -name ${clk_name} [get_ports "clk_i"]
set_clock_uncertainty ${clk_uncertainty_ps} [get_clocks ${clk_name}]

# In2Reg
set input_pins [filter_collection [all_inputs] "name!~*clk*"]
set_driving_cell -no_design_rule -lib_cell ${driving_lib_cell} [remove_from_collection [all_inputs] [get_ports *clk*]]
set_input_delay ${input_delay_ps} -clock ${clk_name} ${input_pins}

set output_pins [filter_collection [all_outputs] "name!~*clk*"]
set_load [load_of [get_lib_pin */${load_lib_pin}]] [all_outputs]
set_output_delay ${output_delay_ps} -clock ${clk_name} ${output_pins}

#set_optimize_registers

