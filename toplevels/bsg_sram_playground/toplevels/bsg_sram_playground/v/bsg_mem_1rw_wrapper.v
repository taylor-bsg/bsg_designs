
`include "bsg_mem_1rw_sync.v"

// Declare hard macros with this imported macro
//   bsg_mem_1rw_sync_macro(words,bits,mux)

module bsg_mem_1rw_wrapper
 #(parameter width_p=-1
   , parameter els_p=-1
   , parameter mux_p=-1
   , parameter addr_width_lp=`BSG_SAFE_CLOG2(els_p)
   )
  (input clk_i
   , input reset_i

   , input [addr_width_lp-1:0] addr_i
   , input [width_p-1:0] data_i
   , input v_i
   , input w_i

   , output [width_p-1:0] data_o
   );

  `bsg_mem_1rw_sync_macro(els_p,width_p,mux_p) else
  begin : not_found
    $fatal("Error! Hardened memory not found for els_p: %d, width_p: %d, mux_p: %d", els_p, width_p, mux_p);
  end

endmodule

